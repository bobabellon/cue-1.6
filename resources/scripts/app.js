var app = angular.module('HybrisCUEApp', [
										  'ui.router',
										  'utilityDirective',
										  'utilityFactory',
										  'slickCarousel',
										  'ngCookies',
										  'ngPopover',
										  '720kb.datepicker',
										  'ng.httpLoader',
										  'navigationUtility',
										  'angular-popover',
										  'ngSanitize',
										  'ngCsv',
										  'toaster',
										  'chart.js',
										  'ui.scroll',
										  
										  /* Controllers */
										  'loginController',
										  'headerController',
										  'homeController',
										  'searchController',
										  'categoryController',
										  'productDetailController',
										  'shoppingCartController',
										  'orderController',
										  'invoiceController',
										  'checkoutController',
										  'placeOrderSuccessfulController',
										  'orderDetailController',
										  'invoicePaymentController',
										  'projectController',
										  'projectDetailController',
										  'accountController',
										  'invoiceDetailController',
										  'reportController',
										  'reportDetailController',
										  'newReportController',
										  
										  /* Page Components */
										  'headerDirective',
										  'homeDirective',
										  'searchDirective',
										  'checkoutDirective',
										  'shoppingCartDirective',
										  'commonDirective',
										  'invoiceDirective',
										  'orderDirective',
										  'productDetailDirective',
										  'categoryDirective',
										  'orderDetailDirective',
										  'invoicePaymentDirective',
										  'projectDirective',
										  'projectDetailDirective',
										  'accountDirective',
										  'invoiceDetailDirective',
										  'reportDirective',
										  'reportDetailDirective',
										  'newReportDirective',
										  'pascalprecht.translate',
										  ]);

var translationsEN = {
		  NAV_CUSTOMER_SERVICE: 'Customer Service',
		  NAV_PROJECTS: 'My Projects',
		  NAV_REPORTS: 'My Reports',
		  NAV_ACCOUNT: 'My Account',
		  NAV_LOGOUT: 'Logout',
		  LIVE_CHAT: 'Live Chat',
		  HOME_LABEL: 'Home',
		  CHECKOUT: 'Checkout',
		  LABEL_ACCT_INFO: 'Account Info',
		  LABEL_FIRST_NAME: 'First Name',
		  LABEL_LAST_NAME: 'Last Name',
		  LABEL_PHONE: 'Phone',
		  LABEL_EXTENSION: 'Extension',
		  LABEL_EMAIL: 'Email Address',
		  LABEL_PASSWORD: 'Password',
		  LABEL_SAVE: 'Save',
		  ACCOUNT_MENU: {
			  BILL_SHIP_INFO : 'Bill & Shipping Info',
			  PAYMENT_INFO : 'Payment Info',
			  EMAIL_SETTINGS : 'Email Settings',
			  NOTIF_SETTINGS : 'Notification Settings',
			  RETURNS : 'Returns',
			  SUPPORT_TICKETS : 'Support Tickets'
		  },
		  TEXT : {
			  CATEG_COVER: 'TOUGH FOOTWEAR FOR TOUGH JOBS',
			  CATEG_HIGH: 'High',
			  CATEG_MID: 'Mid',
			  CATEG_LOW: 'Low',
			  YES: 'Yes',
			  NO: 'No',
			  SUBTOTAL: 'Subtotal',
			  TOTAL: 'Total',
			  INVALID_QUANTITY: 'Invalid Quantity',
			  CLOSE : 'Close',
			  NOTIFICATIONS: 'Notifications',
			  IN_STOCK: 'In stock',
			  OUT_OF_STOCK: 'Out of stock',
			  INVALID_NAME: 'Invalid Name',
			  CHANGE_NAME: 'Change Name',
			  DELETE_CONFIRM: 'Are you sure you want to delete?',
			  DUP_PROJ_ITEM: 'This product is already in this project.  You can update quantity from the Project details page',
			  GRID: 'Grid',
			  LIST: 'List',
			  DATE: 'Date',
			  TO: 'To',
			  VIEW_BY: 'View By',
			  SOLD_TO: 'Sold To',
			  SHIP_TO: 'Ship To',
			  CREATED: 'Created',
			  ITEM: 'Items',
			  DUPLICATE: 'Duplicate',
			  PROJECT: 'Project',
			  QUICK_ADD: 'Catalog Quick Add',
			  CONFIRM_CHANGE: 'Do you want to save your changes?',
			  GO_TO_HOME: 'Go to Home',
			  RECOMMEND_PRODUCT: 'Recommended Products',
			  DOWNLOAD: 'Download',
			  AMT_DUE: 'Amount Due',
			  REQ_FIELD: 'Required Field',
			  PAYMENT_AMT: 'Payment Amount',
			  DATE_TO_SEND: 'Date to Send',
			  LABEL: 'Label',
			  EDIT: 'Edit',
			  SUBMIT_PAYMENT: 'Submit Payment',
			  PAYMENT_SOURCE: 'Payment Source',
			  TOUGH: 'Tough',
			  TOOLS: 'Tools',
			  FOR: 'For',
			  JOBS: 'Jobs',
			  INVALID_CRED: 'Invalid Credentials',
			  SIGNIN: 'Sign in',
			  FORGOT: 'Forgot Password?',
			  REGISTER: 'Register An Account',
			  DESC: 'Description',
			  SPECS: 'Specs',
			  RATINGS: 'Ratings',
			  COLOR: 'Color',
			  SIZE: 'Size',
			  ON: 'on',
			  SHOW_MORE: 'Show more',
			  SHOW_LESS: 'Show less',
			  YOUR_PRICE: 'Your Price',
			  OR: 'Or',
			  SELECT_STORE: 'Select Store',
			  AJAX_POWERTOOLS: 'Ajax Power Tools',
			  AVAILABLE_PICKUP: 'Available to Pick-Up',
			  TODAY: 'Today',
			  SEE_MORE_LOC: 'See More Locations',
			  PRODUCT_DESC: 'Product Description',
			  DETAIL_SPEC: 'Detailed Specs',
			  CUSTOMER_REVIEW: 'Customer Reviews & Ratings',
			  WARRANTY: 'Warranty',
			  TOTAL_PRICE: 'Total Price',
			  NEW: 'New',
			  START: 'Start',
			  END: 'End',
			  DOWNLOAD_PDF: 'Download PDF',
			  DOWNLOAD_CSV: 'Download Spreadsheet',
			  PRINT: 'Print',
			  CUSTOMER: 'Customer',
			  ERROR: 'Oops! An error occured',
			  OK : 'OK',
			  UNIT: 'Report Output',
			  SELECT_UNIT: 'Select Report Output'
		  },
		  DISP_OPTIONS: {
			  DISP_OPT: 'Display Options',
			  SMALL_GRID : 'Small Grid',
			  LARGE_GRID : 'Large Grid',
			  LIST : 'List'
		  },
		  SORT_BY: 'Sort By',
		  PER_PAGE: 'Per Page',
		  FILTER_BY: 'Filter By',
		  FILTER_SUB: {
			  COLOR: 'Color',
			  PRICE: 'Price',
			  FIT: 'Fit',
			  GENDER: 'Gender',
			  SIZE: 'Size'
		  },
		  BUTTON: {
			  APPLY : 'Apply',
			  NEXT: 'Next',
			  ADD_MORE_ITEM: 'Add more items',
			  ADD_TO_CART: 'Add to cart',
			  CANCEL: 'Cancel',
			  SAVE: 'Save',
			  IMPORT_CSV: 'Import CSV',
			  DELETE: 'Delete',
			  SAVE_CHANGES: 'Save Changes',
			  CONT_SHOPPING: 'Continue Shopping',
			  PAY_NOW: 'Pay Now',
			  VIEW_ALL: 'View All',
			  BIG_BUTTON: 'Big Button',
			  ADD: 'Add',
			  CREATE_PROJ: 'Create Project',
			  ADD_TO_PROJ: 'Add to Project',
			  MOVE_TO_CART: 'Move to Cart',
			  SAVE_FOR_LATER: 'Save For Later'
		  },
		  CHECKOUT_TEXT: {
			  HEADING: 'Order Confirmation',
			  CONFIRMATION: 'Are you sure you want to place order?',
			  SECURE_CHECKOUT: 'Secure Checkout',
			  PAYMENT_TYPE: 'Payment Type',
			  CREDIT_CARD: 'Credit Card',
			  BILL_ACCT: 'Bill Account',
			  PO_NO: 'PO Number',
			  COST_CENTER: 'Cost Center',
			  SHIP_ADDRESS: 'Shipping Address',
			  ADDRESS_BOOK: 'Select From Address Book',
			  SHIPPING_METHOD: 'Shipping Method',
			  REVIEW: 'Review',
			  SHIPPING: 'Shipping',
			  PLACE_ORDER: 'Place Order',
			  AGREEMENT_TEXT: 'By placing this order I am confirming I have read and agree to the Terms & Conditions',
			  ORDER_SUCCESS: 'Thank you for placing your order on Power Tools!',
			  ORDER_SUCCESS_MSG: 'Your order #{{orderCode}} was received and is being processed.',
			  ORDER_SUCCESS_EMAIL: 'A confirmation email has been sent to'
		  },
		  ORDER_SUMMARY: 'Order Summary',
		  TABLE_HEADER: {
			  ITEM_NO: 'Item #',
			  QUANTITY: 'Quantity',
			  TOTAL_COST: 'Total Cost',
			  PRODUCT: 'Product',
			  PRICE: 'Price',
			  ITEM_NAME: 'Item Name',
			  OPTION: 'Option',
			  UNIT_PRICE: 'Unit Price',
			  AVAILABILITY: 'Availability',
			  INVOICE_NO: 'Invoice Number',
			  PO_NO: 'PO #',
			  DATE_ORDERED: 'Date Ordered',
			  STATUS: 'Status',
			  ORDER_NO: 'Order #',
			  DATE_ISSUED: 'Date Issued',
			  TOTAL: 'Total',
			  TOTAL_ITEM: 'Total Items',
			  CONTACT_NAME: 'Contact Name',
			  CONTACT_NO: 'Contact Number',
			  CONTACT_EMAIL: 'Contact Email',
			  CREDIT_APPLIED: 'Credits Applied',
			  ORDER_DATE: 'Order Date',
			  TITLE: 'Title',
			  PROJECT: 'Project',
			  PRODUCT_NO: 'Product #',
			  TYPE: 'Type',
			  PRODUCT_GRP: 'Product Group',
			  UOM: 'UOM',
			  SHIP_TO_NAME: 'Ship To Name',
			  CITY: 'City',
			  STATE: 'State',
			  TOTAL_SHIP: 'Total Shipped',
			  JAN: 'Jan',
			  FEB: 'Feb',
			  MAR: 'Mar',
			  APR: 'Apr',
			  MAY: 'May',
			  JUN: 'Jun',
			  JUL: 'Jul',
			  AUG: 'Aug',
			  SEP: 'Sep',
			  OCT: 'Oct',
			  NOV: 'Nov',
			  DEC: 'Dec',
			  MONTH: 'Month',
			  CURR_YEAR_MSF: 'Current Year(MSF)',
			  PREV_YEAR_MSF: 'Previous Year(MSF)',
			  VAR_QTY: 'Var% (Qty)',
			  QUARTER: 'Quarter',
			  INVOICE_AMT: 'Invoice Amount',
			  TOTAL_AMT: 'Total Amount',
			  TOTAL_FOR: 'Total for',
			  CURR_YEAR_$: 'Current Year($)',
			  PREV_YEAR_$: 'Previous Year($)',
			  VAR_$: 'Var%($)',
		  },
		  CART_TEXT:{
			  HEADING: 'Shopping Cart',
			  KEEP_SHOPPING: 'Keep Shopping',
			  GO_TO_CART: 'Go To Cart',
			  ITEM: 'item',
			  ITEMS: 'items',
			  ORDER_SUMMARY: 'Order Summary',
			  PROMO_CODE: 'Promo Code',
			  MY_CART: 'My Cart',
			  SAVED_FOR_LATER: 'Saved For Later'
		  },
		  NAV_MENU: {
			  PRODUCT_CATALOG: 'Product Catalog',
			  QUICK_ORDER: 'Quick Order',
			  ORDERS: 'Orders',
			  INVOICES: 'Invoices',
			  PROJECTS: 'Projects',
			  REPORTS: 'Reports'
		  },
		  PRODUCT_CATALOG_SUB: {
			  HAND_TOOLS: 'Hand Tools',
			  MEASURE_TOOLS: 'Measuring & Layout Tools',
			  FOOTWEAR: 'Footwear',
			  POWER_DRILLS: 'Power Drills',
			  ANGLE_GRINDERS: 'Angle Grinders',
			  SCREWDRIVERS: 'Screwdrivers',
			  SANDERS: 'Sanders',
			  HAND_SAWS: 'Hand Saws',
			  JIGSAWS: 'Jigsaws',
			  NUT_DRIVERS: 'Nut Drivers',
			  ROTARY_HAMMERS: 'Rotary Hammers',
			  STRIPPING_TOOLS: 'Stripping Tools',
			  PUNCHES: 'Punches',
			  SETS: 'Sets',
			  INSTRUMENTS: 'Instruments',
			  MENS: "Men's",
			  WOMENS: "Women's"
		  },
		  FOOTER:{
			  ABOUT_POWER_TOOLS:'About Power Tools Unlimited',
			  CONTACT: 'Contact Us',
			  EVENTS: 'Events',
			  PRESS: 'Press Release',
			  CAREER: 'Career Opportunities',
			  RETURN: 'Return Policy',
			  SHIP_OPTION: 'Shipping Options',
			  HELP: 'Help & Support',
			  FAQ: 'FAQs',
			  SITE_MAP: 'Site Map',
			  WEB_TERM: 'Website Terms of Use',
			  PRIVACY: 'Privacy Policy',
			  STAY_CONNECTED: 'Stay Connected',
			  SIGN_UP: 'Sign up to receive promotions and new product offerings.',
			  ADDRESS: '555 PTU Way &#8226; Philadelphia, PA &#8226; 18201',
			  CONTACT_NO: '555-555-1212'
		  },
		  PROJECT: {
			  CREATE_PROJ: 'Create New Project',
			  GO_TO_PROJ: 'Go to Project',
			  CREATE: 'Create Project'
		  },
		  FREQ_BOUGHT:{
			  HEADING:'Frequently Bought Together'
		  },
		  HOME: {
			  COMPANY_ASSURANCE_TITLE:'Award-Winning Customer Service',
			  HOME_TEXT:'This is an example of body text describing something here on the site. It can contain a link or have a button after a paragraph.',
			  LEARN_MORE: 'Learn More',
			  SATISFACTION: 'Satisfaction Guaranteed',
			  CORPORATE_SERV: 'Corporate Services Tailored to You',
			  HEADING:'TOUGH TOOLS FOR TOUGH JOBS',
			  SEE_DETAIL:'See Details',
			  RECOMMENDED_FOR:'Recommended For You'
		  },
		  INVOICE_TEXT:{
			  PAID: 'Paid',
			  OPEN: 'Open',
			  PAST_DUE: 'Past Due',
			  SCHEDULED: 'Scheduled',
			  NO_INVOICE: 'No invoices yet!',
			  SEARCH_CATALOG: 'Search our catalog to get started.',
			  OPEN_INVOICE: 'Open Invoices',
			  PAID_INVOICE: 'Paid Invoices',
			  WATCHLIST: 'Watch List',
			  INVOICE_NO: 'Invoice #',
			  ADD_TO_WATCHLIST: 'Add To Watchlist',
			  BILLING_ADD: 'Billing Address',
			  SHIPPING_ADD: 'Shipping Address',
			  SOLD_TO_ADD: 'Sold-To Address',
			  PAYER_ADD: 'Payer Address',
			  HEADING: 'Invoice',
			  ORDER_NO: 'Order #',
			  PAY_INVOICE: 'Pay Invoices',
			  INVOICES_TOTAL: 'Invoices Total'
		  },
		  ORDER_TEXT:{
			  SHIPPED: 'Shipped',
			  PENDING: 'Pending',
			  ON_HOLD: 'On Hold',
			  NO_ORDER: 'No orders yet!',
		  },
		  PROJ_TEXT:{
			  SELECT_PROJ: 'Select Project'
		  },
		  REPORT_TEXT:{
			  
		  },
		  SEARCH_TEXT:{
			  SEARCH_RESULT:'Search Results',
			  SEARCHED_FOR: 'You searched for'
		  },
		  REPORT_TEXT:{
			  REPORT_NAME: 'Report Name',
			  PAYER: 'Payer',
			  SELECT_CUSTOMER: 'Select Customer',
			  TIME_PERIOD: 'Time Period',
			  SELECT_TIME: 'Select Time Period',
			  YEAR: 'Year',
			  DATE_RANGE: 'Date Range',
			  DATA_PARAM: 'Data Parameters',
			  DIV: 'Division(s)',
			  SUBCATEG: 'Sub-category',
			  PROD_CATEG: 'Product Category',
			  STATES: 'States',
			  REPORT_LABEL: 'Chart Type',
			  REPORT_FORMAT: 'Report Format',
			  SEL_CHART_TYPE: 'Select Chart Type',
			  REPORT_PREV: 'Report Preview',
			  CHART_TYPE: 'Chart Type',
			  RUN_REPORT: 'Run Report',
			  NEW_REP: 'New Report',
			  STATE_S: 'State(s)',
			  SHIP_TO_ACCT: 'Ship to Account(s)',
			  SAVE_REP: 'Save Report',
			  EDIT_REP: 'Edit Report',
			  DELETE_REP: 'Delete Report',
			  SEL_DIV: 'Select Division',
			  SEL_ACC: 'Select Account',
			  SEL_SUB: 'Select Subcategory'
		  }
		};

app.run(function($state, $rootScope, $document, $anchorScroll,$cookieStore,$window) {
	//console.log($window.localStorage.get('user'));
	$state.go('login');	
});

app.config(['httpMethodInterceptorProvider','config',
    function (httpMethodInterceptorProvider, config) {
		httpMethodInterceptorProvider.whitelistDomain(config.baseUrl);
    }
]);

app.config(['$translateProvider', function ($translateProvider) {
  	// add translation tables
	$translateProvider.translations('en', translationsEN);
	$translateProvider.fallbackLanguage('en');
	$translateProvider.preferredLanguage('en');
}]);

app.config(function($stateProvider, $urlRouterProvider, $locationProvider) {
	$stateProvider
	.state('login', {
		url:"/login",
		cache: false,
		controller: 'loginController',
		templateUrl: 'resources/views/login/login-page.html'
	})
	.state('base', {
	      abstract: true,
	      cache: false,
	      views: {
	    	  '':{
	    		  controller: '',
		    	  templateUrl: 'resources/views/common/base-layout.html'
	    	  },
	    	  'header@base':{
	    		  controller: 'headerController',
			  controllerAs: 'headerCtrl',
	    		  templateUrl: 'resources/views/common/header/header-page.html'
	    	  },
	    	  'footer@base':{
	    		  controller: '',
	    		  templateUrl: 'resources/views/common/footer.html'
	    	  }
	      }
	 })
	.state('base.home', {
		url:'/home',
		cache: false,
		onEnter: scrollToTop,
		views: {
			'content':{
				controller: 'homeController',
				controllerAs: 'hc',
				templateUrl: 'resources/views/home/home-page.html'
			}	
		}
	})
	.state('base.search', {
		url:'/search-results',
		cache: false,
		onEnter: scrollToTop,
		views: {
			'content':{
				controller: 'searchController',
				templateUrl: 'resources/views/search/search-results-page.html'
			}	
		}
	})
	.state('base.category', {
		url:'/category',
		cache: false,
		onEnter: scrollToTop,
		views: {
			'content':{
				controller: 'categoryController',
				templateUrl: 'resources/views/category/category-page.html'
			}	
		}
	})
	.state('base.productDetail', {
		url:'/product-detail',
		cache: false,
		onEnter: scrollToTop,
		views: {
			'content':{
				controller: 'productDetailController',
				templateUrl: 'resources/views/product-detail/product-detail-page.html'
			}	
		}
	})
	.state('base.shoppingCart', {
		url:'/shopping-cart',
		cache: false,
		onEnter: scrollToTop,
		views: {
			'content':{
				controller: 'shoppingCartController',
				controllerAs: 'shoppingCartCtrl',
				templateUrl: 'resources/views/shopping-cart/shopping-cart-page.html'
			}	
		}
	})
	.state('base.orders', {
		url:'/orders',
		cache: false,
		onEnter: scrollToTop,
		views: {
			'content':{
				controller: 'orderController',
				controllerAs: 'oc',
				templateUrl: 'resources/views/order/order-page.html'
			}	
		}
	})
	.state('base.orderDetails', {
		url:'/order-details',
		cache: false,
		onEnter: scrollToTop,
		views: {
			'content':{
				controller: 'orderDetailController',
				controllerAs: 'orderDetailCtrl',
				templateUrl: 'resources/views/order-detail/order-detail-page.html'
			}	
		}
	})
	.state('base.invoice', {
		url:'/invoice',
		cache: false,
		onEnter: scrollToTop,
		views: {
			'content':{
				controller: 'invoiceController',
				controllerAs: 'ic',
				templateUrl: 'resources/views/invoice/invoice-page.html'
			}	
		}
	})
	.state('base.checkout', {
		url:'/checkout',
		cache: false,
		onEnter: scrollToTop,
		views: {
			'content':{
				controller: 'checkoutController',
				controllerAs: 'checkoutCtrl',
				templateUrl: 'resources/views/checkout/checkout-page.html'
			}	
		}
	})
	.state('base.placeOrderSuccessful', {
		url:'/order-successful',
		cache: false,
		onEnter: scrollToTop,
		views: {
			'content':{
				controller: 'placeOrderSuccessfulController',
				templateUrl: 'resources/views/common/place-order-successful.html'
			}	
		}
	})
	.state('base.invoicePayment', {
		url:'/pay-invoice',
		cache: false,
		onEnter: scrollToTop,
		views: {
			'content':{
				controller: 'invoicePaymentController',
				controllerAs: 'invoicePaymentCtrl',
				templateUrl: 'resources/views/invoice-payment/invoice-payment-page.html'
			}	
		}
	})
	.state('base.project', {
		url:'/projects',
		cache: false,
		onEnter: scrollToTop,
		views: {
			'content':{
				controller: 'projectController',
				controllerAs: 'projCtrl',
				templateUrl: 'resources/views/project/project-page.html'
			}	
		}
	})
	.state('base.projectDetail', {
		url:'/project-detail/:projectId',
		cache: false,
		onEnter: scrollToTop,
		views: {
			'content':{
				controller: 'projectDetailController',
				controllerAs: 'pDCtrl',
				templateUrl: 'resources/views/project-detail/project-detail-page.html'
			}	
		}
	})
	.state('base.account', {
		url:'/my-account',
		cache: false,
		onEnter: scrollToTop,
		views: {
			'content':{
				controller: 'accountController',
				controllerAs: 'acctCtrl',
				templateUrl: 'resources/views/account/account-page.html'
			}	
		}
	})
	.state('base.invoiceDetail', {
		url:'/invoice/:invoiceId',
		cache: false,
		onEnter: scrollToTop,
		views: {
			'content':{
				controller: 'invoiceDetailController',
				controllerAs: 'invoiceCtrl',
				templateUrl: 'resources/views/invoice-detail/invoice-detail-page.html'
			}	
		}
	})
	.state('base.reports', {
		url:'/report',
		cache: false,
		onEnter: scrollToTop,
		views: {
			'content':{
				controller: 'reportController',
				controllerAs: 'reportCtrl',
				templateUrl: 'resources/views/report/report-page.html'
			}	
		}
	})
	.state('base.reportDetail', {
		url:'/report/reportId/:reportId',
		cache: false,
		onEnter: scrollToTop,
		views: {
			'content':{
				controller: 'reportDetailController',
				controllerAs: 'reportDetailCtrl',
				templateUrl: 'resources/views/report-detail/report-detail-page.html'
			}	
		}
	}).state('base.createReport', {
		url:'/report/new',
		cache: false,
		onEnter: scrollToTop,
		views: {
			'content':{
				controller: 'newReportController',
				controllerAs: 'newReportCtrl',
				templateUrl: 'resources/views/report/create-report/new-report-page.html'
			}	
		}
	});
});

var scrollToTop = function($anchorScroll){
	$anchorScroll('top-header');
};

app.constant('config', {
//	baseUrl: 'https://104.239.203.28:9002',	
//  baseUrl: 'https://192.237.249.140:9002',	
	baseUrl: 'https://104.239.203.28:9002',	// Cloud Settings
    cataglogId: 'powertoolsProductCatalog',
    catalogVersionId: 'Online',
    imagePath: 'resources/images/',
    catalogPath : '/rest/v2/powertools/catalogs/',
    userPath : '/rest/v2/powertools/users/',
    productPath : '/rest/v2/powertools/products/',
    productSearchPath : '/rest/v2/powertools/products/search',
    projectPath : '/rest/v2/powertools/users/test.powertools@mailinator.com/customerprojects/',
    currency : '$',
    company: 'excellis',
	path: 'powertools',
	angular6Url:"localhost:3000/oncue-web#",
	angular1dot6Url: "localhost:3000/oncue-1dot6/#!"
});