var app = angular.module('navigationUtility', []);

app.directive('goToHome', function ($state) {
    return {
        restrict: 'A',
        scope: true,
        link: function (scope, element, attrs) {
            function redirect() {
                $state.go('base.home');
            };
            element.on('click', redirect);
        }
    };
});

app.directive('goToOrders', function ($state) {
    return {
        restrict: 'A',
        scope: true,
        link: function (scope, element, attrs) {
            function redirect() {
                $state.go('base.orders');
            };
            element.on('click', redirect);
        }
    };
});

app.directive('goToSpecificProduct', function ($state, $cookieStore) {
    return {
        restrict: 'A',
        scope: {
        	productcode: '='
        },
        link: function (scope, element, attrs) {
            function redirect() {
            	$cookieStore.put('productCode', scope.productcode);
        		if ($state.current.url == "/product-detail"){
        			$state.reload();
        		} else {
        			$state.go('base.productDetail');
        		}
            };
            element.on('click', redirect);
        }
    };
});

app.directive('redirectToProduct', function ($state, $cookieStore) {
    return {
        restrict: 'A',
        link: function (scope, element, attrs) {
            function redirect() {
        	    scope.redirectToProduct = attrs["redirectToProduct"];
            	$cookieStore.put('productCode', scope.redirectToProduct);
        		if ($state.current.url == "/product-detail"){
        			$state.reload();
        		} else {
        			$state.go('base.productDetail');
        		}
            };
            element.on('click', redirect);
        }
    };
});

app.directive('goToCategory', function ($state, $cookieStore, ngPopoverFactory) {
    return {
        restrict: 'A',
        scope: {
        	categoryid: '='
        },
        link: function (scope, element, attrs) {
            function redirect() {
            	$cookieStore.put('categoryId', scope.categoryid);
            	
            	if($('.cd-nav-trigger').hasClass('menu-is-open')){
            		var navigationContainer = $('#cd-nav-mobile'),
            		mainNavigation = navigationContainer.find('#cd-main-nav-mobile ul');
            		$('.cd-nav-trigger').toggleClass('menu-is-open');
            		//we need to remove the transitionEnd event handler (we add it when scolling up with the menu open)
            		mainNavigation.off('webkitTransitionEnd otransitionend oTransitionEnd msTransitionEnd transitionend').toggleClass('is-visible');
            	};
            	
            	ngPopoverFactory.closePopover('product-catalog-trigger-desktop');
        		ngPopoverFactory.closePopover('product-catalog-trigger-tablet');
        		document.getElementsByTagName("body")[0].style.overflowY = 'auto';
            	
        		if ($state.current.url == "/category"){
        			$state.reload();
        		} else {
        			$state.go('base.category');
        		}
            };
            element.on('click', redirect);
        }
    };
});

app.directive('goToInvoice', function ($state) {
    return {
        restrict: 'A',
        scope: true,
        link: function (scope, element, attrs) {
            function redirect() {
                $state.go('base.invoice');
            };
            element.on('click', redirect);
        }
    };
});

app.directive('goToReport', function ($state) {
    return {
        restrict: 'A',
        scope: true,
        link: function (scope, element, attrs) {
            function redirect() {
                $state.go('base.reports');
            };
            element.on('click', redirect);
        }
    };
});