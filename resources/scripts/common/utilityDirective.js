var app = angular.module('utilityDirective', []);

app.directive('ngEnter', function() {
	return function(scope, element, attrs) {
		element.bind("keydown keypress", function(event) {
			if (event.which === 13) {
				scope.$apply(function() {
					scope.$eval(attrs.ngEnter);
				});

				event.preventDefault();
				event.target.blur();
			}
		});
	};
});

app.directive('numericOnly', function() {
	return {
		require : 'ngModel',
		restrict : 'A',
		link : function(scope, element, attr, ctrl) {
			function inputValue(val) {
				if (val) {
					var digits = val.toString().replace(/[^0-9]/g, '');

					if (digits !== val) {
						ctrl.$setViewValue(digits);
						ctrl.$render();
					}
					return parseInt(digits, 10);
				}
				return undefined;
			}
			ctrl.$parsers.push(inputValue);
		}
	};
});

app.directive('footableInit', function() {
	return function(scope, element) {
		var footableTable = element.parents('table');

		if (!scope.$last) {
			return false;
		}

		scope.$evalAsync(function() {

			if (!footableTable.hasClass('footable-loaded')) {
				footableTable.footable();
			}

			footableTable.trigger('footable_initialized');
			footableTable.trigger('footable_resize');
			footableTable.data('footable').redraw();

		});
	};
})

app.directive('selectOnClick', ['$window', function ($window) {
    return {
        restrict: 'A',
        link: function (scope, element, attrs) {
            element.on('click', function () {
                if (!$window.getSelection().toString()) {
                    // Required for mobile Safari
                    this.setSelectionRange(0, this.value.length)
                }
            });
        }
    };
}]);

app.directive('a', function() {
    return {
        restrict: 'E',
        link: function(scope, elem, attrs) {
            if(attrs.ngClick || attrs.href === '' || attrs.href === '#'){
                elem.on('click', function(e){
                    e.preventDefault();
                });
            }
        }
   };
});

app.directive('handlePhoneSubmit', function () {
    return function (scope, element, attr) {
        var textFields = $(element).children('input[type=text]');
        $(element).submit(function() {
            textFields.blur();
        });
    };
});

app.directive('fileReader', function() {
	  return {
	    scope: {
	      fileReader:"=",
	      someCtrlFn: '&callbackFn'
	    }
	  ,
	    link: function(scope, element) {
	      $(element).on('change', function(changeEvent) {
	        var files = changeEvent.target.files;
	        var fileTypes = ['csv'];
	        if (files.length) {
	          var extension = files[0].name.split('.').pop().toLowerCase(),  //file extension from input file
	          isSuccess = fileTypes.indexOf(extension) > -1; 
	          if(isSuccess){
	        	  var r = new FileReader();
		          r.onload = function(e) {
		              var contents = e.target.result;
		              scope.$apply(function () {
		            	result = CSV2JSON(contents);
		                scope.fileReader = JSON.stringify(result);
		                scope.testing = result;
		                scope.someCtrlFn({result: JSON.parse(result)});
		              });
		          };	          
		          r.readAsText(files[0]);
	          }	         
	        }
	      });
	    }
	 };
});


function CSVToArray(strData, strDelimiter) {
    // Check to see if the delimiter is defined. If not,
    // then default to comma.
    strDelimiter = (strDelimiter || ",");
    // Create a regular expression to parse the CSV values.
    var objPattern = new RegExp((
    // Delimiters.
    "(\\" + strDelimiter + "|\\r?\\n|\\r|^)" +
    // Quoted fields.
    "(?:\"([^\"]*(?:\"\"[^\"]*)*)\"|" +
    // Standard fields.
    "([^\"\\" + strDelimiter + "\\r\\n]*))"), "gi");
    // Create an array to hold our data. Give the array
    // a default empty first row.
    var arrData = [[]];
    // Create an array to hold our individual pattern
    // matching groups.
    var arrMatches = null;
    // Keep looping over the regular expression matches
    // until we can no longer find a match.
    while (arrMatches = objPattern.exec(strData)) {
        // Get the delimiter that was found.
        var strMatchedDelimiter = arrMatches[1];
        // Check to see if the given delimiter has a length
        // (is not the start of string) and if it matches
        // field delimiter. If id does not, then we know
        // that this delimiter is a row delimiter.
        if (strMatchedDelimiter.length && (strMatchedDelimiter != strDelimiter)) {
            // Since we have reached a new row of data,
            // add an empty row to our data array.
            arrData.push([]);
        }
        // Now that we have our delimiter out of the way,
        // let's check to see which kind of value we
        // captured (quoted or unquoted).
        if (arrMatches[2]) {
            // We found a quoted value. When we capture
            // this value, unescape any double quotes.
            var strMatchedValue = arrMatches[2].replace(
            new RegExp("\"\"", "g"), "\"");
        } else {
            // We found a non-quoted value.
            var strMatchedValue = arrMatches[3];
        }
        // Now that we have our value string, let's add
        // it to the data array.
        arrData[arrData.length - 1].push(strMatchedValue);
    }
    // Return the parsed data.
    return (arrData);
}

function CSV2JSON(csv) {
    var array = CSVToArray(csv);
    var objArray = [];
    for (var i = 1; i < array.length; i++) {
        objArray[i - 1] = {};
        for (var k = 0; k < array[0].length && k < array[i].length; k++) {
            var key = array[0][k];
            objArray[i - 1][key] = array[i][k]
        }
    }

    var json = JSON.stringify(objArray);
    var str = json.replace(/},/g, "},\r\n");

    return str;
}
