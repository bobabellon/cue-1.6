var app = angular.module('projectDetailDirective', []);

app.directive('projectDetailBreadcrumbs', function() {
	return {
		restrict : 'E',
		scope : false,
		templateUrl : 'resources/views/project-detail/project-detail-breadcrumbs.html'
	}
});

app.directive('projectDetailTable', function() {
	return {
		restrict : 'E',
		scope : false,
		templateUrl : 'resources/views/project-detail/project-detail-table.html'
	}
});

app.directive('projectSaveChangesModal', function() {
	return {
		restrict : 'E',
		scope : false,
		templateUrl : 'resources/views/project-detail/project-save-changes-modal.html'
	}
});