var app = angular.module('homeDirective', []);

app.directive('homePageHeading', function() {
	return {
		restrict : 'E',
		scope : false,
		templateUrl : 'resources/views/home/home-heading.html'
	}
});

app.directive('homePageOrderCard', function() {
	return {
		restrict : 'E',
		scope : false,
		templateUrl : 'resources/views/home/home-order-card.html'
	}
});

app.directive('homePageInvoiceCard', function() {
	return {
		restrict : 'E',
		scope : false,
		templateUrl : 'resources/views/home/home-invoice-card.html'
	}
});

app.directive('homePageRecallCard', function() {
	return {
		restrict : 'E',
		scope : false,
		templateUrl : 'resources/views/home/home-recall-card.html'
	}
});

app.directive('homePageRecommendedCard', function() {
	return {
		restrict : 'E',
		scope : false,
		templateUrl : 'resources/views/home/home-recommended-card.html'
	}
});

app.directive('homePageBanner', function() {
	return {
		restrict : 'E',
		scope : false,
		templateUrl : 'resources/views/home/home-banner.html'
	}
});