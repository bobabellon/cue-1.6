var app = angular.module('productDetailDirective', []);

app.directive('productDetailBreadcrumbs', function() {
	return {
		restrict : 'E',
		scope : false,
		templateUrl : 'resources/views/product-detail/product-detail-breadcrumbs.html'
	}
});

app.directive('productDetailHeader', function() {
	return {
		restrict : 'E',
		scope : false,
		templateUrl : 'resources/views/product-detail/product-detail-header.html'
	}
});

app.directive('productDetailCarousel', function() {
	return {
		restrict : 'E',
		scope : false,
		templateUrl : 'resources/views/product-detail/product-detail-carousel.html'
	}
});

app.directive('productDetailDisplayImage', function() {
	return {
		restrict : 'E',
		scope : false,
		templateUrl : 'resources/views/product-detail/product-detail-display-image.html'
	}
});

app.directive('productDetailSelection', function() {
	return {
		restrict : 'E',
		scope : false,
		templateUrl : 'resources/views/product-detail/product-detail-selection.html'
	}
});

app.directive('productDetailTabLabels', function() {
	return {
		restrict : 'E',
		scope : false,
		templateUrl : 'resources/views/product-detail/product-detail-tab-labels.html'
	}
});

app.directive('productDetailTabContents', function() {
	return {
		restrict : 'E',
		scope : false,
		templateUrl : 'resources/views/product-detail/product-detail-tab-contents.html'
	}
});

app.directive('productDetailRatingPagination', function() {
	return {
		restrict : 'E',
		scope : false,
		templateUrl : 'resources/views/product-detail/product-detail-rating-pagination.html'
	}
});

app.directive('addToProjectModal', function() {
	return {
		restrict : 'E',
		scope : false,
		templateUrl : 'resources/views/product-detail/add-to-project-modal.html'
	}
});