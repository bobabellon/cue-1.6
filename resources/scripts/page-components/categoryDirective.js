var app = angular.module('categoryDirective', []);

app.directive('categoryBreadcrumbs', function() {
	return {
		restrict : 'E',
		scope : false,
		templateUrl : 'resources/views/category/category-breadcrumbs.html'
	}
});

app.directive('categoryDisplayOptions', function() {
	return {
		restrict : 'E',
		scope : false,
		templateUrl : 'resources/views/category/category-display-options.html'
	}
});

app.directive('categoryDisplayOptionsTab', function() {
	return {
		restrict : 'E',
		scope : false,
		templateUrl : 'resources/views/category/category-display-options-tab.html'
	}
});

app.directive('categoryCover', function() {
	return {
		restrict : 'E',
		scope : false,
		templateUrl : 'resources/views/category/category-cover.html'
	}
});

app.directive('categorySubCover', function() {
	return {
		restrict : 'E',
		scope : false,
		templateUrl : 'resources/views/category/category-sub-cover.html'
	}
});

app.directive('categoryFilter', function() {
	return {
		restrict : 'E',
		scope : false,
		templateUrl : 'resources/views/category/category-filter.html'
	}
});

app.directive('categoryFilterModal', function() {
	return {
		restrict : 'E',
		scope : false,
		templateUrl : 'resources/views/category/category-filter-modal.html'
	}
});

app.directive('categoryProductCard', function() {
	return {
		restrict : 'E',
		scope : false,
		templateUrl : 'resources/views/category/category-product-card.html'
	}
});

app.directive('categoryPagination', function() {
	return {
		restrict : 'E',
		scope : false,
		templateUrl : 'resources/views/category/category-pagination.html'
	}
});

app.directive('categorySelectedFilter', function() {
	return {
		restrict : 'E',
		scope : false,
		templateUrl : 'resources/views/category/category-selected-filter.html'
	}
});

app.directive('categoryDropdownFilter', function() {
	return {
		restrict : 'E',
		scope : false,
		templateUrl : 'resources/views/category/category-dropdown-filter.html'
	}
});