var app = angular.module('headerDirective', []);

//app.directive('headerTop', function() {
//	return {
//		restrict : 'E',
//		scope : false,
//		templateUrl : 'resources/views/common/header/header-top.html'
//	}
//});
//
//app.directive('headerBottom', function() {
//	return {
//		restrict : 'E',
//		scope : false,
//		templateUrl : 'resources/views/common/header/header-bottom.html'
//	}
//});

app.directive('productCatalog', function() {
	return {
		restrict : 'E',
		scope : false,
		templateUrl : 'resources/views/common/header/product-catalog.html'
	}
});

app.directive('quickOrder', function() {
	return {
		restrict : 'E',
		scope : false,
		templateUrl : 'resources/views/common/header/quick-order.html'
	}
});

app.directive('cart', function() {
	return {
		restrict : 'E',
		scope : false,
		templateUrl : 'resources/views/common/header/cart.html'
	}
});

app.directive('notification', function() {
	return {
		restrict : 'E',
		scope : false,
		templateUrl : 'resources/views/common/header/notification.html'
	}
});

app.directive('smartFixedNavigation', function() {
	return {
		restrict : 'E',
		scope : false,
		templateUrl : 'resources/views/common/header/smart-fixed-navigation.html'
	}
});