var app = angular.module('orderDirective', []);

app.directive('orderBreadcrumbs', function() {
	return {
		restrict : 'E',
		scope : false,
		templateUrl : 'resources/views/order/order-breadcrumbs.html'
	}
});

app.directive('orderTable', function() {
	return {
		restrict : 'E',
		scope : false,
		templateUrl : 'resources/views/order/order-table.html'
	}
});

app.directive('orderPagination', function() {
	return {
		restrict : 'E',
		scope : false,
		templateUrl : 'resources/views/order/order-pagination.html'
	}
});

app.directive('orderFilter', function() {
	return {
		restrict : 'E',
		scope : false,
		templateUrl : 'resources/views/order/order-filter.html'
	}
});

app.directive('orderFilterModal', function() {
	return {
		restrict : 'E',
		scope : false,
		templateUrl : 'resources/views/order/order-filter-modal.html'
	}
});

app.directive('orderHeader', function() {
	return {
		restrict : 'E',
		scope : false,
		templateUrl : 'resources/views/order/order-header.html'
	}
});

app.directive('orderSelectedFilter', function() {
	return {
		restrict : 'E',
		scope : false,
		templateUrl : 'resources/views/order/order-selected-filter.html'
	}
});