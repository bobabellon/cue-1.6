var app = angular.module('accountDirective', []);

app.directive('accountBreadcrumbs', function() {
	return {
		restrict : 'E',
		scope : false,
		templateUrl : 'resources/views/account/account-breadcrumbs.html'
	}
});

app.directive('accountMenuList', function() {
	return {
		restrict : 'E',
		scope : false,
		templateUrl : 'resources/views/account/account-menu-list.html'
	}
});

app.directive('accountInfo', function() {
	return {
		restrict : 'E',
		scope : false,
		templateUrl : 'resources/views/account/account-info.html'
	}
});

app.directive('accountSidenavMobile', function() {
	return {
		restrict : 'E',
		scope : false,
		templateUrl : 'resources/views/account/account-sidenav-mobile.html'
	}
});