var app = angular.module('reportDirective', []);

app.directive('reportBreadcrumbs', function() {
	return {
		restrict : 'E',
		scope : false,
		templateUrl : 'resources/views/report/report-breadcrumbs.html'
	}
});

app.directive('reportHeader', function() {
	return {
		restrict : 'E',
		scope : false,
		templateUrl : 'resources/views/report/report-header.html'
	}
});

app.directive('reportPagination', function() {
	return {
		restrict : 'E',
		scope : false,
		templateUrl : 'resources/views/report/report-pagination.html'
	}
});

app.directive('reportFilter', function() {
	return {
		restrict : 'E',
		scope : false,
		templateUrl : 'resources/views/report/report-filter.html'
	}
});

app.directive('reportTable', function() {
	return {
		restrict : 'E',
		scope : false,
		templateUrl : 'resources/views/report/report-table.html'
	}
});

app.directive('reportSelectedFilter', function() {
	return {
		restrict : 'E',
		scope : false,
		templateUrl : 'resources/views/report/report-selected-filter.html'
	}
});

app.directive('reportModal', function() {
	return {
		restrict : 'E',
		scope : false,
		templateUrl : 'resources/views/report/report-modal.html'
	}
});

app.directive('reportFilterModal', function() {
	return {
		restrict : 'E',
		scope : false,
		templateUrl : 'resources/views/report/report-filter-modal.html'
	}
});

app.directive('reportRenameModal', function() {
	return {
		restrict : 'E',
		scope : false,
		templateUrl : 'resources/views/report/report-rename-modal.html'
	}
});
