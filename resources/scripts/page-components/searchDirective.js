var app = angular.module('searchDirective', []);

app.directive('searchFilter', function() {
	return {
		restrict : 'E',
		scope : false,
		templateUrl : 'resources/views/search/search-filter.html'
	}
});

app.directive('searchPagination', function() {
	return {
		restrict : 'E',
		scope : false,
		templateUrl : 'resources/views/search/search-pagination.html'
	}
});

app.directive('searchProductCard', function() {
	return {
		restrict : 'E',
		scope : false,
		templateUrl : 'resources/views/search/search-product-card.html'
	}
});

app.directive('searchFilterModal', function() {
	return {
		restrict : 'E',
		scope : false,
		templateUrl : 'resources/views/search/search-filter-modal.html'
	}
});

app.directive('searchSelectedFilter', function() {
	return {
		restrict : 'E',
		scope : false,
		templateUrl : 'resources/views/search/search-selected-filter.html'
	}
});

app.directive('searchDropdownFilter', function() {
	return {
		restrict : 'E',
		scope : false,
		templateUrl : 'resources/views/search/search-dropdown-filter.html'
	}
});

app.directive('searchBreadcrumbs', function() {
	return {
		restrict : 'E',
		scope : false,
		templateUrl : 'resources/views/search/search-breadcrumbs.html'
	}
});

app.directive('searchDisplayOptions', function() {
	return {
		restrict : 'E',
		scope : false,
		templateUrl : 'resources/views/search/search-display-options.html'
	}
});

app.directive('searchDisplayOptionsTab', function() {
	return {
		restrict : 'E',
		scope : false,
		templateUrl : 'resources/views/search/search-display-options-tab.html'
	}
});