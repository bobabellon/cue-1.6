var app = angular.module('checkoutDirective', []);

app.directive('checkoutBreadcrumbs', function() {
	return {
		restrict : 'E',
		scope : false,
		templateUrl : 'resources/views/checkout/checkout-breadcrumbs.html'
	}
});

app.directive('checkoutTabs', function() {
	return {
		restrict : 'E',
		scope : false,
		templateUrl : 'resources/views/checkout/checkout-tabs.html'
	}
});

app.directive('checkoutOrderSummary', function() {
	return {
		restrict : 'E',
		scope : false,
		templateUrl : 'resources/views/checkout/checkout-order-summary.html'
	}
});

app.directive('checkoutOrderSummaryModal', function() {
	return {
		restrict : 'E',
		scope : false,
		templateUrl : 'resources/views/checkout/checkout-order-summary-modal.html'
	}
});

app.directive('checkoutOrderConfirmationModal', function() {
	return {
		restrict : 'E',
		scope : false,
		templateUrl : 'resources/views/checkout/checkout-order-confirmation-modal.html'
	}
});