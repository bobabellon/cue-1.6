var app = angular.module('newReportDirective', []);

app.directive('newReportBreadcrumbs', function() {
	return {
		restrict : 'E',
		scope : false,
		templateUrl : 'resources/views/report/create-report/new-report-breadcrumbs.html'
	}
});

app.directive('newReportHeader', function() {
	return {
		restrict : 'E',
		scope : false,
		templateUrl : 'resources/views/report/create-report/new-report-page.html'
	}
});

app.directive('reportYearToDate', function() {
	return {
		restrict : 'E',
		scope : false,
		templateUrl : 'resources/views/report/create-report/report-year-to-date-page.html'
	}
});

app.directive('newReportTable', function() {
	return {
		restrict : 'E',
		scope : false,
		templateUrl : 'resources/views/report/create-report/new-report-table.html'
	}
});
