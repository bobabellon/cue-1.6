var app = angular.module('invoiceDetailDirective', []);

app.directive('invoiceDetailBreadcrumbs', function() {
	return {
		restrict : 'E',
		scope : false,
		templateUrl : 'resources/views/invoice-detail/invoice-detail-breadcrumbs.html'
	}
});

app.directive('invoiceDetailCarousel', function() {
	return {
		restrict : 'E',
		scope : false,
		templateUrl : 'resources/views/invoice-detail/invoice-detail-carousel.html'
	}
});

app.directive('invoiceDetailDisplayImage', function() {
	return {
		restrict : 'E',
		scope : false,
		templateUrl : 'resources/views/invoice-detail/invoice-detail-display-image.html'
	}
});

app.directive('invoiceDetailPayment', function() {
	return {
		restrict : 'E',
		scope : false,
		templateUrl : 'resources/views/invoice-detail/invoice-detail-payment.html'
	}
});

app.directive('invoiceDetailTable', function() {
	return {
		restrict : 'E',
		scope : false,
		templateUrl : 'resources/views/invoice-detail/invoice-detail-table.html'
	}
});

app.directive('invoiceDetailAddress', function() {
	return {
		restrict : 'E',
		scope : false,
		templateUrl : 'resources/views/invoice-detail/invoice-detail-address.html'
	}
});