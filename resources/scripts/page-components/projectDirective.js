var app = angular.module('projectDirective', []);

app.directive('projectBreadcrumbs', function() {
	return {
		restrict : 'E',
		scope : false,
		templateUrl : 'resources/views/project/project-breadcrumbs.html'
	}
});

app.directive('projectFilter', function() {
	return {
		restrict : 'E',
		scope : false,
		templateUrl : 'resources/views/project/project-filter.html'
	}
});

app.directive('projectHeader', function() {
	return {
		restrict : 'E',
		scope : false,
		templateUrl : 'resources/views/project/project-header.html'
	}
});

app.directive('projectList', function() {
	return {
		restrict : 'E',
		scope : false,
		templateUrl : 'resources/views/project/project-list.html'
	}
});

app.directive('projectDisplayOptions', function() {
	return {
		restrict : 'E',
		scope : false,
		templateUrl : 'resources/views/project/project-display-options.html'
	}
});

app.directive('projectDisplayOptionsTab', function() {
	return {
		restrict : 'E',
		scope : false,
		templateUrl : 'resources/views/project/project-display-options-tab.html'
	}
});

app.directive('projectFilterModal', function() {
	return {
		restrict : 'E',
		scope : false,
		templateUrl : 'resources/views/project/project-filter-modal.html'
	}
});

app.directive('projectPopover', function() {
	return {
		restrict : 'E',
		scope : false,
		templateUrl : 'resources/views/project/project-popover.html'
	}
});

app.directive('projectSelectedFilter', function() {
	return {
		restrict : 'E',
		scope : false,
		templateUrl : 'resources/views/project/project-selected-filter.html'
	}
});

app.directive('projectCreateModal', function() {
	return {
		restrict : 'E',
		scope : false,
		templateUrl : 'resources/views/project/project-create-modal.html'
	}
});