var app = angular.module('reportDetailDirective', []);

app.directive('reportDetailBreadcrumbs', function() {
	return {
		restrict : 'E',
		scope : false,
		templateUrl : 'resources/views/report-detail/report-detail-breadcrumbs.html'
	}
});

app.directive('reportDetailHeader', function() {
	return {
		restrict : 'E',
		scope : false,
		templateUrl : 'resources/views/report-detail/report-detail-header.html'
	}
});

app.directive('reportDetailTable', function() {
	return {
		restrict : 'E',
		scope : false,
		templateUrl : 'resources/views/report-detail/report-detail-table.html'
	}
});


app.directive('createReportTable', function() {
	return {
		restrict : 'E',
		scope : false,
		templateUrl : 'resources/views/report-detail/create-report-table.html'
	}
});

app.directive('summaryReportTable', function() {
	return {
		restrict : 'E',
		scope : false,
		templateUrl : 'resources/views/report-detail/summary-report/summary-report-table.html'
	}
});

app.directive('summaryReportHeader', function() {
	return {
		restrict : 'E',
		scope : false,
		templateUrl : 'resources/views/report-detail/summary-report/summary-report-header.html'
	}
});

app.directive('productSummaryReportTable', function() {
	return {
		restrict : 'E',
		scope : false,
		templateUrl : 'resources/views/report-detail/product-summary-report/product-summary-report-table.html'
	}
});

app.directive('shipToSummaryReportTable', function() {
	return {
		restrict : 'E',
		scope : false,
		templateUrl : 'resources/views/report-detail/ship-to-summary-report/ship-to-summary-report-table.html'
	}
});
