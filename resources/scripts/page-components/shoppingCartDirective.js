var app = angular.module('shoppingCartDirective', []);

app.directive('shoppingCartBreadcrumbs', function() {
	return {
		restrict : 'E',
		scope : false,
		templateUrl : 'resources/views/shopping-cart/shopping-cart-breadcrumbs.html'
	}
});

app.directive('shoppingCartTable', function() {
	return {
		restrict : 'E',
		scope : false,
		templateUrl : 'resources/views/shopping-cart/shopping-cart-table.html'
	}
});

app.directive('shoppingCartCheckoutButton', function() {
	return {
		restrict : 'E',
		scope : false,
		templateUrl : 'resources/views/shopping-cart/shopping-cart-checkout-button.html'
	}
});

app.directive('shoppingCartSavedLaterTable', function() {
	return {
		restrict : 'E',
		scope : false,
		templateUrl : 'resources/views/shopping-cart/shopping-cart-saved-later-table.html'
	}
});