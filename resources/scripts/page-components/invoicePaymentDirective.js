var app = angular.module('invoicePaymentDirective', []);

app.directive('invoicePaymentBreadcrumbs', function() {
	return {
		restrict : 'E',
		scope : false,
		templateUrl : 'resources/views/invoice-payment/invoice-payment-breadcrumbs.html'
	}
});

app.directive('invoicePaymentTabs', function() {
	return {
		restrict : 'E',
		scope : false,
		templateUrl : 'resources/views/invoice-payment/invoice-payment-tabs.html'
	}
});

app.directive('invoicePaymentSummary', function() {
	return {
		restrict : 'E',
		scope : false,
		templateUrl : 'resources/views/invoice-payment/invoice-payment-summary.html'
	}
});

