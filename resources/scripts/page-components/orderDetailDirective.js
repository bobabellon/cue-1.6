var app = angular.module('orderDetailDirective', []);

app.directive('orderDetailBreadcrumbs', function() {
	return {
		restrict : 'E',
		scope : false,
		templateUrl : 'resources/views/order-detail/order-detail-breadcrumbs.html'
	}
});

app.directive('orderDetailAddress', function() {
	return {
		restrict : 'E',
		scope : false,
		templateUrl : 'resources/views/order-detail/order-detail-address.html'
	}
});

app.directive('orderDetailTable', function() {
	return {
		restrict : 'E',
		scope : false,
		templateUrl : 'resources/views/order-detail/order-detail-table.html'
	}
});

app.directive('orderDetailSummary', function() {
	return {
		restrict : 'E',
		scope : false,
		templateUrl : 'resources/views/order-detail/order-detail-summary.html'
	}
});

//app.directive('orderDetailPopOver', function() {
//	return {
//		restrict : 'E',
//		scope : false,
//		templateUrl : 'resources/views/order-detail/order-detail-pop-over.html'
//	}
//});

app.directive('orderDetailModal', function() {
	return {
		restrict : 'E',
		scope : false,
		templateUrl : 'resources/views/order-detail/order-detail-modal.html'
	}
});

app.directive('orderDetailMobile', function() {
	return {
		restrict : 'E',
		scope : false,
		templateUrl : 'resources/views/order-detail/order-detail-mobile.html'
	}
});