var app = angular.module('commonDirective', []);

app.directive('commonOrderSummaryComputation', function() {
	return {
		restrict : 'E',
		scope : false,
		templateUrl : 'resources/views/common/order-summary-computation.html'
	}
});

app.directive('recommendedProducts', function() {
	return {
		restrict : 'E',
		scope : false,
		templateUrl : 'resources/views/common/recommended-products.html'
	}
});

app.directive('savedProducts', function() {
	return {
		restrict : 'E',
		scope : false,
		templateUrl : 'resources/views/common/frequently-bought-products.html'
	}
});

app.directive('orderSummaryComputation', function() {
	return {
		restrict : 'E',
		scope : false,
		templateUrl : 'resources/views/shopping-cart/order-summary-computation.html'
	}
});

app.directive('errorModal', function() {
	return {
		restrict : 'E',
		scope : false,
		templateUrl : 'resources/views/common/error-modal.html'
	}
});