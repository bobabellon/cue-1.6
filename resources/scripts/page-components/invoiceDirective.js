var app = angular.module('invoiceDirective', []);

app.directive('invoiceBreadcrumbs', function() {
	return {
		restrict : 'E',
		scope : false,
		templateUrl : 'resources/views/invoice/invoice-breadcrumbs.html'
	}
});

app.directive('invoiceTable', function() {
	return {
		restrict : 'E',
		scope : false,
		templateUrl : 'resources/views/invoice/invoice-table.html'
	}
});

app.directive('invoiceFilter', function() {
	return {
		restrict : 'E',
		scope : false,
		templateUrl : 'resources/views/invoice/invoice-filter.html'
	}
});

app.directive('invoiceFilterModal', function() {
	return {
		restrict : 'E',
		scope : false,
		templateUrl : 'resources/views/invoice/invoice-filter-modal.html'
	}
});

app.directive('invoiceHeader', function() {
	return {
		restrict : 'E',
		scope : false,
		templateUrl : 'resources/views/invoice/invoice-header.html'
	}
});

app.directive('invoiceSelectedFilter', function() {
	return {
		restrict : 'E',
		scope : false,
		templateUrl : 'resources/views/invoice/invoice-selected-filter.html'
	}
});

app.directive('invoicePagination', function() {
	return {
		restrict : 'E',
		scope : false,
		templateUrl : 'resources/views/invoice/invoice-pagination.html'
	}
});

app.directive('invoicePdfViewModal', function() {
	return {
		restrict : 'E',
		scope : false,
		templateUrl : 'resources/views/invoice/invoice-pdf-view-modal.html'
	}
});