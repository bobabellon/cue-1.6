var app =  angular.module('reportService', []);

app.factory('reportService', ['$http','$cookieStore','config', '$q',
	function($http, $cookieStore, config, $q) {
	  	return {
		    getReports: function() {
		    	return [
		           		{'reportId' : 1,'reportName':'Sarah Smith - Invoice Statements for June', 'type':'Invoice Statements', 'dateIssued': '01/03/2017', 'pdfFile': 'PDF'},
		        		{'reportId' : 2,'reportName':'2017 to 2018 Order Totals', 'type':'Previous vs Current Year', 'dateIssued': '03/02/2017', 'pdfFile': 'PDF'},
		        		{'reportId' : 3,'reportName':'Orders for Sept - for AP', 'type':'Open Orders', 'dateIssued': '12/01/2017', 'pdfFile': 'PDF'},
		        		{'reportId' : 4,'reportName':'2017 Orders for Accounting', 'type':'Year-to-Date Orders', 'dateIssued': '07/05/2017', 'pdfFile': 'PDF'},
		        		{'reportId' : 5,'reportName':'Shipments for Late Oct - Early Nov', 'type':'Order Status', 'dateIssued': '06/16/2017', 'pdfFile': 'PDF'},
		        		{'reportId' : 6,'reportName':'Statements - 1st Half of November', 'type':'Invoice Statements', 'dateIssued': '04/20/2017', 'pdfFile': 'PDF'},
		        		{'reportId' : 7,'reportName':'Statements - 2nd Half of November', 'type':'Invoice Statements', 'dateIssued': '08/25/2017', 'pdfFile': 'PDF'},
		        		{'reportId' : 8,'reportName':'Summary Report', 'type':'Summary Report', 'dateIssued': '01/25/2017', 'pdfFile': 'PDF'},
		        		{'reportId' : 9,'reportName':'Product Summary Report', 'type':'Product Summary Report', 'dateIssued': '08/13/2017', 'pdfFile': 'PDF'},
		        		{'reportId' : 10,'reportName':'Ship To Summary Report', 'type':'Ship-To Summary Report', 'dateIssued': '08/22/2017', 'pdfFile': 'PDF'}
		        	]
		    },
		    getSelectedReport: function() {
		    	selectedReport = $cookieStore.get("selectedReport");
				return selectedReport;
		    },
		    setSelectedReport: function(report) {
		    	selectedReport = report;
				$cookieStore.put("selectedReport", selectedReport);
				return true;
		    },
		    getReportDetail: function() {
				return [
					        {
					        	invoiceNumber : '93774923',
					        	dateIssued : '12/01/2017',
					        	poNumber : 'TYX-73883-8193',
					        	contactName : 'Sarah Smith',
					        	contactEmail : 'ssmith@xyzarchitects.com',
					        	status : 'Paid',
					        	creditsApplied : '0.00',
					        	total : '624.52'				        	
					        },
					        {
					        	invoiceNumber : '93774924',
					        	dateIssued : '12/01/2017',
					        	poNumber : 'TYX-73883-8194',
					        	contactName : 'Sarah Smith',
					        	contactEmail : 'ssmith@xyzarchitects.com',
					        	status : 'Paid',
					        	creditsApplied : '0.00',
					        	total : '624.52'
					        },
					        {
					        	invoiceNumber : '93774924',
					        	dateIssued : '12/01/2017',
					        	poNumber : 'TYX-73883-8194',
					        	contactName : 'Sarah Smith',
					        	contactEmail : 'ssmith@xyzarchitects.com',
					        	status : 'Paid',
					        	creditsApplied : '0.00',
					        	total : '624.52'
					        },
					        {
					        	invoiceNumber : '93774924',
					        	dateIssued : '12/01/2017',
					        	poNumber : 'TYX-73883-8194',
					        	contactName : 'Sarah Smith',
					        	contactEmail : 'ssmith@xyzarchitects.com',
					        	status : 'Paid',
					        	creditsApplied : '0.00',
					        	total : '624.52'
					        },
					        {
					        	invoiceNumber : '93774924',
					        	dateIssued : '12/01/2017',
					        	poNumber : 'TYX-73883-8194',
					        	contactName : 'Sarah Smith',
					        	contactEmail : 'ssmith@xyzarchitects.com',
					        	status : 'Paid',
					        	creditsApplied : '0.00',
					        	total : '624.52'
					        },
					        {
					        	invoiceNumber : '93774924',
					        	dateIssued : '12/01/2017',
					        	poNumber : 'TYX-73883-8194',
					        	contactName : 'Sarah Smith',
					        	contactEmail : 'ssmith@xyzarchitects.com',
					        	status : 'Paid',
					        	creditsApplied : '0.00',
					        	total : '624.52'
					        }
				        	
				        ]
		    },
		    getReportOptions: function() {		    	
				return [
				        	{
				        		reportType : "Summary Report"
				        	},
				        	{
				        		reportType : "Product Summary Report"
				        	},
				        	{
				        		reportType : "Ship-To Summary Report"
				        	}
				        ];
		    },
		    getProductSummaryOption: function(option) {
				return $http({
					url: config.baseUrl + "/rest/v2/"+config.path+"/myreports/ProductSummaryReport/" + option,
					method: 'GET',
					crossDomain: true,
					headers: {						
						'Authorization': 'Bearer ' + $window.localStorage.getItem('access_token')
					}
				}).then(function(response){
					return response.data;
				}, function(errResponse){
					console.error('error fetching getProductSummaryOption');
					return $q.reject(errResponse);
				});
			},
		    getReportsData: function(reportName) {
				return $http({
					url: config.baseUrl + "/rest/v2/"+config.path+"/myreports/result/" +reportName ,
					method: 'GET',
					crossDomain: true,
					headers: {						
						'Authorization': 'Bearer ' + $window.localStorage.getItem('access_token')
					}
				}).then(function(response){
					return response.data;
				}, function(errResponse){
					console.error('error fetching getShipToSummaryData');
					return $q.reject(errResponse);
				});
			},
		    logUserData: function(userInfo) {
				return $http({
					url: config.baseUrl + "/rest/v2/"+config.path+"/myreports/util/logUser/",
					method: 'POST',
					crossDomain: true,
					headers: {						
						'Authorization': 'Bearer ' + $window.localStorage.getItem('access_token')
					},
					data : userInfo
				}).then(function(response){
					return response.data;
				}, function(errResponse){
					console.error('error fetching logUserData');
					return $q.reject(errResponse);
				});
			},
		    getDivisions: function() {
				return $http({
					url: config.baseUrl + "/rest/v2/"+config.path+"/myreports/divisions/",
					method: 'GET',
					crossDomain: true,
					headers: {						
						'Authorization': 'Bearer ' + $window.localStorage.getItem('access_token')
					}
				}).then(function(response){
					return response.data;
				}, function(errResponse){
					console.error('error fetching getDivisions');
					return $q.reject(errResponse);
				});
			},
		    getPayers: function(division) {
				return $http({
					url: config.baseUrl + "/rest/v2/"+config.path+"/myreports/divisions/"+division+"/payers/",
					method: 'GET',
					crossDomain: true,
					headers: {						
						'Authorization': 'Bearer ' + $window.localStorage.getItem('access_token')
					}
				}).then(function(response){
					return response.data;
				}, function(errResponse){
					console.error('error fetching getPayers');
					return $q.reject(errResponse);
				});
			},
		    getSoldTo: function(division, payer) {
				return $http({
					url: config.baseUrl + "/rest/v2/"+config.path+"/myreports/divisions/"+division+"/"+payer+"/soldtos/",
					method: 'GET',
					crossDomain: true,
					headers: {						
						'Authorization': 'Bearer ' + $window.localStorage.getItem('access_token')
					}
				}).then(function(response){
					return response.data;
				}, function(errResponse){
					console.error('error fetching getSoldTo');
					return $q.reject(errResponse);
				});
			},
		    getShipTo: function(division, payer, soldTo) {
				return $http({
					url: config.baseUrl + "/rest/v2/"+config.path+"/myreports/divisions/"+division+"/"+payer+"/"+soldTo+"/shiptos/",
					method: 'GET',
					crossDomain: true,
					headers: {						
						'Authorization': 'Bearer ' + $window.localStorage.getItem('access_token')
					}
				}).then(function(response){
					return response.data;
				}, function(errResponse){
					console.error('error fetching getShipTo');
					return $q.reject(errResponse);
				});
			},
		    getSubCateg: function() {
				return $http({
					url: config.baseUrl + "/rest/v2/"+config.path+"/myreports/divisions/'1006'/subcategories",
					method: 'GET',
					crossDomain: true,
					headers: {						
						'Authorization': 'Bearer ' + $window.localStorage.getItem('access_token')
					}
				}).then(function(response){
					return response.data;
				}, function(errResponse){
					console.error('error fetching getSubCateg');
					return $q.reject(errResponse);
				});
			}
	  	};
}]);