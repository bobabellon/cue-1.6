var app =  angular.module('loginService', []);

app.factory('loginService', ['$http',
	function($http) {
	  	return {
		    login: function(userName, password) {
		    	return $http({
					url: 'https://excelliscueclouddev.cfapps.us10.hana.ondemand.com/api/login',
					data:{
						username:userName,
						password,password
					},
	            	method: 'POST'
				});
		    }
	  	};
}]);