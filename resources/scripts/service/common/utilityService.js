var app = angular.module('utilityFactory', ['userService']);

app.factory("utilityFactory", ['userService','config','productService', '$cookieStore', '$q', '$timeout', 'projectService','$window',
	function(userService, config, productService,$cookieStore, $q, $timeout, projectService,$window){
	var q = $q.defer();
	var utilities = {
			displayCurrentRating: displayCurrentRating,
			displayUnratedRating: displayUnratedRating,
			splitArray: splitArray,
			incrementPageIndex: incrementPageIndex,
			decrementPageIndex: decrementPageIndex,
			updatePageIndex: updatePageIndex,
			generatePagination: generatePagination,
			smartFixedNavigation: smartFixedNavigation,
			megaSiteNavigation: megaSiteNavigation,
			getAllShoppingCartItem: getAllShoppingCartItem,
			getProjectItemDetails : getProjectItemDetails,
			retrieveOrderDetails : retrieveOrderDetails,
			getSelectedInvoices : getSelectedInvoices,
			setSelectedInvoices : setSelectedInvoices,
			createDocDefinition : createDocDefinition,
			getInvoiceList : getInvoiceList,
			createDocDefinitionTable : createDocDefinitionTable,
			reorderRows : reorderRows,
			getProjectItemDetailsPerPage : getProjectItemDetailsPerPage,
			addItemsToCartProj :addItemsToCartProj,
			saveToken:saveToken,
			getToken:getToken,
			saveUser:saveUser,
			getUser:getUser,
			saveJwt:saveJwt
	}

	var shoppingCartItems = {};
	var selectedInvoice = []; 
	var cart = {};
	cart.items = [];
	cart.subTotal = 0;
	function displayCurrentRating(rating){
		var totalRatings = [];
		for(var i = 0; i < rating; i++){
			totalRatings.push(i);
		}
		return totalRatings;
	}
	
	function displayUnratedRating(rating){
		rating = 5 - rating;
		var totalRatings = [];
		for(var i = 0; i < rating; i++){
			totalRatings.push(i);
		}
		return totalRatings;
	}
	
	function splitArray(array, objectPerIndex){
		var destinationArray = [];
		while (array.length > 0){
			if(array.length >= objectPerIndex){
				slicedArray = array.slice(0, objectPerIndex);
				array.splice(0, objectPerIndex);
				destinationArray.push(slicedArray);
			} else {
				slicedArray = array.slice(0, array.length);
				array.splice(0, array.length);
				destinationArray.push(slicedArray);
			}
		}
		return destinationArray;
	}
	
	function incrementPageIndex(value, pageArray, productList){
		var returnObject = [];
		value++;
		returnObject.push(value);
		var pageArray = generatePagination(pageArray, productList, value);
		returnObject.push(pageArray);
		return returnObject;
	}
	
	function decrementPageIndex(value, pageArray, productList){
		var returnObject = [];
		value--;
		returnObject.push(value);
		var pageArray = generatePagination(pageArray, productList, value);
		returnObject.push(pageArray);
		return returnObject;
	}
	
	function updatePageIndex(value, pageArray, productList, newValue){
		var returnObject = [];
		value = newValue;
		returnObject.push(value);
		var pageArray = generatePagination(pageArray, productList, value);
		returnObject.push(pageArray);
		return returnObject;
	}
	
	function generatePagination(pageArray, productList, currentIndex){
		pageArray = [];
		if(currentIndex < 2){
			if(productList.length > 3){
				for (var i = 1; i <= 3; i++){
					pageArray.push(i);
				}
			} else {
				for (var i = 1; i <= productList.length; i++){
					pageArray.push(i);
				}
			}
		} else {
			if(productList.length >= 3){
				if(currentIndex+1 == productList.length){
					for (var i = currentIndex-1; i <= productList.length; i++){
						pageArray.push(i);
					}
				} else {
					for (var i = currentIndex; i <= currentIndex+2; i++){
						pageArray.push(i);
					}
				}
			} else {
				for (var i = currentIndex+1; i <= productList.length; i++){
					pageArray.push(i);
				}
			}
		}
		return pageArray;
	}
	
	function smartFixedNavigation(){
		angular.element(document).ready(function ($) {
			// browser window scroll (in pixels) after which the "menu" link is shown
			var offset = 300;

			var navigationContainer = $('#cd-nav-mobile'),
				mainNavigation = navigationContainer.find('#cd-main-nav-mobile ul');

			//hide or show the "menu" link
			checkMenu();
			$(window).scroll(function(){
				checkMenu();
			});

			//open or close the menu clicking on the bottom "menu" link
			$('.cd-nav-trigger').on('click', function(){
				$(this).toggleClass('menu-is-open');
				//we need to remove the transitionEnd event handler (we add it when scolling up with the menu open)
				mainNavigation.off('webkitTransitionEnd otransitionend oTransitionEnd msTransitionEnd transitionend').toggleClass('is-visible');

			});

			function checkMenu() {
				if( $(window).scrollTop() > offset && !navigationContainer.hasClass('is-fixed')) {
					navigationContainer.addClass('is-fixed').find('.cd-nav-trigger').one('webkitAnimationEnd oanimationend msAnimationEnd animationend', function(){
						mainNavigation.addClass('has-transitions');
					});
				} else if ($(window).scrollTop() <= offset) {
					//check if the menu is open when scrolling up
					if( mainNavigation.hasClass('is-visible')  && !$('html').hasClass('no-csstransitions') ) {
						//close the menu with animation
						mainNavigation.addClass('is-hidden').one('webkitTransitionEnd otransitionend oTransitionEnd msTransitionEnd transitionend', function(){
							//wait for the menu to be closed and do the rest
							mainNavigation.removeClass('is-visible is-hidden has-transitions');
							navigationContainer.removeClass('is-fixed');
							$('.cd-nav-trigger').removeClass('menu-is-open');
						});
					//check if the menu is open when scrolling up - fallback if transitions are not supported
					} else if( mainNavigation.hasClass('is-visible')  && $('html').hasClass('no-csstransitions') ) {
							mainNavigation.removeClass('is-visible has-transitions');
							navigationContainer.removeClass('is-fixed');
							$('.cd-nav-trigger').removeClass('menu-is-open');
					//scrolling up with menu closed
					} else {
						navigationContainer.removeClass('is-fixed');
						mainNavigation.removeClass('has-transitions');
					}
				} 
			}
		});
	}
	
	function megaSiteNavigation(){
		angular.element(document).ready(function ($) {
			//if you change this breakpoint in the style.css file (or _layout.scss if you use SASS), don't forget to update this value as well
			var MqL = 1170;
			//move nav element position according to window width
			moveNavigation();
			$(window).on('resize', function(){
				(!window.requestAnimationFrame) ? setTimeout(moveNavigation, 300) : window.requestAnimationFrame(moveNavigation);
			});

			//mobile - open lateral menu clicking on the menu icon
			$('.mega-cd-nav-trigger').on('click', function(event){
				event.preventDefault();
				if( $('.cd-main-content').hasClass('nav-is-visible') ) {
					closeNav();
					$('.cd-overlay').removeClass('is-visible');
				} else {
					$(this).addClass('nav-is-visible');
					$('.cd-primary-nav').addClass('nav-is-visible');
					$('.cd-main-header').addClass('nav-is-visible');
					$('.cd-main-content').addClass('nav-is-visible').one('webkitTransitionEnd otransitionend oTransitionEnd msTransitionEnd transitionend', function(){
						$('body').addClass('overflow-hidden');
					});
					toggleSearch('close');
					$('.cd-overlay').addClass('is-visible');
				}
			});

			//open search form
			$('.cd-search-trigger').on('click', function(event){
				event.preventDefault();
				toggleSearch();
				closeNav();
			});

			//close lateral menu on mobile 
			$('.cd-overlay').on('swiperight', function(){
				if($('.cd-primary-nav').hasClass('nav-is-visible')) {
					closeNav();
					$('.cd-overlay').removeClass('is-visible');
				}
			});
			$('.nav-on-left .cd-overlay').on('swipeleft', function(){
				if($('.cd-primary-nav').hasClass('nav-is-visible')) {
					closeNav();
					$('.cd-overlay').removeClass('is-visible');
				}
			});
			$('.cd-overlay').on('click', function(){
				closeNav();
				toggleSearch('close')
				$('.cd-overlay').removeClass('is-visible');
			});


			//prevent default clicking on direct children of .cd-primary-nav 
			$('.cd-primary-nav').children('.has-children').children('a').on('click', function(event){
				event.preventDefault();
			});
			//open submenu
			$('.has-children').children('a').on('click', function(event){
				if( !checkWindowWidth() ) event.preventDefault();
				var selected = $(this);
				if( selected.next('ul').hasClass('is-hidden') ) {
					//desktop version only
					selected.addClass('selected').next('ul').removeClass('is-hidden').end().parent('.has-children').parent('ul').addClass('moves-out');
					selected.parent('.has-children').siblings('.has-children').children('ul').addClass('is-hidden').end().children('a').removeClass('selected');
					$('.cd-overlay').addClass('is-visible');
				} else {
					selected.removeClass('selected').next('ul').addClass('is-hidden').end().parent('.has-children').parent('ul').removeClass('moves-out');
					$('.cd-overlay').removeClass('is-visible');
				}
				toggleSearch('close');
			});

			//submenu items - go back link
			$('.go-back').on('click', function(){
				$(this).parent('ul').addClass('is-hidden').parent('.has-children').parent('ul').removeClass('moves-out');
			});

			function closeNav() {
				$('.mega-cd-nav-trigger').removeClass('nav-is-visible');
				$('.cd-main-header').removeClass('nav-is-visible');
				$('.cd-primary-nav').removeClass('nav-is-visible');
				$('.has-children ul').addClass('is-hidden');
				$('.has-children a').removeClass('selected');
				$('.moves-out').removeClass('moves-out');
				$('.cd-main-content').removeClass('nav-is-visible').one('webkitTransitionEnd otransitionend oTransitionEnd msTransitionEnd transitionend', function(){
					$('body').removeClass('overflow-hidden');
				});
			}

			function toggleSearch(type) {
				if(type=="close") {
					//close serach 
					$('.cd-search').removeClass('is-visible');
					$('.cd-search-trigger').removeClass('search-is-visible');
					$('.cd-overlay').removeClass('search-is-visible');
				} else {
					//toggle search visibility
					$('.cd-search').toggleClass('is-visible');
					$('.cd-search-trigger').toggleClass('search-is-visible');
					$('.cd-overlay').toggleClass('search-is-visible');
					if($(window).width() > MqL && $('.cd-search').hasClass('is-visible')) $('.cd-search').find('input[type="search"]').focus();
					($('.cd-search').hasClass('is-visible')) ? $('.cd-overlay').addClass('is-visible') : $('.cd-overlay').removeClass('is-visible') ;
				}
			}

			function checkWindowWidth() {
				//check window width (scrollbar included)
				var e = window, 
		            a = 'inner';
		        if (!('innerWidth' in window )) {
		            a = 'client';
		            e = document.documentElement || document.body;
		        }
		        if ( e[ a+'Width' ] >= MqL ) {
					return true;
				} else {
					return false;
				}
			}

			function moveNavigation(){
//				var navigation = $('.mega-cd-nav');
//		  		var desktop = checkWindowWidth();
//		        if ( desktop ) {
//					navigation.detach();
//					navigation.insertBefore('.cd-header-buttons');
//				} else {
//					navigation.detach();
//					navigation.insertAfter('.cd-main-content');
//				}
			}
		});
	}
	
	function getAllShoppingCartItem(){
		q = $q.defer();
		var shoppingCart = {};
		shoppingCart.items = [];
		shoppingCart.subTotal = 0;
		var counter = 0;
		userService.getCarts().then(function(cartResult) {
			var fields = ['images','price','stock', 'categories','name', 'variantOptions', 'description', 'baseOptions'];
			angular.forEach(cartResult.data.carts[0].entries, function(cartEntry) {
				var product = {};
				product.entryNumber = cartEntry.entryNumber;
				product.itemNumber = cartEntry.product.code;
				product.quantity = parseInt(cartEntry.quantity);
				if(cartEntry.product.url.split("/p/").pop() != product.itemNumber){
					product.itemNumber = cartEntry.product.url.split("/p/").pop();
					cartEntry.product.code = cartEntry.product.url.split("/p/").pop();
				}
				//Modify
					
				//Get Image
				var imgUrl = config.baseUrl + config.productPath + cartEntry.product.code + "?fields=" + fields;
				productService.getProduct(imgUrl).then(
					function success(result){
						counter++;
						product.image = config.baseUrl + result.data.images[0].url;
						product.variantOptions = result.data.variantOptions;
						if(result.data.variantOptions == undefined && result.data.baseOptions != undefined){
							product.variantOptions = result.data.baseOptions[0].options;
						}
						if(result.data.stock.stockLevel > 0){
							product.availability = "In Stock";
							cartEntry.product.stock != undefined ? product.stockLevel = cartEntry.product.stock.stockLevel : product.stockLevel = result.data.stock.stockLevel
						}else {						
							product.availability = "Out of Stock";
							product.stockLevel = 0;
						}
						product.categories = result.data.categories;
						product.description = result.data.description;
						product.name = result.data.name;
						product.unitPrice = parseFloat(result.data.price.value);
						shoppingCart.subTotal += parseFloat(result.data.price.value) * parseInt(product.quantity);
						if(counter == cartResult.data.carts[0].entries.length){
							q.resolve(shoppingCart);
						}
					},
					function error(result){
						console.log(result);
					});
				shoppingCart.items.push(product);
			});
			if(cartResult.data.carts[0].entries == undefined){
				q.resolve(shoppingCart);				
			}
		});
		return q.promise;
	}
	
	function getProjectItemDetails(projItem , itemLimit){
		q = $q.defer();
		var projItemList = {};
		projItemList.items = [];
		projItemList.subTotal = 0;
		var counter = 0;
		var index = 1;
		var fields = ['images','price','stock','name', 'description', 'categories', 'variantOptions', 'baseOptions'];

		var remainingItem = [];
		angular.forEach(projItem.entries, function(projEntry) {
			if(index <= itemLimit){
				var product = {};
				product.itemNumber = projEntry.productCode;
				product.quantity = parseInt(projEntry.productQuantity);
				//Modify
					
				//Get Image
				var imgUrl = config.baseUrl + config.productPath + projEntry.productCode + "?fields=" + fields;
				productService.getProduct(imgUrl).then(
					function success(result){
						counter++;
						product.image = config.baseUrl + result.data.images[0].url;
						product.name = result.data.name;
						product.description = result.data.description;
						product.categories = result.data.categories;
						product.variantOptions = result.data.variantOptions;
						if(result.data.variantOptions == undefined && result.data.baseOptions != undefined){
							product.variantOptions = result.data.baseOptions[0].options;
						}
						if(result.data.stock.stockLevel > 0){
							product.availability = "In Stock";
							product.stockLevel = result.data.stock.stockLevel;
						} else {
							product.availability = "Out of Stock";
							product.stockLevel = 0;
						}
						product.unitPrice = parseFloat(result.data.price.value);
						projItemList.subTotal += parseFloat(result.data.price.value) * parseInt(product.quantity);
						if(counter == itemLimit || counter == projItem.entries.length){
							for(i in remainingItem){
								projItemList.items.push(remainingItem[i])
							}
							q.resolve(projItemList);
						}
					},
					function error(result){
						console.log(result);
					});
				projItemList.items.push(product);
				index++;
			}else{
				var product = {};
				product.itemNumber = projEntry.productCode;
				product.quantity = parseInt(projEntry.productQuantity);
				remainingItem.push(product);
			}
		});
		if(projItem.entries == undefined){
			q.resolve(projItemList);				
		}
		return q.promise;
	}
	
	function getProjectItemDetailsPerPage(projEntry , indexStart, indexEnd){
		q = $q.defer();
		var projItemList = {};
		projItemList.items = [];
		projItemList.subTotal = 0;
		indexCount = indexEnd - indexStart;
		var counter = 0;
		var index = indexStart;
		var fields = ['images','price','stock','name', 'description', 'categories', 'variantOptions', 'baseOptions'];

		var remainingItem = [];
		angular.forEach(projEntry, function(projEntries, i) {
//		for(i in projEntry){
			if(i == index && index < indexEnd){
				var product = {};
				product.itemNumber = projEntries.itemNumber;
				product.quantity = parseInt(projEntries.quantity);
					
				//Get Image
				var imgUrl = config.baseUrl + config.productPath + projEntries.itemNumber + "?fields=" + fields;
				productService.getProduct(imgUrl).then(
					function success(result){
						counter++;
						product.image = config.baseUrl + result.data.images[0].url;
						product.name = result.data.name;
						product.description = result.data.description;
						product.categories = result.data.categories;
						product.variantOptions = result.data.variantOptions;
						if(result.data.variantOptions == undefined && result.data.baseOptions != undefined){
							product.variantOptions = result.data.baseOptions[0].options;
						}
						if(result.data.stock.stockLevel > 0){
							product.availability = "In Stock";
							product.stockLevel = result.data.stock.stockLevel;
						} else {
							product.availability = "Out of Stock";
							product.stockLevel = 0;
						}
						product.unitPrice = parseFloat(result.data.price.value);
						projItemList.subTotal += parseFloat(result.data.price.value) * parseInt(product.quantity); 
						projEntry[i] = product;
						if(counter == indexCount){
							q.resolve(projEntry);
						}
					},
					function error(result){
						console.log(result);
					});
				projItemList.items.push(product);
				index++;
			}
		});
		if(projEntry == undefined){
			q.resolve(projItemList);				
		}
		return q.promise;
	}
	
	function retrieveOrderDetails(orderItems){
		q = $q.defer();
		var fields = ['images','price','description', 'categories', 'variantOptions', 'baseOptions'];
		var counter = 0;
		
		angular.forEach(orderItems, function(item) {

			if(item.product.url.split("/p/").pop() != item.product.code){
				item.product.code = item.product.url.split("/p/").pop();
			}
			var url = config.baseUrl + config.productPath + item.product.code + "?fields=" + fields;
			productService.getProduct(url).then(
				function success(result){
					counter++;
					item.image = config.baseUrl + result.data.images[0].url;
					item.variantOptions = result.data.variantOptions;
					if(result.data.variantOptions == undefined && result.data.baseOptions != undefined){
						item.variantOptions = result.data.baseOptions[0].options;
					}
					item.price = result.data.price.value;
					item.description = result.data.description;
					item.categories = result.data.categories;
					//Modify
					item.option = "";
					item.project = "";
					if(counter == orderItems.length){
						q.resolve(orderItems);
					}
				},
				function error(result){
					console.log(result);
			});
		});
		if(orderItems.length == undefined){
			q.resolve(orderItems);				
		}
		return q.promise;
		
	}
	
	function setSelectedInvoices(invoiceList){
		selectedInvoice = invoiceList;
		$window.localStorage.setItem("selectedInvoiceList",invoiceList);
	}
	
	function getSelectedInvoices(){
		selectedInvoice = JSON.parse($window.localStorage.getItem('selectedInvoiceList'));
		return selectedInvoice;
	}
		
	function createDocDefinition(invoice){
		var docDefinition = {};
		//To be changed with true data
		docDefinition = {
				   content: [
				     // if you don't need styles, you can use a simple string to define a paragraph
				     'This is a standard paragraph, using default style',

				     // using a { text: '...' } object lets you set styling properties
				     { text: 'This paragraph will have a bigger font', fontSize: 15 },

				     // if you set pass an array instead of a string, you'll be able
				     // to style any fragment individually
				     {
				       text: [
				         'This paragraph is defined as an array of elements to make it possible to ',
				         { text: 'restyle part of it and make it bigger ', fontSize: 15 },
				         'than the rest.'
				       ]
				     }
				   ]
				 };
		
		return docDefinition;
	}
	
	function saveToken(token){
		$window.localStorage.setItem('access_token',token);
	}
	function getToken(){
		$window.localStorage.getItem('access_token');
	}

	function saveUser(user){
		$window.localStorage.setItem('user',JSON.stringify(user));
	}

	function getUser(){
		return JSON.parse($window.localStorage.getItem('user'));
	}

	function saveJwt(jwt){
		$window.localStorage.setItem('token',jwt);
	}

	return utilities;
	
	function getInvoiceList(){
		var invoiceList = [
		           		{'selected': false, 'invoiceNumber':'1234567891', 'poNumber':756425387, 'dateIssued': '01/03/2017', 'total': 10, 'status': 'OPEN', 'pdfFile': 'PDF', totalItem : 10 , contactName : "Sarah Smith", contactNo: "555-555-1212"},
		        		{'selected': false, 'invoiceNumber':762435418, 'poNumber':'98710283812312312312312312312312312312312312312314545', 'dateIssued': '03/02/2017', 'total': 100, 'status': 'OPEN', 'pdfFile': 'PDF', totalItem : 10 , contactName : "Sarah Smith", contactNo: "555-555-1212"},
		        		{'selected': true, 'invoiceNumber':983745934, 'poNumber':675823723, 'dateIssued': '12/01/2017', 'total': 320, 'status': 'PAID', 'pdfFile': 'PDF', totalItem : 10 , contactName : "Sarah Smith", contactNo: "555-555-1212"},
		        		{'selected': false, 'invoiceNumber':236748734, 'poNumber':523687482, 'dateIssued': '07/05/2017', 'total': 240, 'status': 'PAID', 'pdfFile': 'PDF', totalItem : 10 , contactName : "Sarah Smith", contactNo: "555-555-1212"},
		        		{'selected': true, 'invoiceNumber':287982873, 'poNumber':123657282, 'dateIssued': '06/16/2017', 'total': 90, 'status': 'OPEN', 'pdfFile': 'PDF', totalItem : 10 , contactName : "Sarah Smith", contactNo: "555-555-1212"},
		        		{'selected': false, 'invoiceNumber':516768297, 'poNumber':634534232, 'dateIssued': '04/20/2017', 'total': 1000, 'status': 'PAST DUE', 'pdfFile': 'PDF', totalItem : 10 , contactName : "Sarah Smith", contactNo: "555-555-1212"},
		        		{'selected': false, 'invoiceNumber':438267876, 'poNumber':124674242, 'dateIssued': '08/25/2017', 'total': 1500, 'status': 'PAST DUE', 'pdfFile': 'PDF', totalItem : 10 , contactName : "Sarah Smith", contactNo: "555-555-1212"},
		        	]
		return invoiceList;
	}
	
	function createDocDefinitionTable(invoice){
		var docDefinition = {};
		//To be changed with true data
		docDefinition = {
				   content: [
						{
							style: 'tableExample',
							table: {
								headerRows: 1,
								// dontBreakRows: true,
								// keepWithHeaderRows: 1,
								body: [
									[{text: 'Invoice Number', style: 'tableHeader'}, 
									 {text: 'PO Number', style: 'tableHeader'}, 
									 {text: 'Date Issued', style: 'tableHeader'}, 
									 {text: 'Contact Name', style: 'tableHeader'}, 
									 {text: 'Contact Email', style: 'tableHeader'}, 
									 {text: 'Status', style: 'tableHeader'}, 
									 {text: 'Credits Applied', style: 'tableHeader'},
									 {text: 'Total', style: 'tableHeader'}],
										['123456789', '756425387', '01/03/2017', 'Sarah Smith', 'ssmith@xyzarchitects.com','Paid','$0.00','$100'],
										['123456789', '756425387', '01/03/2017', 'Sarah Smith', 'ssmith@xyzarchitects.com','Paid','$0.00','$100'],
										['123456789', '756425387', '01/03/2017', 'Sarah Smith', 'ssmith@xyzarchitects.com','Paid','$0.00','$100'],
										['123456789', '756425387', '01/03/2017', 'Sarah Smith', 'ssmith@xyzarchitects.com','Paid','$0.00','$100'],
									
								]
							}
						}
				   ]
				 };
		
		return docDefinition;
	}
	
	function reorderRows(newOrder){
		q = $q.defer();		
		q.resolve(newOrder);
		return q.promise;
	}
	
	function addItemsToCartProj(urlList){
		q = $q.defer();
		var promises = [];
		var counter = 0;
		angular.forEach(urlList, function(url) {
			projectService.addToCartProjectItem(url).then(
				function success(result){
//					return result;
					counter++;
					if(counter == urlList.length){
						q.resolve(result);
					}
				},
				function error(result){
//					return result;
					counter++;
					console.log(result);
					if(counter == urlList.length){
						q.resolve(result);
					}
			});
		});
		if(urlList.length == undefined){
			q.resolve();				
		}
//		$q.all(promises).then((res)=>{ 
//			     $q.defer().resolve(res)
//		})
		return q.promise;		
	}
}]);