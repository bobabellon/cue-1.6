var app =  angular.module('projectService', []);

app.factory('projectService', ['$http','$cookieStore','config','$q','$window',
	function($http, $cookieStore, config, $q,$window) {
	  	return {
		    retrieveProjectList : function() {
				return $http({
					url: config.baseUrl + config.userPath + JSON.parse($window.localStorage.getItem('user')).userName + "/customerprojects/",
					method: 'GET',
					crossDomain: true,
					headers: {						
						'Authorization': 'Bearer ' + $window.localStorage.getItem('access_token')
					}
				}).then(function(response){''
					return response.data.customerProjects;
				}, function(errResponse){
					console.error('error fetching retrieveProjectList');
					return $q.reject(errResponse);
				});
			},
			retrieveProjectDetail : function(projectId) {
				return $http({
					url: config.baseUrl + config.userPath + JSON.parse($window.localStorage.getItem('user')).userName + "/customerprojects/" +projectId,
					method: 'GET',
					crossDomain: true,
					headers: {						
						'Authorization': 'Bearer ' + $window.localStorage.getItem('access_token')
					}
				}).then(function(response){
					return response.data;
				}, function(errResponse){
					console.error('error fetching retrieveProjectList');
					return $q.reject(errResponse);
				});
			},
			createProject : function(project) {
				return $http({
					url: config.baseUrl + config.userPath + JSON.parse($window.localStorage.getItem('user')).userName + "/customerprojects/",
					method: 'POST',
					crossDomain: true,
					headers: {						
						'Authorization': 'Bearer ' + $window.localStorage.getItem('access_token')
					},
					contentType: 'application/x-www-form-urlencoded; charset=utf-8',
					data : project
				}).then(function(response){
					return response.data;
				}, function(errResponse){
					console.error('error fetching createProject');
					return $q.reject(errResponse);
				}); 
			},
			deleteProject : function(projectId){
				return $http({
					url: config.baseUrl + config.userPath + JSON.parse($window.localStorage.getItem('user')).userName + "/customerprojects/" + projectId,
					method: 'DELETE',
					crossDomain: true,
					headers: {						
						'Authorization': 'Bearer ' + $window.localStorage.getItem('access_token')
					},
					contentType: 'application/x-www-form-urlencoded; charset=utf-8'
				}).then(function(response){
					return response.data;
				}, function(errResponse){
					console.error('error fetching deleteProject');
					return $q.reject(errResponse);
				}); 
			},
			renameProject : function(project, projectId){
				return $http({
					url: config.baseUrl + config.userPath + JSON.parse($window.localStorage.getItem('user')).userName + "/customerprojects/" + projectId,
					method: 'PUT',
					crossDomain: true,
					headers: {						
						'Authorization': 'Bearer ' + $window.localStorage.getItem('access_token')
					},
					contentType: 'application/x-www-form-urlencoded; charset=utf-8',
					data : project
				}).then(function(response){
					return response.data;
				}, function(errResponse){
					console.error('error fetching renameProject');
					return $q.reject(errResponse);
				}); 
			},
			deleteProjectItem : function(projectId, productCode){
				return $http({
					url: config.baseUrl + config.userPath + JSON.parse($window.localStorage.getItem('user')).userName + "/customerprojects/" + projectId + "/products/" + productCode,
					method: 'DELETE',
					crossDomain: true,
					headers: {						
						'Authorization': 'Bearer ' + $window.localStorage.getItem('access_token')
					},
					contentType: 'application/x-www-form-urlencoded; charset=utf-8'
				}).then(function(response){
					return response;
				}, function(errResponse){
					console.error('error fetching deleteProjectItem');
					return $q.reject(errResponse);
				});  
			},
			addToCartProjectItem : function(url, productList){
				
				return $http({
					url: url,
					method: 'POST',
					crossDomain: true,
					headers: {						
						'Authorization': 'Bearer ' + $window.localStorage.getItem('access_token')
					},
					contentType: 'application/x-www-form-urlencoded; charset=utf-8',
					data : productList
				}).then(function(response){
					return response;
				}, function(errResponse){
					console.error('error fetching addToCartProjectItem');
					return $q.reject(errResponse);
				});  
			}
	  	};
}]);