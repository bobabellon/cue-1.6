var app =  angular.module('categoryService', []);

app.factory('categoryService', ['$http','$cookieStore','config', '$window',
	function($http, $cookieStore, config, $window) {
	  	return {
		    getProductCategory: function(categoryId) {
		    	return $http({
					method: 'GET',
					url: config.baseUrl + config.catalogPath + config.cataglogId + "/" + config.catalogVersionId + "/categories/" + categoryId + "?access_token=" + $window.localStorage.getItem('access_token'),
					contentType: 'application/x-www-form-urlencoded; charset=utf-8',
					crossDomain: true,
					headers: {						
						'Authorization': 'Bearer ' + $window.localStorage.getItem('access_token')
					}
				});
		    },
		    getAllHandToolsCategory: function() {
		    	return $http({
					method: 'GET',
					url: config.baseUrl + config.catalogPath + config.cataglogId + "/" + config.catalogVersionId + "/categories/1355",
					contentType: 'application/x-www-form-urlencoded; charset=utf-8',
					crossDomain: true,
					headers: {						
						'Authorization': 'Bearer ' + $window.localStorage.getItem('access_token')
					}
				});
		    },
		    getAllFootWearCategory: function() {
		    	return $http({
					method: 'GET',
					url: config.baseUrl + config.catalogPath + config.cataglogId + "/" + config.catalogVersionId + "/categories/1802",
					contentType: 'application/x-www-form-urlencoded; charset=utf-8',
					crossDomain: true,
					headers: {						
						'Authorization': 'Bearer ' + $window.localStorage.getItem('access_token')
					}
				});
		    }
	  	}
}]);