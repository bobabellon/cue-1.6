var app =  angular.module('userService', ['utilityFactory']);

app.factory('userService', ['$http','$cookieStore','config', '$q','$window',
	function($http, $cookieStore, config, $q,$window) {
	  	return {
//Cart	  		
		    getCarts: function() {
		    	return $http({
					method: 'GET',
					url: config.baseUrl + config.userPath + JSON.parse($window.localStorage.getItem('user')).userName + "/carts?access_token=" +$window.localStorage.getItem('access_token'),
					contentType: 'application/x-www-form-urlencoded; charset=utf-8',
					crossDomain: true,
					headers: {						
						'Authorization': 'Bearer ' + $window.localStorage.getItem('access_token')
					}
				}).then(function(response){
					return response;
				}, function(errResponse){
					console.error('error fetching getCarts');
					return $q.reject(errResponse);
				});
			},
			getNotifications: function() {
		    	return $http({
					method: 'GET',
					url: "/api/notifications",
					contentType: 'application/x-www-form-urlencoded; charset=utf-8',
					crossDomain: true,
					headers: {						
						'Authorization': 'Bearer ' + $window.localStorage.getItem('token')
					}
				}).then(function(response){
					return response;
				}, function(errResponse){
					console.error('error fetching getCarts');
					return $q.reject(errResponse);
				});
		    },
		    createCart : function() {
				return $http({
					method: 'POST',
					url: config.baseUrl + config.userPath + JSON.parse($window.localStorage.getItem('user')).userName + "/carts",
					contentType: 'application/x-www-form-urlencoded; charset=utf-8',
					crossDomain: true,
					headers: {						
						'Authorization': 'Bearer ' + $window.localStorage.getItem('access_token')
					}
				});
			},
			addToCart : function(url) {
				return $http({
					method: 'POST',
					url: url,
					contentType: 'application/x-www-form-urlencoded; charset=utf-8',
					crossDomain: true,
					headers: {						
						'Authorization': 'Bearer ' + $window.localStorage.getItem('access_token')
					}
				}).then(function(response){
					return response;
				}, function(errResponse){
					console.error('error fetching addToCart');
					return $q.reject(errResponse);
				});
			},
			removeToCart : function(url) {
				return $http({
					method: 'DELETE',
					url: url,
					contentType: 'application/x-www-form-urlencoded; charset=utf-8',
					crossDomain: true,
					headers: {						
						'Authorization': 'Bearer ' + $window.localStorage.getItem('access_token')
					}
				}).then(function(response){
					return response;
				}, function(errResponse){
					console.error('error fetching removeToCart');
					return $q.reject(errResponse);
				});
			},
			updateItemQuantityToCart : function(url) {
				return $http({
					method: 'PUT',
					url: url,
					contentType: 'application/x-www-form-urlencoded; charset=utf-8',
					crossDomain: true,
					headers: {						
						'Authorization': 'Bearer ' + $window.localStorage.getItem('access_token')
					}
				}).then(function(response){
					return response;
				}, function(errResponse){
					console.error('error fetching updateItemQuantityToCart');
					return $q.reject(errResponse);
				});
			},
//Order
			getOrders: function(pageSize, currentPage) {
		    	return $http({
					method: 'GET',
					url: config.baseUrl + config.userPath + JSON.parse($window.localStorage.getItem('user')).userName + "/orders?pageSize=" + pageSize + "&currentPage=" + currentPage,
					contentType: 'application/x-www-form-urlencoded; charset=utf-8',
					crossDomain: true,
					headers: {						
						'Authorization': 'Bearer ' + $window.localStorage.getItem('access_token')
					}
				}).then(function(response){
					return response;
				}, function(errResponse){
					console.error('error fetching getOrders');
					return $q.reject(errResponse);
				});
		    },
		    getOrderDetails: function(orderCode) {
		    	return $http({
					method: 'GET',
					url: config.baseUrl + config.userPath + JSON.parse($window.localStorage.getItem('user')).userName + "/orders/" + orderCode,
					contentType: 'application/x-www-form-urlencoded; charset=utf-8',
					crossDomain: true,
					headers: {						
						'Authorization': 'Bearer ' + $window.localStorage.getItem('access_token')
					}
				}).then(function(response){
					return response;
				}, function(errResponse){
					console.error('error fetching getOrderDetails');
					return $q.reject(errResponse);
				});
		    },
		    addOrders: function(cartId, securityCode) {
		    	return $http({
					method: 'POST',
					url: config.baseUrl + config.userPath + JSON.parse($window.localStorage.getItem('user')).userName + "/orders?cartId=" + cartId, // + "&securityCode=" + securityCode,
					contentType: 'application/x-www-form-urlencoded; charset=utf-8',
					crossDomain: true,
					headers: {						
						'Authorization': 'Bearer ' + $window.localStorage.getItem('access_token')
					}
				});
		    },
//Address
		    getAddress: function() {
		    	return $http({
					method: 'GET',
					url: config.baseUrl + config.userPath + JSON.parse($window.localStorage.getItem('user')).userName + "/addresses",
					contentType: 'application/x-www-form-urlencoded; charset=utf-8',
					crossDomain: true,
					headers: {						
						'Authorization': 'Bearer ' + $window.localStorage.getItem('access_token')
					}
				});
		    },
		    addAddress: function(regionISOCode, firstName, lastName, town, postalCode, line1, titleCode, countryISOCode) {
		    	return $http({
					method: 'POST',
					url: config.baseUrl + config.userPath + JSON.parse($window.localStorage.getItem('user')).userName + "/addresses?region.isocode="
						+ regionISOCode + "&firstName=" + firstName + "&lastName=" + lastName + "&town=" + town + "&postalCode=" + postalCode
						+ "&line1=" + line1 + "&titleCode=" + titleCode + "&country.isocode=" + countryISOCode,
					contentType: 'application/x-www-form-urlencoded; charset=utf-8',
					crossDomain: true,
					headers: {						
						'Authorization': 'Bearer ' + $window.localStorage.getItem('access_token')
					}
				});
		    },
//PaymentDetail
		    getPaymentAllDetails: function() {
		    	return $http({
					method: 'GET',
					url: config.baseUrl + config.userPath + JSON.parse($window.localStorage.getItem('user')).userName + "/paymentdetails",
					contentType: 'application/x-www-form-urlencoded; charset=utf-8',
					crossDomain: true,
					headers: {						
						'Authorization': 'Bearer ' + $window.localStorage.getItem('access_token')
					}
				});
		    },
		    getPaymentDetails: function(paymentDetailsId) {
		    	return $http({
					method: 'GET',
					url: config.baseUrl + config.userPath + JSON.parse($window.localStorage.getItem('user')).userName + "/paymentdetails/" + paymentDetailsId,
					contentType: 'application/x-www-form-urlencoded; charset=utf-8',
					crossDomain: true,
					headers: {						
						'Authorization': 'Bearer ' + $window.localStorage.getItem('access_token')
					}
				});
		    },
		    addPaymentDetails: function(paymentInformation) {
		    	return $http({
					method: 'POST',
					url: config.baseUrl + config.userPath + JSON.parse($window.localStorage.getItem('user')).userName + "/carts/" + JSON.parse($window.localStorage.getItem('cart')).code
						+ "/paymentdetails?billingAddress.country.isocode=" + paymentInformation.billingAddressCountryCode + "&billingAddress.line1="
						+ paymentInformation.billingAddressLine1 + "&saved=" + paymentInformation.saved + "&billingAddress.line2=" + paymentInformation.billingAddressLine2
						+ "&billingAddress.region.isocode=" + paymentInformation.billingAddressRegionISOCode + "&cardType=" + paymentInformation.cardType
						+ "&expiryMonth=" + paymentInformation.expiryMonth + "&billingAddress.titleCode=" + paymentInformation.billingAddressTitleCode
						+ "&defaultPaymentInfo=" + paymentInformation.defaultPaymentInfo + "&expiryYear=" + paymentInformation.expiryYear
						+ "&accountHolderName=" + paymentInformation.accountHolderName + "&billingAddress.postalCode=" + paymentInformation.billingAddressPostalCode
						+ "&billingAddress.lastName=" + paymentInformation.billingAddressLastName + "&billingAddress.town=" + paymentInformation.billingAddressTown
						+ "&cardNumber=" + paymentInformation.cardNumber + "&billingAddress.firstName=" + paymentInformation.billingAddressFirstName,
					contentType: 'application/x-www-form-urlencoded; charset=utf-8',
					crossDomain: true,
					headers: {						
						'Authorization': 'Bearer ' + $window.localStorage.getItem('access_token')
					}
				});
		    },
		    updatePaymentDetails: function(paymentDetailsId, paymentInformation) {
		    	return $http({
					method: 'PUT',
					url: config.baseUrl + config.userPath + JSON.parse($window.localStorage.getItem('user')).userName + "/paymentdetails/" + paymentDetailsId
						+ "?billingAddress.country.isocode=" + paymentInformation.billingAddressCountryCode + "&billingAddress.line1="
						+ paymentInformation.billingAddressLine1 + "&saved=" + paymentInformation.saved + "&billingAddress.line2=" + paymentInformation.billingAddressLine2
						+ "&billingAddress.region.isocode=" + paymentInformation.billingAddressRegionISOCode + "&cardType=" + paymentInformation.cardType
						+ "&expiryMonth=" + paymentInformation.expiryMonth + "&billingAddress.titleCode=" + paymentInformation.billingAddressTitleCode
						+ "&defaultPaymentInfo=" + paymentInformation.defaultPaymentInfo + "&expiryYear=" + paymentInformation.expiryYear
						+ "&accountHolderName=" + paymentInformation.accountHolderName + "&billingAddress.postalCode=" + paymentInformation.billingAddressPostalCode
						+ "&billingAddress.lastName=" + paymentInformation.billingAddressLastName + "&billingAddress.town=" + paymentInformation.billingAddressTown
						+ "&cardNumber=" + paymentInformation.cardNumber + "&billingAddress.firstName=" + paymentInformation.billingAddressFirstName,
					contentType: 'application/x-www-form-urlencoded; charset=utf-8',
					crossDomain: true,
					headers: {						
						'Authorization': 'Bearer ' + $window.localStorage.getItem('access_token')
					}
				});
		    },
		    deletePaymentDetails: function(paymentDetailsId) {
		    	return $http({
					method: 'DELETE',
					url: config.baseUrl + config.userPath + JSON.parse($window.localStorage.getItem('user')).userName + "/paymentdetails/" + paymentDetailsId,
					contentType: 'application/x-www-form-urlencoded; charset=utf-8',
					crossDomain: true,
					headers: {						
						'Authorization': 'Bearer ' + $window.localStorage.getItem('access_token')
					}
				});
		    },
		    setPaymentDetails: function() {
		    	return $http({
					method: 'PUT',
					url: config.baseUrl + config.userPath + JSON.parse($window.localStorage.getItem('user')).userName + "/paymentdetails",
					contentType: 'application/x-www-form-urlencoded; charset=utf-8',
					crossDomain: true,
					headers: {						
						'Authorization': 'Bearer ' + $window.localStorage.getItem('access_token')
					}
				});
		    },
		    updatePaymentDetails: function() {
		    	return $http({
					method: 'PATCH',
					url: config.baseUrl + config.userPath + JSON.parse($window.localStorage.getItem('user')).userName + "/paymentdetails",
					contentType: 'application/x-www-form-urlencoded; charset=utf-8',
					crossDomain: true,
					headers: {						
						'Authorization': 'Bearer ' + $window.localStorage.getItem('access_token')
					}
				});
		    },
//Delivery
		    addAddressAndAssignToDelivery: function(regionISOCode, firstName, lastName, town, postalCode, line1, titleCode, countryISOCode) {
		    	return $http({
					method: 'POST',
					url: config.baseUrl + config.userPath + JSON.parse($window.localStorage.getItem('user')).userName + "/carts/" + JSON.parse($window.localStorage.getItem('cart')).code
						+ "/addresses/delivery?region.isocode=" + regionISOCode + "&firstName=" + firstName + "&lastName=" + lastName + "&town=" 
						+ town + "&postalCode=" + postalCode + "&line1=" + line1 + "&titleCode=" + titleCode + "&country.isocode=" + countryISOCode,
					contentType: 'application/x-www-form-urlencoded; charset=utf-8',
					crossDomain: true,
					headers: {						
						'Authorization': 'Bearer ' + $window.localStorage.getItem('access_token')
					}
				});
		    },
		    setDeliveryAddress: function(addressId) {
		    	return $http({
					method: 'PUT',
					url: config.baseUrl + config.userPath + JSON.parse($window.localStorage.getItem('user')).userName + "/carts/" + JSON.parse($window.localStorage.getItem('cart')).code
						+"/addresses/delivery?addressId=" + addressId,
					contentType: 'application/x-www-form-urlencoded; charset=utf-8',
					crossDomain: true,
					headers: {						
						'Authorization': 'Bearer ' + $window.localStorage.getItem('access_token')
					}
				});
		    },
//Delivery Mode
		    getAllDeliveryModes: function() {
		    	return $http({
					method: 'GET',
					url: config.baseUrl + config.userPath + JSON.parse($window.localStorage.getItem('user')).userName + "/carts/" + JSON.parse($window.localStorage.getItem('cart')).code + "/deliverymodes",
					contentType: 'application/x-www-form-urlencoded; charset=utf-8',
					crossDomain: true,
					headers: {						
						'Authorization': 'Bearer ' + $window.localStorage.getItem('access_token')
					}
				});
		    },
		    getCartDeliveryMode: function() {
		    	return $http({
					method: 'GET',
					url: config.baseUrl + config.userPath + JSON.parse($window.localStorage.getItem('user')).userName + "/carts/" + JSON.parse($window.localStorage.getItem('cart')).code + "/deliverymode",
					contentType: 'application/x-www-form-urlencoded; charset=utf-8',
					crossDomain: true,
					headers: {						
						'Authorization': 'Bearer ' + $window.localStorage.getItem('access_token')
					}
				});
		    },
		    setCartDeliveryMode: function(deliveryModeId) {
		    	return $http({
					method: 'PUT',
					url: config.baseUrl + config.userPath + JSON.parse($window.localStorage.getItem('user')).userName + "/carts/" + JSON.parse($window.localStorage.getItem('cart')).code + "/deliverymode?deliveryModeId=" + deliveryModeId,
					contentType: 'application/x-www-form-urlencoded; charset=utf-8',
					crossDomain: true,
					headers: {						
						'Authorization': 'Bearer ' + $window.localStorage.getItem('access_token')
					}
				});
		    },
		    deleteDeliveryMode: function() {
		    	return $http({
					method: 'DELETE',
					url: config.baseUrl + config.userPath + JSON.parse($window.localStorage.getItem('user')).userName + "/carts/" + $window.localStorage.getItem('cart').code + "/deliverymode",
					contentType: 'application/x-www-form-urlencoded; charset=utf-8',
					crossDomain: true,
					headers: {						
						'Authorization': 'Bearer ' + $window.localStorage.getItem('access_token')
					}
				});
		    },
//User Info
		    getUserDetails: function(userName) {
		    	return $http({
					method: 'GET',
					url: config.baseUrl + config.userPath + userName + "?access_token="+$window.localStorage.getItem('access_token'),
					contentType: 'application/x-www-form-urlencoded; charset=utf-8',
					crossDomain: true,
					headers: {						
						'Authorization': 'Bearer ' + $window.localStorage.getItem('access_token')
					}
				});
		    }
	  	};
}]);