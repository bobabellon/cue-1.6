var app =  angular.module('productService', []);

app.factory('productService', ['$http','config','$cookieStore', '$q',
	function($http, config, $cookieStore, $q) {
		return {
			getProduct : function(url) {
				return $http({
					url: url,
			  	method: 'GET'
				}).then(function(response){
					return response;
				}, function(errResponse){
					console.error('error fetching getProduct');
					return $q.reject(errResponse);
				});
			},
			getReferences : function(url) {
				return $http({
					url: url,
			  	method: 'GET'
				});
			},
			getReviews : function(url) {
				return $http({
					url: url,
			  	method: 'POST'
				});
			},
			searchProduct : function(url) {
				return $http({
					url: url,
                	method: 'GET'
				});
			},
		};
}]);