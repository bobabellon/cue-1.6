var app = angular.module('reportDetailController', [ 'datatables', 'datatables.colreorder', 'pdf', 'reportService', 'datatables.buttons', 'datatables.fixedheader']);
app.controller("reportDetailController", ['$scope', '$rootScope', '$cookieStore','config', 'reportService','$window', 
                                    'DTOptionsBuilder', 'DTColumnDefBuilder', '$compile', '$timeout', 'pdfDelegate', 
                                    'utilityFactory', '$state', '$stateParams','$compile',  
	function($scope, $rootScope, $cookieStore, config, reportService, $window, 
			DTOptionsBuilder, DTColumnDefBuilder, $compile, $timeout, pdfDelegate,
			utilityFactory, $state, $stateParams,$compile){	
	$scope.retrieveReportDetail = function(){
		$scope.reportId = 0;
    	$scope.reportDetail = reportService.getReportDetail();
    	$scope.reportId = parseInt($stateParams.reportId);
    	if($cookieStore.get("reports") != undefined){
        	$scope.report = $cookieStore.get("reports")[$scope.reportId];
    	}
    	$scope.newReport = $cookieStore.get("newReport");
    	if($scope.report != undefined &&($scope.report.type == 'Summary Report' || $scope.report.type == 'Product Summary Report' || $scope.report.type == 'Ship-To Summary Report')){
    		if($scope.report.chartType == undefined){
    			$scope.report.chartType = 'Line Chart'
    		}
    		$scope.newReport = {
    			  "reportName": $scope.report.reportName,
    			  "type": $scope.report.type,
    			  "category": [
    			    "Category 2"
    			  ],
    			  "chartType": $scope.report.chartType,
    			  "customer": "Customer 3",
    			  "division": [
    			    "Gypsum"
    			  ],
    			  "states": [
    			    "New Jersey",
    			    "New York"
    			  ],
    			  "shipToAccount": [
    			    "All"
    			  ],
    			  "reportFormat": "Tabular",
    			  "data": [],
    			  "timePeriod": "This Week",
    			  "unit" : $scope.report.unit
    			}
    	}
    	
    	if($cookieStore.get("reports") == undefined || $cookieStore.get("reports").length == 0 || $scope.report == undefined){
        	$scope.newReport = $cookieStore.get("newReport");
        	$scope.report = {
        			reportName : $scope.newReport.reportName,
            		type : $scope.newReport.type
            	};
    	}
    	 $scope.intitializeTableFormat = function(){
    		   if($scope.newReport.type == 'Ship-To Summary Report'){
    			   	$scope.getTotalValues = function(){
    				    var totalShipped = 0,totalJan = 0, totalFeb = 0, totalMar = 0, totalApr = 0, totalMay = 0, totalJun = 0, totalJul = 0, totalAug = 0, totalSep = 0, totalOct =0, totalNov =0, totalDec =0;
    				    for(i in $scope.shipToData){	    
    				    	totalShipped += $scope.shipToData[i].total.totalList[0];
    				    	totalJan += $scope.shipToData[i].total.totalList[1];
    				    	totalFeb += $scope.shipToData[i].total.totalList[2];
    				    	totalMar += $scope.shipToData[i].total.totalList[3];
    				    	totalApr += $scope.shipToData[i].total.totalList[4];
    				    	totalMay += $scope.shipToData[i].total.totalList[5];
    				    	totalJun += $scope.shipToData[i].total.totalList[6];
    				    	totalJul += $scope.shipToData[i].total.totalList[7];
    				    	totalAug += $scope.shipToData[i].total.totalList[8];
    				    	totalSep += $scope.shipToData[i].total.totalList[9];
    				    	totalOct += $scope.shipToData[i].total.totalList[10];
    				    	totalNov += $scope.shipToData[i].total.totalList[11];
    				    	totalDec += $scope.shipToData[i].total.totalList[12];	
    					}
    					if(totalShipped == 0){totalShipped = '-';}else{totalShipped = parseFloat(totalShipped).toFixed(2);}
    					if(totalJan == 0){totalJan = '-';}else{totalJan = parseFloat(totalJan).toFixed(2);}
    					if(totalFeb == 0){totalFeb = '-';}else{totalFeb = parseFloat(totalFeb).toFixed(2);}
    					if(totalMar == 0){totalMar = '-';}else{totalMar = parseFloat(totalMar).toFixed(2);}
    					if(totalApr == 0){totalApr = '-';}else{totalApr = parseFloat(totalApr).toFixed(2);}
    					if(totalMay == 0){totalMay = '-';}else{totalMay = parseFloat(totalMay).toFixed(2);}
    					if(totalJun == 0){totalJun = '-';}else{totalJun = parseFloat(totalJun).toFixed(2);}
    					if(totalJul == 0){totalJul = '-';}else{totalJul = parseFloat(totalJul).toFixed(2);}
    					if(totalAug == 0){totalAug = '-';}else{totalAug = parseFloat(totalAug).toFixed(2);}
    					if(totalSep == 0){totalSep = '-';}else{totalSep = parseFloat(totalSep).toFixed(2);}
    					if(totalOct == 0){totalOct = '-';}else{totalOct = parseFloat(totalOct).toFixed(2);}
    					if(totalNov == 0){totalNov = '-';}else{totalNov = parseFloat(totalNov).toFixed(2);}
    					if(totalDec == 0){totalDec = '-';}else{totalDec = parseFloat(totalDec).toFixed(2);}
    				    return {totalShipped:totalShipped,totalJan:totalJan,totalFeb:totalFeb,totalMar:totalMar,totalApr:totalApr,totalMay:totalMay,totalJun:totalJun,totalJul:totalJul,totalAug:totalAug,totalSep:totalSep,totalOct:totalOct,totalNov:totalNov,totalDec:totalDec};
    				}
    			    $scope.groupSortingShip=function(){
    			    	var table = $('#ship-to-table').DataTable({
    			    		"responsive":true,
    			    		"lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
    			    		"buttons":[
    			            {
    			            	 extend: 'print',
    			                 orientation: 'landscape',
    			                 text:'<span class="icon" ><i class="material-icons">print</i></span>',
    			                 pageSize: 'LEGAL',
    			                 footer: true,
    			                 className: 'button',
    			            	 customize: function ( win ) {
    			            		 $(win.document.body).css('overflow', 'visible')
    			            		 $(win.document.body)
    			                     .prepend(
    			                         '<img src="'+ $scope.encodedImage + '" style="position:relative; top:0; left:0;" />'
    			                     );
    			                     $(win.document.body).find( 'td:first-child')
    			                     .css( 'display', 'none' );                   
    			                     $(win.document.body).find('table')
    			                     .css( 'font-size', '10pt' )
    			            		 $(win.document.body).find( 'th:first-child')
    			                     .css( 'display', 'none' );
    								 $(win.document.body).find( 'td:nth-child(4)')
    						         .css( 'word-break', 'break-word' );
    								 $(win.document.body).find( 'td:nth-child(5)')
    						         .css( 'word-break', 'break-word' );
    						         $(win.document.body).css('overflow', 'visible')
    			            		 $(win.document.body).find( 'th')
    			                     .css( 'background', '#454f6a' );
    						         var last = null;
					                 var current = null;
					                 var bod = [];					 
					                 var css = '@page { size: landscape; }',
					                    head = win.document.head || win.document.getElementsByTagName('head')[0],
					                    style = win.document.createElement('style');					 
					                 style.type = 'text/css';
					                 style.media = 'print';					  
					                 if (style.styleSheet)
					                 {
					                   style.styleSheet.cssText = css;
					                 }
					                 else
					                 {
					                   style.appendChild(win.document.createTextNode(css));
					                 }					  
					                 head.appendChild(style);
    			            	 },
    			            	  action: function(e, dt, button, config) {
    				                _rowGroupingPrint = 1;
    				                _rowGroupingCSV = 0;
    				                product_summary_Report = 0;
    			                	$.fn.dataTable.ext.buttons.print.action(e, dt, button, config);
    			            	 }
    			            },
    			            {
    			            	 extend: 'csvHtml5',
    			                 title:  $scope.report.reportName,
    			                 text:'<span class="icon"><img class="logo-img report-icon" src="resources/images/reports-icon/reports-export-excel.svg"></span>',
    			                 className: 'button',
    			                 footer: true,
    			                 action:function(e, dt, button, config){
    			                 	_rowGroupingCSV = 1;
    				                _rowGroupingPrint = 0;
    				                product_summary_Report = 0;
    			                 	$.fn.dataTable.ext.buttons.csvHtml5.action(e, dt, button, config);
    			                 }
    			            },
    			            {
    				           	 extend: 'pdf',
    				             title:  $scope.report.reportName,
    				             text:'<span class="icon"><img class="logo-img report-icon" src="resources/images/reports-icon/pdf-icon-blue.svg"></span>',
    				             orientation: 'landscape',
    				             className: 'button',
    				             pageSize: 'LEGAL',
    				             action:function(e, dt, button, config){
    			                 	_rowGroupingCSV = 0;
    				                _rowGroupingPrint = 0;
    				                product_summary_Report = 0;
    			                 	$.fn.dataTable.ext.buttons.pdfHtml5.action(e, dt, button, config);
    			                 },
    				             customize: function ( doc ) {
    				                 var lastColX=null;
    					            var lastColY=null;
    					            var bod = []; 
    					            var subTtl = new Array();
    					            var grandTotal = new Array();
    					            grandTotal[0] = 0;
    					            var headerName = "";
    					            doc.content[1].table.body.forEach(function(line, i){
    					                if(lastColX != line[0].text && line[0].text != ''){
    					                    if(i !=1){
    					                    	
    					                    	for(var z = 1; z < 14;z++){
    					                			if(subTtl[z] == 0){subTtl[z] ='-';}
    					                			else{subTtl[z] = parseFloat(subTtl[z]).toFixed(2);}
    					                		}
    					                    	headerName = lastColX.substring(0, lastColX.indexOf('*'));
    					                     bod.push(['','','Total for State','',subTtl[0],subTtl[1],subTtl[2],subTtl[3],subTtl[4],subTtl[5],subTtl[6],subTtl[7],subTtl[8],subTtl[9],subTtl[10],subTtl[11],subTtl[12],subTtl[13]]);
    					                     bod.push(['','','Total for ' + headerName,'',subTtl[0],subTtl[1],subTtl[2],subTtl[3],subTtl[4],subTtl[5],subTtl[6],subTtl[7],subTtl[8],subTtl[9],subTtl[10],subTtl[11],subTtl[12],subTtl[13]]);
    					                     subTtl = [];
    					                    }
    					                    bod.push([{text:line[0].text.substring(0, line[0].text.indexOf('*')), style:'rowHeader'},'','','','','','','','','','','','','','','','','']);
    					                    lastColX=line[0].text;
    					                }
    					                if( i < doc.content[1].table.body.length){
    					                    bod.push(['',{text:line[1].text, style:'defaultStyle'},{text:line[2].text, style:'defaultStyle'},{text:line[3].text, style:'defaultStyle'},{text:line[4].text, style:'defaultStyle'},{text:line[5].text, style:'defaultStyle'},{text:line[6].text, style:'defaultStyle'},{text:line[7].text, style:'defaultStyle'},{text:line[8].text, style:'defaultStyle'},{text:line[9].text, style:'defaultStyle'},{text:line[10].text, style:'defaultStyle'},{text:line[11].text, style:'defaultStyle'},{text:line[12].text, style:'defaultStyle'},{text:line[13].text, style:'defaultStyle'},{text:line[14].text, style:'defaultStyle'},{text:line[15].text, style:'defaultStyle'},{text:line[16].text, style:'defaultStyle'},{text:line[17].text, style:'defaultStyle'}]);   
    					                    subTtl[0] = line[4].text;
    					                	for(var z = 1; z < 14;z++){
    					                		 if (typeof subTtl[z] =='undefined'){
    				                        		 subTtl[z] = 0; 
    				                    		 }
    					                		 if (typeof grandTotal[z] =='undefined'){
    				                        		 grandTotal[z] = 0; 
    				                    		 }
    					                		if(!isNaN(parseFloat(line[z+4].text))){subTtl[z] += parseFloat(line[z+4].text);grandTotal[z] += parseFloat(line[z+4].text);}
    					                	}
    					                }
    					                 
    					            });
    					            for(var z = 1; z < 14;z++){
    					                			if(subTtl[z] == 0){subTtl[z] ='-';}
    					                			else{subTtl[z] = parseFloat(subTtl[z]).toFixed(2);}
    					                			if(grandTotal[z] == 0){grandTotal[z] ='-';}
    					                			else{grandTotal[z] = parseFloat(grandTotal[z]).toFixed(2);}
    					                		}
    					                     bod.push(['','','Total for State','',subTtl[0],subTtl[1],subTtl[2],subTtl[3],subTtl[4],subTtl[5],subTtl[6],subTtl[7],subTtl[8],subTtl[9],subTtl[10],subTtl[11],subTtl[12],subTtl[13]]);
    					                     bod.push(['','','Total for ' + headerName,'',subTtl[0],subTtl[1],subTtl[2],subTtl[3],subTtl[4],subTtl[5],subTtl[6],subTtl[7],subTtl[8],subTtl[9],subTtl[10],subTtl[11],subTtl[12],subTtl[13]]);
    					                     bod.push(['','','Total for Gypsum Products','','',grandTotal[1],grandTotal[2],grandTotal[3],grandTotal[4],grandTotal[5],grandTotal[6],grandTotal[7],grandTotal[8],grandTotal[9],grandTotal[10],grandTotal[11],grandTotal[12],grandTotal[13]]);
    					            doc.content[1].table.headerRows = 1;
    					            doc.content[1].table.body = bod;
    					            doc.content[1].layout = 'lightHorizontalLines';
    					             
    					            doc.styles = {
    					                rowHeader: {
    					                    bold: true,
    					                    color: 'black',
    					                    whiteSpace:'nowrap',
    					                },
    					                defaultStyle: {
    					                fontSize: 8,
    					                color: 'black'
    					                } 
    					            }
    					            
    				                 doc.content.splice( 1, 0, {
    				                     image: $scope.encodedImage,
    				                     alignment: 'center',
    				                     width : '500'                  
    				                 } );
    			             }
    			           }
    			        ],
    				        "columnDefs": [
    				            { "visible": false, "targets": 0 },
    				            {
    							targets: [ 0 ],
    							orderData: [ 4, 0 ]
    						}, {
    							targets: [ 1 ],
    							orderData: [ 4, 1 ]
    						}, {
    							targets: [ 2 ],
    							orderData: [ 4, 2 ]
    						}, {
    							targets: [ 3 ],
    							orderData: [ 4, 3 ]
    						}, {
    							targets: [ 4 ],
    							orderData: [ 4, 0 ]
    						}, {
    							targets: [ 5 ],
    							orderData: [ 4, 5 ]
    						}, {
    							targets: [ 6 ],
    							orderData: [ 4, 6 ]
    						}, {
    							targets: [ 7 ],
    							orderData: [ 4, 7 ]
    						}, {
    							targets: [ 8 ],
    							orderData: [ 4, 8 ]
    						}, {
    							targets: [ 9 ],
    							orderData: [ 4, 9 ]
    						}, {
    							targets: [ 10 ],
    							orderData: [ 4, 10 ]
    						}, {
    							targets: [ 11 ],
    							orderData: [ 4, 11 ]
    						}, {
    							targets: [ 12 ],
    							orderData: [ 4, 12 ]
    						}, {
    							targets: [ 13 ],
    							orderData: [ 4, 13 ]
    						}, {
    							targets: [ 14 ],
    							orderData: [ 4, 14 ]
    						}, {
    							targets: [ 15 ],
    							orderData: [ 4, 15 ]
    						}, {
    							targets: [ 16 ],
    							orderData: [ 4, 16 ]
    						}, {
    							targets: [ 17 ],
    							orderData: [ 4, 17 ]
    						}
    				        ],
    				        "orderFixed": [0, 'asc'],
    				        "order": [[ 4, 'asc' ]],
    				        "displayLength": 10,
    				        "rowGroup": {
    					        dataSrc: 0
    					    },
    				        "drawCallback": function ( settings ) {
    				            var api = this.api();
    				            var rows = api.rows( {page:'current'} ).nodes();
    				            var last=null;
    				            var colonne = api.row(0).data().length;
    				            var totale = new Array();
    				            totale['Totale']= new Array();
    			    			var subtotale = new Array();
    				            var groupid = -1;
    				            var groupRow= 0;
    				            var headerName = "";
    				            api.column(0, {page:'current'} ).data().each( function ( group, i ) {
    				                if ( last !== group ) {
    				                	headerName = group.substring(0, group.indexOf('*'));
    				                	if(i !=0){
    				                		$(rows).eq( i ).before(
    				                    	  '<tr class="report-footer"><td colspan="3" class="has-text-right">Total for State</td></tr>'
    				                    	);
//    				                		$(rows).eq( i ).before(
//    	    				                    	  '<tr class="report-footer-sec"><td colspan="3" class="has-text-right">Total for ' +last.substring(0, last.indexOf('*'))+ '</td></tr>'
//    	    				                    	);
    				                	}
    				                    $(rows).eq( i ).before(
    				                        '<tr class="report-title"><td colspan="17">'+headerName+'</td></tr>'
    				                    );
    				                    last = group;
    				                	groupRow++;
    				                }
    					            val = api.row(api.row($(rows).eq( i )).index()).data();
//    					            console.log(val)
    				                $.each(val,function(index2,val2){
    						                if (typeof subtotale[groupRow] =='undefined'){
    						                    subtotale[groupRow] = new Array();
    						                }
    				                        if (typeof subtotale[groupRow][index2] =='undefined'){
    				                            subtotale[groupRow][index2] = 0;
    				                        }
    				                        if(val2 == '-'){val2 = 0;}
    				                        if(index2 == 4){subtotale[groupRow][index2] = val2;}
    				                        else{subtotale[groupRow][index2] += parseFloat(val2);}
    				                });
    				            } );
    				            $('#ship-to-table tbody tr:last-child').after(
    				                    	  '<tr class="report-footer"><td colspan="3" class="has-text-right">Total for State</td></tr>'
    				             );
    				            var groupRow1= 1;
    				            $('#ship-to-table tbody').find('.report-footer').each(function (i,v) {
    			                    var rowCount = $(this).nextUntil('.report-footer').length;
    			                        var subtd = '<td>'+subtotale[groupRow1][4]+'</td>';
    			                        for (var a=5;a<colonne;a++)
    			                        { 
    			                        	if(subtotale[groupRow1][a] == 0){subtd += '<td>-</td>';}
    			                        	else{subtd += '<td>'+parseFloat(subtotale[groupRow1][a]).toFixed(2)+'</td>';}
    			                        }
    			                        $(this).append(subtd);
    			                       groupRow1++;
    			                });
    				            
    				            var totalHTML = '';
    				            /*angular.forEach($scope.sectors, function(sector){
    				            	totalHTML += 
    				            	'<tr class="report-mFooter total-all">' +
    				            	'<td class="has-text-right" colspan="4">' + sector.name.split('_').join(' ') + '</td>' +
    								'<td class="has-text-left">' + sector.total.toFixed(3) + '</td>' +
    								'<td class="has-text-left">' + sector.janTotal.toFixed(3) + '</td>' +
    								'<td class="has-text-left">' + sector.febTotal.toFixed(3) + '</td>' +
    								'<td class="has-text-left">' + sector.marTotal.toFixed(3) + '</td>' +
    								'<td class="has-text-left">' + sector.aprTotal.toFixed(3) + '</td>' +
    								'<td class="has-text-left">' + sector.mayTotal.toFixed(3) + '</td>' +
    								'<td class="has-text-left">' + sector.junTotal.toFixed(3) + '</td>' +
    								'<td class="has-text-left">' + sector.julTotal.toFixed(3) + '</td>' +
    								'<td class="has-text-left">' + sector.augTotal.toFixed(3) + '</td>' +
    								'<td class="has-text-left">' + sector.sepTotal.toFixed(3) + '</td>' +
    								'<td class="has-text-left">' + sector.octTotal.toFixed(3) + '</td>' +
    								'<td class="has-text-left">' + sector.novTotal.toFixed(3) + '</td>' +
    								'<td class="has-text-left">' + sector.decTotal.toFixed(3) + '</td>' + 
    								'</tr>'
    				            });
    				            $(".total-all").remove();	
    				            $(totalHTML).insertBefore( ".report-mFooter" );*/
//    				            $('#ship-to-table tbody tr:last-child').after(
//    				                    	  '<tr class="report-footer-sec"><td colspan="3" class="has-text-right">Total for ' +headerName+ '</td></tr>'
//    				             );
//    				            var groupRow1= 1;
//    				            $('#ship-to-table tbody').find('.report-footer-sec').each(function (i,v) {
//    			                    var rowCount = $(this).nextUntil('.report-footer-sec').length;
//    			                        var subtd = '<td>'+subtotale[groupRow1][4]+'</td>';
//    			                        for (var a=5;a<colonne;a++)
//    			                        { 
//    			                        	if(subtotale[groupRow1][a] == 0){subtd += '<td>-</td>';}
//    			                        	else{subtd += '<td>'+parseFloat(subtotale[groupRow1][a]).toFixed(2)+'</td>';}
//    			                        }
//    			                        $(this).append(subtd);
//    			                       groupRow1++;
//    			                });
    				        }
    				    } );
    				    $('#ship-to-table tbody').on( 'click', 'tr.report-title', function () {
    				        var currentOrder = table.order()[0];
    				        if (currentOrder[0] === 0 && currentOrder[1] === 'asc' ) {
    				           table.order( [ 0, 'desc' ],[ 4, 'desc' ] ).draw();
    				        }
    				        else {
    				           table.order( [ 0, 'asc' ],[ 0, 'asc' ] ).draw(); 
    				        }
    				    } );
    				    table.buttons( 0, null ).container().appendTo( '.report-export' );
    				    
    			                if($('.ship-to-summary-tab').width() >= 920){
    					    		 $('#ship-to-table tbody td').each(function (i,v) {
    					    		 	if($(this).css('display') == 'none'){$(this).show();}
    					    		 	
    					    		 });
    					    		 $('#ship-to-table thead th').each(function (i,v) {
    					    		 	if($(this).css('display') == 'none'){$(this).show();}
    					    		 	
    					    		 });
    					    		 $('#ship-to-table').removeClass('collapsed');
    					    	}
    			                
    			                $scope.resetSort = function(col){
    		    	            	 var currentOrder = table.order()[0];
    	    				        if (currentOrder[0] === col && currentOrder[1] === 'desc' ) {
    	    				        	$timeout( function(){ table.order( [ 0, 'asc' ]).draw(); }, 10);
    	    				           
    	    				        }    			    	                
    	    			    	}
    					}
    					$timeout( function(){ $scope.groupSortingShip(); }, 1000);
    		    }   
    		  	else if($scope.newReport.type == 'Product Summary Report'){
    			   	$scope.getTotalPValues = function(){
    				    var totalAmount = 0;
    		    		var label = []
			    		var keyNames = Object.keys($scope.reportSummary.total);
		    			for(i in keyNames){
		    				totalAmount += $scope.reportSummary.total[keyNames[i]][0].total$;
		    			} 
    				   /* for(i in $scope.ProductData){	    
    				    	totalAmount += $scope.ProductData[i].total.totalList[1];	
    					}*/
    					if(totalAmount == 0){totalAmount = '-';}else{totalAmount = parseFloat(totalAmount).toFixed(2);}
    				    return totalAmount;
    				}
    				$scope.groupSorting=function(){
    			    	var table = $('#product-to-table').DataTable({
    			    		"responsive":true,
    			    		"lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
    			    		"buttons":[
    			            {
    			            	 extend: 'print',
    			                 text:'<span class="icon" ><i class="material-icons">print</i></span>',
    			                 pageSize: 'LEGAL',
    			                 footer: true,
    			                 className: 'button',
    			            	 customize: function ( win ) {
    			            		 $(win.document.body).css('overflow', 'visible')
    			            		 $(win.document.body)
    			                     .prepend(
    			                         '<img src="'+ $scope.encodedImage + '" style="position:relative; top:0; left:0;" />'
    			                     );
    			                     $(win.document.body).find( 'td:first-child')
    			                     .css( 'display', 'none' );                   
    			                     $(win.document.body).find('table')
    			                     .css( 'font-size', '10pt' )
    			            		 $(win.document.body).find( 'th:first-child')
    			                     .css( 'display', 'none' );
    						         $(win.document.body).css('overflow', 'visible')
    			            		 $(win.document.body).find( 'th')
    			                     .css( 'background', '#454f6a' );
    			            	 },
    			            	  action: function(e, dt, button, config) {
    				                _rowGroupingPrint = 1;
    				                _rowGroupingCSV = 0;
    				                product_summary_Report = 1;
    			                	$.fn.dataTable.ext.buttons.print.action(e, dt, button, config);
    			            	 }
    			            },
    			            {
    			            	 extend: 'csvHtml5',
    			                 title:  $scope.report.reportName,
    			                 text:'<span class="icon"><img class="logo-img report-icon" src="resources/images/reports-icon/reports-export-excel.svg"></span>',
    			                 className: 'button',
    			                 footer: true,
    			                 action:function(e, dt, button, config){
    			                 	_rowGroupingCSV = 1;
    				                _rowGroupingPrint = 0;
    				                product_summary_Report = 1;
    			                 	$.fn.dataTable.ext.buttons.csvHtml5.action(e, dt, button, config);
    			                 }
    			            },
    			            {
    				           	 extend: 'pdf',
    				             title:  $scope.report.reportName,
    				             text:'<span class="icon"><img class="logo-img report-icon" src="resources/images/reports-icon/pdf-icon-blue.svg"></span>',
    				             className: 'button',
    				             pageSize: 'LEGAL',
    				             action:function(e, dt, button, config){
    			                 	_rowGroupingCSV = 0;
    				                _rowGroupingPrint = 0;
    				                product_summary_Report = 0;
    			                 	$.fn.dataTable.ext.buttons.pdfHtml5.action(e, dt, button, config);
    			                 },
    				             customize: function ( doc ) {
    				                 var lastColX=null;
    					            var lastColY=null;
    					            var bod = []; 
    					            var subTtl = new Array();
    					            var grandTotal = 0;
    					            doc.content[1].table.body.forEach(function(line, i){
    					                if(lastColX != line[0].text && line[0].text != ''){
    					                    if(i !=1){
    					                    	
    					                    	for(var z = 1; z < 5;z++){
    					                			if(z==2 || z==4){subTtl[z] = parseFloat(subTtl[z]).toFixed(2);}
    					                		}
    					                     bod.push(['','Total',subTtl[2],'',subTtl[4]]);
    					                     subTtl = [];
    					                    }
    					                    bod.push([{text:line[0].text, style:'rowHeader'},'','','','']);
    					                    lastColX=line[0].text;
    					                }
    					                if( i < doc.content[1].table.body.length){
    					                    bod.push(['',{text:line[1].text, style:'defaultStyle'},{text:line[2].text, style:'defaultStyle'},{text:line[3].text, style:'defaultStyle'},{text:line[4].text, style:'defaultStyle'}]);   
    					                   for(var z = 1; z < 5;z++){
    					                		 if (typeof subTtl[z] =='undefined'){
    				                        		 subTtl[z] = 0; 
    				                    		 }
    				                    		if(z == 2 || z == 4){
    					                		if(!isNaN(parseFloat(line[z].text))){subTtl[z] += parseFloat(line[z].text);}}
    					                		if(z == 4 && !isNaN(parseFloat(line[z].text)) ){grandTotal += parseFloat(line[z].text);}
    					                	}
    					                }
    					                 
    					            });
    					            for(var z = 1; z < 5;z++){
    					                			if(subTtl[z] == 0){subTtl[z] ='-';}
    					                			else{subTtl[z] = parseFloat(subTtl[z]).toFixed(2);}
    					                		}
    					                		grandTotal = parseFloat(grandTotal).toFixed(2)
    					                     bod.push(['','Total',subTtl[2],'',subTtl[4]]);
    					                     bod.push(['','Total Amount','','',grandTotal]);
    					             
    					            doc.content[1].table.headerRows = 1;
    					            doc.content[1].table.body = bod;
    					            doc.content[1].layout = 'lightHorizontalLines';
    					             
    					            doc.styles = {
    					                rowHeader: {
    					                    bold: true,
    					                    color: 'black',
    					                    whiteSpace:'nowrap',
    					                },
    					                defaultStyle: {
    					                fontSize: 8,
    					                color: 'black'
    					                } 
    					            }
    					            
    				                 doc.content.splice( 1, 0, {
    				                     image: $scope.encodedImage,
    				                     alignment: 'center',
    				                     width : '500'                  
    				                 } );
    			             }
    			           }
    			        ],
    				        "columnDefs": [
    				            { "visible": false, "targets": 0 },
    				        ],
    				        "orderFixed": [0, 'asc'],
    				        "order": [[ 0, 'asc' ]],
    				        "displayLength": 10,
    				        "rowGroup": {
    					        dataSrc: 0
    					    },
    				        "drawCallback": function ( settings ) {
    				            var api = this.api();
    				            var rows = api.rows( {page:'current'} ).nodes();
    				            var last=null;
    				            var colonne = api.row(0).data().length;
    				            var totale = new Array();
    				            totale['Totale']= new Array();
    			    			var subtotale = new Array();
    				            var groupid = -1;
    				            var groupRow= 0;
    				 
    				            api.column(0, {page:'current'} ).data().each( function ( group, i ) {
    				                if ( last !== group ) {
    				                	if(i !=0){
    				                		$(rows).eq( i ).before(
    				                    	  '<tr class="report-footer"><td class="has-text-right">Total</td></tr>'
    				                    	);
    				                	}
    				                    $(rows).eq( i ).before(
    				                        '<tr class="report-title"><td colspan="4">'+group+'</td></tr>'
    				                    );
    				                    last = group;
    				                	groupRow++;
    				                }
    					            val = api.row(api.row($(rows).eq( i )).index()).data();    
    				                $.each(val,function(index2,val2){
    						                if (typeof subtotale[groupRow] =='undefined'){
    						                    subtotale[groupRow] = new Array();
    						                }
    				                        if (typeof subtotale[groupRow][index2] =='undefined'){
    				                            subtotale[groupRow][index2] = 0;
    				                        }
    				                        if(val2 == '-'){val2 = 0;}
    				                        if(index2 == 2 || index2 == 4){subtotale[groupRow][index2] += parseFloat(val2);}
    				                });
    				            } );
    				            $('#product-to-table tbody tr:last-child').after(
    				                    	  '<tr class="report-footer"><td class="has-text-right">Total </td></tr>'
    				             );
    				            var groupRow1= 1;
    				            $('#product-to-table tbody').find('.report-footer').each(function (i,v) {
    			                    var rowCount = $(this).nextUntil('.report-footer').length;
    			                        var subtd = '';
    			                        for (var a=2;a<colonne;a++)
    			                        { 
    			                        	if((a == 2 || a == 4)){
    			                        		if(subtotale[groupRow1] != undefined && isNaN(parseFloat(subtotale[groupRow1][a]))){
    			                        			subtd += '<td class="has-text-right">0</td>';
    			                        		}else{
        			                        		subtd += '<td class="has-text-right">'+parseFloat(subtotale[groupRow1][a]).toFixed(2)+'</td>';
    			                        		}
    			                        	}
    			                        	if(a==3){subtd +='<td>&nbsp;</td>';}
    			                        }
    			                        $(this).append(subtd);
    			                       groupRow1++;
    			                });
    				        }
    				    } );
    				    $('#product-to-table tbody').on( 'click', 'tr.report-title', function () {
    				        var currentOrder = table.order()[0];
    				        if (currentOrder[0] === 0 && currentOrder[1] === 'asc' ) {
    				           table.order( [ 0, 'desc' ]).draw();
    				        }
    				        else {
    				           table.order( [ 0, 'asc' ]).draw(); 
    				        }
    				    } );
    				    table.buttons( 0, null ).container().appendTo( '.report-export' );
    				    $scope.resetSort = function(col){
	    	            	 var currentOrder = table.order()[0];
    				        if (currentOrder[0] === col && currentOrder[1] === 'desc' ) {
    				        	$timeout( function(){ table.order( [ 0, 'asc' ]).draw(); }, 10);    				           
    				        }    			    	                
    			    	}
    				}
    				$timeout( function(){ 
    					$scope.groupSorting(); 
						$compile($(".report-footer")) ($scope);
						$compile($(".report-title")) ($scope);
    				}, 1000);
    		    }
    		  	else if($scope.newReport.type == 'Summary Report'){

    			    $scope.groupSortingShip=function(){
        				if($scope.newReport.unit == "Both"){
        					var csvColumnHide = 'th:not(:nth-child(8))';
        					var columnSort = [
        		    				            { "visible": false, "targets": 0 },
        		    				            {
        		    							targets: [ 0 ],
        		    							orderData: [ 8, 0 ]
        		    						}, {
        		    							targets: [ 1 ],
        		    							orderData: [ 8, 1 ]
        		    						}, {
        		    							targets: [ 2 ],
        		    							orderData: [ 8, 2 ]
        		    						}, {
        		    							targets: [ 3 ],
        		    							orderData: [ 8, 3 ]
        		    						}, {
        		    							targets: [ 4 ],
        		    							orderData: [ 8, 4 ]
        		    						}, {
        		    							targets: [ 5 ],
        		    							orderData: [ 8, 5 ]
        		    						}, {
        		    							targets: [ 6 ],
        		    							orderData: [ 8, 6 ]
        		    						}, {
        		    							targets: [ 7 ],
        		    							orderData: [ 8, 7 ]
        		    						}, {
        		    							targets: [ 8 ],
        		    							orderData: [ 8, 0 ]
        		    						}
        		    				        ]
        				}else{
        					var csvColumnHide = 'th:not(:nth-child(5))';
        					var columnSort = [
      		    				            { "visible": false, "targets": 0 },
      		    				            {
      		    							targets: [ 0 ],
      		    							orderData: [ 5, 0 ]
      		    						}, {
      		    							targets: [ 1 ],
      		    							orderData: [ 5, 1 ]
      		    						}, {
      		    							targets: [ 2 ],
      		    							orderData: [ 5, 2 ]
      		    						}, {
      		    							targets: [ 3 ],
      		    							orderData: [ 5, 3 ]
      		    						}, {
      		    							targets: [ 4 ],
      		    							orderData: [ 5, 4 ]
      		    						}, {
      		    							targets: [ 5 ],
      		    							orderData: [ 5, 0 ]
      		    						}
      		    				        ]
        				}
    			    	var table = $('#summary-report-table').DataTable({
    			    		"responsive":true,
    			    		"lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
    			    		"iDisplayLength": 10,
    			    		"buttons":[,
    			            {
    			            	 extend: 'print',
    			                 orientation: 'landscape',
    			                 text:'<span class="icon" ><i class="material-icons">print</i></span>',
    			                 pageSize: 'LEGAL',
    			                 footer: true,
    			                 className: 'button',
    			            	 customize: function ( win ) {
    			            		 $(win.document.body).css('overflow', 'visible')
    			            		 $(win.document.body)
    			                     .prepend(
    			                         '<img src="'+ $scope.encodedImage + '" style="position:relative; top:0; left:0;" />'
    			                     );
    			            		 if($scope.newReport.unit == "Both"){
	    			                     $(win.document.body).find( 'td:first-child')
	    			                     .css( 'display', 'none' );      
	    			                     $(win.document.body).find( 'td:nth-child(9)')
	    			                     .css( 'display', 'none' );                
	    			                     $(win.document.body).find('table')
	    			                     .css( 'font-size', '10pt' )
	    			            		 $(win.document.body).find( 'th:first-child')
	    			                     .css( 'display', 'none' );     
	    			                     $(win.document.body).find( 'th:nth-child(9)')
	    			                     .css( 'display', 'none' ); 
    			            		 }else{
	    			                     $(win.document.body).find( 'td:first-child')
	    			                     .css( 'display', 'none' );      
	    			                     $(win.document.body).find( 'td:nth-child(6)')
	    			                     .css( 'display', 'none' );                
	    			                     $(win.document.body).find('table')
	    			                     .css( 'font-size', '10pt' )
	    			            		 $(win.document.body).find( 'th:first-child')
	    			                     .css( 'display', 'none' );     
	    			                     $(win.document.body).find( 'th:nth-child(6)')
	    			                     .css( 'display', 'none' ); 
    			            		 }
    						         $(win.document.body).css('overflow', 'visible')
    			            		 $(win.document.body).find( 'th')
    			                     .css( 'background', '#454f6a' );
    			            	 },
    			            	  action: function(e, dt, button, config) {
    				                _rowGroupingPrint = 1;
    				                _rowGroupingCSV = 0;
    				                summary_Report = 1;
    			                	$.fn.dataTable.ext.buttons.print.action(e, dt, button, config);
    			            	 }
    			            },
    			            {
    			            	 extend: 'csvHtml5',
    			                 title:  $scope.report.reportName,
    			                 text:'<span class="icon"><img class="logo-img report-icon" src="resources/images/reports-icon/reports-export-excel.svg"></span>',
    			                 className: 'button',
    			                 footer: true,
    			                 exportOptions: {
    			                     columns: csvColumnHide
    			                 },
    			                 action:function(e, dt, button, config){
    			                 	_rowGroupingCSV = 1;
    				                _rowGroupingPrint = 0;
    				                summary_Report = 1;
    			                 	$.fn.dataTable.ext.buttons.csvHtml5.action(e, dt, button, config);
    			                 }
    			            
    			            },
    			            {
    				           	 extend: 'pdf',
    				             title:  $scope.report.reportName,
    				             text:'<span class="icon"><img class="logo-img report-icon" src="resources/images/reports-icon/pdf-icon-blue.svg"></span>',
    				             className: 'button',
    				             pageSize: 'LEGAL',
    				             action:function(e, dt, button, config){
    			                 	_rowGroupingCSV = 0;
    				                _rowGroupingPrint = 0;
    				                summary_Report = 0;
    			                 	$.fn.dataTable.ext.buttons.pdfHtml5.action(e, dt, button, config);
    			                 },
    				             customize: function ( doc ) {
    				                var lastColX=null;
    					            var lastColY=null;
    					            var bod = []; 
    					            var subTtl = new Array();
    					            var grandTotal = 0;

   			            		 	if($scope.newReport.unit == "Both"){
	    					            doc.content[1].table.body.forEach(function(line, i){
	    					                if(lastColX != line[0].text && line[0].text != ''){
	    					                    if(i !=1){			                    	
	    					                    	for(var z = 2; z < 8;z++){
	    					                			subTtl[z] = parseFloat(subTtl[z]).toFixed(2);
	    					                		}
	    						                    bod.push([{text:'Total', style:'rowHeader'},{text:subTtl[2], style:'rowHeader'},{text:subTtl[3], style:'rowHeader'},{text:subTtl[4], style:'rowHeader'},{text:subTtl[5], style:'rowHeader'},{text:subTtl[6], style:'rowHeader'},{text:subTtl[7], style:'rowHeader'}]);
	        					                    bod.push([{text:'', style:'rowHeader'},'','','','','','']);
	    						                    subTtl = [];
	    					                    }
	    					                    bod.push([{text:line[0].text, style:'rowHeader'},'','','','','','']);
	    					                    lastColX=line[0].text;
	    					                }
	    					                if( i < doc.content[1].table.body.length && line.length != 1){
	    					                	   if(line[1].text != '1st Qtr' && line[1].text != '2nd Qtr'&& line[1].text != '3rd Qtr'&& line[1].text != '4th Qtr'&& line[1].text != 'Total'){
	    								                bod.push([{text:line[1].text, style:'defaultStyle'},{text:line[2].text, style:'defaultStyle'},{text:line[3].text, style:'defaultStyle'},{text:line[4].text, style:'defaultStyle'},{text:line[5].text, style:'defaultStyle'},{text:line[6].text, style:'defaultStyle'},{text:line[7].text, style:'defaultStyle'}]); 
	    								           }else{
	    								        	   bod.push([{text:line[1].text, style:'rowHeader'},{text:line[2].text, style:'rowHeader'},{text:line[3].text, style:'rowHeader'},{text:line[4].text, style:'rowHeader'},{text:line[5].text, style:'rowHeader'},{text:line[6].text, style:'rowHeader'},{text:line[7].text, style:'rowHeader'}]); 
	    								        	   if(line[1].text == "Total"){
	    	        					                    bod.push([{text:'', style:'rowHeader'},'','','','','','']);
	    								        	   }
	    									       }
	    					                   for(var z = 2; z < 8;z++){
	    					                	   if(line[1].text != '1st Qtr' && line[1].text != '2nd Qtr'&& line[1].text != '3rd Qtr'&& line[1].text != '4th Qtr'&& line[1].text != 'Total'){
	    						                		 if (typeof subTtl[z] =='undefined'){
	    					                        		 subTtl[z] = 0; 
	    					                    		 }
	    						                		if(!isNaN(parseFloat(line[z].text))){subTtl[z] += parseFloat(line[z].text);}
	    						                		if(!isNaN(parseFloat(line[z].text)) ){grandTotal += parseFloat(line[z].text);}
	    					                	   }
	    					                	}
	    					                }
	    					                 
	    					            });
	    					            for(var z = 2; z < 8;z++){
	    		                			if(subTtl[z] == 0){subTtl[z] ='0';}
	    		                			else{subTtl[z] = parseFloat(subTtl[z]).toFixed(2);}
	    		                		}
	    			                    bod.push([{text:'Total', style:'rowHeader'},{text:subTtl[2], style:'rowHeader'},{text:subTtl[3], style:'rowHeader'},{text:subTtl[4], style:'rowHeader'},{text:subTtl[5], style:'rowHeader'},{text:subTtl[6], style:'rowHeader'},{text:subTtl[7], style:'rowHeader'}]);
   			            		 	}else{
	    					            doc.content[1].table.body.forEach(function(line, i){
	    					                if(lastColX != line[0].text && line[0].text != ''){
	    					                    if(i !=1){			                    	
	    					                    	for(var z = 2; z < 5;z++){
    					                				subTtl[z] = parseFloat(subTtl[z]).toFixed(2);
	    					                		}
	    						                    bod.push([{text:'Total', style:'rowHeader'},{text:subTtl[2], style:'rowHeader'},{text:subTtl[3], style:'rowHeader'},{text:subTtl[4], style:'rowHeader'}]);
	        					                    bod.push([{text:'', style:'rowHeader'},'','','']);
	    						                    subTtl = [];
	    					                    }
	    					                    bod.push([{text:line[0].text, style:'rowHeader'},'','','']);
	    					                    lastColX=line[0].text;
	    					                }
	    					                if( i < doc.content[1].table.body.length && line.length != 1){
	    					                	   if(line[1].text != '1st Qtr' && line[1].text != '2nd Qtr'&& line[1].text != '3rd Qtr'&& line[1].text != '4th Qtr'&& line[1].text != 'Total'){
	    								                bod.push([{text:line[1].text, style:'defaultStyle'},{text:line[2].text, style:'defaultStyle'},{text:line[3].text, style:'defaultStyle'},{text:line[4].text, style:'defaultStyle'}]); 
	    								           }else{
	    								        	   bod.push([{text:line[1].text, style:'rowHeader'},{text:line[2].text, style:'rowHeader'},{text:line[3].text, style:'rowHeader'},{text:line[4].text, style:'rowHeader'}]); 
	    								        	   if(line[1].text == "Total"){
	    	        					                    bod.push([{text:'', style:'rowHeader'},'','','']);
	    								        	   }
	    									       }
	    					                   for(var z = 2; z < 5;z++){
	    					                	   if(line[1].text != '1st Qtr' && line[1].text != '2nd Qtr'&& line[1].text != '3rd Qtr'&& line[1].text != '4th Qtr'&& line[1].text != 'Total'){
	    						                		 if (typeof subTtl[z] =='undefined'){
	    					                        		 subTtl[z] = 0; 
	    					                    		 }
	    						                		if(!isNaN(parseFloat(line[z].text))){subTtl[z] += parseFloat(line[z].text);}
	    						                		if(!isNaN(parseFloat(line[z].text)) ){grandTotal += parseFloat(line[z].text);}
	    					                	   }
	    					                	}
	    					                }
	    					                 
	    					            });
	    					            for(var z = 2; z < 5;z++){
	    		                			if(subTtl[z] == 0){subTtl[z] ='0';}
	    		                			else{subTtl[z] = parseFloat(subTtl[z]).toFixed(2);}
	    		                		}
	    			                    bod.push([{text:'Total', style:'rowHeader'},{text:subTtl[2], style:'rowHeader'},{text:subTtl[3], style:'rowHeader'},{text:subTtl[4], style:'rowHeader'}]);
   			            		 		
   			            		 	}
    					            doc.content[1].table.headerRows = 1;
    					            doc.content[1].table.body = bod;
    					            doc.content[1].layout = 'lightHorizontalLines';
    					             
    					            doc.styles = {
    					                rowHeader: {
    					                    bold: true,
    					                    color: 'black',
    					                    whiteSpace:'nowrap',
    					                },
    					                defaultStyle: {
    					                color: 'black'
    					                } 
    					            }
    					            
    				                 doc.content.splice( 1, 0, {
    				                     image: $scope.encodedImage,
    				                     alignment: 'center',
    				                     width : '500'                  
    				                 } );
    			             }
    			           }
    			        ],
    				        "columnDefs": columnSort,
    				        "orderFixed": [0, 'asc'],
    				        "order": [],
    				        "displayLength": 10,
    				        "rowGroup": {
    					        dataSrc: 0
    					    },
    				        "drawCallback": function ( settings ) {
    				            var api = this.api();
    				            var rows = api.rows( {page:'current'} ).nodes();
    				            var last=null;
    				            var colonne = api.row(0).data().length;
    				            var totale = new Array();
    				            totale['Totale']= new Array();
    			    			var subtotale = new Array();
    				            var groupid = -1;
    				            var groupRow= 0;
    		            		var name = null;
    		            		var lastGroup = null
    				            api.column(0, {page:'current'} ).data().each( function ( group, i ) {
    				                if ( last !== group ) {
    				                	if(i !=0){
    				                		if(last!= null){
    				                			name = last;
    				                		}else{
    				                			name = group;
    				                		}
    		                				var totalData = $scope.reportSummaryList.filter(function( obj ) {
    		                					  return obj.name == name;
    		                				})[0];
    		                				if($scope.newReport.unit == "Both"){
    		                					
    		                				}
    				                		$(rows).eq( i ).before(
    				                				$compile(
    				                				'<tr class="total-qtr group report-title light-blue" class="ng-scope odd" role="row">'+
    				                				'<td class="report-content is-hidden-desktop" ng-click="toggleTotalDiv('+ i +')">Total</td>'+
    				                				'<td class="report-content is-hidden-touch"><span>Total</span></td>'+
    				                				'<td class="report-content"><span>'+ totalData.total.currYrMSF+ '</span></td>'+
    				                				'<td class="report-content"><span>'+ totalData.total.currYr$+ '</span></td>'+
    				                				'<td class="report-content"><span>'+ totalData.total.prevYrMSF+ '</span></td>'+
    				                				'<td class="report-content"><span>'+ totalData.total.prevYr$+ '</span></td>'+
    				                				'<td class="report-content"><span>'+ totalData.total.var$+ '</span></td>'+
    				                				'<td class="report-content"><span>'+ totalData.total.varQty+ '</span></td>'+
    				                				'<td class=" is-hidden"><span>'+ group+ '4</span></td>'+
    				                				+'</tr>'+
    				                				'<tr class="total-child-'+ i +' child is-hidden"><td class="child" colspan="2"><ul data-dtr-index="32" class="dtr-details"><li data-dtr-index="3" data-dt-row="32" data-dt-column="3" class="is-hidden-tablet"><span class="dtr-title">Current Year($)</span> <span class="dtr-data">'+ 
    				                				totalData.total.currYr$+ 
    				                				'</span></li><li data-dtr-index="4" data-dt-row="32" data-dt-column="4" class="is-hidden-tablet"><span class="dtr-title">Previous Year(MSF)</span> <span class="dtr-data">'+ 
    				                				totalData.total.prevYrMSF+ '</span></li><li data-dtr-index="5" data-dt-row="32" data-dt-column="5"><span class="dtr-title">Previous Year($)</span> <span class="dtr-data">'+ 
    				                				totalData.total.prevYr$+ '</span></li><li data-dtr-index="6" data-dt-row="32" data-dt-column="6"><span class="dtr-title">Var% (Qty)</span> <span class="dtr-data">'+ 
    				                				totalData.total.varQty+ '</span></li><li data-dtr-index="7" data-dt-row="32" data-dt-column="7"><span class="dtr-title">Var%($)</span> <span class="dtr-data">'
    				                				+ totalData.total.var$+ '</span></li></ul></td></tr>'
    				                				)($scope)
    				                    	);
    				                	}
    				                    last = group;
    				                    lastGroup = group;
    				                	groupRow++;
    				                }
    				            } );

    		                	var pageInfo = rows.page.info();
    		                	if(pageInfo.page == (pageInfo.pages - 1)){
    		        				var totalData = $scope.reportSummaryList.filter(function( obj ) {
    		          					  return obj.name == lastGroup;
    		          				})[0];
    		                		$(rows).eq(rows.length -1 ).after(
    		                				$compile(
    		                				'<tr class="total-qtr report-title group report-footer light-blue" class="ng-scope odd" role="row">'+
    		                				'<td class="report-content is-hidden-desktop" ng-click="toggleTotalDiv('+ i +')">Total</td>'+
    		                				'<td class="report-content is-hidden-touch"><span>Total</span></td>'+
    		                				'<td class="report-content"><span>'+ totalData.total.currYrMSF+ '</span></td>'+
    		                				'<td class="report-content"><span>'+ totalData.total.currYr$+ '</span></td>'+
    		                				'<td class="report-content"><span>'+ totalData.total.prevYrMSF+ '</span></td>'+
    		                				'<td class="report-content"><span>'+ totalData.total.prevYr$+ '</span></td>'+
    		                				'<td class="report-content"><span>'+ totalData.total.var$+ '</span></td>'+
    		                				'<td class="report-content"><span>'+ totalData.total.varQty+ '</span></td>'+
    		                				'<td class=" is-hidden"><span>'+ lastGroup+ '4</span></td>'+
    		                				+'</tr>'+
    		                				'<tr class="total-child-'+ i +' child is-hidden"><td class="child" colspan="2"><ul data-dtr-index="32" class="dtr-details"><li data-dtr-index="3" data-dt-row="32" data-dt-column="3" class="is-hidden-tablet"><span class="dtr-title">Current Year($)</span> <span class="dtr-data">'+ 
    		                				totalData.total.currYr$+ 
    		                				'</span></li><li data-dtr-index="4" data-dt-row="32" data-dt-column="4" class="is-hidden-tablet"><span class="dtr-title">Previous Year(MSF)</span> <span class="dtr-data">'+ 
    		                				totalData.total.prevYrMSF+ '</span></li><li data-dtr-index="5" data-dt-row="32" data-dt-column="5"><span class="dtr-title">Previous Year($)</span> <span class="dtr-data">'+ 
    		                				totalData.total.prevYr$+ '</span></li><li data-dtr-index="6" data-dt-row="32" data-dt-column="6"><span class="dtr-title">Var% (Qty)</span> <span class="dtr-data">'+ 
    		                				totalData.total.varQty+ '</span></li><li data-dtr-index="7" data-dt-row="32" data-dt-column="7"><span class="dtr-title">Var%($)</span> <span class="dtr-data">'
    		                				+ totalData.total.var$+ '</span></li></ul></td></tr>'
    		                				)($scope)
    		                    	);
    		                	}
    				        }
    				    } );
    			    	
    			    	$('#summary-report-table tbody').on( 'click', 'tr.report-title', function () {
    				        var currentOrder = table.order()[0];
    				        if (currentOrder[0] === 0 && currentOrder[1] === 'asc' ) {
    				           table.order( [ 0, 'desc' ]).draw();
    				        }
    				        else {
    				           table.order( [ 0, 'asc' ]).draw(); 
    				        }
    				    } );
    			    	table.buttons( 0, null ).container().appendTo( '.report-export' );
    			    	
    			    	$scope.resetSort = function(col){
	    	            	 var currentOrder = table.order()[0];
	   				        if (currentOrder[0] === col && currentOrder[1] === 'desc' ) {
	   				        	$timeout( function(){ table.order( [ 0, 'asc' ]).draw(); }, 10);
	   				           
	   				        }    			    	                
	   			    	}
    			    	
    			    	
    					}
    					$timeout( function(){ 
    						$scope.groupSortingShip(); 
    						$compile($(".report-footer")) ($scope);
    					}, 1000);
    					$scope.toggleTotalDiv = function(index){
    					   if(angular.element(document.querySelectorAll('.total-child-'+ index)).hasClass('is-hidden')){
    						   angular.element(document.querySelectorAll('.total-child-'+ index)).removeClass('is-hidden')
    					   }else{
    						   angular.element(document.querySelectorAll('.total-child-'+ index)).addClass('is-hidden')				   
    					   }
    				   }
    		  		}
    		  	}
    	//CHart js
        $scope.initializeChart = function(){
        	if($scope.newReport != undefined){
    	    	if($scope.newReport.chartType == 'Bar Chart' || $scope.newReport.chartType == 'Line Chart'){
    	    		$scope.labels = [];
    	    		$scope.data = [];
    	    		$scope.series = [];
    		    	if($scope.newReport.type == 'Summary Report'){
    		    		var currYear = [];
    		    		var prevYear = [];
    		    		var varQty = [];
    		    		$scope.labels.push('Jan','Feb','Mar', 'Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec')
    		    		for( i in $scope.reportSummaryList){	
    		    			currYear = []
    		    			for(c in $scope.reportSummaryList[i].monthSummary){
    		    				if($scope.reportSummaryList[i].monthSummary[c].month != '1st Qtr' && $scope.reportSummaryList[i].monthSummary[c].month != '2nd Qtr' &&
    		    						$scope.reportSummaryList[i].monthSummary[c].month != '3rd Qtr' && $scope.reportSummaryList[i].monthSummary[c].month != '4th Qtr'
    		    							&& $scope.reportSummaryList[i].monthSummary[c].month != 'Total'){
    		    					if($scope.reportSummaryList[i].monthSummary[c].varQty != undefined){
        		    					varQty.push($scope.reportSummaryList[i].monthSummary[c].varQty);    		    						
    		    					}else{
        		    					varQty.push($scope.reportSummaryList[i].monthSummary[c].var$);      		    						
    		    					}
//    				    			$scope.labels.push($scope.reportSummaryList[i].monthSummary[c].month)
    		    				}
    		    			}
    		    			$scope.series.push($scope.reportSummaryList[i].name)
    			    		$scope.data.push(varQty);
    		    		}
    	
    		    	}else if($scope.newReport.type == 'Product Summary Report'){
    		    			var label = []
    			    		var keyNames = Object.keys($scope.reportSummary.total);
    		    			for(i in keyNames){
    		    				$scope.labels.push(keyNames[i].replace(/[_-]/g, " ").split(" "));
    		    				$scope.data.push($scope.reportSummary.total[keyNames[i]][0].total$)
    		    			} 
    		    	}else if($scope.newReport.type == 'Ship-To Summary Report'){
    		    		var listData = [];
    		    		for(i in $scope.shipToData){
    	    				$scope.labels.push($scope.shipToData[i].total.name)		    
    	    				if(isNaN($scope.shipToData[i].total.totalList[0])){
    	    					$scope.data.push($scope.shipToData[i].total.totalList[0].replace(/\D/g,''));
    	    				}else{
    	    					$scope.data.push($scope.shipToData[i].total.totalList[0]);    	    					
    	    				}
    		    		}
    		    	}    	   
    	    	    $scope.onClick = function (points, evt) {
    	    	      console.log(points, evt);
    	    	    };
    	    	    
    	    		
    	    	}else if($scope.newReport.chartType == 'Pie Chart'){
    	    		$scope.labels = [];
    	    		$scope.data = [];
    	    		$scope.series = [];
    	    		if($scope.newReport.type == 'Summary Report'){
    		    		var currYear = [];
    		    		var prevYear = [];
    		    		var varQty = [];
    		    		for( i in $scope.reportSummaryList){	
    		    			$scope.labels.push($scope.reportSummaryList[i].name)
    		    			if($scope.reportSummaryList[i].monthSummary[16].varQty != undefined){
		    					varQty.push($scope.reportSummaryList[i].monthSummary[16].varQty);    		    						
	    					}else{
		    					varQty.push($scope.reportSummaryList[i].monthSummary[16].varQty);      		    						
	    					}
//    		    			varQty.push($scope.reportSummaryList[i].total.currYr$);
    		    		}
    		    		$scope.data.push(varQty);
    		    	}else if($scope.newReport.type == 'Product Summary Report'){
    		    		var label = []
			    		var keyNames = Object.keys($scope.reportSummary.total);
		    			for(i in keyNames){
		    				$scope.labels.push(keyNames[i].replace(/[_-]/g, " "));
		    				$scope.data.push($scope.reportSummary.total[keyNames[i]][0].total$)
		    			}     		 
    		    	}else if($scope.newReport.type == 'Ship-To Summary Report'){
    		    		var listData = [];
    		    		for(i in $scope.shipToData){
    	    				$scope.labels.push($scope.shipToData[i].total.name)	
    	    				if(isNaN($scope.shipToData[i].total.totalList[0])){
    	    					$scope.data.push($scope.shipToData[i].total.totalList[0].replace(/\D/g,''));
    	    				}else{
    	    					$scope.data.push($scope.shipToData[i].total.totalList[0]);    	    					
    	    				}
    		    		}
    		    	}
    	    	}
        	}
        	
        	$scope.datasetOverride = [{ yAxisID: 'y-axis-1' }, { yAxisID: 'y-axis-2' }];
    	    $scope.options = {
    	      scales: {
    	        yAxes: [
    	          {
    	            id: 'y-axis-1',
    	            type: 'linear',
    	            display: true,
    	            position: 'left'
    	          },
    	          {
    	            id: 'y-axis-2',
    	            type: 'linear',
    	            display: true,
    	            position: 'right'
    	          }
    	        ],
    	        xAxes:[]
    	      },
    	      bezierCurve: false, 
    	      animation: {
        	    onComplete: function(animation){
        	        $scope.encodedImage = this.toBase64Image();//this gives the base64 image
        	    }
    	      }
    	    };
        }
    	
    	
    	
    	if($scope.newReport != undefined){
	    	if($scope.newReport.type == 'Summary Report'){
	    		var productSummaryType = null;
				if($scope.newReport.unit != undefined){
					if($scope.newReport.unit == 'Both'){
						productSummaryType = 'MainSummary';					
					}else if($scope.newReport.unit == 'MSF'){
						productSummaryType = 'MainSummaryVolume';						
					}else if($scope.newReport.unit == 'Dollar'){
						productSummaryType = 'MainSummaryDollar';						
					}
				}				
					reportService.getReportsData(productSummaryType).then(function(result){
//					var result = reportService.getHardcode();
		    			var reportSummary = result.result.data;  
						if(result.result.data != undefined){
							$scope.reportSummaryList = [];
				    		angular.forEach(reportSummary.datas, function(data){
					    		var summary = {};
				    			summary.name = data.sector.replace(/[_-]/g, " ");
				    			summary.monthSummary = [];
			    				var quarter = 1;
				    			for(i in data.Month){
				    				if(data.Month[i] != 'Total'){
					    				if(i == 4){
					    					quarter = 2;
					    				}else if(i == 8){
					    					quarter = 3;	    					
					    				}else if(i == 12){
					    					quarter = 4;	    					
					    				}
					    				if(data['Current_Year'] != undefined && data['Previous_Year'] != undefined &&
					    						data['Current_Year($)'] != undefined && data['Previous_Year($)'] != undefined){
					    					summary.monthSummary.push(
						    						{
						    							'month' : data.Month[i],
						    							'currYrMSF' : data['Current_Year'][i],
						    							'currYr$' : data['Current_Year($)'][i],
						    							'prevYrMSF' : data['Previous_Year'][i],
						    							'var$' : data['Var($)'][i],
						    							'prevYr$' : data['Previous_Year($)'][i],
						    							'varQty' : data['Var'][i],
						    							'quarter' : quarter
						    						}
						    				) 
					    				}else if(data['Current_Year($)'] != undefined && data['Previous_Year($)'] != undefined &&
					    						 data['Current_Year'] == undefined && data['Previous_Year'] == undefined){
					    					summary.monthSummary.push(
						    						{
						    							'month' : data.Month[i],
						    							'currYr$' : data['Current_Year($)'][i],
						    							'prevYr$' : data['Previous_Year($)'][i],
						    							'var$' : data['Var($)'][i],
						    							'quarter' : quarter
						    						}
						    				)
					    				}else if(data['Current_Year'] != undefined && data['Previous_Year'] != undefined &&
					    						 data['Current_Year($)'] == undefined && data['Previous_Year($)'] == undefined){
					    					summary.monthSummary.push(
						    						{
						    							'month' : data.Month[i],
						    							'currYrMSF' : data['Current_Year'][i],
						    							'prevYrMSF' : data['Previous_Year'][i],
						    							'varQty' : data['Var'][i],
						    							'quarter' : quarter
						    						}
						    				)
					    				}
				    					   
				    				}else{
				    					if(data['Current_Year'] != undefined && data['Previous_Year'] != undefined&&
					    						data['Current_Year($)'] != undefined && data['Previous_Year($)'] != undefined){
					    					summary.monthSummary.push(
						    						{
						    							'month' : data.Month[i],
						    							'currYrMSF' : data['Current_Year'][i],
						    							'currYr$' : data['Current_Year($)'][i],
						    							'prevYrMSF' : data['Previous_Year'][i],
						    							'var$' : data['Var($)'][i],
						    							'prevYr$' : data['Previous_Year($)'][i],
						    							'varQty' : data['Var'][i],
						    							'quarter' : quarter
						    						}
						    				) 
					    				}else if(data['Current_Year($)'] != undefined && data['Previous_Year($)'] != undefined &&
					    						 data['Current_Year'] == undefined && data['Previous_Year'] == undefined){
					    					summary.monthSummary.push(
						    						{
						    							'month' : data.Month[i],
						    							'currYr$' : data['Current_Year($)'][i],
						    							'prevYr$' : data['Previous_Year($)'][i],
						    							'var$' : data['Var($)'][i],
						    							'quarter' : quarter
						    						}
						    				)
					    				}else if(data['Current_Year'] != undefined && data['Previous_Year'] != undefined &&
					    						 data['Current_Year($)'] == undefined && data['Previous_Year($)'] == undefined){
					    					summary.monthSummary.push(
						    						{
						    							'month' : data.Month[i],
						    							'currYrMSF' : data['Current_Year'][i],
						    							'prevYrMSF' : data['Previous_Year'][i],
						    							'varQty' : data['Var'][i],
						    							'quarter' : quarter
						    						}
						    				)
					    				}
				    				}
				    			}
				    			$scope.reportSummaryList.push(summary)
				    		}); 
				    	    $scope.initializeChart();
				    	    $scope.intitializeTableFormat();
						}else if(result.result.validLicense == "FALSE"){
			    			$scope.errorMsg = result.result.message;
			    			$scope.errorModalTrigger = "is-active";							
						}
					})
	    	}else if($scope.newReport.type == 'Product Summary Report'){
	    		var productSummaryType = null;
				if($scope.newReport.unit != undefined){
					if($scope.newReport.unit == 'Both'){
						productSummaryType = 'ProductSummaryBoth';					
					}else if($scope.newReport.unit == 'MSF'){
						productSummaryType = 'ProductSummaryVolume';						
					}else if($scope.newReport.unit == 'Dollar'){
						productSummaryType = 'ProductSummaryDollar';						
					}
				}else{
					productSummaryType = "ProductSummaryBoth";
				}
	    		reportService.getReportsData(productSummaryType).then(function(result){
		    		$scope.reportSummary = result.result.data;
					if(result.result.data != undefined){  
			    		var keyNames = Object.keys(result.result.data.datas);
			    		var newObject = Object.keys(result.result.data.datas).map(function(i){return result.result.data.datas[i]});
			    		var summaryData = [];
			    		for(i in keyNames){
			    			var obj = {};
			    			obj[keyNames[i]] = newObject[i]
				    		summaryData.push(obj)
			    		}
			    		console.log(summaryData);
			    		$scope.reportSummary.datas = summaryData;
			    		$scope.ProductData = [];
			    		angular.forEach($scope.reportSummary.datas, function(data, key){
			    			var reportData = {}
			    			reportData.summary = []
		    				for ( property in data ) {
		      				  data.name = property; 
		      				  reportData.name = property.replace(/[_-]/g, " "); 
		      				}
							for(i in data[data.name]){
		  	    				var isValid = true;
		  	    				var totalName = '';
		  	    				for ( property in data[data.name][i] ) {
		  	    					if(property.indexOf('Total for Product') > -1){
		  	    						isValid = false
		  	    						totalName = property;
		  	    					} 
		  	      				}
		  	    				if(isValid){
		  	    					data[data.name][i].group = reportData.name
		  	    					reportData.summary.push(data[data.name][i])	    					
		  	    				}else if(totalName.indexOf('Total for Product')> -1){      	    					
		  	    					reportData.total = {
		  	    							'name' : totalName,
		  	    							'totalList' : data[data.name][i][totalName]
		  	    					}
		  	    				}
		  	    			}
							$scope.ProductData.push(reportData);					
			    		});
			    	    $scope.initializeChart();
			    	    $scope.intitializeTableFormat();
					}else if(result.result.validLicense == "FALSE"){
		    			$scope.errorMsg = result.result.message;
		    			$scope.errorModalTrigger = "is-active";							
					}
	    		},
	    		function error(result){
	    			console.log(result);
	    			$scope.errorMsg = "Oops! An error occured";
	    			$scope.errorModalTrigger = "is-active";
	    		});
	    	}else if($scope.newReport.type == 'Ship-To Summary Report'){
	    		var summaryType = null;
				if($scope.newReport.unit != undefined){
					if($scope.newReport.unit == 'Both'){
						summaryType = 'ShipToSummaryBoth';					
					}else if($scope.newReport.unit == 'MSF'){
						summaryType = 'ShipToSummaryVolume';						
					}else if($scope.newReport.unit == 'Dollar'){
						summaryType = 'ShipToSummaryDollar';						
					}
				}else{
					summaryType = "ShipToSummaryBoth";
				}
				
				if($scope.newReport.unit == 'Boths'){
		    		reportService.getReportsData(summaryType).then(function(result){
		    			$scope.reportSummary = result.result.data;
						if(result.result.data != undefined){
			    			$scope.shipToData = [];
				    		angular.forEach($scope.reportSummary.datas, function(data, key){
				    			var reportData = {};
				    			reportData.summary = []
			    				for ( property in data ) {
			      				  data.name = property; 
			      				  reportData.name = property.replace(/[_-]/g, " "); 
			      				}
			    				if(data.name == "Gypsum_GlasRoc ($)"){
			    					reportData.type = "$";    					
			    				}else{
			    					reportData.type = "MSF";    			    					
			    				}
								for(i in data[data.name]){
			  	    				var isValid = true;
			  	    				var totalName = '';
			  	    				for ( property in data[data.name][i] ) {
			  	    					if(property.indexOf('Total for State')> -1){
			  	    						isValid = false
			  	    						totalName = property;
			  	    					} 
			  	      				}
			  	    				if(isValid){
			  	    					data[data.name][i].group = reportData.name
			  	    					reportData.summary.push(data[data.name][i])	    					
			  	    				}else if(totalName.indexOf('Total for State')> -1){      	    					
			  	    					reportData.total = {
			  	    							'name' : totalName,
			  	    							'totalList' : data[data.name][i][totalName]
			  	    					}
			  	    				}
			  	    			}
								$scope.shipToData.push(reportData);					
				    		});
				    	    $scope.initializeChart();
				    	    $scope.intitializeTableFormat();
						}else if(result.result.validLicense == "FALSE"){
			    			$scope.errorMsg = result.result.message;
			    			$scope.errorModalTrigger = "is-active";							
						}
		    		},
		    		function error(result){
		    			console.log(result);
		    			$scope.errorMsg = "Oops! An error occured";
		    			$scope.errorModalTrigger = "is-active";
		    		});
				}else{
					reportService.getReportsData(summaryType).then(function(result){
						$scope.reportSummary = result.result.data;
						if(result.result.data != undefined){
			    			$scope.shipToData = [];
			    			
			    			$scope.sectors = [];
			    			
			    			angular.forEach($scope.reportSummary.datas, function(data, key){
			    				
			    				var isSectorFound = $scope.sectors.some(function(obj) {
			    				    return obj.name === data.sector;
			    				}); 
			    				
			    				if(!isSectorFound){
			    					var sector = {
			    						name: data.sector,
			    						total: 0,
			    						janTotal: 0,
			    						febTotal: 0,
			    						marTotal: 0,
			    						aprTotal: 0,
			    						mayTotal: 0,
			    						junTotal: 0,
			    						julTotal: 0,
			    						augTotal: 0,
			    						sepTotal: 0,
			    						octTotal: 0,
			    						novTotal: 0,
			    						decTotal: 0,
			    					}
			    					$scope.sectors.push(sector);
			    				}
			    				
				    			var reportData = {};
				    			reportData.unit = $scope.newReport.unit;
				    			reportData.summary = [];
			      				reportData.name =  " "; 
				    			if($scope.newReport.unit == "Both" || $scope.newReport.unit == "MSF"){
				    				reportData.type = "MSF";  
				    			}else{
				    				reportData.type = "$"; 
				    			}

			    				if(data.sector.length != 0){
									reportData.name = data.sector.replace(/[_-]/g, " ");
								}else{
									reportData.name = '';	    							
								}
								
			    				var bothData=angular.copy(reportData);
			    				bothData.type = "$"; 
			    				for(i in data.element){
			    					if(data.element[i].Ship_To != undefined){
			    						data.element[i].group = data.element[i].Ship_To;
			    						
			    	    				if(data.element[i].City.length == 0){
			    	    					data.element[i].City = '-';	    							
			    						}
		
			    	    				if(data.element[i].Ship_To_Name.length == 0){
			    	    					data.element[i].Ship_To_Name = '-';
			    						}
			    	    				//store original data element
			    						var dataTemp = angular.copy(data);
			    						if(data.element[i].TotalamountShipped != undefined){
				    						data.element[i]['Total ($) Shipped'] = data.element[i].TotalamountShipped;			    							
			    						}else{
				    						data.element[i]['Total (MSF) Shipped'] = data.element[i].TotalShippedQty;			    							
			    						}
			    						
			    	    				if(data.element[i].Jan != undefined){
			    	    					data.element[i]['Jan(MSF)'] = data.element[i].Jan
			    	    				} 
			    	    				if(data.element[i].Feb != undefined){
			    	    					data.element[i]['Feb(MSF)'] = data.element[i].Feb
			    	    				}
			    	    				if(data.element[i].Mar != undefined){
			    	    					data.element[i]['Mar(MSF)'] = data.element[i].Mar
			    	    				}
			    	    				if(data.element[i].Apr != undefined){
			    	    					data.element[i]['Apr(MSF)'] = data.element[i].Apr
			    	    				}
			    	    				if(data.element[i].May != undefined){
			    	    					data.element[i]['May(MSF)'] = data.element[i].May
			    	    				}
			    	    				if(data.element[i].Jun != undefined){
			    	    					data.element[i]['Jun(MSF)'] = data.element[i].Jun
			    	    				}
			    	    				if(data.element[i].Jul != undefined){
			    	    					data.element[i]['Jul(MSF)'] = data.element[i].Jul
			    	    				}
			    	    				if(data.element[i].Aug != undefined){
			    	    					data.element[i]['Aug(MSF)'] = data.element[i].Aug
			    	    				}
			    	    				if(data.element[i].Sep != undefined){
			    	    					data.element[i]['Sep(MSF)'] = data.element[i].Sep
			    	    				}
			    	    				if(data.element[i].Oct != undefined){
			    	    					data.element[i]['Oct(MSF)'] = data.element[i].Oct
			    	    				}
			    	    				if(data.element[i].Nov != undefined){
			    	    					data.element[i]['Nov(MSF)'] = data.element[i].Nov
			    	    				}
			    	    				if(data.element[i].Dec != undefined){
			    	    					data.element[i]['Dec(MSF)'] = data.element[i].Dec
			    	    				}
			    	    				
			    	    				if(data.element[i].Ship_To_Name.length == 0){
			    	    					data.element[i].Ship_To_Name = '-';
			    						}
					    				reportData.summary.push(data.element[i]);
			    	    				
			    	    				if(dataTemp.element[i].Jan_Amount != undefined){
			    	    					dataTemp.element[i]['Jan($)'] = dataTemp.element[i].Jan_Amount
			    	    				} 
			    	    				if(dataTemp.element[i].Feb_Amount != undefined){
			    	    					dataTemp.element[i]['Feb($)'] = dataTemp.element[i].Feb_Amount
			    	    				}
			    	    				if(dataTemp.element[i].Mar_Amount != undefined){
			    	    					dataTemp.element[i]['Mar(MSF)'] = dataTemp.element[i].Mar_Amount
			    	    				}
			    	    				if(dataTemp.element[i].Apr_Amount != undefined){
			    	    					dataTemp.element[i]['Apr($)'] = dataTemp.element[i].Apr_Amount
			    	    				}
			    	    				if(dataTemp.element[i].May_Amount != undefined){
			    	    					dataTemp.element[i]['May($)'] = dataTemp.element[i].May_Amount
			    	    				}
			    	    				if(dataTemp.element[i].Jun_Amount != undefined){
			    	    					dataTemp.element[i]['Jun($)'] = dataTemp.element[i].Jun_Amount
			    	    				}
			    	    				if(dataTemp.element[i].Jul_Amount != undefined){
			    	    					dataTemp.element[i]['Jul($)'] = dataTemp.element[i].Jul_Amount
			    	    				}
			    	    				if(dataTemp.element[i].Aug_Amount != undefined){
			    	    					dataTemp.element[i]['Aug($)'] = dataTemp.element[i].Aug_Amount
			    	    				}
			    	    				if(dataTemp.element[i].Sep_Amount != undefined){
			    	    					dataTemp.element[i]['Sep($)'] = dataTemp.element[i].Sep_Amount
			    	    				}
			    	    				if(dataTemp.element[i].Oct_Amount != undefined){
			    	    					dataTemp.element[i]['Oct($)'] = dataTemp.element[i].Oct_Amount
			    	    				}
			    	    				if(dataTemp.element[i].Nov_Amount != undefined){
			    	    					dataTemp.element[i]['Nov($)'] = dataTemp.element[i].Nov_Amount
			    	    				}
			    	    				if(dataTemp.element[i].Dec_Amount != undefined){
			    	    					dataTemp.element[i]['Dec($)'] = dataTemp.element[i].Dec_Amount
			    	    					dataTemp.element[i]['Total ($) Shipped'] = dataTemp.element[i].TotalShippedAmount;
			    	    					bothData.summary.push(dataTemp.element[i]);
			    	    				}   	    				
			    	    				
			    					}else if(data.element[i].Totals != undefined){
			    						reportData.total = {
				    							'name' : data.element[i].Totals,
				    							'totalList' : data.element[i].T
				    					}
			    						if(data.element[i].T.length > 13){
			    							bothData.total = {
					    							'name' : data.element[i].Totals,
					    							'totalList' : []
					    					}
			    							var dollarTotal = data.element[i].T;
			    							for(i in dollarTotal){
			    								if(i >= 13){
			    									bothData.total.totalList.push(dollarTotal[i]);			    									
			    								}
			    							}
			    						}
			    					}
			    				}
								$scope.shipToData.push(reportData);		
								if(bothData.summary.length != 0){
									$scope.shipToData.push(bothData);										
								}
				    		});
				    	    $scope.initializeChart();
				    	    $scope.intitializeTableFormat();
						}else if(result.result.validLicense == "FALSE"){
			    			$scope.errorMsg = result.result.message;
			    			$scope.errorModalTrigger = "is-active";							
						}
						
						//compute total per state
						if($scope.newReport.unit == 'MSF'){
							angular.forEach($scope.sectors, function(sector){								
								angular.forEach($scope.reportSummary.datas, function(data){
									if(data.sector == sector.name){
										var totalObj = data.element[data.element.length - 1].T;
										var total = splitStr(totalObj[0], ' MSF');
										sector.total+= parseFloat(total);
										
										var janTotal = splitStr(totalObj[1], ' MSF');
										sector.janTotal+= parseFloat(janTotal);
										
										var febTotal = splitStr(totalObj[2], ' MSF');
										sector.febTotal+= parseFloat(febTotal);
										
										var marTotal = splitStr(totalObj[3], ' MSF');
										sector.marTotal+= parseFloat(marTotal);
										
										var aprTotal = splitStr(totalObj[4], ' MSF');
										sector.aprTotal+= parseFloat(aprTotal);
										
										var mayTotal = splitStr(totalObj[5], ' MSF');
										sector.mayTotal+= parseFloat(mayTotal);
										
										var junTotal = splitStr(totalObj[6], ' MSF');
										sector.junTotal+= parseFloat(junTotal);
										
										var julTotal = splitStr(totalObj[7], ' MSF');
										sector.julTotal+= parseFloat(julTotal);
										
										var augTotal = splitStr(totalObj[8], ' MSF');
										sector.augTotal+= parseFloat(augTotal);
										
										var sepTotal = splitStr(totalObj[9], ' MSF');
										sector.sepTotal+= parseFloat(sepTotal);
										
										var octTotal = splitStr(totalObj[10], ' MSF');
										sector.octTotal+= parseFloat(octTotal);
										
										var novTotal = splitStr(totalObj[11], ' MSF');
										sector.novTotal+= parseFloat(novTotal);
										
										var decTotal = splitStr(totalObj[12], ' MSF');
										sector.decTotal+= parseFloat(decTotal);
									}
								});
							});
						}
						
						console.log($scope.sectors)
					},
		    		function error(result){
		    			console.log(result);
		    			$scope.errorMsg = "Oops! An error occured";
		    			$scope.errorModalTrigger = "is-active";
		    		});
				}
	    	}
    	}
    }
	
	function splitStr(str, pattern){
		return str.split(pattern);
	}
	
    $scope.retrieveReportDetail();    
    $scope.reportToggle = true;
    $scope.toggleRename = function(){
    	$scope.reportToggle = !$scope.reportToggle;    	
    }

	$scope.counter = 0;
	$scope.isCopied = false;
	$scope.isSortClicked = false;
	
   $scope.sortByGroup = function(){
	   $scope.isSortClicked = true;
   }    
    $scope.exportReportExcel = function(){
    	angular.element(document.querySelectorAll(".buttons-csv")).trigger('click');
	}
    $scope.printReport = function(){
    	angular.element(document.querySelectorAll(".buttons-print")).trigger('click');
	}
    
    $scope.exportReportPdf = function(){
    	angular.element(document.querySelectorAll(".buttons-pdf")).trigger('click');
	}
    
    $scope.saveReport = function(){
    	var reports = $cookieStore.get("reports");
    	var savedReport ={
    			'reportName': $scope.report.reportName, 
    			'type': $scope.newReport.type,
    			'chartType': $scope.newReport.chartType,
    			'dateIssued': new Date(),
    			'user' : $cookieStore.get('user').userName,
    			'unit': $scope.newReport.unit
    	}
    	if(reports == undefined){
    		reports = [];
    	}
    	if($scope.reportId == undefined){
        	reports.push(savedReport);    		
    	}else{
    		reports[$scope.reportId] = savedReport;
    	}    	
    	var userInfo = {
				"username" : $cookieStore.get('user').userName,
				"timestamp" : new Date(),
				"reportType" : savedReport.type,
				"action" : "SAVE",
				"company": config.company,
				"__class__": "Logger"
				  
		}
		
		reportService.logUserData(userInfo).then(function(result){
			console.log(result)
		},
		function error(result){
			console.log(result);
			$scope.errorMsg = "Oops! An error occured";
			$scope.errorModalTrigger = "is-active";
		});
    	$cookieStore.put("reports", reports);
    	$state.go('base.reports');
    }
    
    $scope.errorModalTrigger = "";
	$scope.errorRedirect = function(){
		$state.go('base.reports');
		$scope.errorModalTrigger = "";
	}
	
	$scope.createReport = function(){
		$cookieStore.put("editReport", true);
		$scope.report.index = $scope.reportId;
		$cookieStore.put("editReportObj", $scope.report);
		$cookieStore.put("reportOption", {reportType:$scope.report.type});
		$state.go('base.createReport');
	}
   
   
   
}]);