var app = angular.module('loginController', ['loginService',
											 'userService',
											 'categoryService']);

app.controller("loginController", ['$scope', 'utilityFactory','$cookieStore','$state','config','loginService','userService','$rootScope','$window','categoryService',
	function($scope, utilityFactory, $cookieStore, $state, config, loginService, userService, $rootScope, $window, categoryService){
	$scope.login = function(email, password){
		loginService.login(email, password).then(
			function success(result){
				utilityFactory.saveToken(result.data.access_token);
				utilityFactory.saveJwt(result.data.token);
				
				var user = {
					userName: email
				};
				
				// userService.getUserDetails(user.userName).then(
				// 	function success(result){
						user.firstName = result.data.firstName;
						user.lastName = result.data.lastName;
						user.name = result.data.name;
						user.uid = result.data.uid
						utilityFactory.saveUser(user);
						$rootScope.user = JSON.parse($window.localStorage.getItem('user'));
						$window.localStorage.removeItem('freeTextSearch');
						
						//Get Invoices
						//Modify
						$rootScope.invoices = [
							{'selected': false, 'invoiceNumber':123456789, 'poNumber':756425387, 'dateIssued': '01/03/2017', 'total': 10, 'status': 'OPEN', 'pdfFile': 'PDF'},
							{'selected': false, 'invoiceNumber':762435418, 'poNumber':987102838, 'dateIssued': '03/02/2017', 'total': 100, 'status': 'OPEN', 'pdfFile': 'PDF'},
							{'selected': true, 'invoiceNumber':983745934, 'poNumber':675823723, 'dateIssued': '12/01/2017', 'total': 320, 'status': 'PAID', 'pdfFile': 'PDF'},
							{'selected': false, 'invoiceNumber':236748734, 'poNumber':523687482, 'dateIssued': '07/05/2017', 'total': 240, 'status': 'PAID', 'pdfFile': 'PDF'},
							{'selected': true, 'invoiceNumber':287982873, 'poNumber':123657282, 'dateIssued': '06/16/2017', 'total': 90, 'status': 'OPEN', 'pdfFile': 'PDF'},
							{'selected': false, 'invoiceNumber':516768297, 'poNumber':634534232, 'dateIssued': '04/20/2017', 'total': 1000, 'status': 'PAST DUE', 'pdfFile': 'PDF'},
							{'selected': false, 'invoiceNumber':438267876, 'poNumber':124674242, 'dateIssued': '08/25/2017', 'total': 1500, 'status': 'PAST DUE', 'pdfFile': 'PDF'},
						];
						$window.localStorage.setItem("invoiceList", JSON.stringify($rootScope.invoices));
						
						//Get Recommended Products
						//Modify
						//Recommended Products
						$rootScope.recommendedProducts = [
							{
								'productCode': 3793687,
								'name':'Pit Boss 6" Steel Toe 1',
								'image': config.imagePath + "/shoes/" +  'timberland_direct_attach_brown_boot_angle.png',
								'price': 75
							},
							{
								'productCode': 3793687,
								'name':'PowerWelt Waterproof 6" Steel Tow 2',
								'image': config.imagePath + "/shoes/" +  'timberland_direct_attach_brown_boot_back.png',
								'price': 75
							},
							{
								'name':'Mudsill Low Steel Toe 3',
								'image': config.imagePath + "/shoes/" +  'timberland_direct_attach_brown_boot_side.png',
								'price': 75
							},
							{
								'productCode': 3793687,
								'name':'Pit Boss 6" Steel Toe 4',
								'image': config.imagePath + "/shoes/" +  'timberland_direct_attach_brown_boot_angle.png',
								'price': 75
							},
							{
								'name':'PowerWelt Waterproof 6" Steel Tow 5',
								'image': config.imagePath + "/shoes/" +  'timberland_direct_attach_brown_boot_back.png',
								'price': 75
							},
							{
								'productCode': 3793687,
								'name':'Mudsill Low Steel Toe 6',
								'image': config.imagePath + "/shoes/" +  'timberland_direct_attach_brown_boot_side.png',
								'price': 75
							},
							{
								'productCode': 3793687,
								'name':'Pit Boss 6" Steel Toe 7',
								'image': config.imagePath + "/shoes/" +  'timberland_direct_attach_brown_boot_angle.png',
								'price': 75
							},
							{
								'productCode': 3793687,
								'name':'PowerWelt Waterproof 6" Steel Tow 8',
								'image': config.imagePath + "/shoes/" +  'timberland_direct_attach_brown_boot_back.png',
								'price': 75
							},
							{
								'productCode': 3793687,
								'name':'PowerWelt Waterproof 6" Steel Tow 9',
								'image': config.imagePath + "/shoes/" +  'timberland_direct_attach_brown_boot_back.png',
								'price': 75
							},
							{
								'productCode': 3793687,
								'name':'PowerWelt Waterproof 6" Steel Tow 10',
								'image': config.imagePath + "/shoes/" +  'timberland_direct_attach_brown_boot_back.png',
								'price': 75
							},
							{
								'productCode': 3793687,
								'name':'PowerWelt Waterproof 6" Steel Tow 11',
								'image': config.imagePath + "/shoes/" +  'timberland_direct_attach_brown_boot_back.png',
								'price': 75
							}
						];
						$window.localStorage.setItem("recommendedProducts", JSON.stringify($rootScope.recommendedProducts));
						
						//Get Orders
						//Modify
						var perPage = 10;
						userService.getOrders(perPage, 0).then(
							function success(result){
								console.log(result);
								
								$rootScope.order = result.data;
								if($rootScope.order.length == 0 || $rootScope.order.length == undefined){
									$rootScope.order.orders = [];
								}
								$window.localStorage.setItem("order", JSON.stringify($rootScope.order));
								
								//Get Hand Tools Category
								categoryService.getAllHandToolsCategory().then(
									function success(result){
										console.log(result);
										
										$rootScope.handToolsCategory = result.data;
										if($rootScope.handToolsCategory.length == 0){
											$rootScope.handToolsCategory = [];
										};
										$window.localStorage.setItem("handToolsCategory", JSON.stringify($rootScope.handToolsCategory));
										
										//Get Footwear Category
										categoryService.getAllFootWearCategory().then(
											function success(result){
												console.log(result);
												
												$rootScope.footWearCategory = result.data;
												if($rootScope.footWearCategory.length == 0){
													$rootScope.footWearCategory = [];
												};
												$window.localStorage.setItem("footWearCategory", JSON.stringify($rootScope.footWearCategory));
												
												//Get Measuring and Layout Tools
												//Modify
												
												//Get Carts
												userService.getCarts().then(
													function success(cartResult){
														if(cartResult.data.hasOwnProperty("carts")){
															console.log("User has a cart");
															console.log(cartResult.data.carts[0]);	//remove
															utilityFactory.getAllShoppingCartItem().then(function(result){
																$rootScope.shoppingCart = result;
															});
															$window.localStorage.setItem('cart', JSON.stringify({ code: cartResult.data.carts[0].code }));
															$state.go('base.home');
														} else {
															userService.createCart().then(function(cartResult) {
																console.log("creating cart");
																utilityFactory.getAllShoppingCartItem().then(function(result){
																	$rootScope.shoppingCart = result;
																});
																$window.localStorage.setItem('cart', JSON.stringify({ code: cartResult.data.code}));
																$state.go('base.home');
															});
														}
													},
													function error(result){
														console.log(result);
												});
											},
											function error(result){
												console.log(result);
										});
									},
									function error(result){
										console.log(result);
										//remove line below if the setup is back to powertools
										$state.go('base.home');
								});
							},
							function error(result){
								console.log(result);
						});
				// 	},
				// 	function error(result){
				// 		console.log(result);
				// });
			},
			function error(result){
				console.log(result);
				console.log("Error!");
				if(result.data.error == "invalid_grant" && result.data.error_description == "Bad credentials"){
					$scope.invalidCredentials = true;
				}
		});
	};
	
	$scope.invalidCredentials = false;
	
	$scope.clearError = function(){
		$scope.invalidCredentials = false;
	}
	
	$scope.headlines = [
		{
		'image': config.imagePath + 'jigsaw_hdr.jpg',
		'message': 'This is an example of a callout message over a photo. It can be used to say something about a product',
		'title': 'THIS IS AN EXAMPLE OF A HEADLINE',
		'contents': [
			{
				'content': 'This is an example of body text describing something here on the site. It can contain a link or have a button after the paragraph.'
			}]
		},
		{
		'image': config.imagePath + 'jigsaw_hdr.jpg',
		'message': '',
		'title': 'THIS IS AN EXAMPLE OF A HEADLINE',
		'contents': [
			{
				'content': 'This is an example of body text describing something here on the site. It can contain a link or have a button after the paragraph.'
			},
			{
				'content': 'This is an example of body text describing something here on the site. It can contain a link or have a button after the paragraph.'
			}]
		}
	];
}]);