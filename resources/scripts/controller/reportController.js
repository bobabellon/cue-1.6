var app = angular.module('reportController', [ 'datatables', 'datatables.colreorder', 'pdf', 'reportService']);

app.controller("reportController", ['$scope', '$rootScope', '$cookieStore','config', 'reportService','$window', 
                                    'DTOptionsBuilder', 'DTColumnDefBuilder', '$compile', '$timeout', 'pdfDelegate', 
                                    'utilityFactory', '$state', '$stateParams', 'toaster', '$q',
	function($scope, $rootScope, $cookieStore, config, reportService, $window, 
			DTOptionsBuilder, DTColumnDefBuilder, $compile, $timeout, pdfDelegate, 
			utilityFactory, $state, $stateParams, toaster,$q){	
	// datatables Init
	$scope.prevReport = "";
	var vm = this;
	vm.dtInstance = {};
	vm.dtOptions = DTOptionsBuilder.newOptions()
        // Activate col reorder plugin
		.withOption('paging', true)
        .withOption('responsive', true)
        .withOption('info', false)
        .withColReorder()
        .withColReorderOption('iFixedColumnsLeft', 1)
        .withDOM('<"top"flp<"clear">>rt<"bottom"ip<"clear">>')
    vm.dtColumnDefs = [
        DTColumnDefBuilder.newColumnDef(0).notSortable(),
        DTColumnDefBuilder.newColumnDef(2).notSortable(),
        DTColumnDefBuilder.newColumnDef(5).notSortable()
    ];
	
	$scope.searchReports = function() {
		vm.dtInstance.DataTable.search($scope.searchReport).draw();
    };
    
    $scope.resetSort = function(element){
		var counter = 0;		
		vm.dtInstance.DataTable.on('order.dt', function(){
			if(vm.dtInstance.DataTable.settings().order().length === 1 && counter == 0){
	            let order = vm.dtInstance.DataTable.settings().order()[0],
	            	th = $("#report-table th:eq(" + order[0] + ")");
	            if(th.attr("data-sort-next") === "false"){
	            	vm.dtInstance.DataTable.order([]).draw();
	                th.removeAttr("data-sort-next");
	            }else{
	            	th.attr("data-sort-next", order[1] !== "desc");
	            }
	            $("#report-table th").each(function(k, v){
	            	if(k !== order[0]){
	                	$("#report-table th:eq(" + k + ")").removeAttr("data-sort-next")
	                }
	            });
	            counter++;
	        }
	    });
	}
	
	$scope.retrieveReportList = function(){
		$scope.reportList = [];
		var temp = $cookieStore.get("reports");
		for(i in temp){
			if(temp[i].user == $cookieStore.get('user').userName){
				temp[i].reportIndex = i;
				$scope.reportList.push(temp[i]);
			}
		}
	}
	$scope.retrieveReportList();
	
	$scope.retrieveReportsData = function(){
		var reportData = reportService.getReportDetail();
		var headers = Object.keys(reportData[0]);
		$scope.headers = []
		angular.forEach(headers, function(item){
			$scope.headers.push($scope.camelCaseToWords(item));
		})
		return reportData;
	}
	

    $scope.toggleRenameReport = function(condition, report, index){
		if(condition === true){
			$scope.renameIndex= index;
			$scope.isValidReportRename = true;	
			$scope.reportNameChange = report.reportName;
			$scope.oldReport = report;
			$scope.renameReportModalTrigger = "is-active";
		} else {
			$scope.renameReportModalTrigger = "";
			$scope.isValidReportRename = true;	
		};
    }
    
    $scope.errorModalTrigger = "";
	$scope.errorRedirect = function(){
		$state.go('base.reports');
		$scope.errorModalTrigger = "";
	}
	
    $scope.validateReportRename = function(){
		if($scope.reportNameChange.length == 0){
			$scope.isValidReportRename = false;			
		}else{
			$scope.isValidReportRename = true;				
		}
	}
    
    $scope.saveReportName = function(){
		$scope.validateReportRename();
		if($scope.isValidReportRename){
			$scope.reportList[$scope.renameIndex].reportName = $scope.reportNameChange;
			$cookieStore.put("reports", $scope.reportList);
	    	$scope.toggleRenameReport(false);  
			toaster.pop('success', "", "Changes Saved");
		}
	}
    
    $scope.retrieveNameChange = function(reportName, reportId){
    	$scope["reportName" + reportId] = reportName;
    }
    
	$scope.camelCaseToWords = function(str){
	    return str.match(/^[a-z]+|[A-Z][a-z]*/g).map(function(x){
	        return x[0].toUpperCase() + x.substr(1).toLowerCase();
	    }).join(' ');
	};
	
	$scope.perPageDropdownList = [5,10,15,20,25,30,35,40];
	$scope.ordersPerPage = 10;
	
	$scope.maxDate = new Date().toDateString();
	$scope.filterByDate = function(){
		$scope.minDate = new Date($scope.dateFromFilter).toDateString();
		
		if($scope.dateFromFilter != undefined && $scope.dateFromFilter.length > 0 && $scope.dateFromFilter != "Invalid Date" &&
				$scope.dateToFilter != undefined && $scope.dateToFilter.length > 0 &&  $scope.dateToFilter != "Invalid Date"){
			
			if(angular.isDate(new Date($scope.dateFromFilter)) && angular.isDate(new Date($scope.dateToFilter))){
				
				var dateFrom = new Date($scope.dateFromFilter);
				var dateTo = new Date($scope.dateToFilter);
				
				var filteredReportList = [];
				var copiedReportList = $cookieStore.get("reports");

				/*var savedReport = $cookieStore.get("savedReport");
				if(savedReport != null){
					savedReport["reportId"] = copiedReportList.length;
					copiedReportList.push(savedReport);
				}	*/			
				angular.forEach(copiedReportList, function(report) {
					var reportDate = new Date(report.dateIssued);
					
					if(reportDate.getFullYear() == dateFrom.getFullYear() 
							&& reportDate.getMonth() == dateFrom.getMonth()
							&& reportDate.getDate() == dateFrom.getDate()){
						filteredReportList.push(report);
						
					}else if(reportDate.getFullYear() == dateTo.getFullYear() 
							&& reportDate.getMonth() == dateTo.getMonth()
							&& reportDate.getDate() == dateTo.getDate()){
						filteredReportList.push(report);
						
					}else if(reportDate > dateFrom && reportDate < dateTo){
						filteredReportList.push(report);
						
					};
				});
				$scope.reportList = filteredReportList;
			};
			
		};
	};
	
	$scope.goToReportDetail = function(report, index){
		/*angular.forEach($scope.reportList, function(item, key){
			 if(report.reportId == item.reportId){
				reportService.setSelectedReport(item);
			 }
	     });*/
		$state.go('base.reportDetail', {
	        reportId: report.reportIndex
	    });
	}
	
	$scope.getReportOptions = function(){
		$scope.reportOption = reportService.getReportOptions();		
	}	
	$scope.getReportOptions();
	
	$scope.createReport = function(option){
		$cookieStore.put("editReport", false);
		$cookieStore.put("reportOption", option);
		 $state.go('base.createReport');
	}
	
	// Modals
	$scope.deleteReportModalTrigger = "";
	$scope.toggleDeleteReportModal = function(condition, value){
		$scope.reportToDelete = value;
		if(condition === true){
			$scope.deleteReportModalTrigger = "is-active";
		} else {
			$scope.deleteReportModalTrigger = "";
		};	
	}
	$scope.modalTrigger = "";
	$scope.toggleReportFilterModal = function(condition){
		if(condition === true){
			$scope.modalTrigger = "is-active";
		} else {
			$scope.modalTrigger = "";
		};
	};
	
	// Popover and Pagination
	$scope.closeProjectPopover = function(){
		var $win = $(window); // or $box parent container
		$win.on("click.Bst", function(event){
			 var $box = $("report-table .context-menu");
			if ( 
			    $box.has(event.target).length == 0 // checks if descendants of
													// $box was clicked
			    &&
			    !$box.is(event.target) // checks if the $box itself was clicked
			  ){
					angular.element(document.querySelectorAll("report-table .angular-popover")).addClass("hide-popover-element");
					angular.element(document.querySelectorAll("report-table .angular-popover-triangle")).addClass("hide-popover-element");
				}
		});
	}
	$scope.closeProjectPopover();
	$scope.toggle = false;
	$scope.closeDropDown = function(){
		var $win = $(window); // or $box parent container
		$win.on("click.Bst", function(event){
			 var $box = $(".dropdown");
			if ( 
			    $box.has(event.target).length == 0 // checks if descendants of
													// $box was clicked
			    &&
			    !$box.is(event.target) // checks if the $box itself was clicked
			  ){
					angular.element(document.querySelectorAll(".dropdown")).removeClass("is-active");
					$scope.toggle = false;
				}
		});
	}
	$scope.closeDropDown();
	$scope.toggleDropDown = function(){
		$scope.toggle = !$scope.toggle;
		if($scope.toggle){
			angular.element(document.querySelectorAll(".dropdown")).addClass("is-active");
		}
	}
	
	$scope.initializePdfPrint = function (reportName, url){
		$scope.docDefinition = utilityFactory.createDocDefinitionTable(reportName);		 
		var pdfDocGenerator = pdfMake.createPdf($scope.docDefinition);
		pdfDocGenerator.getDataUrl(function(dataUrl){
			 $scope.pdfUrl = dataUrl;
				pdfDelegate
			    .$getByHandle('invoice-pdf-container')
			    .load($scope.pdfUrl);
		});
		pdfMake.createPdf($scope.docDefinition).print();
	}
	
	$scope.downloadPdf = function(reportName){
		$scope.docDefinition = utilityFactory.createDocDefinitionTable(reportName);	
		pdfMake.createPdf($scope.docDefinition).download(reportName + ".pdf");
	}
	
	$scope.deleteReport = function(){		
		$scope.reportList.splice($scope.reportToDelete, 1);
		$cookieStore.put("reports", $scope.reportList)
		$scope.toggleDeleteReportModal(false, '');
	}
	$scope.totalPages = 1;
	$scope.invoiceIndex = 0;
	  // Initialize Pagination
    /*$scope.initPagination = function(){
		$scope.totalPagesList = [];
		
		var lowCounter = 1;
		var lowIndex = angular.copy($scope.invoiceIndex);
		
		while (lowCounter >= 1) {
		    if((lowIndex - lowCounter) >= 0){
		    	$scope.totalPagesList.push(lowIndex - lowCounter);
		    }
		    lowCounter--;
		}
		$scope.totalPagesList.push($scope.invoiceIndex);
		var highCounter = 1;
		var highIndex = angular.copy($scope.invoiceIndex);
		while (highCounter < 2) {
		    if((highIndex + highCounter) > $scope.invoiceIndex && (highIndex + highCounter) < $scope.reportList.length){
		    	$scope.totalPagesList.push(highIndex + highCounter);
		    }
		    highCounter++;
		}
    }
    $scope.initPagination();*/
	
    $scope.changePage = function(index){
    	$scope.invoiceIndex = index;
    	$scope.initPagination();
	};
	
	$scope.retrieveReportDetail = function(report, action){
		$scope.reportAction = action;
		$scope.reportId = 0;
    	$scope.report = report;
    	$scope.reportDetail = report;
    	$scope.reportId = parseInt(report.reportId);
    	$scope.newReport = report;
    	if($scope.prevReport == report.reportName){
    		if($scope.reportAction == 'pdf'){
    			angular.element(document.querySelectorAll(".buttons-pdf")).trigger('click');
    		}else if($scope.reportAction =='csv'){
    			angular.element(document.querySelectorAll(".buttons-csv")).trigger('click');
    		}else if($scope.reportAction=='print'){
    			angular.element(document.querySelectorAll(".buttons-print")).trigger('click');
    		}
    		return "";
    	}
    	$scope.prevReport = report.reportName;
    	
    	if($scope.report != undefined &&($scope.report.type == 'Summary Report' || $scope.report.type == 'Product Summary Report' || $scope.report.type == 'Ship-To Summary Report')){
    		if($scope.report.chartType == undefined){
    			$scope.report.chartType = 'Line Chart'
    		}
    		$scope.newReport = {
    			  "reportName": $scope.report.reportName,
    			  "type": $scope.report.type,
    			  "category": [
    			    "Category 2"
    			  ],
    			  "chartType": $scope.report.chartType,
    			  "customer": "Customer 3",
    			  "division": [
    			    "Gypsum"
    			  ],
    			  "states": [
    			    "New Jersey",
    			    "New York"
    			  ],
    			  "shipToAccount": [
    			    "All"
    			  ],
    			  "reportFormat": "Tabular",
    			  "data": [],
    			  "timePeriod": "This Week"
    			}
    	}
    	
    	if($scope.reportId ==0){
        	$scope.newReport = $cookieStore.get("newReport");
        	$scope.report = {
        			reportName : $scope.newReport.reportName,
            		type : $scope.newReport.type
            	};
    	}
    	 $scope.intitializeTableFormat = function(){
    		   if($scope.newReport.type == 'Ship-To Summary Report'){
    			   	$scope.getTotalValues = function(){
    				    var totalShipped = 0,totalJan = 0, totalFeb = 0, totalMar = 0, totalApr = 0, totalMay = 0, totalJun = 0, totalJul = 0, totalAug = 0, totalSep = 0, totalOct =0, totalNov =0, totalDec =0;
    				    for(i in $scope.shipToData){	    
    				    	totalShipped += $scope.shipToData[i].total.totalList[0];
    				    	totalJan += $scope.shipToData[i].total.totalList[1];
    				    	totalFeb += $scope.shipToData[i].total.totalList[2];
    				    	totalMar += $scope.shipToData[i].total.totalList[3];
    				    	totalApr += $scope.shipToData[i].total.totalList[4];
    				    	totalMay += $scope.shipToData[i].total.totalList[5];
    				    	totalJun += $scope.shipToData[i].total.totalList[6];
    				    	totalJul += $scope.shipToData[i].total.totalList[7];
    				    	totalAug += $scope.shipToData[i].total.totalList[8];
    				    	totalSep += $scope.shipToData[i].total.totalList[9];
    				    	totalOct += $scope.shipToData[i].total.totalList[10];
    				    	totalNov += $scope.shipToData[i].total.totalList[11];
    				    	totalDec += $scope.shipToData[i].total.totalList[12];	
    					}
    					if(totalShipped == 0){totalShipped = '-';}else{totalShipped = parseFloat(totalShipped).toFixed(2);}
    					if(totalJan == 0){totalJan = '-';}else{totalJan = parseFloat(totalJan).toFixed(2);}
    					if(totalFeb == 0){totalFeb = '-';}else{totalFeb = parseFloat(totalFeb).toFixed(2);}
    					if(totalMar == 0){totalMar = '-';}else{totalMar = parseFloat(totalMar).toFixed(2);}
    					if(totalApr == 0){totalApr = '-';}else{totalApr = parseFloat(totalApr).toFixed(2);}
    					if(totalMay == 0){totalMay = '-';}else{totalMay = parseFloat(totalMay).toFixed(2);}
    					if(totalJun == 0){totalJun = '-';}else{totalJun = parseFloat(totalJun).toFixed(2);}
    					if(totalJul == 0){totalJul = '-';}else{totalJul = parseFloat(totalJul).toFixed(2);}
    					if(totalAug == 0){totalAug = '-';}else{totalAug = parseFloat(totalAug).toFixed(2);}
    					if(totalSep == 0){totalSep = '-';}else{totalSep = parseFloat(totalSep).toFixed(2);}
    					if(totalOct == 0){totalOct = '-';}else{totalOct = parseFloat(totalOct).toFixed(2);}
    					if(totalNov == 0){totalNov = '-';}else{totalNov = parseFloat(totalNov).toFixed(2);}
    					if(totalDec == 0){totalDec = '-';}else{totalDec = parseFloat(totalDec).toFixed(2);}
    				    return {totalShipped:totalShipped,totalJan:totalJan,totalFeb:totalFeb,totalMar:totalMar,totalApr:totalApr,totalMay:totalMay,totalJun:totalJun,totalJul:totalJul,totalAug:totalAug,totalSep:totalSep,totalOct:totalOct,totalNov:totalNov,totalDec:totalDec};
    				}
    			    $scope.groupSortingShip=function(){
    			    	var table = $('#ship-to-table').DataTable({
    			    		"responsive":true,
    			    		"lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
    			    		"buttons":[
    			            {
    			            	 extend: 'print',
    			                 orientation: 'landscape',
    			                 text:'<span class="icon" ><i class="material-icons">print</i></span>',
    			                 pageSize: 'LEGAL',
    			                 footer: true,
    			                 className: 'button',
    			            	 customize: function ( win ) {
    			            		 $(win.document.body).css('overflow', 'visible')
    			            		 $(win.document.body)
    			                     .prepend(
    			                         '<img src="'+ $scope.encodedImage + '" style="position:relative; top:0; left:0;" />'
    			                     );
    			                     $(win.document.body).find( 'td:first-child')
    			                     .css( 'display', 'none' );                   
    			                     $(win.document.body).find('table')
    			                     .css( 'font-size', '10pt' )
    			            		 $(win.document.body).find( 'th:first-child')
    			                     .css( 'display', 'none' );
    								 $(win.document.body).find( 'td:nth-child(4)')
    						         .css( 'word-break', 'break-word' );
    								 $(win.document.body).find( 'td:nth-child(5)')
    						         .css( 'word-break', 'break-word' );
    						         $(win.document.body).css('overflow', 'visible')
    			            		 $(win.document.body).find( 'th')
    			                     .css( 'background', '#454f6a' );
    			            	 },
    			            	  action: function(e, dt, button, config) {
    				                _rowGroupingPrint = 1;
    				                _rowGroupingCSV = 0;
    				                product_summary_Report = 0;
    			                	$.fn.dataTable.ext.buttons.print.action(e, dt, button, config);
    			            	 }
    			            },
    			            {
    			            	 extend: 'csvHtml5',
    			                 title:  $scope.report.reportName,
    			                 text:'<span class="icon"><img class="logo-img report-icon" src="resources/images/reports-icon/reports-export-excel.svg"></span>',
    			                 className: 'button',
    			                 footer: true,
    			                 action:function(e, dt, button, config){
    			                 	_rowGroupingCSV = 1;
    				                _rowGroupingPrint = 0;
    				                product_summary_Report = 0;
    			                 	$.fn.dataTable.ext.buttons.csvHtml5.action(e, dt, button, config);
    			                 }
    			            },
    			            {
    				           	 extend: 'pdf',
    				             title:  $scope.report.reportName,
    				             text:'<span class="icon"><img class="logo-img report-icon" src="resources/images/reports-icon/pdf-icon-blue.svg"></span>',
    				             orientation: 'landscape',
    				             className: 'button',
    				             pageSize: 'LEGAL',
    				             action:function(e, dt, button, config){
    			                 	_rowGroupingCSV = 0;
    				                _rowGroupingPrint = 0;
    				                product_summary_Report = 0;
    			                 	$.fn.dataTable.ext.buttons.pdfHtml5.action(e, dt, button, config);
    			                 },
    				             customize: function ( doc ) {
    				                 var lastColX=null;
    					            var lastColY=null;
    					            var bod = []; 
    					            var subTtl = new Array();
    					            var grandTotal = new Array();
    					            grandTotal[0] = 0;
    					            doc.content[1].table.body.forEach(function(line, i){
    					                if(lastColX != line[0].text && line[0].text != ''){
    					                    if(i !=1){
    					                    	
    					                    	for(var z = 1; z < 14;z++){
    					                			if(subTtl[z] == 0){subTtl[z] ='-';}
    					                			else{subTtl[z] = parseFloat(subTtl[z]).toFixed(2);}
    					                		}
    					                     bod.push(['','','Total for State','',subTtl[0],subTtl[1],subTtl[2],subTtl[3],subTtl[4],subTtl[5],subTtl[6],subTtl[7],subTtl[8],subTtl[9],subTtl[10],subTtl[11],subTtl[12],subTtl[13]]);
    					                     subTtl = [];
    					                    }
    					                    bod.push([{text:line[0].text, style:'rowHeader'},'','','','','','','','','','','','','','','','','']);
    					                    lastColX=line[0].text;
    					                }
    					                if( i < doc.content[1].table.body.length){
    					                    bod.push(['',{text:line[1].text, style:'defaultStyle'},{text:line[2].text, style:'defaultStyle'},{text:line[3].text, style:'defaultStyle'},{text:line[4].text, style:'defaultStyle'},{text:line[5].text, style:'defaultStyle'},{text:line[6].text, style:'defaultStyle'},{text:line[7].text, style:'defaultStyle'},{text:line[8].text, style:'defaultStyle'},{text:line[9].text, style:'defaultStyle'},{text:line[10].text, style:'defaultStyle'},{text:line[11].text, style:'defaultStyle'},{text:line[12].text, style:'defaultStyle'},{text:line[13].text, style:'defaultStyle'},{text:line[14].text, style:'defaultStyle'},{text:line[15].text, style:'defaultStyle'},{text:line[16].text, style:'defaultStyle'},{text:line[17].text, style:'defaultStyle'}]);   
    					                    subTtl[0] = line[4].text;
    					                	for(var z = 1; z < 14;z++){
    					                		 if (typeof subTtl[z] =='undefined'){
    				                        		 subTtl[z] = 0; 
    				                    		 }
    					                		 if (typeof grandTotal[z] =='undefined'){
    				                        		 grandTotal[z] = 0; 
    				                    		 }
    					                		if(!isNaN(parseFloat(line[z+4].text))){subTtl[z] += parseFloat(line[z+4].text);grandTotal[z] += parseFloat(line[z+4].text);}
    					                	}
    					                }
    					                 
    					            });
    					            for(var z = 1; z < 14;z++){
    					                			if(subTtl[z] == 0){subTtl[z] ='-';}
    					                			else{subTtl[z] = parseFloat(subTtl[z]).toFixed(2);}
    					                			if(grandTotal[z] == 0){grandTotal[z] ='-';}
    					                			else{grandTotal[z] = parseFloat(grandTotal[z]).toFixed(2);}
    					                		}
    					                     bod.push(['','','Total for State','',subTtl[0],subTtl[1],subTtl[2],subTtl[3],subTtl[4],subTtl[5],subTtl[6],subTtl[7],subTtl[8],subTtl[9],subTtl[10],subTtl[11],subTtl[12],subTtl[13]]);
    					                     bod.push(['','','Total for Gypsum Products','','',grandTotal[1],grandTotal[2],grandTotal[3],grandTotal[4],grandTotal[5],grandTotal[6],grandTotal[7],grandTotal[8],grandTotal[9],grandTotal[10],grandTotal[11],grandTotal[12],grandTotal[13]]);
    					            doc.content[1].table.headerRows = 1;
    					            doc.content[1].table.body = bod;
    					            doc.content[1].layout = 'lightHorizontalLines';
    					             
    					            doc.styles = {
    					                rowHeader: {
    					                    bold: true,
    					                    color: 'black',
    					                    whiteSpace:'nowrap',
    					                },
    					                defaultStyle: {
    					                fontSize: 8,
    					                color: 'black'
    					                } 
    					            }
    					            
    				                 doc.content.splice( 1, 0, {
    				                     image: $scope.encodedImage,
    				                     alignment: 'center',
    				                     width : '500'                  
    				                 } );
    			             }
    			           }
    			        ],
    				        "columnDefs": [
    				            { "visible": false, "targets": 0 },
    				            {
    							targets: [ 0 ],
    							orderData: [ 4, 0 ]
    						}, {
    							targets: [ 1 ],
    							orderData: [ 4, 1 ]
    						}, {
    							targets: [ 2 ],
    							orderData: [ 4, 2 ]
    						}, {
    							targets: [ 3 ],
    							orderData: [ 4, 3 ]
    						}, {
    							targets: [ 4 ],
    							orderData: [ 4, 0 ]
    						}, {
    							targets: [ 5 ],
    							orderData: [ 4, 5 ]
    						}, {
    							targets: [ 6 ],
    							orderData: [ 4, 6 ]
    						}, {
    							targets: [ 7 ],
    							orderData: [ 4, 7 ]
    						}, {
    							targets: [ 8 ],
    							orderData: [ 4, 8 ]
    						}, {
    							targets: [ 9 ],
    							orderData: [ 4, 9 ]
    						}, {
    							targets: [ 10 ],
    							orderData: [ 4, 10 ]
    						}, {
    							targets: [ 11 ],
    							orderData: [ 4, 11 ]
    						}, {
    							targets: [ 12 ],
    							orderData: [ 4, 12 ]
    						}, {
    							targets: [ 13 ],
    							orderData: [ 4, 13 ]
    						}, {
    							targets: [ 14 ],
    							orderData: [ 4, 14 ]
    						}, {
    							targets: [ 15 ],
    							orderData: [ 4, 15 ]
    						}, {
    							targets: [ 16 ],
    							orderData: [ 4, 16 ]
    						}, {
    							targets: [ 17 ],
    							orderData: [ 4, 17 ]
    						}
    				        ],
    				        "orderFixed": [0, 'asc'],
    				        "order": [[ 4, 'asc' ]],
    				        "displayLength": 10,
    				        "rowGroup": {
    					        dataSrc: 0
    					    },
    				        "drawCallback": function ( settings ) {
    				            var api = this.api();
    				            var rows = api.rows( {page:'current'} ).nodes();
    				            var last=null;
    				            var colonne = api.row(0).data().length;
    				            var totale = new Array();
    				            totale['Totale']= new Array();
    			    			var subtotale = new Array();
    				            var groupid = -1;
    				            var groupRow= 0;
    				 
    				            api.column(0, {page:'current'} ).data().each( function ( group, i ) {
    				                if ( last !== group ) {
    				                	if(i !=0){
    				                		$(rows).eq( i ).before(
    				                    	  '<tr class="report-footer"><td colspan="3" class="has-text-right">Total for State</td></tr>'
    				                    	);
    				                	}
    				                    $(rows).eq( i ).before(
    				                        '<tr class="report-title"><td colspan="17">'+group+'</td></tr>'
    				                    );
    				                    last = group;
    				                	groupRow++;
    				                }
    					            val = api.row(api.row($(rows).eq( i )).index()).data();    
    				                $.each(val,function(index2,val2){
    						                if (typeof subtotale[groupRow] =='undefined'){
    						                    subtotale[groupRow] = new Array();
    						                }
    				                        if (typeof subtotale[groupRow][index2] =='undefined'){
    				                            subtotale[groupRow][index2] = 0;
    				                        }
    				                        if(val2 == '-'){val2 = 0;}
    				                        if(index2 == 4){subtotale[groupRow][index2] = val2;}
    				                        else{subtotale[groupRow][index2] += parseFloat(val2);}
    				                });
    				            } );
    				            $('#ship-to-table tbody tr:last-child').after(
    				                    	  '<tr class="report-footer"><td colspan="3" class="has-text-right">Total for State</td></tr>'
    				             );
    				            var groupRow1= 1;
    				            $('#ship-to-table tbody').find('.report-footer').each(function (i,v) {
    			                    var rowCount = $(this).nextUntil('.report-footer').length;
    			                        var subtd = '<td>'+subtotale[groupRow1][4]+'</td>';
    			                        for (var a=5;a<colonne;a++)
    			                        { 
    			                        	if(subtotale[groupRow1][a] == 0){subtd += '<td>-</td>';}
    			                        	else{subtd += '<td>'+parseFloat(subtotale[groupRow1][a]).toFixed(2)+'</td>';}
    			                        }
    			                        $(this).append(subtd);
    			                       groupRow1++;
    			                });
    				            

    		        	        $timeout( function(){ 
    		        	        	if($scope.reportAction == 'pdf'){
    		        	    			angular.element(document.querySelectorAll(".buttons-pdf")).trigger('click');
    		        	    		}else if($scope.reportAction =='csv'){
    		        	    			angular.element(document.querySelectorAll(".buttons-csv")).trigger('click');
    		        	    		}else if($scope.reportAction=='print'){
    		        	    			angular.element(document.querySelectorAll(".buttons-print")).trigger('click');
    		        	    		}  					
    							}, 2000);
    				        }
    				    } );
    				    $('#ship-to-table tbody').on( 'click', 'tr.report-title', function () {
    				        var currentOrder = table.order()[0];
    				        if (currentOrder[0] === 0 && currentOrder[1] === 'asc' ) {
    				           table.order( [ 0, 'desc' ],[ 4, 'desc' ] ).draw();
    				        }
    				        else {
    				           table.order( [ 0, 'asc' ],[ 0, 'asc' ] ).draw(); 
    				        }
    				    } );
    			    	angular.element( document.querySelector( '.report-export' ) ).html('');
    				    table.buttons( 0, null ).container().appendTo( '.report-export' );
    				    
    			                if($('.ship-to-summary-tab').width() >= 920){
    					    		 $('#ship-to-table tbody td').each(function (i,v) {
    					    		 	if($(this).css('display') == 'none'){$(this).show();}
    					    		 	
    					    		 });
    					    		 $('#ship-to-table thead th').each(function (i,v) {
    					    		 	if($(this).css('display') == 'none'){$(this).show();}
    					    		 	
    					    		 });
    					    		 $('#ship-to-table').removeClass('collapsed');
    					    	}
    					}
    					$timeout( function(){ 
    						$scope.groupSortingShip(); 					
    					}, 500);
    		    }   
    		  	else if($scope.newReport.type == 'Product Summary Report'){
    			   	$scope.getTotalPValues = function(){
    				    var totalAmount = 0;
    		    		var label = []
			    		var keyNames = Object.keys($scope.reportSummary.total);
		    			for(i in keyNames){
		    				totalAmount += $scope.reportSummary.total[keyNames[i]][0].total$;
		    			} 
    				   /* for(i in $scope.ProductData){	    
    				    	totalAmount += $scope.ProductData[i].total.totalList[1];	
    					}*/
    					if(totalAmount == 0){totalAmount = '-';}else{totalAmount = parseFloat(totalAmount).toFixed(2);}
    				    return totalAmount;
    				}
    				$scope.groupSorting=function(){
    			    	var table = $('#product-to-table').DataTable({
    			    		"responsive":true,
    			    		"lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
    			    		"buttons":[
    			            {
    			            	 extend: 'print',
    			                 text:'<span class="icon" ><i class="material-icons">print</i></span>',
    			                 pageSize: 'LEGAL',
    			                 footer: true,
    			                 className: 'button',
    			            	 customize: function ( win ) {
    			            		 $(win.document.body).css('overflow', 'visible')
    			            		 $(win.document.body)
    			                     .prepend(
    			                         '<img src="'+ $scope.encodedImage + '" style="position:relative; top:0; left:0;" />'
    			                     );
    			                     $(win.document.body).find( 'td:first-child')
    			                     .css( 'display', 'none' );                   
    			                     $(win.document.body).find('table')
    			                     .css( 'font-size', '10pt' )
    			            		 $(win.document.body).find( 'th:first-child')
    			                     .css( 'display', 'none' );
    						         $(win.document.body).css('overflow', 'visible')
    			            		 $(win.document.body).find( 'th')
    			                     .css( 'background', '#454f6a' );
    			            	 },
    			            	  action: function(e, dt, button, config) {
    				                _rowGroupingPrint = 1;
    				                _rowGroupingCSV = 0;
    				                product_summary_Report = 1;
    			                	$.fn.dataTable.ext.buttons.print.action(e, dt, button, config);
    			            	 }
    			            },
    			            {
    			            	 extend: 'csvHtml5',
    			                 title:  $scope.report.reportName,
    			                 text:'<span class="icon"><img class="logo-img report-icon" src="resources/images/reports-icon/reports-export-excel.svg"></span>',
    			                 className: 'button',
    			                 footer: true,
    			                 action:function(e, dt, button, config){
    			                 	_rowGroupingCSV = 1;
    				                _rowGroupingPrint = 0;
    				                product_summary_Report = 1;
    			                 	$.fn.dataTable.ext.buttons.csvHtml5.action(e, dt, button, config);
    			                 }
    			            },
    			            {
    				           	 extend: 'pdf',
    				             title:  $scope.report.reportName,
    				             text:'<span class="icon"><img class="logo-img report-icon" src="resources/images/reports-icon/pdf-icon-blue.svg"></span>',
    				             className: 'button',
    				             pageSize: 'LEGAL',
    				             action:function(e, dt, button, config){
    			                 	_rowGroupingCSV = 0;
    				                _rowGroupingPrint = 0;
    				                product_summary_Report = 0;
    			                 	$.fn.dataTable.ext.buttons.pdfHtml5.action(e, dt, button, config);
    			                 },
    				             customize: function ( doc ) {
    				                 var lastColX=null;
    					            var lastColY=null;
    					            var bod = []; 
    					            var subTtl = new Array();
    					            var grandTotal = 0;
    					            doc.content[1].table.body.forEach(function(line, i){
    					                if(lastColX != line[0].text && line[0].text != ''){
    					                    if(i !=1){
    					                    	
    					                    	for(var z = 1; z < 5;z++){
    					                			if(z==2 || z==4){subTtl[z] = parseFloat(subTtl[z]).toFixed(2);}
    					                		}
    					                     bod.push(['','Total',subTtl[2],'',subTtl[4]]);
    					                     subTtl = [];
    					                    }
    					                    bod.push([{text:line[0].text, style:'rowHeader'},'','','','']);
    					                    lastColX=line[0].text;
    					                }
    					                if( i < doc.content[1].table.body.length){
    					                    bod.push(['',{text:line[1].text, style:'defaultStyle'},{text:line[2].text, style:'defaultStyle'},{text:line[3].text, style:'defaultStyle'},{text:line[4].text, style:'defaultStyle'}]);   
    					                   for(var z = 1; z < 5;z++){
    					                		 if (typeof subTtl[z] =='undefined'){
    				                        		 subTtl[z] = 0; 
    				                    		 }
    				                    		if(z == 2 || z == 4){
    					                		if(!isNaN(parseFloat(line[z].text))){subTtl[z] += parseFloat(line[z].text);}}
    					                		if(z == 4 && !isNaN(parseFloat(line[z].text)) ){grandTotal += parseFloat(line[z].text);}
    					                	}
    					                }
    					                 
    					            });
    					            for(var z = 1; z < 5;z++){
    					                			if(subTtl[z] == 0){subTtl[z] ='-';}
    					                			else{subTtl[z] = parseFloat(subTtl[z]).toFixed(2);}
    					                		}
    					                		grandTotal = parseFloat(grandTotal).toFixed(2)
    					                     bod.push(['','Total',subTtl[2],'',subTtl[4]]);
    					                     bod.push(['','Total Amount','','',grandTotal]);
    					             
    					            doc.content[1].table.headerRows = 1;
    					            doc.content[1].table.body = bod;
    					            doc.content[1].layout = 'lightHorizontalLines';
    					             
    					            doc.styles = {
    					                rowHeader: {
    					                    bold: true,
    					                    color: 'black',
    					                    whiteSpace:'nowrap',
    					                },
    					                defaultStyle: {
    					                fontSize: 8,
    					                color: 'black'
    					                } 
    					            }
    					            
    				                 doc.content.splice( 1, 0, {
    				                     image: $scope.encodedImage,
    				                     alignment: 'center',
    				                     width : '500'                  
    				                 } );
    			             }
    			           }
    			        ],
    				        "columnDefs": [
    				            { "visible": false, "targets": 0 },
    				        ],
    				        "orderFixed": [0, 'asc'],
    				        "order": [[ 0, 'asc' ]],
    				        "displayLength": 10,
    				        "rowGroup": {
    					        dataSrc: 0
    					    },
    				        "drawCallback": function ( settings ) {
    				            var api = this.api();
    				            var rows = api.rows( {page:'current'} ).nodes();
    				            var last=null;
    				            var colonne = api.row(0).data().length;
    				            var totale = new Array();
    				            totale['Totale']= new Array();
    			    			var subtotale = new Array();
    				            var groupid = -1;
    				            var groupRow= 0;
    				 
    				            api.column(0, {page:'current'} ).data().each( function ( group, i ) {
    				                if ( last !== group ) {
    				                	if(i !=0){
    				                		$(rows).eq( i ).before(
    				                    	  '<tr class="report-footer"><td class="has-text-right">Total</td></tr>'
    				                    	);
    				                	}
    				                    $(rows).eq( i ).before(
    				                        '<tr class="report-title"><td colspan="4">'+group+'</td></tr>'
    				                    );
    				                    last = group;
    				                	groupRow++;
    				                }
    					            val = api.row(api.row($(rows).eq( i )).index()).data();    
    				                $.each(val,function(index2,val2){
    						                if (typeof subtotale[groupRow] =='undefined'){
    						                    subtotale[groupRow] = new Array();
    						                }
    				                        if (typeof subtotale[groupRow][index2] =='undefined'){
    				                            subtotale[groupRow][index2] = 0;
    				                        }
    				                        if(val2 == '-'){val2 = 0;}
    				                        if(index2 == 2 || index2 == 4){subtotale[groupRow][index2] += parseFloat(val2);}
    				                });
    				            } );
    				            $('#product-to-table tbody tr:last-child').after(
    				                    	  '<tr class="report-footer"><td class="has-text-right">Total </td></tr>'
    				             );
    				            var groupRow1= 1;
    				            $('#product-to-table tbody').find('.report-footer').each(function (i,v) {
    			                    var rowCount = $(this).nextUntil('.report-footer').length;
    			                        var subtd = '';
    			                        for (var a=2;a<colonne;a++)
    			                        { 
    			                        	if(a == 2 || a == 4){
    			                        		if(isNaN(parseFloat(subtotale[groupRow1][a]))){
    			                        			subtd += '<td class="has-text-right">0</td>';
    			                        		}else{
        			                        		subtd += '<td class="has-text-right">'+parseFloat(subtotale[groupRow1][a]).toFixed(2)+'</td>';
    			                        		}
    			                        	}
    			                        	if(a==3){subtd +='<td>&nbsp;</td>';}
    			                        }
    			                        $(this).append(subtd);
    			                       groupRow1++;
    			                });

    		        	        $timeout( function(){ 
    		        	        	if($scope.reportAction == 'pdf'){
    		        	    			angular.element(document.querySelectorAll(".buttons-pdf")).trigger('click');
    		        	    		}else if($scope.reportAction =='csv'){
    		        	    			angular.element(document.querySelectorAll(".buttons-csv")).trigger('click');
    		        	    		}else if($scope.reportAction=='print'){
    		        	    			angular.element(document.querySelectorAll(".buttons-print")).trigger('click');
    		        	    		}  					
    							}, 2000);
    				        }
    				    } );
    				    $('#product-to-table tbody').on( 'click', 'tr.report-title', function () {
    				        var currentOrder = table.order()[0];
    				        if (currentOrder[0] === 0 && currentOrder[1] === 'asc' ) {
    				           table.order( [ 0, 'desc' ]).draw();
    				        }
    				        else {
    				           table.order( [ 0, 'asc' ]).draw(); 
    				        }
    				    } );
    			    	angular.element( document.querySelector( '.report-export' ) ).html('');
    				    table.buttons( 0, null ).container().appendTo( '.report-export' );
    					}
    				$timeout( function(){ $scope.groupSorting(); }, 500);
    		    }
    		  	else if($scope.newReport.type == 'Summary Report'){

    			    $scope.groupSortingShip=function(){
    			    	var table = $('#summary-report-table').DataTable({
    			    		"responsive":true,
    			    		"lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
    			    		"iDisplayLength": 10,
    			    		"buttons":[,
    			            {
    			            	 extend: 'print',
    			                 orientation: 'landscape',
    			                 text:'<span class="icon" ><i class="material-icons">print</i></span>',
    			                 pageSize: 'LEGAL',
    			                 footer: true,
    			                 className: 'button',
    			            	 customize: function ( win ) {
    			            		 $(win.document.body).css('overflow', 'visible')
    			            		 $(win.document.body)
    			                     .prepend(
    			                         '<img src="'+ $scope.encodedImage + '" style="position:relative; top:0; left:0;" />'
    			                     );
    			                     $(win.document.body).find( 'td:first-child')
    			                     .css( 'display', 'none' );      
    			                     $(win.document.body).find( 'td:nth-child(9)')
    			                     .css( 'display', 'none' );                
    			                     $(win.document.body).find('table')
    			                     .css( 'font-size', '10pt' )
    			            		 $(win.document.body).find( 'th:first-child')
    			                     .css( 'display', 'none' );     
    			                     $(win.document.body).find( 'th:nth-child(9)')
    			                     .css( 'display', 'none' ); 
    						         $(win.document.body).css('overflow', 'visible')
    			            		 $(win.document.body).find( 'th')
    			                     .css( 'background', '#454f6a' );
    			            	 },
    			            	  action: function(e, dt, button, config) {
    				                _rowGroupingPrint = 1;
    				                _rowGroupingCSV = 0;
    				                summary_Report = 1;
    			                	$.fn.dataTable.ext.buttons.print.action(e, dt, button, config);
    			            	 }
    			            },
    			            {
    			            	 extend: 'csvHtml5',
    			                 title:  $scope.report.reportName,
    			                 text:'<span class="icon"><img class="logo-img report-icon" src="resources/images/reports-icon/reports-export-excel.svg"></span>',
    			                 className: 'button',
    			                 footer: true,
    			                 exportOptions: {
    			                     columns: 'th:not(:nth-child(8))'
    			                 },
    			                 action:function(e, dt, button, config){
    			                 	_rowGroupingCSV = 1;
    				                _rowGroupingPrint = 0;
    				                summary_Report = 1;
    			                 	$.fn.dataTable.ext.buttons.csvHtml5.action(e, dt, button, config);
    			                 }
    			            
    			            },
    			            {
    				           	 extend: 'pdf',
    				             title:  $scope.report.reportName,
    				             text:'<span class="icon"><img class="logo-img report-icon" src="resources/images/reports-icon/pdf-icon-blue.svg"></span>',
    				             className: 'button',
    				             pageSize: 'LEGAL',
    				             action:function(e, dt, button, config){
    			                 	_rowGroupingCSV = 0;
    				                _rowGroupingPrint = 0;
    				                summary_Report = 0;
    			                 	$.fn.dataTable.ext.buttons.pdfHtml5.action(e, dt, button, config);
    			                 },
    				             customize: function ( doc ) {
    				                var lastColX=null;
    					            var lastColY=null;
    					            var bod = []; 
    					            var subTtl = new Array();
    					            var grandTotal = 0;
    					            doc.content[1].table.body.forEach(function(line, i){
    					                if(lastColX != line[0].text && line[0].text != ''){
    					                    if(i !=1){			                    	
    					                    	for(var z = 2; z < 8;z++){
    					                			subTtl[z] = parseFloat(subTtl[z]).toFixed(2);
    					                		}
    						                    bod.push([{text:'Total', style:'rowHeader'},{text:subTtl[2], style:'rowHeader'},{text:subTtl[3], style:'rowHeader'},{text:subTtl[4], style:'rowHeader'},{text:subTtl[5], style:'rowHeader'},{text:subTtl[6], style:'rowHeader'},{text:subTtl[7], style:'rowHeader'}]);
    						                    subTtl = [];
    					                    }
    					                    bod.push([{text:line[0].text, style:'rowHeader'},'','','','','','']);
    					                    lastColX=line[0].text;
    					                }
    					                if( i < doc.content[1].table.body.length && line.length != 1){
    					                	   if(line[1].text != '1st Qtr' && line[1].text != '2nd Qtr'&& line[1].text != '3rd Qtr'&& line[1].text != '4th Qtr'&& line[1].text != 'Total'){
    								                bod.push([{text:line[1].text, style:'defaultStyle'},{text:line[2].text, style:'defaultStyle'},{text:line[3].text, style:'defaultStyle'},{text:line[4].text, style:'defaultStyle'},{text:line[5].text, style:'defaultStyle'},{text:line[6].text, style:'defaultStyle'},{text:line[7].text, style:'defaultStyle'}]); 
    								           }else{
    								        	   bod.push([{text:line[1].text, style:'rowHeader'},{text:line[2].text, style:'rowHeader'},{text:line[3].text, style:'rowHeader'},{text:line[4].text, style:'rowHeader'},{text:line[5].text, style:'rowHeader'},{text:line[6].text, style:'rowHeader'},{text:line[7].text, style:'rowHeader'}]); 
    									       }
    					                   for(var z = 2; z < 8;z++){
    					                	   if(line[1].text != '1st Qtr' && line[1].text != '2nd Qtr'&& line[1].text != '3rd Qtr'&& line[1].text != '4th Qtr'&& line[1].text != 'Total'){
    						                		 if (typeof subTtl[z] =='undefined'){
    					                        		 subTtl[z] = 0; 
    					                    		 }
    						                		if(!isNaN(parseFloat(line[z].text))){subTtl[z] += parseFloat(line[z].text);}
    						                		if(!isNaN(parseFloat(line[z].text)) ){grandTotal += parseFloat(line[z].text);}
    					                	   }
    					                	}
    					                }
    					                 
    					            });
    					            for(var z = 2; z < 8;z++){
    		                			if(subTtl[z] == 0){subTtl[z] ='0';}
    		                			else{subTtl[z] = parseFloat(subTtl[z]).toFixed(2);}
    		                		}
    			                     bod.push([{text:'Total', style:'rowHeader'},{text:subTtl[2], style:'rowHeader'},{text:subTtl[3], style:'rowHeader'},{text:subTtl[4], style:'rowHeader'},{text:subTtl[5], style:'rowHeader'},{text:subTtl[6], style:'rowHeader'},{text:subTtl[7], style:'rowHeader'}]);
    					             
    					            doc.content[1].table.headerRows = 1;
    					            doc.content[1].table.body = bod;
    					            doc.content[1].layout = 'lightHorizontalLines';
    					             
    					            doc.styles = {
    					                rowHeader: {
    					                    bold: true,
    					                    color: 'black',
    					                    whiteSpace:'nowrap',
    					                },
    					                defaultStyle: {
    					                color: 'black'
    					                } 
    					            }
    					            
    				                 doc.content.splice( 1, 0, {
    				                     image: $scope.encodedImage,
    				                     alignment: 'center',
    				                     width : '500'                  
    				                 } );
    			             }
    			           }
    			        ],
    				        "columnDefs": [
    				            { "visible": false, "targets": 0 },
    				            {
    							targets: [ 0 ],
    							orderData: [ 8, 0 ]
    						}, {
    							targets: [ 1 ],
    							orderData: [ 8, 1 ]
    						}, {
    							targets: [ 2 ],
    							orderData: [ 8, 2 ]
    						}, {
    							targets: [ 3 ],
    							orderData: [ 8, 3 ]
    						}, {
    							targets: [ 4 ],
    							orderData: [ 8, 4 ]
    						}, {
    							targets: [ 5 ],
    							orderData: [ 8, 5 ]
    						}, {
    							targets: [ 6 ],
    							orderData: [ 8, 6 ]
    						}, {
    							targets: [ 7 ],
    							orderData: [ 8, 7 ]
    						}, {
    							targets: [ 8 ],
    							orderData: [ 8, 0 ]
    						}
    				        ],
    				        "orderFixed": [0, 'asc'],
    				        "order": [],
    				        "displayLength": 10,
    				        "rowGroup": {
    					        dataSrc: 0
    					    },
    				        "drawCallback": function ( settings ) {
    				            var api = this.api();
    				            var rows = api.rows( {page:'current'} ).nodes();
    				            var last=null;
    				            var colonne = api.row(0).data().length;
    				            var totale = new Array();
    				            totale['Totale']= new Array();
    			    			var subtotale = new Array();
    				            var groupid = -1;
    				            var groupRow= 0;
    		            		var name = null;
    		            		var lastGroup = null
    				            api.column(0, {page:'current'} ).data().each( function ( group, i ) {
    				                if ( last !== group ) {
    				                	if(i !=0){
    				                		if(last!= null){
    				                			name = last;
    				                		}else{
    				                			name = group;
    				                		}
    		                				var totalData = $scope.reportSummaryList.filter(function( obj ) {
    		                					  return obj.name == name;
    		                				})[0];
    				                		$(rows).eq( i ).before(
    				                				$compile(
    				                				'<tr class="total-qtr group report-title light-blue" class="ng-scope odd" role="row">'+
    				                				'<td class="report-content is-hidden-desktop" ng-click="toggleTotalDiv('+ i +')">Total</td>'+
    				                				'<td class="report-content is-hidden-touch"><span>Total</span></td>'+
    				                				'<td class="report-content"><span>'+ totalData.total.currYrMSF+ '</span></td>'+
    				                				'<td class="report-content"><span>'+ totalData.total.currYr$+ '</span></td>'+
    				                				'<td class="report-content"><span>'+ totalData.total.prevYrMSF+ '</span></td>'+
    				                				'<td class="report-content"><span>'+ totalData.total.prevYr$+ '</span></td>'+
    				                				'<td class="report-content"><span>'+ totalData.total.var$+ '</span></td>'+
    				                				'<td class="report-content"><span>'+ totalData.total.varQty+ '</span></td>'+
    				                				'<td class=" is-hidden"><span>'+ group+ '4</span></td>'+
    				                				+'</tr>'+
    				                				'<tr class="total-child-'+ i +' child is-hidden"><td class="child" colspan="2"><ul data-dtr-index="32" class="dtr-details"><li data-dtr-index="3" data-dt-row="32" data-dt-column="3" class="is-hidden-tablet"><span class="dtr-title">Current Year($)</span> <span class="dtr-data">'+ 
    				                				totalData.total.currYr$+ 
    				                				'</span></li><li data-dtr-index="4" data-dt-row="32" data-dt-column="4" class="is-hidden-tablet"><span class="dtr-title">Previous Year(MSF)</span> <span class="dtr-data">'+ 
    				                				totalData.total.prevYrMSF+ '</span></li><li data-dtr-index="5" data-dt-row="32" data-dt-column="5"><span class="dtr-title">Previous Year($)</span> <span class="dtr-data">'+ 
    				                				totalData.total.prevYr$+ '</span></li><li data-dtr-index="6" data-dt-row="32" data-dt-column="6"><span class="dtr-title">Var% (Qty)</span> <span class="dtr-data">'+ 
    				                				totalData.total.varQty+ '</span></li><li data-dtr-index="7" data-dt-row="32" data-dt-column="7"><span class="dtr-title">Var%($)</span> <span class="dtr-data">'
    				                				+ totalData.total.var$+ '</span></li></ul></td></tr>'
    				                				)($scope)
    				                    	);
    				                	}
    				                    last = group;
    				                    lastGroup = group;
    				                	groupRow++;
    				                }
    				            } );

    		                	var pageInfo = rows.page.info();
    		                	if(pageInfo.page == (pageInfo.pages - 1)){
    		        				var totalData = $scope.reportSummaryList.filter(function( obj ) {
    		          					  return obj.name == lastGroup;
    		          				})[0];
    		                		$(rows).eq(rows.length -1 ).after(
    		                				$compile(
    		                				'<tr class="total-qtr report-title group report-footer light-blue" class="ng-scope odd" role="row">'+
    		                				'<td class="report-content is-hidden-desktop" ng-click="toggleTotalDiv('+ i +')">Total</td>'+
    		                				'<td class="report-content is-hidden-touch"><span>Total</span></td>'+
    		                				'<td class="report-content"><span>'+ totalData.total.currYrMSF+ '</span></td>'+
    		                				'<td class="report-content"><span>'+ totalData.total.currYr$+ '</span></td>'+
    		                				'<td class="report-content"><span>'+ totalData.total.prevYrMSF+ '</span></td>'+
    		                				'<td class="report-content"><span>'+ totalData.total.prevYr$+ '</span></td>'+
    		                				'<td class="report-content"><span>'+ totalData.total.var$+ '</span></td>'+
    		                				'<td class="report-content"><span>'+ totalData.total.varQty+ '</span></td>'+
    		                				'<td class=" is-hidden"><span>'+ lastGroup+ '4</span></td>'+
    		                				+'</tr>'+
    		                				'<tr class="total-child-'+ i +' child is-hidden"><td class="child" colspan="2"><ul data-dtr-index="32" class="dtr-details"><li data-dtr-index="3" data-dt-row="32" data-dt-column="3" class="is-hidden-tablet"><span class="dtr-title">Current Year($)</span> <span class="dtr-data">'+ 
    		                				totalData.total.currYr$+ 
    		                				'</span></li><li data-dtr-index="4" data-dt-row="32" data-dt-column="4" class="is-hidden-tablet"><span class="dtr-title">Previous Year(MSF)</span> <span class="dtr-data">'+ 
    		                				totalData.total.prevYrMSF+ '</span></li><li data-dtr-index="5" data-dt-row="32" data-dt-column="5"><span class="dtr-title">Previous Year($)</span> <span class="dtr-data">'+ 
    		                				totalData.total.prevYr$+ '</span></li><li data-dtr-index="6" data-dt-row="32" data-dt-column="6"><span class="dtr-title">Var% (Qty)</span> <span class="dtr-data">'+ 
    		                				totalData.total.varQty+ '</span></li><li data-dtr-index="7" data-dt-row="32" data-dt-column="7"><span class="dtr-title">Var%($)</span> <span class="dtr-data">'
    		                				+ totalData.total.var$+ '</span></li></ul></td></tr>'
    		                				)($scope)
    		                    	);
    		                	}

    		        	        $timeout( function(){ 
    		        	        	if($scope.reportAction == 'pdf'){
    		        	    			angular.element(document.querySelectorAll(".buttons-pdf")).trigger('click');
    		        	    		}else if($scope.reportAction =='csv'){
    		        	    			angular.element(document.querySelectorAll(".buttons-csv")).trigger('click');
    		        	    		}else if($scope.reportAction=='print'){
    		        	    			angular.element(document.querySelectorAll(".buttons-print")).trigger('click');
    		        	    		}  					
    							}, 2000);
    				        }
    				    } );
    			    	
    			    	$('#summary-report-table tbody').on( 'click', 'tr.report-title', function () {
    				        var currentOrder = table.order()[0];
    				        if (currentOrder[0] === 0 && currentOrder[1] === 'asc' ) {
    				           table.order( [ 0, 'desc' ]).draw();
    				        }
    				        else {
    				           table.order( [ 0, 'asc' ]).draw(); 
    				        }
    				    } );
    			    	angular.element( document.querySelector( '.report-export' ) ).html('');
    			    	table.buttons( 0, null ).container().appendTo( '.report-export' );
    					}
    					$timeout( function(){ 
    						$scope.groupSortingShip(); 
    						$compile($(".report-footer")) ($scope);
    					}, 1000);
    					$scope.toggleTotalDiv = function(index){
    					   if(angular.element(document.querySelectorAll('.total-child-'+ index)).hasClass('is-hidden')){
    						   angular.element(document.querySelectorAll('.total-child-'+ index)).removeClass('is-hidden')
    					   }else{
    						   angular.element(document.querySelectorAll('.total-child-'+ index)).addClass('is-hidden')				   
    					   }
    				   }
    		  		}
    		  	}
    	//CHart js
        $scope.initializeChart = function(){
        	if($scope.newReport != undefined){
    	    	if($scope.newReport.chartType == 'Bar Chart' || $scope.newReport.chartType == 'Line Chart'){
    	    		$scope.labels = [];
    	    		$scope.data = [];
    	    		$scope.series = [];
    		    	if($scope.newReport.type == 'Summary Report'){
    		    		var currYear = [];
    		    		var prevYear = [];
    		    		var varQty = [];
    		    		$scope.labels.push('Jan','Feb','Mar', 'Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec')
    		    		for( i in $scope.reportSummaryList){	
    		    			currYear = []
    		    			for(c in $scope.reportSummaryList[i].monthSummary){
    		    				if($scope.reportSummaryList[i].monthSummary[c].month != '1st Qtr' && $scope.reportSummaryList[i].monthSummary[c].month != '2nd Qtr' &&
    		    						$scope.reportSummaryList[i].monthSummary[c].month != '3rd Qtr' && $scope.reportSummaryList[i].monthSummary[c].month != '4th Qtr'
    		    							&& $scope.reportSummaryList[i].monthSummary[c].month != 'Total'){
    				    			currYear.push($scope.reportSummaryList[i].monthSummary[c].currYr$);
//    				    			$scope.labels.push($scope.reportSummaryList[i].monthSummary[c].month)
    		    				}
    		    			}
    		    			$scope.series.push($scope.reportSummaryList[i].name)
    			    		$scope.data.push(currYear);
    		    		}
    	
    		    	}else if($scope.newReport.type == 'Product Summary Report'){
    		    			var label = []
    			    		var keyNames = Object.keys($scope.reportSummary.total);
    		    			for(i in keyNames){
    		    				$scope.labels.push(keyNames[i].replace(/[_-]/g, " "));
    		    				$scope.data.push($scope.reportSummary.total[keyNames[i]][0].total$)
    		    			} 
    		    	}else if($scope.newReport.type == 'Ship-To Summary Report'){
    		    		var listData = [];
    		    		for(i in $scope.shipToData){
    	    				$scope.labels.push($scope.shipToData[i].total.name)		    
    	    				$scope.data.push($scope.shipToData[i].total.totalList[0]);	
    		    		}
    		    	}    	   
    	    	    $scope.onClick = function (points, evt) {
    	    	      console.log(points, evt);
    	    	    };
    	    	    
    	    		
    	    	}else if($scope.newReport.chartType == 'Pie Chart'){
    	    		$scope.labels = [];
    	    		$scope.data = [];
    	    		$scope.series = [];
    	    		if($scope.newReport.type == 'Summary Report'){
    		    		var currYear = [];
    		    		var prevYear = [];
    		    		var varQty = [];
    		    		for( i in $scope.reportSummaryList){	
    		    			$scope.labels.push($scope.reportSummaryList[i].name)	
    		    			varQty.push($scope.reportSummaryList[i].total.currYr$);
    		    		}
    		    		$scope.data.push(varQty);
    		    	}else if($scope.newReport.type == 'Product Summary Report'){
    		    		var label = []
			    		var keyNames = Object.keys($scope.reportSummary.total);
		    			for(i in keyNames){
		    				$scope.labels.push(keyNames[i].replace(/[_-]/g, " "));
		    				$scope.data.push($scope.reportSummary.total[keyNames[i]][0].total$)
		    			}     		 
    		    	}else if($scope.newReport.type == 'Ship-To Summary Report'){
    		    		var listData = [];
    		    		for(i in $scope.shipToData){
    	    				$scope.labels.push($scope.shipToData[i].total.name)		    
    	    				$scope.data.push($scope.shipToData[i].total.totalList[0]);	
    		    		}
    		    	}
    	    	}
        	}
        	
        	$scope.datasetOverride = [{ yAxisID: 'y-axis-1' }, { yAxisID: 'y-axis-2' }];
    	    $scope.options = {
    	      scales: {
    	        yAxes: [
    	          {
    	            id: 'y-axis-1',
    	            type: 'linear',
    	            display: true,
    	            position: 'left'
    	          },
    	          {
    	            id: 'y-axis-2',
    	            type: 'linear',
    	            display: true,
    	            position: 'right'
    	          }
    	        ],
    	        xAxes:[]
    	      },
    	      bezierCurve: false, 
    	      animation: {
        	    onComplete: function(animation){
        	        $scope.encodedImage = this.toBase64Image();//this gives the base64 image
        	    }
    	      }
    	    };
        }
    	
    	
    	
    	if($scope.newReport != undefined){
	    	if($scope.newReport.type == 'Summary Report'){
	    		var productSummaryType = "MainSummary";
		    		reportService.getReportsData(productSummaryType).then(function(result){
		    			var reportSummary = result.result.data;  
			    		$scope.reportSummaryList = [];
			    		angular.forEach(reportSummary.datas, function(data){
				    		var summary = {};
			    			summary.name = data.sector.replace(/[_-]/g, " ");
			    			summary.monthSummary = []
		    				var quarter = 1;
			    			for(i in data.Month){
			    				if(data.Month[i] != 'Total'){
				    				if(i == 4){
				    					quarter = 2;
				    				}else if(i == 8){
				    					quarter = 3;	    					
				    				}else if(i == 12){
				    					quarter = 4;	    					
				    				}
			    					summary.monthSummary.push(
				    						{
				    							'month' : data.Month[i],
				    							'currYrMSF' : data['Current_Year(MSF)'][i],
				    							'currYr$' : data['Current_Year($)'][i],
				    							'prevYrMSF' : data['Previous_Year(MSF)'][i],
				    							'var$' : data['Var%($)'][i],
				    							'prevYr$' : data['Previous_Year($)'][i],
				    							'varQty' : data['Var%(QTY)'][i],
				    							'quarter' : quarter
				    						}
				    				)    
			    				}else{
			    					summary.total = {
				    							'month' : data.Month[i],
				    							'currYrMSF' : data['Current_Year(MSF)'][i],
				    							'currYr$' : data['Current_Year($)'][i],
				    							'prevYrMSF' : data['Previous_Year(MSF)'][i],
				    							'var$' : data['Var%($)'][i],
				    							'prevYr$' : data['Previous_Year($)'][i],
				    							'varQty' : data['Var%(QTY)'][i]
				    						}
			    				}
			    			}
			    			$scope.reportSummaryList.push(summary)
			    		}); 
			    	    $scope.initializeChart();
			    	    $scope.intitializeTableFormat();
		    		});
	    	}else if($scope.newReport.type == 'Product Summary Report'){
	    		var productSummaryType = "ProductSummaryBoth"
	    		reportService.getReportsData(productSummaryType).then(function(result){
		    		$scope.reportSummary = result.result.data;  
		    		var keyNames = Object.keys(result.result.data.datas);
		    		var newObject = Object.keys(result.result.data.datas).map(function(i){return result.result.data.datas[i]});
		    		var summaryData = [];
		    		for(i in keyNames){
		    			var obj = {};
		    			obj[keyNames[i]] = newObject[i]
			    		summaryData.push(obj)
		    		}
		    		console.log(summaryData);
		    		$scope.reportSummary.datas = summaryData;
		    		$scope.ProductData = [];
		    		angular.forEach($scope.reportSummary.datas, function(data, key){
		    			var reportData = {}
		    			reportData.summary = []
	    				for ( property in data ) {
	      				  data.name = property; 
	      				  reportData.name = property.replace(/[_-]/g, " "); 
	      				}
						for(i in data[data.name]){
	  	    				var isValid = true;
	  	    				var totalName = '';
	  	    				for ( property in data[data.name][i] ) {
	  	    					if(property.indexOf('Total for Product')){
	  	    						isValid = false
	  	    						totalName = property;
	  	    					} 
	  	      				}
	  	    				if(isValid){
	  	    					data[data.name][i].group = reportData.name
	  	    					reportData.summary.push(data[data.name][i])	    					
	  	    				}else if(totalName.indexOf('Total for Product')){      	    					
	  	    					reportData.total = {
	  	    							'name' : totalName,
	  	    							'totalList' : data[data.name][i][totalName]
	  	    					}
	  	    				}
	  	    			}
						$scope.ProductData.push(reportData);					
		    		});
		    	    $scope.initializeChart();
		    	    $scope.intitializeTableFormat();
	    		});
	    	}else if($scope.newReport.type == 'Ship-To Summary Report'){
	    		reportService.getReportsData("ShipToSummary").then(function(result){
	    			$scope.reportSummary = result.result.data;
	    			$scope.shipToData = [];
		    		angular.forEach($scope.reportSummary.datas, function(data, key){
		    			var reportData = {}
		    			reportData.summary = []
	    				for ( property in data ) {
	      				  data.name = property; 
	      				  reportData.name = property.replace(/[_-]/g, " "); 
	      				}
	    				if(data.name == "Gypsum_GlasRoc ($)"){
	    					reportData.type = "$";    					
	    				}else{
	    					reportData.type = "MSF";    			    					
	    				}
						for(i in data[data.name]){
	  	    				var isValid = true;
	  	    				var totalName = '';
	  	    				for ( property in data[data.name][i] ) {
	  	    					if(property.indexOf('Total for State')){
	  	    						isValid = false
	  	    						totalName = property;
	  	    					} 
	  	      				}
	  	    				if(isValid){
	  	    					data[data.name][i].group = reportData.name
	  	    					reportData.summary.push(data[data.name][i])	    					
	  	    				}else if(totalName.indexOf('Total for State')){      	    					
	  	    					reportData.total = {
	  	    							'name' : totalName,
	  	    							'totalList' : data[data.name][i][totalName]
	  	    					}
	  	    				}
	  	    			}
						$scope.shipToData.push(reportData);					
		    		});
		    	    $scope.initializeChart();
		    	    $scope.intitializeTableFormat();
	    		}); 
	    		
	    	}
    	}
    	
    }
	
	
	
}]);