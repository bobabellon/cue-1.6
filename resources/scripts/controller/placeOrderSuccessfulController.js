var app = angular.module('placeOrderSuccessfulController', []);

app.controller("placeOrderSuccessfulController", ['$scope','$state','$cookieStore', '$rootScope','$window',
	function($scope, $state, $cookieStore, $rootScope,$window){
	
	$scope.email = JSON.parse($window.localStorage.getItem('user')).userName;
	$scope.orderCode = $cookieStore.get('orderCode');
	
	//Navigation
	$scope.goToSearchResults = function(){
		$cookieStore.put('orderCode', "");
		$state.go('base.search');
	};
}]);