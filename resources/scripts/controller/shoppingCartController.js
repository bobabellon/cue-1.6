var app = angular.module('shoppingCartController', ['userService' , 'datatables', 'datatables.colreorder']);

app.controller("shoppingCartController", ['$scope', 'utilityFactory','$cookieStore','$state','config','userService','productService','utilityFactory','$rootScope','$window', 'DTOptionsBuilder', 'DTColumnDefBuilder', '$timeout', '$compile', '$q', '$filter',
	function($scope, utilityFactory, $cookieStore, $state, config, userService, productService, utilityFactory, $rootScope, $window, DTOptionsBuilder, DTColumnDefBuilder, $timeout, $compile , $q, $filter){
	var q = $q.defer();
    $scope.$timeout = $timeout;
   /* var vm = this;
	vm.dtInstance = {};
    vm.dtOptions = DTOptionsBuilder.newOptions()
        // Activate col reorder plugin
        .withOption('responsive', true)
        .withOption('paging', false)
        .withColReorder()
        .withColReorderOption('iFixedColumnsLeft', 1)
    vm.dtColumnDefs = [
        DTColumnDefBuilder.newColumnDef(0).notSortable(),
        DTColumnDefBuilder.newColumnDef(1).notSortable(),
        DTColumnDefBuilder.newColumnDef(10).notSortable(),
    ];

    $scope.rerenderTable = function(){
    	vm.dtInstance.rerender();	
	}
    
    $scope.resetSort = function(element){
		var counter = 0;		
		vm.dtInstance.DataTable.on('order.dt', function(){
			if(vm.dtInstance.DataTable.settings().order().length === 1 && counter == 0){
	            let order = vm.dtInstance.DataTable.settings().order()[0],
	            	th = $("shopping-cart-table th:eq(" + order[0] + ")");
	            if(th.attr("data-sort-next") === "false"){
	            	vm.dtInstance.DataTable.order([]).draw();
	                th.removeAttr("data-sort-next");
	            }else{
	            	th.attr("data-sort-next", order[1] !== "desc");
	            }
	            $("shopping-cart-table th").each((k, v) => {
	            	if(k !== order[0]){
	                	$("shopping-cart-table th:eq(" + k + ")").removeAttr("data-sort-next")
	                }
	            });
	            counter++
	        }
	    });
	}
    $scope.loadAngular = function(){
    	$compile($(".dtr-details")) ($scope);
    }*/

//	$scope.shoppingCartItem = $scope.shoppingCart;
    
	$scope.GetAllShoppingCartItem = function(){
		var chain = $q.when();
		chain = chain.then(function() {		
			utilityFactory.getAllShoppingCartItem().then(function(result){
				$rootScope.shoppingCart = result;
//				$scope.shoppingCartItems = $rootScope.shoppingCart;
				angular.forEach($rootScope.shoppingCart.items, function(value){
					value.options = [];
					value.itemNumber.indexOf('_') >= 0 ? value.hasOptions = true : value.hasOptions = false;
					if(value.categories.length != 0 && value.categories.length != undefined){
						angular.forEach(value.categories, function(option) {
							if(option.code.indexOf('B2B_') >= 0){
								value.options.push((option.code.split("B2B_").pop()).replace(/_/g , "."));
							}
						})
					}
				});
			});
		});
	};

	$scope.GetAllShoppingCartItem();	
	
	$scope.cartDockOnScroll = function(){
		var $win = $('body'); // or $box parent container
		$win.on("scroll", function(event){
			 if ($win.scrollTop() >= 300) {
		        	$('.order-summary-fixed').addClass('fixed');
		        	$('.order-summary-fixed-mobile').addClass('fixed-mobile');
		        } else {
		        	$('.order-summary-fixed').removeClass('fixed');
		        	$('.order-summary-fixed-mobile').removeClass('fixed-mobile');
		        }			
		});
		
	}
	$scope.cartDockOnScroll();
	$scope.removeToCart = function(index){
		var fields = ['statusMessage'];
		var url = config.baseUrl + config.userPath + $window.localStorage.getItem('user').userName + "/carts/" + $window.localStorage.getItem('cart').code + "/entries/" + index ;
		userService.removeToCart(url).then(
			function success(result){
				console.log(result);
				if(result.status == 200){
					$scope.GetAllShoppingCartItem();
				}
			},
			function error(result){
				console.log(result);
		});
	}
	$scope.isOrderVisible = false;
	$scope.toggleOrderDetails = function(result){
		$scope.isOrderVisible = result
	}
	
	$scope.updateItemQuantityToCart = function(index, quantity, product, isValid){
		if(isValid && product.quantity != quantity){
			var fields = ['statusMessage'];
			var url = config.baseUrl + config.userPath + $window.localStorage.getItem('user').userName + "/carts/" + $window.localStorage.getItem('cart').code + "/entries/" + index 
			+ "?qty=" + quantity;
			if(quantity != undefined){
				userService.updateItemQuantityToCart(url).then(
					function success(result){
						console.log(result);
						utilityFactory.getAllShoppingCartItem().then(function(result){
							$scope.shoppingCart = result;
							angular.forEach($scope.shoppingCart.items, function(value){
								value.options = [];
								value.itemNumber.indexOf('_') >= 0 ? value.hasOptions = true : value.hasOptions = false;
								if(value.categories.length != 0 && value.categories.length != undefined){
									angular.forEach(value.categories, function(option) {
										if(option.code.indexOf('B2B_') >= 0){
											value.options.push((option.code.split("B2B_").pop()).replace(/_/g , "."));
										}
									})
								}
							});
						});
					},
					function error(result){
						console.log(result);
				});
			}
		}
	}
	
	$rootScope.shippingFee = 75.00;
	$scope.subTotal = 0;
	
	$scope.goToCheckout = function(){
		if($scope.shoppingCart.items.length > 0){
			$state.go('base.checkout');
		};
	};
	
	$scope.getSubTotal = function(collection){
		var total = 0;

        collection.forEach(function (item) {
        	total += parseFloat(item['unitPrice']) * item['quantity'];
        });

        $rootScope.subTotal =  total;
	}
	 $scope.saveProductList = $cookieStore.get("savedProducts");
	//SAVE FOR LATER  and Move to cart FUNCTIONS
	 $scope.saveItemForLater =function(product){
		 $scope.removeToCart(product.entryNumber)
		 var saveProductList = $cookieStore.get("savedProducts");
		 if(saveProductList != undefined){
			 saveProductList.push(product);
			 $cookieStore.put("savedProducts", saveProductList);
		 }else{
			 saveProductList = [];
			 saveProductList.push(product);
			 $cookieStore.put("savedProducts", saveProductList);			 
		 }		 
		 $scope.saveProductList = saveProductList;
	 }
	 
	 $scope.moveToCart = function(product){
			if(product.stockLevel > product.quantity && product.quantity != undefined && product.quantity != 0){
				//Modify
				var pickUpStore = "USA";
				var fields = ['statusMessage'];
				
				var url = config.baseUrl + config.userPath + $window.localStorage.getItem('user').userName + "/carts/" + $window.localStorage.getItem('cart').code + "/entries?code=" + 
				product.itemNumber + "&qty=" + product.quantity + "&pickUpStore=" + pickUpStore + "?fields=" + fields;
				userService.addToCart(url).then(
					function success(result){
						console.log(result);
						$scope.removeToSavedItem(product);
						$scope.GetAllShoppingCartItem();
					},
					function error(result){
						console.log(result);
				});
			}
		};
	
	 $scope.removeToSavedItem = function(item){
		 var saveProductList = $cookieStore.get("savedProducts");
		 saveProductList =$filter('filter')(saveProductList, function(product) {
		        return product.itemNumber !== item.itemNumber;
		    });
		 $cookieStore.put("savedProducts", saveProductList);	
		 $scope.saveProductList =saveProductList;
	 }
//	$scope.getSubTotal($scope.shoppingCart.item);
	 $scope.initializeReccomendedProducts = function(){
		//Recommended Products
			$scope.recommendedProducts = JSON.parse($window.localStorage.getItem("recommendedProducts"));
			
			//Recommended Products - Desktop
			$scope.recommendedProductsDesktop = [];
			angular.copy($scope.recommendedProducts, $scope.recommendedProductsDesktop);
			$scope.recommendedProductsDesktop = utilityFactory.splitArray($scope.recommendedProductsDesktop, 4);
			$scope.recommendedProductsPagesDesktop = [];
			$scope.recommendedProductsIndexDesktop = 0;
			$scope.generateRecommendedProductsPageDesktop = function(recommendedProductsIndex){
				$scope.recommendedProductsPagesDesktop = utilityFactory.generatePagination($scope.recommendedProductsPagesDesktop, $scope.recommendedProductsDesktop, recommendedProductsIndex);
			}
			$scope.generateRecommendedProductsPageDesktop($scope.recommendedProductsIndexDesktop);
			
			//Recommended Products - Mobile
			$scope.recommendedProductsMobile = [];
			angular.copy($scope.recommendedProducts, $scope.recommendedProductsMobile);
			$scope.recommendedProductsPagesMobile = [];
			$scope.recommendedProductsIndexMobile = 0;
			$scope.generateRecommendedProductsPageMobile = function(recommendedProductsIndex){
				$scope.recommendedProductsPagesMobile = utilityFactory.generatePagination($scope.recommendedProductsPagesMobile, $scope.recommendedProductsMobile, recommendedProductsIndex);
			}
			$scope.generateRecommendedProductsPageMobile($scope.recommendedProductsIndexMobile);
			
			
			//Initialization
			$scope.visibleRecommendedProductsDesktop = $scope.recommendedProductsDesktop[$scope.recommendedProductsIndexDesktop];
			
			$scope.incrementPageIndex = function(index, product){
				if(product == "recommendedProductMobile"){
					returnObject = utilityFactory.incrementPageIndex($scope[""+index], $scope.recommendedProductsPagesMobile, $scope.recommendedProductsMobile);
					$scope.recommendedProductsIndexMobile = returnObject[0];
					$scope.recommendedProductsPagesMobile = returnObject[1];
				} else if(product == "savedProductMobile"){
					returnObject = utilityFactory.incrementPageIndex($scope[""+index], $scope.savedProductsPagesMobile, $scope.savedProductsMobile);
					$scope.savedProductsIndexMobile = returnObject[0];
					$scope.savedProductsPagesMobile = returnObject[1];
				} else if(product == "recommendedProductDesktop"){
					returnObject = utilityFactory.incrementPageIndex($scope[""+index], $scope.recommendedProductsPagesDesktop, $scope.recommendedProductsDesktop);
					$scope.recommendedProductsIndexDesktop = returnObject[0];
					$scope.recommendedProductsPagesDesktop = returnObject[1];
					$scope.visibleRecommendedProductsDesktop = $scope.recommendedProductsDesktop[$scope.recommendedProductsIndexDesktop];
				} else if(product == "savedProductDesktop"){
					returnObject = utilityFactory.incrementPageIndex($scope[""+index], $scope.savedProductsPagesDesktop, $scope.savedProductsDesktop);
					$scope.savedProductsIndexDesktop = returnObject[0];
					$scope.savedProductsPagesDesktop = returnObject[1];
					$scope.visibleSavedProductsDesktop = $scope.savedProductsDesktop[$scope.savedProductsIndexDesktop];
				} else if (product == "customerReviewsAndComments"){
					returnObject = utilityFactory.incrementPageIndex($scope[""+index], $scope.customerReviewsAndRatingsPage, $scope.customerReviewsAndRatingsList);
					$scope.customerReviewsAndRatingsIndex = returnObject[0];
					$scope.customerReviewsAndRatingsPage = returnObject[1];
					$scope.visibleCustomerReviewsAndRatingsList = $scope.customerReviewsAndRatingsList[$scope.customerReviewsAndRatingsIndex];
				}
			}
			
			$scope.decrementPageIndex = function(index, product){
				if(product == "recommendedProductMobile"){
					returnObject = utilityFactory.decrementPageIndex($scope[""+index], $scope.recommendedProductsPagesMobile, $scope.recommendedProductsMobile);
					$scope.recommendedProductsIndexMobile = returnObject[0];
					$scope.recommendedProductsPagesMobile = returnObject[1];
				} else if(product == "savedProductMobile"){
					returnObject = utilityFactory.decrementPageIndex($scope[""+index], $scope.savedProductsPagesMobile, $scope.savedProductsMobile);
					$scope.savedProductsIndexMobile = returnObject[0];
					$scope.savedProductsPagesMobile = returnObject[1];
				} else if(product == "recommendedProductDesktop"){
					returnObject = utilityFactory.decrementPageIndex($scope[""+index], $scope.recommendedProductsPagesDesktop, $scope.recommendedProductsDesktop);
					$scope.recommendedProductsIndexDesktop = returnObject[0];
					$scope.recommendedProductsPagesDesktop = returnObject[1];
					$scope.visibleRecommendedProductsDesktop = $scope.recommendedProductsDesktop[$scope.recommendedProductsIndexDesktop];
				} else if(product == "savedProductDesktop"){
					returnObject = utilityFactory.decrementPageIndex($scope[""+index], $scope.savedProductsPagesDesktop, $scope.savedProductsDesktop);
					$scope.savedProductsIndexDesktop = returnObject[0];
					$scope.savedProductsPagesDesktop = returnObject[1];
					$scope.visibleSavedProductsDesktop = $scope.savedProductsDesktop[$scope.savedProductsIndexDesktop];
				} else if (product == "customerReviewsAndComments"){
					returnObject = utilityFactory.decrementPageIndex($scope[""+index], $scope.customerReviewsAndRatingsPage, $scope.customerReviewsAndRatingsList);
					$scope.customerReviewsAndRatingsIndex = returnObject[0];
					$scope.customerReviewsAndRatingsPage = returnObject[1];
					$scope.visibleCustomerReviewsAndRatingsList = $scope.customerReviewsAndRatingsList[$scope.customerReviewsAndRatingsIndex];
				}
			}
			
			$scope.updatePageIndex = function(index, value, product){
				if(product == "recommendedProductMobile"){
					returnObject = utilityFactory.updatePageIndex($scope[""+index], $scope.recommendedProductsPagesMobile, $scope.recommendedProductsMobile, value);
					$scope.recommendedProductsIndexMobile = returnObject[0];
					$scope.recommendedProductsPagesMobile = returnObject[1];
				} else if(product == "savedProductMobile"){
					returnObject = utilityFactory.updatePageIndex($scope[""+index], $scope.savedProductsPagesMobile, $scope.savedProductsMobile, value);
					$scope.savedProductsIndexMobile = returnObject[0];
					$scope.savedProductsPagesMobile = returnObject[1];
				} else if(product == "recommendedProductDesktop"){
					returnObject = utilityFactory.updatePageIndex($scope[""+index], $scope.recommendedProductsPagesDesktop, $scope.recommendedProductsDesktop, value);
					$scope.recommendedProductsIndexDesktop = returnObject[0];
					$scope.recommendedProductsPagesDesktop = returnObject[1];
					$scope.visibleRecommendedProductsDesktop = $scope.recommendedProductsDesktop[$scope.recommendedProductsIndexDesktop];
				} else if(product == "savedProductDesktop"){
					returnObject = utilityFactory.updatePageIndex($scope[""+index], $scope.savedProductsPagesDesktop, $scope.savedProductsDesktop, value);
					$scope.savedProductsIndexDesktop = returnObject[0];
					$scope.savedProductsPagesDesktop = returnObject[1];
					$scope.visibleSavedProductsDesktop = $scope.savedProductsDesktop[$scope.savedProductsIndexDesktop];
				} else if (product == "customerReviewsAndComments"){
					returnObject = utilityFactory.updatePageIndex($scope[""+index], $scope.customerReviewsAndRatingsPage, $scope.customerReviewsAndRatingsList, value);
					$scope.customerReviewsAndRatingsIndex = returnObject[0];
					$scope.customerReviewsAndRatingsPage = returnObject[1];
					$scope.visibleCustomerReviewsAndRatingsList = $scope.customerReviewsAndRatingsList[$scope.customerReviewsAndRatingsIndex];
				}
			}
	 }
	 $scope.initializeReccomendedProducts();
	//Navigation
	
	//MODALS
	$scope.modalTrigger = "";
	$scope.toggleOrderDetailsModal = function(condition, index){
		if(condition === true){
			$scope.modalTrigger = "is-active";	
			initProductDetails(index);
		} else {
			$scope.modalTrigger = "";
			$scope.initializeReccomendedProducts();
		}
	};
	
	$scope.showOrderDetailsMobile = function(index){
		initProductDetails(index);
		document.getElementById("order-detail-mobile").style.height = "100%";
		document.getElementsByTagName("body")[0].style.overflowY = 'hidden';
		document.getElementById("overlay").style.display = "block";
	};
	
	$scope.hideOrderDetailsMobile = function(){
		document.getElementById("order-detail-mobile").style.height = "0px";
		document.getElementsByTagName("body")[0].style.overflowY = 'auto';
		document.getElementById("overlay").style.display = "none";
	};
	
	function initProductDetails(index){		
		$scope.product = index;
		$scope.hasColor = false;
		$scope.hasFit = false;
		$scope.hasSize = false;
		$scope.imageList = [];
		var productOptionList = []
		
		//Modify
		$scope.product.product = {}
		$scope.product.product.code = $scope.product.itemNumber;
		$scope.product.product.name = $scope.product.name;
		$scope.product.rating = 3;
		
		if($scope.product.variantOptions != undefined){
			$scope.hasColor = true;
			$scope.hasFit = true;
			$scope.hasSize = true;
		}					
		//Get other Fields
		var url = config.baseUrl + config.productPath + $scope.product.product.code + "?fields=images,categories";
		productService.getProduct(url).then(
			function success(result){
				if(result.data.categories != undefined){
					for(i in result.data.categories){
						if(result.data.categories[i].code.indexOf('B2B_') >= 0 && $scope.product.product.code.indexOf('_')){
							$scope.imageList.push(
									{
									"code" : $scope.product.product.code,
									"url" : config.baseUrl + result.data.images[0].url,
									"color" : result.data.categories[2].code.split("_").pop()
									});
							if(result.data.categories[0].code.indexOf('B2B_') >= 0){
								$scope.product.size = (result.data.categories[0].code.split("B2B_").pop()).replace(/_/g , ".");
							}
							break;
						}
					}
				}else{
					$scope.imageList.push(
							{
							"code" : value.code,
							"url" : config.baseUrl + result.data.images[0].url
							});
				}
			},
			function error(result){
				console.log(result);
		});	
		$scope.product.customerReviewAndRatings = [
		                               			{
		                               				'shortComment': 'Solid, Dependable, Comfortable 1',
		                               				'customerName': 'John S.',
		                               				'date': 'January 25, 2017',
		                               				'rating': 5,
		                               				'size': 12,
		                               				'color': 'Brown',
		                               				'showBtnShowAll': true,
		                               				'showAllComment': false,
		                               				'longComment': [
		                               					'Lorem ipsum dolor sit amet, consectetur adipiscing elit. In pharetra dictum eros, non tempor sem ornare et. Fusce sed arcu non ligula accumsan eleifend. Phasellus vulputate massa et ligula bibendum, id posuere turpis feugiat.',
		                               					'Nam nec posuere eros. Donec rhoncus lacinia pharetra. Morbi scelerisque, ante a porttitor tincidunt, metus lorem sodales mauris, non faucibus eros dolor et mauris. Ut vel placerat diam. Morbi at lorem hendrerit, luctus enim vitae, semper est.',
		                               					'In tempus risus hendrerit purus rhoncus, sed scelerisque mauris luctus. Aliquam erat volutpat. Donec nibh massa, mollis vitae ante eu, tincidunt commodo nisi. Aenean imperdiet blandit est ac consequat. Duis imperdiet lectus sem, non pretium orci sollicitudin mollis. Sed condimentum sem eu sapien mattis sagittis. Mauris bibendum, mi in accumsan vulputate, ipsum eros elementum metus, eu fermentum enim lorem vel ante. Maecenas egestas pharetra ipsum, nec malesuada sapien sollicitudin sit amet. Nulla facilisi. Phasellus iaculis turpis dui, eget efficitur justo finibus et. Donec id leo felis.'
		                               				]
		                               			},
		                               			{
		                               				'shortComment': 'Solid, Dependable, Comfortable 2',
		                               				'customerName': 'John S.',
		                               				'date': 'January 25, 2017',
		                               				'rating': 3,
		                               				'size': 12,
		                               				'color': 'Brown',
		                               				'showBtnShowAll': true,
		                               				'showAllComment': false,
		                               				'longComment': [
		                               					'Lorem ipsum dolor sit amet, consectetur adipiscing elit. In pharetra dictum eros, non tempor sem ornare et. Fusce sed arcu non ligula accumsan eleifend. Phasellus vulputate massa et ligula bibendum, id posuere turpis feugiat.',
		                               					'Nam nec posuere eros. Donec rhoncus lacinia pharetra. Morbi scelerisque, ante a porttitor tincidunt, metus lorem sodales mauris, non faucibus eros dolor et mauris. Ut vel placerat diam. Morbi at lorem hendrerit, luctus enim vitae, semper est.',
		                               					'In tempus risus hendrerit purus rhoncus, sed scelerisque mauris luctus. Aliquam erat volutpat. Donec nibh massa, mollis vitae ante eu, tincidunt commodo nisi. Aenean imperdiet blandit est ac consequat. Duis imperdiet lectus sem, non pretium orci sollicitudin mollis. Sed condimentum sem eu sapien mattis sagittis. Mauris bibendum, mi in accumsan vulputate, ipsum eros elementum metus, eu fermentum enim lorem vel ante. Maecenas egestas pharetra ipsum, nec malesuada sapien sollicitudin sit amet. Nulla facilisi. Phasellus iaculis turpis dui, eget efficitur justo finibus et. Donec id leo felis.'
		                               				]
		                               			},
		                               			{
		                               				'shortComment': 'Solid, Dependable, Comfortable 3',
		                               				'customerName': 'John S.',
		                               				'date': 'January 25, 2017',
		                               				'rating': 0,
		                               				'size': 12,
		                               				'color': 'Brown',
		                               				'showBtnShowAll': false,
		                               				'showAllComment': true,
		                               				'longComment': [
		                               					'Lorem ipsum dolor sit amet, consectetur adipiscing elit. In pharetra dictum eros, non tempor sem ornare et. Fusce sed arcu non ligula accumsan eleifend. Phasellus vulputate massa et ligula bibendum, id posuere turpis feugiat.'
		                               					]
		                               			},
		                               			{
		                               				'shortComment': 'Solid, Dependable, Comfortable 4',
		                               				'customerName': 'John S.',
		                               				'date': 'January 25, 2017',
		                               				'rating': 3,
		                               				'size': 12,
		                               				'color': 'Brown',
		                               				'showBtnShowAll': true,
		                               				'showAllComment': false,
		                               				'longComment': [
		                               					'Lorem ipsum dolor sit amet, consectetur adipiscing elit. In pharetra dictum eros, non tempor sem ornare et. Fusce sed arcu non ligula accumsan eleifend. Phasellus vulputate massa et ligula bibendum, id posuere turpis feugiat.',
		                               					'Nam nec posuere eros. Donec rhoncus lacinia pharetra. Morbi scelerisque, ante a porttitor tincidunt, metus lorem sodales mauris, non faucibus eros dolor et mauris. Ut vel placerat diam. Morbi at lorem hendrerit, luctus enim vitae, semper est.',
		                               					'In tempus risus hendrerit purus rhoncus, sed scelerisque mauris luctus. Aliquam erat volutpat. Donec nibh massa, mollis vitae ante eu, tincidunt commodo nisi. Aenean imperdiet blandit est ac consequat. Duis imperdiet lectus sem, non pretium orci sollicitudin mollis. Sed condimentum sem eu sapien mattis sagittis. Mauris bibendum, mi in accumsan vulputate, ipsum eros elementum metus, eu fermentum enim lorem vel ante. Maecenas egestas pharetra ipsum, nec malesuada sapien sollicitudin sit amet. Nulla facilisi. Phasellus iaculis turpis dui, eget efficitur justo finibus et. Donec id leo felis.'
		                               				]
		                               			},
		                               			{
		                               				'shortComment': 'Solid, Dependable, Comfortable 5',
		                               				'customerName': 'John S.',
		                               				'date': 'January 25, 2017',
		                               				'rating': 3,
		                               				'size': 12,
		                               				'color': 'Brown',
		                               				'showBtnShowAll': true,
		                               				'showAllComment': false,
		                               				'longComment': [
		                               					'Lorem ipsum dolor sit amet, consectetur adipiscing elit. In pharetra dictum eros, non tempor sem ornare et. Fusce sed arcu non ligula accumsan eleifend. Phasellus vulputate massa et ligula bibendum, id posuere turpis feugiat.',
		                               					'Nam nec posuere eros. Donec rhoncus lacinia pharetra. Morbi scelerisque, ante a porttitor tincidunt, metus lorem sodales mauris, non faucibus eros dolor et mauris. Ut vel placerat diam. Morbi at lorem hendrerit, luctus enim vitae, semper est.',
		                               					'In tempus risus hendrerit purus rhoncus, sed scelerisque mauris luctus. Aliquam erat volutpat. Donec nibh massa, mollis vitae ante eu, tincidunt commodo nisi. Aenean imperdiet blandit est ac consequat. Duis imperdiet lectus sem, non pretium orci sollicitudin mollis. Sed condimentum sem eu sapien mattis sagittis. Mauris bibendum, mi in accumsan vulputate, ipsum eros elementum metus, eu fermentum enim lorem vel ante. Maecenas egestas pharetra ipsum, nec malesuada sapien sollicitudin sit amet. Nulla facilisi. Phasellus iaculis turpis dui, eget efficitur justo finibus et. Donec id leo felis.'
		                               				]
		                               			},
		                               			{
		                               				'shortComment': 'Solid, Dependable, Comfortable 6',
		                               				'customerName': 'John S.',
		                               				'date': 'January 25, 2017',
		                               				'rating': 3,
		                               				'size': 12,
		                               				'color': 'Brown',
		                               				'showBtnShowAll': true,
		                               				'showAllComment': false,
		                               				'longComment': [
		                               					'Lorem ipsum dolor sit amet, consectetur adipiscing elit. In pharetra dictum eros, non tempor sem ornare et. Fusce sed arcu non ligula accumsan eleifend. Phasellus vulputate massa et ligula bibendum, id posuere turpis feugiat.',
		                               					'Nam nec posuere eros. Donec rhoncus lacinia pharetra. Morbi scelerisque, ante a porttitor tincidunt, metus lorem sodales mauris, non faucibus eros dolor et mauris. Ut vel placerat diam. Morbi at lorem hendrerit, luctus enim vitae, semper est.',
		                               					'In tempus risus hendrerit purus rhoncus, sed scelerisque mauris luctus. Aliquam erat volutpat. Donec nibh massa, mollis vitae ante eu, tincidunt commodo nisi. Aenean imperdiet blandit est ac consequat. Duis imperdiet lectus sem, non pretium orci sollicitudin mollis. Sed condimentum sem eu sapien mattis sagittis. Mauris bibendum, mi in accumsan vulputate, ipsum eros elementum metus, eu fermentum enim lorem vel ante. Maecenas egestas pharetra ipsum, nec malesuada sapien sollicitudin sit amet. Nulla facilisi. Phasellus iaculis turpis dui, eget efficitur justo finibus et. Donec id leo felis.'
		                               				]
		                               			},
		                               			{
		                               				'shortComment': 'Solid, Dependable, Comfortable 7',
		                               				'customerName': 'John S.',
		                               				'date': 'January 25, 2017',
		                               				'rating': 3,
		                               				'size': 12,
		                               				'color': 'Brown',
		                               				'showBtnShowAll': true,
		                               				'showAllComment': false,
		                               				'longComment': [
		                               					'Lorem ipsum dolor sit amet, consectetur adipiscing elit. In pharetra dictum eros, non tempor sem ornare et. Fusce sed arcu non ligula accumsan eleifend. Phasellus vulputate massa et ligula bibendum, id posuere turpis feugiat.',
		                               					'Nam nec posuere eros. Donec rhoncus lacinia pharetra. Morbi scelerisque, ante a porttitor tincidunt, metus lorem sodales mauris, non faucibus eros dolor et mauris. Ut vel placerat diam. Morbi at lorem hendrerit, luctus enim vitae, semper est.',
		                               					'In tempus risus hendrerit purus rhoncus, sed scelerisque mauris luctus. Aliquam erat volutpat. Donec nibh massa, mollis vitae ante eu, tincidunt commodo nisi. Aenean imperdiet blandit est ac consequat. Duis imperdiet lectus sem, non pretium orci sollicitudin mollis. Sed condimentum sem eu sapien mattis sagittis. Mauris bibendum, mi in accumsan vulputate, ipsum eros elementum metus, eu fermentum enim lorem vel ante. Maecenas egestas pharetra ipsum, nec malesuada sapien sollicitudin sit amet. Nulla facilisi. Phasellus iaculis turpis dui, eget efficitur justo finibus et. Donec id leo felis.'
		                               				]
		                               			},
		                               			{
		                               				'shortComment': 'Solid, Dependable, Comfortable 8',
		                               				'customerName': 'John S.',
		                               				'date': 'January 25, 2017',
		                               				'rating': 3,
		                               				'size': 12,
		                               				'color': 'Brown',
		                               				'showBtnShowAll': true,
		                               				'showAllComment': false,
		                               				'longComment': [
		                               					'Lorem ipsum dolor sit amet, consectetur adipiscing elit. In pharetra dictum eros, non tempor sem ornare et. Fusce sed arcu non ligula accumsan eleifend. Phasellus vulputate massa et ligula bibendum, id posuere turpis feugiat.',
		                               					'Nam nec posuere eros. Donec rhoncus lacinia pharetra. Morbi scelerisque, ante a porttitor tincidunt, metus lorem sodales mauris, non faucibus eros dolor et mauris. Ut vel placerat diam. Morbi at lorem hendrerit, luctus enim vitae, semper est.',
		                               					'In tempus risus hendrerit purus rhoncus, sed scelerisque mauris luctus. Aliquam erat volutpat. Donec nibh massa, mollis vitae ante eu, tincidunt commodo nisi. Aenean imperdiet blandit est ac consequat. Duis imperdiet lectus sem, non pretium orci sollicitudin mollis. Sed condimentum sem eu sapien mattis sagittis. Mauris bibendum, mi in accumsan vulputate, ipsum eros elementum metus, eu fermentum enim lorem vel ante. Maecenas egestas pharetra ipsum, nec malesuada sapien sollicitudin sit amet. Nulla facilisi. Phasellus iaculis turpis dui, eget efficitur justo finibus et. Donec id leo felis.'
		                               				]
		                               			},
		                               			{
		                               				'shortComment': 'Solid, Dependable, Comfortable 9',
		                               				'customerName': 'John S.',
		                               				'date': 'January 25, 2017',
		                               				'rating': 3,
		                               				'size': 12,
		                               				'color': 'Brown',
		                               				'showBtnShowAll': true,
		                               				'showAllComment': false,
		                               				'longComment': [
		                               					'Lorem ipsum dolor sit amet, consectetur adipiscing elit. In pharetra dictum eros, non tempor sem ornare et. Fusce sed arcu non ligula accumsan eleifend. Phasellus vulputate massa et ligula bibendum, id posuere turpis feugiat.',
		                               					'Nam nec posuere eros. Donec rhoncus lacinia pharetra. Morbi scelerisque, ante a porttitor tincidunt, metus lorem sodales mauris, non faucibus eros dolor et mauris. Ut vel placerat diam. Morbi at lorem hendrerit, luctus enim vitae, semper est.',
		                               					'In tempus risus hendrerit purus rhoncus, sed scelerisque mauris luctus. Aliquam erat volutpat. Donec nibh massa, mollis vitae ante eu, tincidunt commodo nisi. Aenean imperdiet blandit est ac consequat. Duis imperdiet lectus sem, non pretium orci sollicitudin mollis. Sed condimentum sem eu sapien mattis sagittis. Mauris bibendum, mi in accumsan vulputate, ipsum eros elementum metus, eu fermentum enim lorem vel ante. Maecenas egestas pharetra ipsum, nec malesuada sapien sollicitudin sit amet. Nulla facilisi. Phasellus iaculis turpis dui, eget efficitur justo finibus et. Donec id leo felis.'
		                               				]
		                               			},
		                               			{
		                               				'shortComment': 'Solid, Dependable, Comfortable 10',
		                               				'customerName': 'John S.',
		                               				'date': 'January 25, 2017',
		                               				'rating': 3,
		                               				'size': 12,
		                               				'color': 'Brown',
		                               				'showBtnShowAll': true,
		                               				'showAllComment': false,
		                               				'longComment': [
		                               					'Lorem ipsum dolor sit amet, consectetur adipiscing elit. In pharetra dictum eros, non tempor sem ornare et. Fusce sed arcu non ligula accumsan eleifend. Phasellus vulputate massa et ligula bibendum, id posuere turpis feugiat.',
		                               					'Nam nec posuere eros. Donec rhoncus lacinia pharetra. Morbi scelerisque, ante a porttitor tincidunt, metus lorem sodales mauris, non faucibus eros dolor et mauris. Ut vel placerat diam. Morbi at lorem hendrerit, luctus enim vitae, semper est.',
		                               					'In tempus risus hendrerit purus rhoncus, sed scelerisque mauris luctus. Aliquam erat volutpat. Donec nibh massa, mollis vitae ante eu, tincidunt commodo nisi. Aenean imperdiet blandit est ac consequat. Duis imperdiet lectus sem, non pretium orci sollicitudin mollis. Sed condimentum sem eu sapien mattis sagittis. Mauris bibendum, mi in accumsan vulputate, ipsum eros elementum metus, eu fermentum enim lorem vel ante. Maecenas egestas pharetra ipsum, nec malesuada sapien sollicitudin sit amet. Nulla facilisi. Phasellus iaculis turpis dui, eget efficitur justo finibus et. Donec id leo felis.'
		                               				]
		                               			}
		                               		];
		                               		
   		//Customer Reviews & Comments
   		$scope.customerReviewsAndRatingsList = [];
   		angular.copy($scope.product.customerReviewAndRatings, $scope.customerReviewsAndRatingsList);
   		$scope.customerReviewsAndRatingsList = utilityFactory.splitArray($scope.customerReviewsAndRatingsList, 3);
   		$scope.customerReviewsAndRatingsPage = [];
   		$scope.customerReviewsAndRatingsIndex = 0;
   		
   		$scope.generateReviewsAndRatingsIndexPage = function(customerReviewsAndRatingsIndex){
   			$scope.customerReviewsAndRatingsPage = utilityFactory.generatePagination($scope.customerReviewsAndRatingsPage, $scope.customerReviewsAndRatingsList, customerReviewsAndRatingsIndex);
   		};
   		$scope.generateReviewsAndRatingsIndexPage($scope.customerReviewsAndRatingsIndex);
   		
   		$scope.visibleCustomerReviewsAndRatingsList = $scope.customerReviewsAndRatingsList[$scope.customerReviewsAndRatingsIndex];
   		$scope.displayCurrentRating = function(rating){
   			return utilityFactory.displayCurrentRating(rating);
   		}
   		
   		$scope.displayUnratedRating = function(rating){
   			return utilityFactory.displayUnratedRating(rating);
   		}
   		
   		$scope.starRating = [
   			{'id':'1', 'value': '4 Stars & Up'},
   		    {'id':'2', 'value': '3 Stars & Up'},
   		    {'id':'3', 'value': '2 Stars & Up'},
   		    {'id':'4', 'value': '1 Stars & Up'}
   		];
   		$scope.selectedStarRating = $scope.starRating[0];
   		
   		//Customer Reviews & Comments
   		$scope.showMoreComment = function(customerReviewAndRating){
   			customerReviewAndRating.showAllComment = true;
   			customerReviewAndRating.showBtnShowAll = !customerReviewAndRating.showBtnShowAll;
   		}
   		
   		$scope.incrementPageIndex = function(index, product){
   			returnObject = utilityFactory.incrementPageIndex($scope[""+index], $scope.customerReviewsAndRatingsPage, $scope.customerReviewsAndRatingsList);
   			$scope.customerReviewsAndRatingsIndex = returnObject[0];
   			$scope.customerReviewsAndRatingsPage = returnObject[1];
   			$scope.visibleCustomerReviewsAndRatingsList = $scope.customerReviewsAndRatingsList[$scope.customerReviewsAndRatingsIndex];
   		};
   		
   		$scope.decrementPageIndex = function(index, product){
   			returnObject = utilityFactory.decrementPageIndex($scope[""+index], $scope.customerReviewsAndRatingsPage, $scope.customerReviewsAndRatingsList);
   			$scope.customerReviewsAndRatingsIndex = returnObject[0];
   			$scope.customerReviewsAndRatingsPage = returnObject[1];
   			$scope.visibleCustomerReviewsAndRatingsList = $scope.customerReviewsAndRatingsList[$scope.customerReviewsAndRatingsIndex];
   		};
   		
   		$scope.updatePageIndex = function(index, value, product){
   			returnObject = utilityFactory.updatePageIndex($scope[""+index], $scope.customerReviewsAndRatingsPage, $scope.customerReviewsAndRatingsList, value);
   			$scope.customerReviewsAndRatingsIndex = returnObject[0];
   			$scope.customerReviewsAndRatingsPage = returnObject[1];
   			$scope.visibleCustomerReviewsAndRatingsList = $scope.customerReviewsAndRatingsList[$scope.customerReviewsAndRatingsIndex];
   		};
	}
	
	
	
}]);