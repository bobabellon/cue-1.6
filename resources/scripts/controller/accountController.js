var app = angular.module('accountController', []);

app.controller("accountController", ['$scope','config','$cookieStore','$rootScope','$window',
	function($scope, config, $cookieStore, $rootScope,$window){
	$rootScope.user = JSON.parse($window.localStorage.getItem('user'));
	$scope.showAccountMenu = function(){
		document.getElementById("account-sidenav").style.width = "75%";
		document.getElementsByTagName("body")[0].style.overflowY = 'hidden';
	};
	
	$scope.hideAccountMenu = function(){
		document.getElementById("account-sidenav").style.width = "0px";
		document.getElementsByTagName("body")[0].style.overflowY = 'auto';
	};
	
	$scope.closeAccountMenu = function(){
		var $win = $(window); // or $box parent container
		$win.on("click.Bst", function(event){
			 var $box = $(".account-container");
			 var $acctBtn = $(".acct-btn");
			if ( 
			    $box.has(event.target).length == 0 //checks if descendants of $box was clicked
			    &&
			    !$box.is(event.target) //checks if the $box itself was clicked
			  ){
					if ( 
					    $acctBtn.has(event.target).length == 0 //checks if descendants of $box was clicked
					    &&
					    !$acctBtn.is(event.target) //checks if the $box itself was clicked
					  ){
							document.getElementById("account-sidenav").style.width = "0px";
							document.getElementsByTagName("body")[0].style.overflowY = 'auto';
						}
				}
		});
	}
	$scope.closeAccountMenu();
}])