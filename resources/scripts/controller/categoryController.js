var app = angular.module('categoryController', ['productService',
												'categoryService']);

app.controller("categoryController", ['$scope','productService','config','$cookieStore','$rootScope','categoryService',
	function($scope, productService, config, $cookieStore, $rootScope, categoryService){
	
	//Initialization
	$scope.productList = [];
	$scope.sortByDropdownList = [];
	$scope.perPageDropdownList = [5,10,15,20,25,30,35,40];
//	$rootScope.currentPage = 0;
	if($rootScope.currentPage == 0 || $rootScope.currentPage == undefined){
		$rootScope.currentPage = 0;		
	}
	$scope.totalPagesList = [];
	
	$scope.initializeProducts = function(){
		var url = "";
		if($scope.selectedItemPerPage == undefined && $scope.selectedSortBy == undefined){
			url = config.baseUrl + config.productPath + "search?query=" + $cookieStore.get('categoryId') + "&currentPage=" + $rootScope.currentPage;
		} else {
			url = config.baseUrl + config.productPath + "search?query=" + $cookieStore.get('categoryId') +
					"&pageSize=" + $scope.selectedProductPerPage + "&sort=" + $scope.selectedSortBy.code + "&currentPage=" + $rootScope.currentPage;
		}
		productService.searchProduct(url).then(
			function success(result){
				$rootScope.suggestedProducts = [];
				$scope.searchedProducts = result.data.products;
				$scope.productList = $scope.searchedProducts;

				console.log(result.data);
				
				var fields = ['images','code'];
				
				//Get Product Category
				categoryService.getProductCategory($cookieStore.get('categoryId')).then(
					function success(result){
						$scope.productCategory = result.data.name;
						
						//Get images
						angular.forEach($scope.productList, function(product) {
							//Get Product
							var imgUrl = config.baseUrl + config.productPath + product.code + "?fields=" + fields;
							productService.getProduct(imgUrl).then(
								function success(result){
									product.image = config.baseUrl + result.data.images[0].url;
									
									//modify
									product.rating = 4;
									product.color = 'Black',
									product.colorAvailable = ['Black','Yellow','Brown'];
								},
								function error(result){
									console.log(result);
							});
						});
					},
					function error(result){
						console.log(result);
				});

				//Get & initialize sorts
				$scope.sortByDropdownList = result.data.sorts;
				angular.forEach($scope.sortByDropdownList, function(value) {
					if(value.selected){
						$scope.selectedSortBy = value;
					}
				});
				
				//Initialize selected per page
				angular.forEach($scope.perPageDropdownList, function(value) {
					if(value == result.data.pagination.pageSize){
						$scope.selectedProductPerPage = value;
					}
				});
				
				//Initialize Pagination
				$scope.totalPages = result.data.pagination.totalPages;
				$scope.totalPagesList = [];
				
				var lowCounter = 2;
				var lowIndex = angular.copy($rootScope.currentPage);
				
				while (lowCounter >= 1) {
				    if((lowIndex - lowCounter) >= 0){
				    	$scope.totalPagesList.push(lowIndex - lowCounter);
				    };
				    lowCounter--;
				};
				$scope.totalPagesList.push($rootScope.currentPage);
				var highCounter = 1;
				var highIndex = angular.copy($rootScope.currentPage);
				
				while (highCounter < 3) {
				    if((highIndex + highCounter) > $rootScope.currentPage && (highIndex + highCounter) < $scope.totalPages){
				    	$scope.totalPagesList.push(highIndex + highCounter);
				    };
				    highCounter++;
				};
				
			},
			function error(result){
				console.log(result);
				console.log("Error!");
		});
	};
	$scope.initializeProducts();
	
	$scope.changePage = function(pageNumber){
		$rootScope.currentPage = pageNumber;
		$scope.initializeProducts();
	};
	
	
	
	
	
	
	$scope.colorFilter = [{'name':'Biege', 'selected': false, 'type': 'color'},
						  {'name':'Black', 'selected': false, 'type': 'color'},
						  {'name':'Blue', 'selected': false, 'type': 'color'},
						  {'name':'Brown', 'selected': false, 'type': 'color'},
						  {'name':'Olive', 'selected': false, 'type': 'color'},
						  {'name':'Wheat', 'selected': false, 'type': 'color'},
						  {'name':'Yellow', 'selected': false, 'type': 'color'}];
	
	$scope.priceFilter = [{'name':'$25-$50', 'value': [25,50], 'selected': false, 'type': 'price'},
		  				  {'name':'$51-$100', 'value': [51,100], 'selected': false, 'type': 'price'},
		  				  {'name':'$100+', 'value': [101,9999999],'selected': false, 'type': 'price'}];
	
	$scope.fitFilter = [{'name':'Medium', 'selected': false, 'type': 'fit'},
		  				{'name':'Wide', 'selected': false, 'type': 'fit'}];
	
	$scope.genderFilter = [{'name':"Men's", 'selected': false, 'type': 'gender'},
						   {'name':"Women's", 'selected': false, 'type': 'gender'}];
	
	$scope.sizeFilter = [{'name':'5', 'selected': false, 'type': 'size'},
					     {'name':'5.5', 'selected': false, 'type': 'size'},
					     {'name':'6', 'selected': false, 'type': 'size'},
					     {'name':'6.5', 'selected': false, 'type': 'size'},
					     {'name':'13.5', 'selected': false, 'type': 'size'},
					     {'name':'14.5', 'selected': false, 'type': 'size'}];
	
	$scope.selectedFilter = [];
	$scope.selectedFilterPerRow = [];
	
	$scope.addFilter = function(filter, selected){
		if(filter.type == "size"){
			if(filter.name.substring(0, 4) != "Size"){
				filter.name = "Size " + filter.name;
			}
		}
		var index = -1;
		angular.forEach($scope.selectedFilter, function(value) {
			if(value.name == filter.name){
				index = $scope.selectedFilter.indexOf(value)
			}
		});
		//Add
	    if(index == -1 && selected){
	    	$scope.selectedFilter.push(filter);
	    	if(filter.type == "price"){
				var prices = [];
				angular.forEach($scope.selectedFilter, function(filter) {
					if(filter.type == "price"){
						angular.forEach(filter.value, function(value) {
			  				prices.push(value);
						});
					}
				});
				$scope.filterProductByPrice(prices);
	    	};
	      
	    //Remove
	    } else if (!selected && index != -1){
	      $scope.selectedFilter.splice(index, 1);
	      filter.selected = false;
	      
	      if(filter.type == "color"){
	    	  angular.forEach($scope.colorFilter, function(value) {
	  			if(value.name == filter.name){
	  				value.selected = false;
	  			}
	  		});
	      }
	      
	      if(filter.type == "price"){
	    	  angular.forEach($scope.priceFilter, function(value) {
	  			if(value.name == filter.name){
	  				value.selected = false;
	  			};
	  			
	  			var prices = [];
	  			angular.forEach($scope.selectedFilter, function(filter) {
					if(filter.type == "price"){
						angular.forEach(filter.value, function(value) {
			  				prices.push(value);
						});
					};
				});
	  			if(prices.length > 0){
	  				$scope.filterProductByPrice(prices);
	  			} else {
	  				$scope.productList = $scope.searchedProducts;
	  			};
	  		});
	      };
	      
	      if(filter.type == "fit"){
	    	  angular.forEach($scope.fitFilter, function(value) {
	  			if(value.name == filter.name){
	  				value.selected = false;
	  			}
	  		});
	      }
	      
	      if(filter.type == "gender"){
	    	  angular.forEach($scope.genderFilter, function(value) {
	  			if(value.name == filter.name){
	  				value.selected = false;
	  			}
	  		});
	      }
	      
	      if(filter.type == "size"){
	    	  angular.forEach($scope.sizeFilter, function(value) {
	  			if(value.name == filter.name){
	  				value.selected = false;
	  			}
	  		});
	      }
	      
	      if(filter.name.substring(0, 4) == "Size"){
				filter.name = filter.name.substring(5);
			}
	    }
//	    $scope.selectedFilterPerRow = $scope.generateFilterPerRow();
	}
	
	$scope.filterProductByPrice = function(prices){
		var minPrice = Array.min(prices);
		var maxPrice = Array.max(prices);
		
		var filteredProductList = [];
		var copiedProductList = angular.copy($scope.searchedProducts);
		angular.forEach(copiedProductList, function(product) {
			if(product.price.value >= minPrice && product.price.value <= maxPrice){
				filteredProductList.push(product);
			};
		});
		$scope.productList = filteredProductList;
	};
	
	Array.min = function( array ){
	    return Math.min.apply( Math, array );
	};
	
	Array.max = function( array ){
	    return Math.max.apply( Math, array );
	};
	
	$scope.selectColor = function(product, color){
		product.color = color;
	};
	
	$scope.itemOnHover = function(index, bool) {
	    if(bool === true) {
	        $scope["hoverEffect"+ index] = "item-hover-effect";
	    } else if (bool === false) {
	    	$scope["hoverEffect"+ index] = "";
	    }
	};
	
	$scope.displayCurrentRating = function(rating){
		var totalRatings = [];
		for(var i = 0; i < rating; i++){
			totalRatings.push(i);
		}
		return totalRatings;
	};
	
	$scope.displayUnratedRating = function(rating){
		rating = 5 - rating;
		var totalRatings = [];
		for(var i = 0; i < rating; i++){
			totalRatings.push(i);
		}
		return totalRatings;
	};
	
	$scope.modalTrigger = "";
	$scope.toggleCategoryFilterModal = function(condition){
		if(condition === true){
			$scope.modalTrigger = "is-active";
		} else {
			$scope.modalTrigger = "";
		}
	};
	
	$scope.isDisplayOptionsOpen = false;
	$scope.selectedDisplayOption = 2;
	
	$scope.toggleDisplayOptions = function(){
		$scope.isDisplayOptionsOpen = !$scope.isDisplayOptionsOpen;
	};
	
	$scope.selectDisplay = function(num){
		$scope.selectedDisplayOption = num;
		$scope.isDisplayOptionsOpen = false;
	};
	
	//Navigation
	$scope.goToHomePage = function(){
		$state.go('base.home');
	};
}])