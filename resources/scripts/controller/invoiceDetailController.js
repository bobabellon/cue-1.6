var app = angular.module('invoiceDetailController', [ 'datatables', 'datatables.colreorder', 'pdf', 'productService']);

app.controller("invoiceDetailController", ['$scope', '$rootScope', '$cookieStore','config','$window', 'DTOptionsBuilder', 'DTColumnDefBuilder', '$compile', '$timeout', 'pdfDelegate', 'utilityFactory', '$state', '$stateParams', 'productService', 
	function($scope, $rootScope, $cookieStore, config, $window, DTOptionsBuilder, DTColumnDefBuilder, $compile, $timeout, pdfDelegate, utilityFactory, $state, $stateParams, productService){
	//datatables Init
	var vm = this;
	vm.dtInstance = {};
	vm.dtOptions = DTOptionsBuilder.newOptions()
        // Activate col reorder plugin
        .withOption('responsive', true)
        .withOption('paging', false)
        .withOption('info', false)
        .withColReorder()
        .withColReorderOption('iFixedColumnsLeft', 1)
    vm.dtColumnDefs = [
        DTColumnDefBuilder.newColumnDef(0).notSortable(),
        DTColumnDefBuilder.newColumnDef(1).notSortable()
    ];	

	$scope.invoiceId = $stateParams.invoiceId;
	$scope.resetSort = function(element){
		var counter = 0;		
		vm.dtInstance.DataTable.on('order.dt', function(){
			if(vm.dtInstance.DataTable.settings().order().length === 1 && counter == 0){
	            let order = vm.dtInstance.DataTable.settings().order()[0],
	            	th = $("invoice-detail-table th:eq(" + order[0] + ")");
	            if(th.attr("data-sort-next") === "false"){
	            	vm.dtInstance.DataTable.order([]).draw();
	                th.removeAttr("data-sort-next");
	            }else{
	            	th.attr("data-sort-next", order[1] !== "desc");
	            }
	            $("invoice-detail-table th").each(function(k, v){
	            	if(k !== order[0]){
	                	$("invoice-detail-table th:eq(" + k + ")").removeAttr("data-sort-next")
	                }
	            });
	            counter++
	        }
	    });
	}
	$scope.retrieveInvoiceDetail = function(){
		invoiceList = utilityFactory.getSelectedInvoices();
		$scope.invoice = invoiceList[0];
	}
	$scope.billAccount = {'name': 'Maria Smith', 'address1': '5241 East Anderson Ave.', 'address2': 'Wayne PA 19080'};
	
	$scope.retrieveInvoiceDetail();
	$scope.getInvoiceItems = function(){
			$scope.invoiceItemList =  {"entries" : [
			                                  {
			                                      "productCode": "3881065",
			                                      "productQuantity": 1
			                                    },
			                                    {
			                                      "productCode": "26038000_5",
			                                      "productQuantity": 1
			                                    },
			                                    {
			                                      "productCode": "33034000_63",
			                                      "productQuantity": 1
			                                    },
			                                    {
			                                      "productCode": "3592865",
			                                      "productQuantity": 2
			                                    },
			                                    {
			                                      "productCode": "33031000_1",
			                                      "productQuantity": 1
			                                    }
			                                  ]}
			if($scope.invoiceItemList.entries != undefined){
				utilityFactory.getProjectItemDetails($scope.invoiceItemList).then(function(result){
					$scope.invoiceItems = result.items;						
					angular.forEach($scope.invoiceItems, function(value) {
						value.options = [];
						value.itemNumber.indexOf('_') >= 0 ? value.hasOptions = true : value.hasOptions = false;
						if(value.categories.length != 0 && value.categories.length != undefined){
							angular.forEach(value.categories, function(option) {
								if(option.code.indexOf('B2B_') >= 0){
									value.options.push((option.code.split("B2B_").pop()).replace(/_/g , "."));
								}
							})						
						}
				    	console.log($scope.invoiceItems)
					});			
				});
			}
		
	}
	$scope.getInvoiceItems();
	
	//MODALS
	$scope.modalTrigger = "";
	$scope.toggleOrderDetailsModal = function(condition, index){
		if(condition === true){
			$scope.modalTrigger = "is-active";	
			initProductDetails(index);
		} else {
			$scope.modalTrigger = "";
		}
	};
	
	$scope.showOrderDetailsMobile = function(index){
		initProductDetails(index);
		document.getElementById("order-detail-mobile").style.height = "100%";
		document.getElementsByTagName("body")[0].style.overflowY = 'hidden';
		document.getElementById("overlay").style.display = "block";
	};
	
	$scope.hideOrderDetailsMobile = function(){
		document.getElementById("order-detail-mobile").style.height = "0px";
		document.getElementsByTagName("body")[0].style.overflowY = 'auto';
		document.getElementById("overlay").style.display = "none";
	};
	
	$scope.selectColor = function(product, color){
		product.color = color;
		for(i in $scope.imageList){
			if(color == $scope.imageList[i].code){
				product.image = $scope.imageList[i].url;
			}
		}
	};
	
	function initProductDetails(index){		
		$scope.product = index;
		$scope.hasColor = false;
		$scope.hasFit = false;
		$scope.hasSize = false;
		$scope.imageList = [];
		var productOptionList = []
		
		//Modify
		$scope.product.product = {}
		$scope.product.product.code = $scope.product.itemNumber;
		$scope.product.product.name = $scope.product.name;
		$scope.product.rating = 3;
		
		if($scope.product.variantOptions != undefined){
			$scope.hasColor = true;
			$scope.hasFit = true;
			$scope.hasSize = true;
		}					
		//Get other Fields
		var url = config.baseUrl + config.productPath + $scope.product.product.code + "?fields=images,categories";
		productService.getProduct(url).then(
			function success(result){
				if(result.data.categories != undefined){
					for(i in result.data.categories){
						if(result.data.categories[i].code.indexOf('B2B_') >= 0 && $scope.product.product.code.indexOf('_')){
							$scope.imageList.push(
									{
									"code" : $scope.product.product.code,
									"url" : config.baseUrl + result.data.images[0].url,
									"color" : result.data.categories[2].code.split("_").pop()
									});
							if(result.data.categories[0].code.indexOf('B2B_') >= 0){
								$scope.product.size = (result.data.categories[0].code.split("B2B_").pop()).replace(/_/g , ".");
							}
							break;
						}
					}
				}else{
					$scope.imageList.push(
							{
							"code" : value.code,
							"url" : config.baseUrl + result.data.images[0].url
							});
				}
			},
			function error(result){
				console.log(result);
		});	
		
		
		$scope.product.customerReviewAndRatings = [
			{
				'shortComment': 'Solid, Dependable, Comfortable 1',
				'customerName': 'John S.',
				'date': 'January 25, 2017',
				'rating': 5,
				'size': 12,
				'color': 'Brown',
				'showBtnShowAll': true,
				'showAllComment': false,
				'longComment': [
					'Lorem ipsum dolor sit amet, consectetur adipiscing elit. In pharetra dictum eros, non tempor sem ornare et. Fusce sed arcu non ligula accumsan eleifend. Phasellus vulputate massa et ligula bibendum, id posuere turpis feugiat.',
					'Nam nec posuere eros. Donec rhoncus lacinia pharetra. Morbi scelerisque, ante a porttitor tincidunt, metus lorem sodales mauris, non faucibus eros dolor et mauris. Ut vel placerat diam. Morbi at lorem hendrerit, luctus enim vitae, semper est.',
					'In tempus risus hendrerit purus rhoncus, sed scelerisque mauris luctus. Aliquam erat volutpat. Donec nibh massa, mollis vitae ante eu, tincidunt commodo nisi. Aenean imperdiet blandit est ac consequat. Duis imperdiet lectus sem, non pretium orci sollicitudin mollis. Sed condimentum sem eu sapien mattis sagittis. Mauris bibendum, mi in accumsan vulputate, ipsum eros elementum metus, eu fermentum enim lorem vel ante. Maecenas egestas pharetra ipsum, nec malesuada sapien sollicitudin sit amet. Nulla facilisi. Phasellus iaculis turpis dui, eget efficitur justo finibus et. Donec id leo felis.'
				]
			},
			{
				'shortComment': 'Solid, Dependable, Comfortable 2',
				'customerName': 'John S.',
				'date': 'January 25, 2017',
				'rating': 3,
				'size': 12,
				'color': 'Brown',
				'showBtnShowAll': true,
				'showAllComment': false,
				'longComment': [
					'Lorem ipsum dolor sit amet, consectetur adipiscing elit. In pharetra dictum eros, non tempor sem ornare et. Fusce sed arcu non ligula accumsan eleifend. Phasellus vulputate massa et ligula bibendum, id posuere turpis feugiat.',
					'Nam nec posuere eros. Donec rhoncus lacinia pharetra. Morbi scelerisque, ante a porttitor tincidunt, metus lorem sodales mauris, non faucibus eros dolor et mauris. Ut vel placerat diam. Morbi at lorem hendrerit, luctus enim vitae, semper est.',
					'In tempus risus hendrerit purus rhoncus, sed scelerisque mauris luctus. Aliquam erat volutpat. Donec nibh massa, mollis vitae ante eu, tincidunt commodo nisi. Aenean imperdiet blandit est ac consequat. Duis imperdiet lectus sem, non pretium orci sollicitudin mollis. Sed condimentum sem eu sapien mattis sagittis. Mauris bibendum, mi in accumsan vulputate, ipsum eros elementum metus, eu fermentum enim lorem vel ante. Maecenas egestas pharetra ipsum, nec malesuada sapien sollicitudin sit amet. Nulla facilisi. Phasellus iaculis turpis dui, eget efficitur justo finibus et. Donec id leo felis.'
				]
			},
			{
				'shortComment': 'Solid, Dependable, Comfortable 3',
				'customerName': 'John S.',
				'date': 'January 25, 2017',
				'rating': 0,
				'size': 12,
				'color': 'Brown',
				'showBtnShowAll': false,
				'showAllComment': true,
				'longComment': [
					'Lorem ipsum dolor sit amet, consectetur adipiscing elit. In pharetra dictum eros, non tempor sem ornare et. Fusce sed arcu non ligula accumsan eleifend. Phasellus vulputate massa et ligula bibendum, id posuere turpis feugiat.'
					]
			},
			{
				'shortComment': 'Solid, Dependable, Comfortable 4',
				'customerName': 'John S.',
				'date': 'January 25, 2017',
				'rating': 3,
				'size': 12,
				'color': 'Brown',
				'showBtnShowAll': true,
				'showAllComment': false,
				'longComment': [
					'Lorem ipsum dolor sit amet, consectetur adipiscing elit. In pharetra dictum eros, non tempor sem ornare et. Fusce sed arcu non ligula accumsan eleifend. Phasellus vulputate massa et ligula bibendum, id posuere turpis feugiat.',
					'Nam nec posuere eros. Donec rhoncus lacinia pharetra. Morbi scelerisque, ante a porttitor tincidunt, metus lorem sodales mauris, non faucibus eros dolor et mauris. Ut vel placerat diam. Morbi at lorem hendrerit, luctus enim vitae, semper est.',
					'In tempus risus hendrerit purus rhoncus, sed scelerisque mauris luctus. Aliquam erat volutpat. Donec nibh massa, mollis vitae ante eu, tincidunt commodo nisi. Aenean imperdiet blandit est ac consequat. Duis imperdiet lectus sem, non pretium orci sollicitudin mollis. Sed condimentum sem eu sapien mattis sagittis. Mauris bibendum, mi in accumsan vulputate, ipsum eros elementum metus, eu fermentum enim lorem vel ante. Maecenas egestas pharetra ipsum, nec malesuada sapien sollicitudin sit amet. Nulla facilisi. Phasellus iaculis turpis dui, eget efficitur justo finibus et. Donec id leo felis.'
				]
			},
			{
				'shortComment': 'Solid, Dependable, Comfortable 5',
				'customerName': 'John S.',
				'date': 'January 25, 2017',
				'rating': 3,
				'size': 12,
				'color': 'Brown',
				'showBtnShowAll': true,
				'showAllComment': false,
				'longComment': [
					'Lorem ipsum dolor sit amet, consectetur adipiscing elit. In pharetra dictum eros, non tempor sem ornare et. Fusce sed arcu non ligula accumsan eleifend. Phasellus vulputate massa et ligula bibendum, id posuere turpis feugiat.',
					'Nam nec posuere eros. Donec rhoncus lacinia pharetra. Morbi scelerisque, ante a porttitor tincidunt, metus lorem sodales mauris, non faucibus eros dolor et mauris. Ut vel placerat diam. Morbi at lorem hendrerit, luctus enim vitae, semper est.',
					'In tempus risus hendrerit purus rhoncus, sed scelerisque mauris luctus. Aliquam erat volutpat. Donec nibh massa, mollis vitae ante eu, tincidunt commodo nisi. Aenean imperdiet blandit est ac consequat. Duis imperdiet lectus sem, non pretium orci sollicitudin mollis. Sed condimentum sem eu sapien mattis sagittis. Mauris bibendum, mi in accumsan vulputate, ipsum eros elementum metus, eu fermentum enim lorem vel ante. Maecenas egestas pharetra ipsum, nec malesuada sapien sollicitudin sit amet. Nulla facilisi. Phasellus iaculis turpis dui, eget efficitur justo finibus et. Donec id leo felis.'
				]
			},
			{
				'shortComment': 'Solid, Dependable, Comfortable 6',
				'customerName': 'John S.',
				'date': 'January 25, 2017',
				'rating': 3,
				'size': 12,
				'color': 'Brown',
				'showBtnShowAll': true,
				'showAllComment': false,
				'longComment': [
					'Lorem ipsum dolor sit amet, consectetur adipiscing elit. In pharetra dictum eros, non tempor sem ornare et. Fusce sed arcu non ligula accumsan eleifend. Phasellus vulputate massa et ligula bibendum, id posuere turpis feugiat.',
					'Nam nec posuere eros. Donec rhoncus lacinia pharetra. Morbi scelerisque, ante a porttitor tincidunt, metus lorem sodales mauris, non faucibus eros dolor et mauris. Ut vel placerat diam. Morbi at lorem hendrerit, luctus enim vitae, semper est.',
					'In tempus risus hendrerit purus rhoncus, sed scelerisque mauris luctus. Aliquam erat volutpat. Donec nibh massa, mollis vitae ante eu, tincidunt commodo nisi. Aenean imperdiet blandit est ac consequat. Duis imperdiet lectus sem, non pretium orci sollicitudin mollis. Sed condimentum sem eu sapien mattis sagittis. Mauris bibendum, mi in accumsan vulputate, ipsum eros elementum metus, eu fermentum enim lorem vel ante. Maecenas egestas pharetra ipsum, nec malesuada sapien sollicitudin sit amet. Nulla facilisi. Phasellus iaculis turpis dui, eget efficitur justo finibus et. Donec id leo felis.'
				]
			},
			{
				'shortComment': 'Solid, Dependable, Comfortable 7',
				'customerName': 'John S.',
				'date': 'January 25, 2017',
				'rating': 3,
				'size': 12,
				'color': 'Brown',
				'showBtnShowAll': true,
				'showAllComment': false,
				'longComment': [
					'Lorem ipsum dolor sit amet, consectetur adipiscing elit. In pharetra dictum eros, non tempor sem ornare et. Fusce sed arcu non ligula accumsan eleifend. Phasellus vulputate massa et ligula bibendum, id posuere turpis feugiat.',
					'Nam nec posuere eros. Donec rhoncus lacinia pharetra. Morbi scelerisque, ante a porttitor tincidunt, metus lorem sodales mauris, non faucibus eros dolor et mauris. Ut vel placerat diam. Morbi at lorem hendrerit, luctus enim vitae, semper est.',
					'In tempus risus hendrerit purus rhoncus, sed scelerisque mauris luctus. Aliquam erat volutpat. Donec nibh massa, mollis vitae ante eu, tincidunt commodo nisi. Aenean imperdiet blandit est ac consequat. Duis imperdiet lectus sem, non pretium orci sollicitudin mollis. Sed condimentum sem eu sapien mattis sagittis. Mauris bibendum, mi in accumsan vulputate, ipsum eros elementum metus, eu fermentum enim lorem vel ante. Maecenas egestas pharetra ipsum, nec malesuada sapien sollicitudin sit amet. Nulla facilisi. Phasellus iaculis turpis dui, eget efficitur justo finibus et. Donec id leo felis.'
				]
			},
			{
				'shortComment': 'Solid, Dependable, Comfortable 8',
				'customerName': 'John S.',
				'date': 'January 25, 2017',
				'rating': 3,
				'size': 12,
				'color': 'Brown',
				'showBtnShowAll': true,
				'showAllComment': false,
				'longComment': [
					'Lorem ipsum dolor sit amet, consectetur adipiscing elit. In pharetra dictum eros, non tempor sem ornare et. Fusce sed arcu non ligula accumsan eleifend. Phasellus vulputate massa et ligula bibendum, id posuere turpis feugiat.',
					'Nam nec posuere eros. Donec rhoncus lacinia pharetra. Morbi scelerisque, ante a porttitor tincidunt, metus lorem sodales mauris, non faucibus eros dolor et mauris. Ut vel placerat diam. Morbi at lorem hendrerit, luctus enim vitae, semper est.',
					'In tempus risus hendrerit purus rhoncus, sed scelerisque mauris luctus. Aliquam erat volutpat. Donec nibh massa, mollis vitae ante eu, tincidunt commodo nisi. Aenean imperdiet blandit est ac consequat. Duis imperdiet lectus sem, non pretium orci sollicitudin mollis. Sed condimentum sem eu sapien mattis sagittis. Mauris bibendum, mi in accumsan vulputate, ipsum eros elementum metus, eu fermentum enim lorem vel ante. Maecenas egestas pharetra ipsum, nec malesuada sapien sollicitudin sit amet. Nulla facilisi. Phasellus iaculis turpis dui, eget efficitur justo finibus et. Donec id leo felis.'
				]
			},
			{
				'shortComment': 'Solid, Dependable, Comfortable 9',
				'customerName': 'John S.',
				'date': 'January 25, 2017',
				'rating': 3,
				'size': 12,
				'color': 'Brown',
				'showBtnShowAll': true,
				'showAllComment': false,
				'longComment': [
					'Lorem ipsum dolor sit amet, consectetur adipiscing elit. In pharetra dictum eros, non tempor sem ornare et. Fusce sed arcu non ligula accumsan eleifend. Phasellus vulputate massa et ligula bibendum, id posuere turpis feugiat.',
					'Nam nec posuere eros. Donec rhoncus lacinia pharetra. Morbi scelerisque, ante a porttitor tincidunt, metus lorem sodales mauris, non faucibus eros dolor et mauris. Ut vel placerat diam. Morbi at lorem hendrerit, luctus enim vitae, semper est.',
					'In tempus risus hendrerit purus rhoncus, sed scelerisque mauris luctus. Aliquam erat volutpat. Donec nibh massa, mollis vitae ante eu, tincidunt commodo nisi. Aenean imperdiet blandit est ac consequat. Duis imperdiet lectus sem, non pretium orci sollicitudin mollis. Sed condimentum sem eu sapien mattis sagittis. Mauris bibendum, mi in accumsan vulputate, ipsum eros elementum metus, eu fermentum enim lorem vel ante. Maecenas egestas pharetra ipsum, nec malesuada sapien sollicitudin sit amet. Nulla facilisi. Phasellus iaculis turpis dui, eget efficitur justo finibus et. Donec id leo felis.'
				]
			},
			{
				'shortComment': 'Solid, Dependable, Comfortable 10',
				'customerName': 'John S.',
				'date': 'January 25, 2017',
				'rating': 3,
				'size': 12,
				'color': 'Brown',
				'showBtnShowAll': true,
				'showAllComment': false,
				'longComment': [
					'Lorem ipsum dolor sit amet, consectetur adipiscing elit. In pharetra dictum eros, non tempor sem ornare et. Fusce sed arcu non ligula accumsan eleifend. Phasellus vulputate massa et ligula bibendum, id posuere turpis feugiat.',
					'Nam nec posuere eros. Donec rhoncus lacinia pharetra. Morbi scelerisque, ante a porttitor tincidunt, metus lorem sodales mauris, non faucibus eros dolor et mauris. Ut vel placerat diam. Morbi at lorem hendrerit, luctus enim vitae, semper est.',
					'In tempus risus hendrerit purus rhoncus, sed scelerisque mauris luctus. Aliquam erat volutpat. Donec nibh massa, mollis vitae ante eu, tincidunt commodo nisi. Aenean imperdiet blandit est ac consequat. Duis imperdiet lectus sem, non pretium orci sollicitudin mollis. Sed condimentum sem eu sapien mattis sagittis. Mauris bibendum, mi in accumsan vulputate, ipsum eros elementum metus, eu fermentum enim lorem vel ante. Maecenas egestas pharetra ipsum, nec malesuada sapien sollicitudin sit amet. Nulla facilisi. Phasellus iaculis turpis dui, eget efficitur justo finibus et. Donec id leo felis.'
				]
			}
		];
		
		//Customer Reviews & Comments
		$scope.customerReviewsAndRatingsList = [];
		angular.copy($scope.product.customerReviewAndRatings, $scope.customerReviewsAndRatingsList);
		$scope.customerReviewsAndRatingsList = utilityFactory.splitArray($scope.customerReviewsAndRatingsList, 3);
		$scope.customerReviewsAndRatingsPage = [];
		$scope.customerReviewsAndRatingsIndex = 0;
		
		$scope.generateReviewsAndRatingsIndexPage = function(customerReviewsAndRatingsIndex){
			$scope.customerReviewsAndRatingsPage = utilityFactory.generatePagination($scope.customerReviewsAndRatingsPage, $scope.customerReviewsAndRatingsList, customerReviewsAndRatingsIndex);
		};
		$scope.generateReviewsAndRatingsIndexPage($scope.customerReviewsAndRatingsIndex);
		
		$scope.visibleCustomerReviewsAndRatingsList = $scope.customerReviewsAndRatingsList[$scope.customerReviewsAndRatingsIndex];
	};
	
	
	$scope.displayCurrentRating = function(rating){
		return utilityFactory.displayCurrentRating(rating);
	}
	
	$scope.displayUnratedRating = function(rating){
		return utilityFactory.displayUnratedRating(rating);
	}
	
	$scope.starRating = [
		{'id':'1', 'value': '4 Stars & Up'},
	    {'id':'2', 'value': '3 Stars & Up'},
	    {'id':'3', 'value': '2 Stars & Up'},
	    {'id':'4', 'value': '1 Stars & Up'}
	];
	$scope.selectedStarRating = $scope.starRating[0];
	
	//Customer Reviews & Comments
	$scope.showMoreComment = function(customerReviewAndRating){
		customerReviewAndRating.showAllComment = true;
		customerReviewAndRating.showBtnShowAll = !customerReviewAndRating.showBtnShowAll;
	}
	
	$scope.incrementPageIndex = function(index, product){
		returnObject = utilityFactory.incrementPageIndex($scope[""+index], $scope.customerReviewsAndRatingsPage, $scope.customerReviewsAndRatingsList);
		$scope.customerReviewsAndRatingsIndex = returnObject[0];
		$scope.customerReviewsAndRatingsPage = returnObject[1];
		$scope.visibleCustomerReviewsAndRatingsList = $scope.customerReviewsAndRatingsList[$scope.customerReviewsAndRatingsIndex];
	};
	
	$scope.decrementPageIndex = function(index, product){
		returnObject = utilityFactory.decrementPageIndex($scope[""+index], $scope.customerReviewsAndRatingsPage, $scope.customerReviewsAndRatingsList);
		$scope.customerReviewsAndRatingsIndex = returnObject[0];
		$scope.customerReviewsAndRatingsPage = returnObject[1];
		$scope.visibleCustomerReviewsAndRatingsList = $scope.customerReviewsAndRatingsList[$scope.customerReviewsAndRatingsIndex];
	};
	
	$scope.updatePageIndex = function(index, value, product){
		returnObject = utilityFactory.updatePageIndex($scope[""+index], $scope.customerReviewsAndRatingsPage, $scope.customerReviewsAndRatingsList, value);
		$scope.customerReviewsAndRatingsIndex = returnObject[0];
		$scope.customerReviewsAndRatingsPage = returnObject[1];
		$scope.visibleCustomerReviewsAndRatingsList = $scope.customerReviewsAndRatingsList[$scope.customerReviewsAndRatingsIndex];
	};
	
	$scope.slickConfigTabMobile = {
			infinite: false,
			slidesToShow: 3,
			slidesToScroll: 1,
			prevArrow: ".carousel-previous",
			nextArrow: ".carousel-next",
	        method: {}
	};
	
	$scope.slickConfigDesktop = {
			vertical: true,
			infinite: false,
			slidesToShow: 3,
			slidesToScroll: 1,
			prevArrow: ".carousel-previous-desktop",
			nextArrow: ".carousel-next-desktop",
	        method: {},
	};
}]);