var app = angular.module('productDetailController', ['productService',
													 'userService',
													 'categoryService',
													 'projectService']);

app.controller("productDetailController", ['$scope', 'utilityFactory','$cookieStore','$state','config','productService','userService','$rootScope','$anchorScroll','$location','$window','categoryService', 'projectService', '$timeout',
	function($scope, utilityFactory, $cookieStore, $state, config, productService, userService, $rootScope, $anchorScroll, $location, $window, categoryService, projectService ,$timeout){
	
	//Initialization
	$scope.$timeout = $timeout;
	var productCode = $cookieStore.get('productCode');
	var fields = ['stock','availableForPickup','images'];
	$scope.recommendedProducts = JSON.parse($window.localStorage.getItem("recommendedProducts"));
	$scope.productQuantity = 1;
	
	$scope.initializeProduct = function(){
		var url = config.baseUrl + config.productPath + productCode;
		productService.getProduct(url).then(
			function success(result){
				$scope.product = result.data;
				
				$scope.hasColor = false;
				$scope.hasFit = false;
				$scope.hasSize = false;
				$scope.imageList = [];
				var productOptionList = [];
				$scope.product.sizeAvailable = [];
				
				if(result.data.variantOptions == undefined && result.data.baseOptions != undefined){
					$scope.product.variantOptions = result.data.baseOptions[0].options;
				}
				
				for(i in $scope.product.categories){
					if($scope.product.categories[i].code.indexOf("B2B_") >= 0){
						$scope.hasFit = true;
						$scope.hasColor = true;
						$scope.hasSize = true;
						break;
					}
				}
				
				
				
				angular.forEach($scope.product.variantOptions, function(value) {

					var fieldList = ['images','categories', 'price', 'description', 'name']	
					if(productOptionList.indexOf(value.code.split("_")[0]) == -1 || productOptionList.length == 0) {
						productOptionList.push(value.code.split("_")[0]);						
						//Get other Fields					
						var url = config.baseUrl + config.productPath + value.code + "?fields=" + fieldList;
						productService.getProduct(url).then(
							function success(result){
								if(result.data.categories != undefined){
									for(i in result.data.categories){
										if(result.data.categories[i].code.indexOf('B2B_') >= 0 && result.data.categories.length == 3){
											$scope.imageList.push(
													{
													"code" : value.code,
													"url" : config.baseUrl + result.data.images[0].url,
													"color" : result.data.categories[2].code.split("_").pop()
													});
											break;
										}else if(result.data.categories[i].code.indexOf('B2B_') >= 0 && result.data.categories.length == 2){
											$scope.imageList.push(
													{
													"code" : value.code,
													"url" : config.baseUrl + result.data.images[0].url,
													"color" : result.data.categories[1].code.split("_").pop()
													});
											break;
										}
									}
								}else{
									$scope.imageList.push(
											{
											"code" : value.code,
											"url" : config.baseUrl + result.data.images[0].url
											});
								}
							},
							function error(result){
								console.log(result);
						});
					}
					
					
					//Get other Fields
					var urlPath = config.baseUrl + config.productPath + value.code + "?fields=" + fieldList;
					productService.getProduct(urlPath).then(
						function success(data){
							value.categories = data.data.categories;
							value.images = data.data.images;
							value.price = data.data.price;
							value.description = data.data.description;
							value.name = data.data.name;
							
							for(i in value.categories){
								var size = value.categories[i].code.split("_").pop();
								if(value.categories[i].code.indexOf('B2B_') >= 0){
									var size = parseFloat((value.categories[0].code.split("B2B_").pop()).replace(/_/g , "."));
									if(value.categories[2] != undefined){
										value.attributes = size + "-" + value.categories[2].code.split("_").pop();										
									}else{
										value.attributes = size + "-" + value.categories[1].code.split("_").pop();
									}
									if($scope.product.sizeAvailable.indexOf(size)==-1){
										$scope.product.sizeAvailable.push(size);
									}
								}
							}
						},
						function error(data){
							console.log(data);
					});
					
				});
				
				categoryService.getProductCategory(result.data.categories[0].code).then(
					function success(result){
						$scope.productCategory = result.data.name;		
						
						//Get other Fields
						var url = config.baseUrl + config.productPath + productCode + "?fields=" + fields;
						productService.getProduct(url).then(
							function success(result){
								$scope.product.images = result.data.images;								
								angular.forEach($scope.product.images, function(value) {
									value.url = config.baseUrl + value.url;
								});								
								//Modify
								$scope.product.selectedImage = $scope.product.images[0];
								$scope.product.rating = 3;
							},
							function error(result){
								console.log(result);
						});
					},
					function error(result){
						console.log(result);
				});
			},
			function error(result){
				console.log(result);
		});
	};
	$scope.initializeProduct();
	
	$scope.addToCart = function(product, quantity){
		if(product.stock.stockLevel > quantity && quantity != undefined && quantity != 0){
			//Modify
			var pickUpStore = "USA";
			var fields = ['statusMessage'];
			var url = config.baseUrl + config.userPath + JSON.parse($window.localStorage.getItem('user')).userName + "/carts/" + JSON.parse($window.localStorage.getItem('cart')).code + "/entries?code=" + 
			product.code + "&qty=" + quantity + "&pickUpStore=" + pickUpStore + "?fields=" + fields;
			userService.addToCart(url).then(
				function success(result){
					console.log(result);
					$state.go('base.shoppingCart');
				},
				function error(result){
					console.log(result);
			});
		}
	};
	
	$scope.showPickUpStore = false;
	
	$scope.togglePickUpInStore = function(){
		$scope.showPickUpStore = !$scope.showPickUpStore;
	};
	
	
	$scope.productDescription = [
		'After Dark Full-Grain Leather',
		'Seam-sealed waterproof leather uppers for durability and dry, comfortable feet',
		'Steel safety toe for protection',
		'Direct-inject construction for durability',
		'Padded top collar for comfort',
		'Lightweight blown-thermal plastic midsole for flexibility and cushioning',
		'Mesh lining with Agion®'
	];
	
	$scope.productDetailsSpecs = [
		{'name': 'Steel Safety Toe', 'details': 'Meets I/75 and C/75 impact and compression ASTM F2412-11 and F2413-11 safety standards.'},
		{'name': 'Seam-Sealed Waterproof', 'details': 'Upper is sealed at the seams to provided durable, long-lasting waterproof protection.'},
		{'name': 'Waterproof Leather', 'details': 'Waterproof leather has been specially treated to give it waterproof properties, causing liquids to bead up and shed from the surface.'},
		{'name': 'Electrical Hazard Protection', 'details': 'These boots meet ASTM F2412-11, ASTM F2413-11 and ASTM F2892-11 electrical hazard standards to provide secondary underfoot protection against the hazards of stepping on live electrical circuits, electrically energized conductors, parts, or apparatus.'},
		{'name': 'Insulated', 'details': 'Lightweight, non-quilted, high-lofted, quick-drying, moisture-resistant insulation keeps feet warm in any weather.'},
		{'name': 'Timberland PRO® 24/7 Comfort Suspension™', 'details': 'The Timberland PRO® 24/7 Comfort Suspension™ technology is a unique comfort system that meets the most rigorous work site demands - 24 hours a day, 7 days a week. It supports the arch and cushions every step to help to reduce foot fatigue.'}
	];
	
	$scope.starRating = [
		{'id':'1', 'value': '4 Stars & Up'},
	    {'id':'2', 'value': '3 Stars & Up'},
	    {'id':'3', 'value': '2 Stars & Up'},
	    {'id':'4', 'value': '1 Stars & Up'}
	];
	$scope.selectedStarRating = $scope.starRating[0];
	
	$scope.customerReviewAndRatings = [
		{
			'shortComment': 'Solid, Dependable, Comfortable 1',
			'customerName': 'John S.',
			'date': 'January 25, 2017',
			'rating': 5,
			'size': 12,
			'color': 'Brown',
			'showBtnShowAll': true,
			'showAllComment': false,
			'longComment': [
				'Lorem ipsum dolor sit amet, consectetur adipiscing elit. In pharetra dictum eros, non tempor sem ornare et. Fusce sed arcu non ligula accumsan eleifend. Phasellus vulputate massa et ligula bibendum, id posuere turpis feugiat.',
				'Nam nec posuere eros. Donec rhoncus lacinia pharetra. Morbi scelerisque, ante a porttitor tincidunt, metus lorem sodales mauris, non faucibus eros dolor et mauris. Ut vel placerat diam. Morbi at lorem hendrerit, luctus enim vitae, semper est.',
				'In tempus risus hendrerit purus rhoncus, sed scelerisque mauris luctus. Aliquam erat volutpat. Donec nibh massa, mollis vitae ante eu, tincidunt commodo nisi. Aenean imperdiet blandit est ac consequat. Duis imperdiet lectus sem, non pretium orci sollicitudin mollis. Sed condimentum sem eu sapien mattis sagittis. Mauris bibendum, mi in accumsan vulputate, ipsum eros elementum metus, eu fermentum enim lorem vel ante. Maecenas egestas pharetra ipsum, nec malesuada sapien sollicitudin sit amet. Nulla facilisi. Phasellus iaculis turpis dui, eget efficitur justo finibus et. Donec id leo felis.'
			]
		},
		{
			'shortComment': 'Solid, Dependable, Comfortable 2',
			'customerName': 'John S.',
			'date': 'January 25, 2017',
			'rating': 3,
			'size': 12,
			'color': 'Brown',
			'showBtnShowAll': true,
			'showAllComment': false,
			'longComment': [
				'Lorem ipsum dolor sit amet, consectetur adipiscing elit. In pharetra dictum eros, non tempor sem ornare et. Fusce sed arcu non ligula accumsan eleifend. Phasellus vulputate massa et ligula bibendum, id posuere turpis feugiat.',
				'Nam nec posuere eros. Donec rhoncus lacinia pharetra. Morbi scelerisque, ante a porttitor tincidunt, metus lorem sodales mauris, non faucibus eros dolor et mauris. Ut vel placerat diam. Morbi at lorem hendrerit, luctus enim vitae, semper est.',
				'In tempus risus hendrerit purus rhoncus, sed scelerisque mauris luctus. Aliquam erat volutpat. Donec nibh massa, mollis vitae ante eu, tincidunt commodo nisi. Aenean imperdiet blandit est ac consequat. Duis imperdiet lectus sem, non pretium orci sollicitudin mollis. Sed condimentum sem eu sapien mattis sagittis. Mauris bibendum, mi in accumsan vulputate, ipsum eros elementum metus, eu fermentum enim lorem vel ante. Maecenas egestas pharetra ipsum, nec malesuada sapien sollicitudin sit amet. Nulla facilisi. Phasellus iaculis turpis dui, eget efficitur justo finibus et. Donec id leo felis.'
			]
		},
		{
			'shortComment': 'Solid, Dependable, Comfortable 3',
			'customerName': 'John S.',
			'date': 'January 25, 2017',
			'rating': 0,
			'size': 12,
			'color': 'Brown',
			'showBtnShowAll': false,
			'showAllComment': true,
			'longComment': [
				'Lorem ipsum dolor sit amet, consectetur adipiscing elit. In pharetra dictum eros, non tempor sem ornare et. Fusce sed arcu non ligula accumsan eleifend. Phasellus vulputate massa et ligula bibendum, id posuere turpis feugiat.'
				]
		},
		{
			'shortComment': 'Solid, Dependable, Comfortable 4',
			'customerName': 'John S.',
			'date': 'January 25, 2017',
			'rating': 3,
			'size': 12,
			'color': 'Brown',
			'showBtnShowAll': true,
			'showAllComment': false,
			'longComment': [
				'Lorem ipsum dolor sit amet, consectetur adipiscing elit. In pharetra dictum eros, non tempor sem ornare et. Fusce sed arcu non ligula accumsan eleifend. Phasellus vulputate massa et ligula bibendum, id posuere turpis feugiat.',
				'Nam nec posuere eros. Donec rhoncus lacinia pharetra. Morbi scelerisque, ante a porttitor tincidunt, metus lorem sodales mauris, non faucibus eros dolor et mauris. Ut vel placerat diam. Morbi at lorem hendrerit, luctus enim vitae, semper est.',
				'In tempus risus hendrerit purus rhoncus, sed scelerisque mauris luctus. Aliquam erat volutpat. Donec nibh massa, mollis vitae ante eu, tincidunt commodo nisi. Aenean imperdiet blandit est ac consequat. Duis imperdiet lectus sem, non pretium orci sollicitudin mollis. Sed condimentum sem eu sapien mattis sagittis. Mauris bibendum, mi in accumsan vulputate, ipsum eros elementum metus, eu fermentum enim lorem vel ante. Maecenas egestas pharetra ipsum, nec malesuada sapien sollicitudin sit amet. Nulla facilisi. Phasellus iaculis turpis dui, eget efficitur justo finibus et. Donec id leo felis.'
			]
		},
		{
			'shortComment': 'Solid, Dependable, Comfortable 5',
			'customerName': 'John S.',
			'date': 'January 25, 2017',
			'rating': 3,
			'size': 12,
			'color': 'Brown',
			'showBtnShowAll': true,
			'showAllComment': false,
			'longComment': [
				'Lorem ipsum dolor sit amet, consectetur adipiscing elit. In pharetra dictum eros, non tempor sem ornare et. Fusce sed arcu non ligula accumsan eleifend. Phasellus vulputate massa et ligula bibendum, id posuere turpis feugiat.',
				'Nam nec posuere eros. Donec rhoncus lacinia pharetra. Morbi scelerisque, ante a porttitor tincidunt, metus lorem sodales mauris, non faucibus eros dolor et mauris. Ut vel placerat diam. Morbi at lorem hendrerit, luctus enim vitae, semper est.',
				'In tempus risus hendrerit purus rhoncus, sed scelerisque mauris luctus. Aliquam erat volutpat. Donec nibh massa, mollis vitae ante eu, tincidunt commodo nisi. Aenean imperdiet blandit est ac consequat. Duis imperdiet lectus sem, non pretium orci sollicitudin mollis. Sed condimentum sem eu sapien mattis sagittis. Mauris bibendum, mi in accumsan vulputate, ipsum eros elementum metus, eu fermentum enim lorem vel ante. Maecenas egestas pharetra ipsum, nec malesuada sapien sollicitudin sit amet. Nulla facilisi. Phasellus iaculis turpis dui, eget efficitur justo finibus et. Donec id leo felis.'
			]
		},
		{
			'shortComment': 'Solid, Dependable, Comfortable 6',
			'customerName': 'John S.',
			'date': 'January 25, 2017',
			'rating': 3,
			'size': 12,
			'color': 'Brown',
			'showBtnShowAll': true,
			'showAllComment': false,
			'longComment': [
				'Lorem ipsum dolor sit amet, consectetur adipiscing elit. In pharetra dictum eros, non tempor sem ornare et. Fusce sed arcu non ligula accumsan eleifend. Phasellus vulputate massa et ligula bibendum, id posuere turpis feugiat.',
				'Nam nec posuere eros. Donec rhoncus lacinia pharetra. Morbi scelerisque, ante a porttitor tincidunt, metus lorem sodales mauris, non faucibus eros dolor et mauris. Ut vel placerat diam. Morbi at lorem hendrerit, luctus enim vitae, semper est.',
				'In tempus risus hendrerit purus rhoncus, sed scelerisque mauris luctus. Aliquam erat volutpat. Donec nibh massa, mollis vitae ante eu, tincidunt commodo nisi. Aenean imperdiet blandit est ac consequat. Duis imperdiet lectus sem, non pretium orci sollicitudin mollis. Sed condimentum sem eu sapien mattis sagittis. Mauris bibendum, mi in accumsan vulputate, ipsum eros elementum metus, eu fermentum enim lorem vel ante. Maecenas egestas pharetra ipsum, nec malesuada sapien sollicitudin sit amet. Nulla facilisi. Phasellus iaculis turpis dui, eget efficitur justo finibus et. Donec id leo felis.'
			]
		},
		{
			'shortComment': 'Solid, Dependable, Comfortable 7',
			'customerName': 'John S.',
			'date': 'January 25, 2017',
			'rating': 3,
			'size': 12,
			'color': 'Brown',
			'showBtnShowAll': true,
			'showAllComment': false,
			'longComment': [
				'Lorem ipsum dolor sit amet, consectetur adipiscing elit. In pharetra dictum eros, non tempor sem ornare et. Fusce sed arcu non ligula accumsan eleifend. Phasellus vulputate massa et ligula bibendum, id posuere turpis feugiat.',
				'Nam nec posuere eros. Donec rhoncus lacinia pharetra. Morbi scelerisque, ante a porttitor tincidunt, metus lorem sodales mauris, non faucibus eros dolor et mauris. Ut vel placerat diam. Morbi at lorem hendrerit, luctus enim vitae, semper est.',
				'In tempus risus hendrerit purus rhoncus, sed scelerisque mauris luctus. Aliquam erat volutpat. Donec nibh massa, mollis vitae ante eu, tincidunt commodo nisi. Aenean imperdiet blandit est ac consequat. Duis imperdiet lectus sem, non pretium orci sollicitudin mollis. Sed condimentum sem eu sapien mattis sagittis. Mauris bibendum, mi in accumsan vulputate, ipsum eros elementum metus, eu fermentum enim lorem vel ante. Maecenas egestas pharetra ipsum, nec malesuada sapien sollicitudin sit amet. Nulla facilisi. Phasellus iaculis turpis dui, eget efficitur justo finibus et. Donec id leo felis.'
			]
		},
		{
			'shortComment': 'Solid, Dependable, Comfortable 8',
			'customerName': 'John S.',
			'date': 'January 25, 2017',
			'rating': 3,
			'size': 12,
			'color': 'Brown',
			'showBtnShowAll': true,
			'showAllComment': false,
			'longComment': [
				'Lorem ipsum dolor sit amet, consectetur adipiscing elit. In pharetra dictum eros, non tempor sem ornare et. Fusce sed arcu non ligula accumsan eleifend. Phasellus vulputate massa et ligula bibendum, id posuere turpis feugiat.',
				'Nam nec posuere eros. Donec rhoncus lacinia pharetra. Morbi scelerisque, ante a porttitor tincidunt, metus lorem sodales mauris, non faucibus eros dolor et mauris. Ut vel placerat diam. Morbi at lorem hendrerit, luctus enim vitae, semper est.',
				'In tempus risus hendrerit purus rhoncus, sed scelerisque mauris luctus. Aliquam erat volutpat. Donec nibh massa, mollis vitae ante eu, tincidunt commodo nisi. Aenean imperdiet blandit est ac consequat. Duis imperdiet lectus sem, non pretium orci sollicitudin mollis. Sed condimentum sem eu sapien mattis sagittis. Mauris bibendum, mi in accumsan vulputate, ipsum eros elementum metus, eu fermentum enim lorem vel ante. Maecenas egestas pharetra ipsum, nec malesuada sapien sollicitudin sit amet. Nulla facilisi. Phasellus iaculis turpis dui, eget efficitur justo finibus et. Donec id leo felis.'
			]
		},
		{
			'shortComment': 'Solid, Dependable, Comfortable 9',
			'customerName': 'John S.',
			'date': 'January 25, 2017',
			'rating': 3,
			'size': 12,
			'color': 'Brown',
			'showBtnShowAll': true,
			'showAllComment': false,
			'longComment': [
				'Lorem ipsum dolor sit amet, consectetur adipiscing elit. In pharetra dictum eros, non tempor sem ornare et. Fusce sed arcu non ligula accumsan eleifend. Phasellus vulputate massa et ligula bibendum, id posuere turpis feugiat.',
				'Nam nec posuere eros. Donec rhoncus lacinia pharetra. Morbi scelerisque, ante a porttitor tincidunt, metus lorem sodales mauris, non faucibus eros dolor et mauris. Ut vel placerat diam. Morbi at lorem hendrerit, luctus enim vitae, semper est.',
				'In tempus risus hendrerit purus rhoncus, sed scelerisque mauris luctus. Aliquam erat volutpat. Donec nibh massa, mollis vitae ante eu, tincidunt commodo nisi. Aenean imperdiet blandit est ac consequat. Duis imperdiet lectus sem, non pretium orci sollicitudin mollis. Sed condimentum sem eu sapien mattis sagittis. Mauris bibendum, mi in accumsan vulputate, ipsum eros elementum metus, eu fermentum enim lorem vel ante. Maecenas egestas pharetra ipsum, nec malesuada sapien sollicitudin sit amet. Nulla facilisi. Phasellus iaculis turpis dui, eget efficitur justo finibus et. Donec id leo felis.'
			]
		},
		{
			'shortComment': 'Solid, Dependable, Comfortable 10',
			'customerName': 'John S.',
			'date': 'January 25, 2017',
			'rating': 3,
			'size': 12,
			'color': 'Brown',
			'showBtnShowAll': true,
			'showAllComment': false,
			'longComment': [
				'Lorem ipsum dolor sit amet, consectetur adipiscing elit. In pharetra dictum eros, non tempor sem ornare et. Fusce sed arcu non ligula accumsan eleifend. Phasellus vulputate massa et ligula bibendum, id posuere turpis feugiat.',
				'Nam nec posuere eros. Donec rhoncus lacinia pharetra. Morbi scelerisque, ante a porttitor tincidunt, metus lorem sodales mauris, non faucibus eros dolor et mauris. Ut vel placerat diam. Morbi at lorem hendrerit, luctus enim vitae, semper est.',
				'In tempus risus hendrerit purus rhoncus, sed scelerisque mauris luctus. Aliquam erat volutpat. Donec nibh massa, mollis vitae ante eu, tincidunt commodo nisi. Aenean imperdiet blandit est ac consequat. Duis imperdiet lectus sem, non pretium orci sollicitudin mollis. Sed condimentum sem eu sapien mattis sagittis. Mauris bibendum, mi in accumsan vulputate, ipsum eros elementum metus, eu fermentum enim lorem vel ante. Maecenas egestas pharetra ipsum, nec malesuada sapien sollicitudin sit amet. Nulla facilisi. Phasellus iaculis turpis dui, eget efficitur justo finibus et. Donec id leo felis.'
			]
		}
	];
	
	//Customer Reviews & Comments
	$scope.customerReviewsAndRatingsList = [];
	angular.copy($scope.customerReviewAndRatings, $scope.customerReviewsAndRatingsList);
	$scope.customerReviewsAndRatingsList = utilityFactory.splitArray($scope.customerReviewsAndRatingsList, 3);
	$scope.customerReviewsAndRatingsPage = [];
	$scope.customerReviewsAndRatingsIndex = 0;
	$scope.generateReviewsAndRatingsIndexPage = function(customerReviewsAndRatingsIndex){
		$scope.customerReviewsAndRatingsPage = utilityFactory.generatePagination($scope.customerReviewsAndRatingsPage, $scope.customerReviewsAndRatingsList, customerReviewsAndRatingsIndex);
	}
	$scope.generateReviewsAndRatingsIndexPage($scope.customerReviewsAndRatingsIndex);
	
	//Initialization
	$scope.visibleCustomerReviewsAndRatingsList = $scope.customerReviewsAndRatingsList[$scope.customerReviewsAndRatingsIndex];
	
	$scope.showMoreComment = function(customerReviewAndRating){
		customerReviewAndRating.showAllComment = true;
		customerReviewAndRating.showBtnShowAll = !customerReviewAndRating.showBtnShowAll;
	}
	
	$scope.displayCurrentRating = function(rating){
		return utilityFactory.displayCurrentRating(rating);
	}
	
	$scope.displayUnratedRating = function(rating){
		return utilityFactory.displayUnratedRating(rating);
	}
	
	$scope.productWarranty = {
			'name': 'LIMITED WARRANTY',
			'detail1': "Since 1973, Timberland has been committed to offering the finest products, using only the best materials and applying strict quality control standards. Timberland products are warranted to be free of defects in material and workmanship for 12 months from date of purchase. Our warranty is an expression of our confidence in the material and workmanship of our company's products, and assures unsurpassed quality for rugged, dependable products. This, however, is not a guarantee against normal wear and tear. Nor does it apply to product that has been damaged by misuse, neglect, accident, modification or unauthorized repair.",
			'detail2': "If, upon inspection, we determine that the product is defective, we reserve the right to replace the product or issue you a credit, depending on the nature of the defect.  This warranty gives you specific legal rights, and you may also have other rights which vary from state to state.  Some states do not allow limitations on how long an implied warranty lasts, so the above limitation may not apply to you."
	};
	
	//Recommended Products - Desktop
	$scope.recommendedProductsDesktop = [];
	angular.copy($scope.recommendedProducts, $scope.recommendedProductsDesktop);
	$scope.recommendedProductsDesktop = utilityFactory.splitArray($scope.recommendedProductsDesktop, 3);
	$scope.recommendedProductsPagesDesktop = [];
	$scope.recommendedProductsIndexDesktop = 0;
	$scope.generateRecommendedProductsPageDesktop = function(recommendedProductsIndex){
		$scope.recommendedProductsPagesDesktop = utilityFactory.generatePagination($scope.recommendedProductsPagesDesktop, $scope.recommendedProductsDesktop, recommendedProductsIndex);
	}
	$scope.generateRecommendedProductsPageDesktop($scope.recommendedProductsIndexDesktop);
	
	//Recommended Products - Mobile
	$scope.recommendedProductsMobile = [];
	angular.copy($scope.recommendedProducts, $scope.recommendedProductsMobile);
	$scope.recommendedProductsPagesMobile = [];
	$scope.recommendedProductsIndexMobile = 0;
	$scope.generateRecommendedProductsPageMobile = function(recommendedProductsIndex){
		$scope.recommendedProductsPagesMobile = utilityFactory.generatePagination($scope.recommendedProductsPagesMobile, $scope.recommendedProductsMobile, recommendedProductsIndex);
	}
	$scope.generateRecommendedProductsPageMobile($scope.recommendedProductsIndexMobile);
	
	
	//Saved Products
	$scope.savedProducts = $rootScope.savedProducts;
	
	//Saved Products - Desktop
	$scope.savedProductsDesktop = [];
	angular.copy($scope.savedProducts, $scope.savedProductsDesktop);
	$scope.savedProductsDesktop = utilityFactory.splitArray($scope.savedProductsDesktop, 2);
	$scope.savedProductsIndexDesktop = 0;
	$scope.savedProductsPagesDesktop = [];
	$scope.generateSavedProductsPageDesktop = function(savedProductsIndex){
		$scope.savedProductsPagesDesktop = utilityFactory.generatePagination($scope.savedProductsPagesDesktop, $scope.savedProductsDesktop, savedProductsIndex);
	}
	$scope.generateSavedProductsPageDesktop($scope.recommendedProductsIndexDesktop);
	
	//Saved Products - Mobile
	$scope.savedProductsMobile = [];
	angular.copy($scope.savedProducts, $scope.savedProductsMobile);
	$scope.savedProductsIndexMobile = 0;
	$scope.savedProductsPagesMobile = [];
	$scope.generateSavedProductsPageMobile = function(savedProductsIndex){
		$scope.savedProductsPagesMobile = utilityFactory.generatePagination($scope.savedProductsPagesMobile, $scope.savedProductsMobile, savedProductsIndex);
	}
	$scope.generateSavedProductsPageMobile($scope.savedProductsIndexMobile);
	
	//Initialization
	$scope.visibleRecommendedProductsDesktop = $scope.recommendedProductsDesktop[$scope.recommendedProductsIndexDesktop];
	$scope.visibleSavedProductsDesktop = $scope.savedProductsDesktop[$scope.savedProductsIndexDesktop];
	
	$scope.incrementPageIndex = function(index, product){
		if(product == "recommendedProductMobile"){
			returnObject = utilityFactory.incrementPageIndex($scope[""+index], $scope.recommendedProductsPagesMobile, $scope.recommendedProductsMobile);
			$scope.recommendedProductsIndexMobile = returnObject[0];
			$scope.recommendedProductsPagesMobile = returnObject[1];
		} else if(product == "savedProductMobile"){
			returnObject = utilityFactory.incrementPageIndex($scope[""+index], $scope.savedProductsPagesMobile, $scope.savedProductsMobile);
			$scope.savedProductsIndexMobile = returnObject[0];
			$scope.savedProductsPagesMobile = returnObject[1];
		} else if(product == "recommendedProductDesktop"){
			returnObject = utilityFactory.incrementPageIndex($scope[""+index], $scope.recommendedProductsPagesDesktop, $scope.recommendedProductsDesktop);
			$scope.recommendedProductsIndexDesktop = returnObject[0];
			$scope.recommendedProductsPagesDesktop = returnObject[1];
			$scope.visibleRecommendedProductsDesktop = $scope.recommendedProductsDesktop[$scope.recommendedProductsIndexDesktop];
		} else if(product == "savedProductDesktop"){
			returnObject = utilityFactory.incrementPageIndex($scope[""+index], $scope.savedProductsPagesDesktop, $scope.savedProductsDesktop);
			$scope.savedProductsIndexDesktop = returnObject[0];
			$scope.savedProductsPagesDesktop = returnObject[1];
			$scope.visibleSavedProductsDesktop = $scope.savedProductsDesktop[$scope.savedProductsIndexDesktop];
		} else if (product == "customerReviewsAndComments"){
			returnObject = utilityFactory.incrementPageIndex($scope[""+index], $scope.customerReviewsAndRatingsPage, $scope.customerReviewsAndRatingsList);
			$scope.customerReviewsAndRatingsIndex = returnObject[0];
			$scope.customerReviewsAndRatingsPage = returnObject[1];
			$scope.visibleCustomerReviewsAndRatingsList = $scope.customerReviewsAndRatingsList[$scope.customerReviewsAndRatingsIndex];
		}
	};
	
	$scope.decrementPageIndex = function(index, product){
		if(product == "recommendedProductMobile"){
			returnObject = utilityFactory.decrementPageIndex($scope[""+index], $scope.recommendedProductsPagesMobile, $scope.recommendedProductsMobile);
			$scope.recommendedProductsIndexMobile = returnObject[0];
			$scope.recommendedProductsPagesMobile = returnObject[1];
		} else if(product == "savedProductMobile"){
			returnObject = utilityFactory.decrementPageIndex($scope[""+index], $scope.savedProductsPagesMobile, $scope.savedProductsMobile);
			$scope.savedProductsIndexMobile = returnObject[0];
			$scope.savedProductsPagesMobile = returnObject[1];
		} else if(product == "recommendedProductDesktop"){
			returnObject = utilityFactory.decrementPageIndex($scope[""+index], $scope.recommendedProductsPagesDesktop, $scope.recommendedProductsDesktop);
			$scope.recommendedProductsIndexDesktop = returnObject[0];
			$scope.recommendedProductsPagesDesktop = returnObject[1];
			$scope.visibleRecommendedProductsDesktop = $scope.recommendedProductsDesktop[$scope.recommendedProductsIndexDesktop];
		} else if(product == "savedProductDesktop"){
			returnObject = utilityFactory.decrementPageIndex($scope[""+index], $scope.savedProductsPagesDesktop, $scope.savedProductsDesktop);
			$scope.savedProductsIndexDesktop = returnObject[0];
			$scope.savedProductsPagesDesktop = returnObject[1];
			$scope.visibleSavedProductsDesktop = $scope.savedProductsDesktop[$scope.savedProductsIndexDesktop];
		} else if (product == "customerReviewsAndComments"){
			returnObject = utilityFactory.decrementPageIndex($scope[""+index], $scope.customerReviewsAndRatingsPage, $scope.customerReviewsAndRatingsList);
			$scope.customerReviewsAndRatingsIndex = returnObject[0];
			$scope.customerReviewsAndRatingsPage = returnObject[1];
			$scope.visibleCustomerReviewsAndRatingsList = $scope.customerReviewsAndRatingsList[$scope.customerReviewsAndRatingsIndex];
		}
	};
	
	$scope.updatePageIndex = function(index, value, product){
		if(product == "recommendedProductMobile"){
			returnObject = utilityFactory.updatePageIndex($scope[""+index], $scope.recommendedProductsPagesMobile, $scope.recommendedProductsMobile, value);
			$scope.recommendedProductsIndexMobile = returnObject[0];
			$scope.recommendedProductsPagesMobile = returnObject[1];
		} else if(product == "savedProductMobile"){
			returnObject = utilityFactory.updatePageIndex($scope[""+index], $scope.savedProductsPagesMobile, $scope.savedProductsMobile, value);
			$scope.savedProductsIndexMobile = returnObject[0];
			$scope.savedProductsPagesMobile = returnObject[1];
		} else if(product == "recommendedProductDesktop"){
			returnObject = utilityFactory.updatePageIndex($scope[""+index], $scope.recommendedProductsPagesDesktop, $scope.recommendedProductsDesktop, value);
			$scope.recommendedProductsIndexDesktop = returnObject[0];
			$scope.recommendedProductsPagesDesktop = returnObject[1];
			$scope.visibleRecommendedProductsDesktop = $scope.recommendedProductsDesktop[$scope.recommendedProductsIndexDesktop];
		} else if(product == "savedProductDesktop"){
			returnObject = utilityFactory.updatePageIndex($scope[""+index], $scope.savedProductsPagesDesktop, $scope.savedProductsDesktop, value);
			$scope.savedProductsIndexDesktop = returnObject[0];
			$scope.savedProductsPagesDesktop = returnObject[1];
			$scope.visibleSavedProductsDesktop = $scope.savedProductsDesktop[$scope.savedProductsIndexDesktop];
		} else if (product == "customerReviewsAndComments"){
			returnObject = utilityFactory.updatePageIndex($scope[""+index], $scope.customerReviewsAndRatingsPage, $scope.customerReviewsAndRatingsList, value);
			$scope.customerReviewsAndRatingsIndex = returnObject[0];
			$scope.customerReviewsAndRatingsPage = returnObject[1];
			$scope.visibleCustomerReviewsAndRatingsList = $scope.customerReviewsAndRatingsList[$scope.customerReviewsAndRatingsIndex];
		}
	};
	
	$scope.displayCurrentRating = function(rating){
		return utilityFactory.displayCurrentRating(rating);
	};
	
	$scope.displayUnratedRating = function(rating){
		return utilityFactory.displayUnratedRating(rating);
	};
	
	$scope.selectedSize = function(bool){
		if(bool){
			return "square-number-active";
		} else {
			return "";
		}	
	};
	
	$scope.selectSize = function(product, size){
		product.size = size;
		if(product.color == null){
			product.color = $scope.imageList[0].code;
			$scope.searchAttribute(product.size + "-" +$scope.imageList[0].color);		
		}else{
			var color = "";
			for(i in $scope.imageList){
				if($scope.imageList[i].code == product.color){
					color  = $scope.imageList[i].color
				}
			}
			$scope.searchAttribute(product.size + "-" + color);				
		}	
	};
	
	$scope.selectColor = function(product, code){
		product.color = code;
		$scope.getColorImages(code);	
		if(product.size == null){
			$scope.product.sizeAvailable.sort();
			product.size = $scope.product.sizeAvailable[0]; 
			var color = "";
			for(i in $scope.imageList){
				if($scope.imageList[i].code == product.color){
					color  = $scope.imageList[i].color
				}
			}
			$scope.searchAttribute(product.size + "-" + color);
		}else{
			var color = "";
			for(i in $scope.imageList){
				if($scope.imageList[i].code == product.color){
					color  = $scope.imageList[i].color
				}
			}
			$scope.searchAttribute(product.size + "-" + color);
		}
	};
	
	$scope.getColorImages = function(itemProductCode){
		//Get other Fields
		var counter = 0;
		var url = config.baseUrl + config.productPath + itemProductCode + "?fields=" + fields;
		productService.getProduct(url).then(
			function success(result){
				var imageList = result.data.images;
				angular.forEach($scope.product.images, function(value) {
					value.url = config.baseUrl + imageList[counter].url;
					counter++;
				});			
				//Modify
				$scope.product.selectedImage = $scope.product.images[0];
			},
			function error(result){
				console.log(result);
		});
	}
	
	$scope.searchAttribute = function(attrib){
		for(i in $scope.product.variantOptions){
			if(attrib == $scope.product.variantOptions[i].attributes){
				$scope.product.code = $scope.product.variantOptions[i].code;
				$scope.product.price = $scope.product.variantOptions[i].price;
				$scope.product.price.formattedValue = "$" + $scope.product.variantOptions[i].price.value;
				$scope.product.description = $scope.product.variantOptions[i].description;
				$scope.product.manufacturer = $scope.product.variantOptions[i].manufacturer;
				$scope.product.name = $scope.product.variantOptions[i].name;
				$scope.product.numberOfReviews = $scope.product.variantOptions[i].numberOfReviews;
				$scope.product.stock = $scope.product.variantOptions[i].stock;
				$scope.product.purchasable = $scope.product.variantOptions[i].purchasable;
				$scope.product.summary = $scope.product.variantOptions[i].summary;
				$scope.product.url = $scope.product.variantOptions[i].url;
				break;
			}
		}
	}
	
	$scope.slickConfigTabMobile = {
			infinite: false,
			slidesToShow: 3,
			slidesToScroll: 1,
			prevArrow: ".carousel-previous",
			nextArrow: ".carousel-next",
	        method: {}
	};
	
	$scope.slickConfigDesktop = {
			vertical: true,
			infinite: false,
			slidesToShow: 3,
			slidesToScroll: 1,
//			adaptiveHeight: true,
			prevArrow: ".carousel-previous-desktop",
			nextArrow: ".carousel-next-desktop",
	        method: {},
	};
	
	$scope.selectImage = function(product, image){
		product.selectedImage = image;
	};
	
	$scope.tabsMobile = ['PRODUCT DESCRIPTION','DETAILED SPECS','REVIEWS & RATINGS','WARRANTY'];
	$scope.tabsMobileDefault = angular.copy($scope.tabsMobile);
	
	$scope.showNav = false;
	$scope.toggleShowNav = function(){
		$scope.showNav = !$scope.showNav;
	};
	
	$scope.tab = 1;
	$scope.changeTab = function(tabName){
		var tabs = $scope.tabsMobileDefault;
		for(i in tabs){
			if(tabName == tabs[i]){
				$scope.tab = parseInt(i) + 1;
			}
		}
	};
	
	$scope.selectTabMobile = function(index, tabName) {
		if(!$scope.showNav){
			var temp = $scope.tabsMobile[0];
			$scope.tabsMobile[0] = $scope.tabsMobile[index];
			$scope.tabsMobile[index] = temp;
		};
	};
	
	$scope.focusTabContent = function(){
		$anchorScroll('tab-content');
	};
	
	$scope.focusTabContentMobile = function(){
		if(!$scope.showNav){
			$anchorScroll('tab-content-select');
		};
	};
	
	$scope.goToReview = function(){
		$scope.tab = 3;
		$scope.focusTabContent();
		//For mobile
		$scope.changeTab("REVIEWS & RATINGS");
		$scope.selectTabMobile(2,"REVIEWS & RATINGS");
		$scope.focusTabContentMobile();
	}
	//Project FUnctions
	$scope.oldProjectList = [];
	$scope.retrieveProjectList = function(){
		projectService.retrieveProjectList().then(function(result){
			$scope.projectList = result;
			angular.forEach($scope.oldProjectList, function(item) {
				if(!item.isAdded){
					if(item.customerProjectId == $scope.addedProject.customerProjectId){
						item.isAdded = true;
					}
				}
				for(i in item.entries){
//					if(item.entries[i].productCode == productCode){
//						item.isAdded = true;
//						break;
//					}else{
//						item.isAdded = false;						
//					}
				}
			});			

			angular.forEach($scope.projectList, function(item) {
				for(i in $scope.oldProjectList){
					if($scope.oldProjectList[i].customerProjectId == item.customerProjectId){
						item.isAdded = $scope.oldProjectList[i].isAdded;
					}
				}
			});
			
		});
	}
	$scope.retrieveProjectList();
	
	$scope.addProject = function(){
		if($scope.productQuantity == 0){
			$scope.productQuantity = 1;
		}
		$scope.validateProjectName();
		$scope.addedProject = {};
		if($scope.isValidProjectName){
			if($scope.productQuantity != undefined && $scope.productQuantity != 0){
				var project = {
						name : $scope.projectName,
						entries :
							 	[
	                                { 
	                                	productCode : $scope.product.code, 
	                                	productQuantity: $scope.productQuantity
	                                }
				                ]
					}				
			}else{
				var project = {
						name : $scope.projectName
					}
			}
			
			projectService.createProject(project).then(function(result){
				$scope.retrieveProjectList();
			})
			$scope.toggleProjectCreateModal(false);
		}
		
	}
	
	$scope.addToExistingProject = function(projectId){
		var noDuplicate = true;
		if($scope.productQuantity == 0){
			$scope.productQuantity = 1;
		}
		$scope.projectIdAdd = projectId;
		$scope.addedProject = {};
		$scope.oldProjectList = angular.copy($scope.projectList);
		angular.forEach($scope.oldProjectList, function(item) {
			if(item.isAdded == undefined){
				item.isAdded = false;
			}
		});

		$scope.projectItems = [];
		$scope.selectedProject = null;
		projectService.retrieveProjectDetail(projectId).then(function(result){
			var projItem = result.entries;
			for(i in projItem){
				if(projItem[i].productCode == $scope.product.code){
					//handle duplicate
					noDuplicate = false;
					$scope.selectedProject = {
							customerProjectId : projectId
					} 
					$scope.toggleDuplicateModal(true);
					
					break;
				}
			}
			
			if(noDuplicate){
				var project = {
						name : $scope.projectName,
						entries :
							 	[
		                            { 
		                            	productCode : $scope.product.code, 
		                            	productQuantity: $scope.productQuantity
		                            }
				                ]
					}
				$scope.addedProject = project;
				$scope.addedProject.customerProjectId = projectId;
				projectService.renameProject(project, projectId).then(function(result){
					$scope.retrieveProjectList();
				})
			}
			
		});		
		
		
	}
	
	
	$scope.isValidProjectName = true;	
	$scope.validateProjectName = function(){
		if($scope.projectName.length == 0){
			$scope.isValidProjectName = false;			
		}else{
			$scope.isValidProjectName = true;				
		}
	}
	
	//MODALS
	$scope.addToProjModal = "";
	$scope.toggleAddToProjModal = function(condition){
		if(condition === true){
			$scope.addToProjModal = "is-active";
		} else {
			$scope.addToProjModal = "";
		};
	};
	
	$scope.createProjmodalTrigger = "";
	$scope.toggleProjectCreateModal = function(condition){
		$scope.projectName = "";
		if(condition === true){
			$scope.createProjmodalTrigger = "is-active";
		} else {
			$scope.createProjmodalTrigger = "";
			$scope.isValidProjectName = true;	
		};
	};
	
	$scope.dupesModalTrigger = "";
	$scope.toggleDuplicateModal = function(condition){
		if(condition === true){
			$scope.dupesModalTrigger = "is-active";
		} else {
			$scope.dupesModalTrigger = "";
		};
	};
	
	$scope.goToProjectDetail = function(project){
		$state.go('base.projectDetail', {
	        projectId: project.customerProjectId
	      });
	}
	
	$scope.closeAddProjectPopover = function(){
		var $win = $(window); // or $box parent container
		$win.on("click.Bst", function(event){
			 var $box = $(".add-to-project");
			if ( 
			    $box.has(event.target).length == 0 //checks if descendants of $box was clicked
			    &&
			    !$box.is(event.target) //checks if the $box itself was clicked
			  ){
					angular.element(document.querySelectorAll(".add-to-project .angular-popover")).addClass("hide-popover-element");
					angular.element(document.querySelectorAll(".add-to-project .angular-popover-triangle")).addClass("hide-popover-element");
				}
		});
	}
	$scope.closeAddProjectPopover();	
}])