var app = angular.module('projectDetailController', [ 'projectService', 'datatables', 'datatables.colreorder', 'userService', 'productService']);

app.controller("projectDetailController", ['$scope', '$rootScope', '$state', '$stateParams', '$window','utilityFactory','$cookieStore','$state'
                                           ,'config','projectService', 'DTOptionsBuilder', 'DTColumnDefBuilder', 'utilityFactory', 'userService', '$q', '$timeout', 'productService', '$filter', 'toaster', '$transitions',
	function($scope, $rootScope, $state, $stateParams, $window, utilityFactory, $cookieStore, $state, config, projectService
			, DTOptionsBuilder, DTColumnDefBuilder, utilityFactory, userService, $q, $timeout, productService, $filter,toaster,$transitions){
	var q = $q.defer();
	$scope.projectId = $stateParams.projectId;
	$scope.$timeout = $timeout;
	$scope.getProjectItems = function(){
		$scope.itemLimit = 10;
		$scope.projectItems = [];
		projectService.retrieveProjectDetail($scope.projectId).then(function(result){
			var projItem = result;
			$scope.projectName = projItem.name;
			if(projItem.entries != undefined){
//				utilityFactory.getProjectItemDetails(projItem, $scope.itemLimit).then(function(result){
					//Saves row reordering index in cache
					$scope.itemListTemp = result.entries;		
					angular.forEach($scope.itemListTemp, function(product) {
						product.image = config.baseUrl + product.images[0].url;
						product.itemNumber = product.productCode;
						product.quantity = product.productQuantity; 
						product.description = product.description;
						product.variantOptions = product.variantOptions;
						if(product.variantOptions == undefined && product.baseOptions != undefined){
							product.variantOptions = product.baseOptions[0].options;
						}
						if(parseInt(product.stockLevel) > 0){
							product.availability = "In Stock";
							product.stockLevel = parseInt(product.stockLevel);
						} else {
							product.availability = "Out of Stock";
							product.stockLevel = 0;
						}
						product.unitPrice = parseFloat(product.price.value);
					});
					
			    	$scope.oldOrder = angular.copy($scope.itemListTemp);	
			    	$scope.rowOrder = $cookieStore.get('rowOrderCache'+$scope.projectId);
			    	if($scope.rowOrder != undefined){
			        	for(i in $scope.rowOrder){        
			            	for(index in $scope.oldOrder){
			            		if($scope.oldOrder[index].itemNumber == $scope.rowOrder[i]){
			            			$scope.itemListTemp[i] = $scope.oldOrder[index];
			            		}
			            	}
			        	}    		
			    	}
			    	//retrieves additional product data
			    	$scope.projectItems = $scope.itemListTemp;
			    	var stop = false;
			    	var counter = 0;
					angular.forEach($scope.projectItems, function(value) {
							value.options = [];
							value.itemNumber.indexOf('_') >= 0 ? value.hasOptions = true : value.hasOptions = false;
							if(value.categories != undefined && value.categories.length != 0 && value.categories.length != undefined){
								angular.forEach(value.categories, function(option) {
									if(option.code.indexOf('B2B_') >= 0){
										value.options.push((option.code.split("B2B_").pop()).replace(/_/g , "."));
									}
								})						
							}
					});			
			    	$scope.initRowReorder();
			    	console.log($scope.projectItems)
			    	if($scope.saveProject){
						toaster.pop('success', "", "Changes Saved")
						$scope.saveProject = false
			    	}
//				});
			}
			if(result.result != undefined && result.result.validLicense == "FALSE"){
    			$scope.errorMsg = result.result.message;
    			$scope.errorModalTrigger = "is-active";							
			}
		},
		function error(result){
			console.log(result);
			$scope.errorMsg = "Oops! An error occured";
			$scope.errorModalTrigger = "is-active";
		});
		
	}
	$scope.getProjectItems();
	
	//datatables Init
	var vm = this;
	vm.dtInstance = {};
	
	var order = [0]
	vm.dtOptions = DTOptionsBuilder.newOptions()
        // Activate col reorder plugin
        .withOption('responsive', true)
        .withOption('paging', true)
        .withOption('info', false)
        .withDOM('<"top"flp<"clear">>rt<"bottom"ip<"clear">>')
        .withOption('lengthMenu', [[10,25,50, 100,-1],[10, 25, 50, 100, "All"]])
        .withColReorder()
        .withColReorderOption('iFixedColumnsLeft', 1)
        .withOption('stateSave', true)
        .withOption('pageLength', 10)
        .withOption('retrieve', true)
        .withOption('destroy', true)
        .withLanguage({
        	"sEmptyTable":     "You don't have any products in this project yet.",
        	"paginate": {
                "next": '<button class="pagination-next transparent" ng-disabled="currentPage+1 == totalPages" ng-click="changePage(currentPage+1)">'+
            		'<i class="material-icons">keyboard_arrow_right</i></button>',
                "previous": '<button class="pagination-previous transparent" ng-disabled="currentPage == 0" ng-click="changePage(currentPage-1)">'+
							'<i class="material-icons">keyboard_arrow_left</i></button>'
            }
        })
        .withOption('order' , [[ 0, 'desc' ]])
        .withOption('rowReorder',{
            selector: 'td:not(.no-reorder)',
        })
        .withOption("stateSaveParams", function (settings, data) {
            data.search.search = "";
            data.order = [[ 0, 'asc' ]]
        })
        .withOption("fnRowCallback", function( nRow, aData, iDisplayIndex, iDisplayIndexFull) {
        	if($scope.headerTrigger){
        		if(iDisplayIndex == 0){
            		$scope.rowOrderIndexC = [];  
            		$scope.rowCounter = 1
            	}
            	var entry = [];
            	entry.push(nRow.getElementsByClassName("item-no")[0].text);
            	nRow.getElementsByClassName("index-no")[0].innerText = $scope.rowCounter - 1;
            	$scope.rowOrderIndexC.push(entry);
            	if( $scope.projectItems.length != undefined && $scope.rowCounter == $scope.projectItems.length){
                	$cookieStore.put('rowOrderCache'+$scope.projectId , $scope.rowOrderIndexC);
                	console.log($cookieStore.get('rowOrderCache'+$scope.projectId));
            	}
            	$scope.rowCounter++;
        	}else{
        		var rowOrder = $cookieStore.get('rowOrderCache'+ $scope.projectId );
        		if(iDisplayIndex == 0){
            		$scope.rowOrderIndexC = [];  
            		$scope.rowCounter = 1
            	}
            	$scope.rowCounter++;
        	}   
		})
		

	$scope.resetSort = function(element){
		var counter = 0;		
		vm.dtInstance.DataTable.on('order.dt', function(){
			if(vm.dtInstance.DataTable.settings().order().length === 1 && counter == 0){
	            let order = vm.dtInstance.DataTable.settings().order()[0],
	            	th = $("project-detail-table th:eq(" + order[0] + ")");
	            if(th.attr("data-sort-next") === "false"){
	            	vm.dtInstance.DataTable.order([]).draw();
	                th.removeAttr("data-sort-next");
	            }else{
	            	th.attr("data-sort-next", order[1] !== "desc");
	            }
	            $("project-detail-table th").each(function(k, v){
	            	if(k !== order[0]){
	                	$("project-detail-table th:eq(" + k + ")").removeAttr("data-sort-next")
	                }
	            });
	            counter++;
	        }
	    });
	}
	
	
	$scope.rerenderTable = function(){
		$scope.getProjectItems();
    	$scope.headerTrigger = false;
	}
	
	$scope.rowOrderIndexC = [];
	$scope.rowCounter = 1;
	$scope.triggerSort = function(){
		$scope.headerTrigger = true;
	}
	
	
    vm.dtColumnDefs = [
        DTColumnDefBuilder.newColumnDef(6).notSortable(),
        DTColumnDefBuilder.newColumnDef(1).notSortable(),
        DTColumnDefBuilder.newColumnDef(9).notSortable()
    ];
    
	
	$scope.filterProductTable = function() {
		vm.dtInstance.DataTable.search($scope.searchProjectProduct).draw();
    };
    
    $scope.redrawSort = function(){
    	vm.dtInstance.DataTable.order([0, 'asc']);    	
    }
    $scope.loadedPageRecord = function(){
    	$scope.loadPage = [];
    	$scope.loadPage.push(0);
    }
    $scope.loadedPageRecord();
    $scope.initRowReorder = function(){
    	$scope.runOnce =true;
    	//row reorder listener
	    vm.dtInstance.DataTable.on('row-reorder', function ( e, diff, edit ) {
	    	$scope.runOnce =true;
	    	$scope.rowCounterC = 0;
	    	$scope.retrieveOrder();
	    });
	    
    }

	$scope.runOnce =true;
    $scope.retrieveOrder = function(){
    	var rowOrderIndex = [];
    	$scope.rowCounter = 1
    	var elem = angular.element( document.querySelector( 'project-detail-table table' ) );
    	var indexList = []
    	angular.forEach(elem.children()[1].children, function(tr, key) {
		  var entry = [];
		  entry.push(tr.getElementsByClassName("item-no")[0].text); 
		  rowOrderIndex.push(entry);
		  indexList.push(parseInt(tr.getElementsByClassName("index-no")[0].innerText));
		  if($scope.rowCounter == $scope.projectItems.length){
		      $cookieStore.put('rowOrderCache'+$scope.projectId , rowOrderIndex); 
		  }
		  $scope.rowCounter++;
		});
    	$cookieStore.put('rowOrderCache'+$scope.projectId , rowOrderIndex);
    	if($scope.headerTrigger){
        	$scope.rerenderTable();        		
    	}	
    }
    
	$scope.goToMyProjects = function(){
		$state.go('base.project');	
	}
	
	$scope.addToCart = function(){
		var product = $scope.projectItems;
		var productList = [];
		for(i in product){
			if(product[i].stockLevel >= product[i].quantity && product[i].quantity != undefined && product[i].quantity != 0){
				//Modify		
				var productParam = product[i].itemNumber  + ":" + product[i].quantity;
				productList.push(productParam);
			}
		}
		
		var pickUpStore = "USA";
		var fields = ['BASIC'];	
		var url = config.baseUrl + config.userPath + JSON.parse($window.localStorage.getItem('user')).userName + "/customerprojects/carts/" + JSON.parse($window.localStorage.getItem('cart')).code + "/entries"
		projectService.addToCartProjectItem(url, productList).then(
			function success(result){
					utilityFactory.getAllShoppingCartItem().then(function(result){
						$rootScope.shoppingCart = result;
					});
			},
			function error(result){
				console.log(result);
		});		
	};
	
	
	$scope.deleteProjectItem = function(productCode){
		projectService.deleteProjectItem($scope.projectId, productCode).then(function(result){
			$scope.getProjectItems();
		});
	}
	
	$scope.addToExistingProject = function(){
		var hasDuplicate = false;
		var projectId = $scope.projectId;
		var duplicateItem = {};
		if($scope.searchProductItem.item.quantity  != undefined || $scope.searchProductItem.item.quantity  != 0){			
			angular.forEach($scope.projectItems , function(item){
				if(item.itemNumber == $scope.searchProductItem.item.code){
					hasDuplicate = true;
					duplicateItem = item;
				}
			});
			if(!hasDuplicate){
				var project = {
						name : $scope.projectName,
						entries :
							 	[
		                            { 
		                            	productCode : $scope.searchProductItem.item.code, 
		                            	productQuantity: $scope.searchProductItem.item.quantity
		                            }
				                ]
					}
				$scope.addedProject = project;
				$scope.addedProject.customerProjectId = projectId;
				projectService.renameProject(project, projectId).then(function(result){
					if($scope.isFromCsv){
						if($scope.isLastFromCsv){
							$scope.getProjectItems();
							$scope.isFromCsv = false;
							$scope.isLastFromCsv = false;							
						}
					}else{
						$scope.getProjectItems();
					}
				})
			}else{				
				if($scope.searchProductItem.item.quantity != 0 && $scope.searchProductItem.item.quantity != undefined){
					projectService.deleteProjectItem($scope.projectId, duplicateItem.itemNumber).then(function(result){
						var totalItem = parseInt($scope.searchProductItem.item.quantity) + parseInt(duplicateItem.quantity);
						var project = {
								name : $scope.projectName,
								entries :
									 	[
				                            { 
				                            	productCode : $scope.searchProductItem.item.code, 
				                            	productQuantity: totalItem
				                            }
						                ]
							}
						$scope.addedProject = project;
						$scope.addedProject.customerProjectId = projectId;
						projectService.renameProject(project, projectId).then(function(result){
							if($scope.isFromCsv){
								if($scope.isLastFromCsv){
									$scope.getProjectItems();
									$scope.isFromCsv = false;
									$scope.isLastFromCsv = false;
								}
							}else{
								$scope.getProjectItems();
							}
						})
					});
				}
			}
		}
	}
	
	$scope.updateProjectQuantity = function(){
		var chain = $q.when();
		var project = {
				name : $scope.projectName,
				entries :
					 	[]
			}
		var productList = $scope.changedProductList;
		var itemCount = 0;
		var projectId = $scope.projectId;
		var isInvalidCount = 0;
		for(i in productList){
			var productSelect = $filter("filter")($scope.projectItems, {itemNumber: productList[i]});
			if(!(productSelect[0].quantity != undefined && productSelect[0].quantity != 0)){
				isInvalidCount++;
			}
		}
		if($scope.changedProductList.length != undefined && $scope.changedProductList.length != 0 && isInvalidCount ==0 ){
			for(i in productList){
				chain = chain.then(function() {	
					return projectService.deleteProjectItem($scope.projectId, productList[itemCount]).then(
							function(result){
								var newTemp = $filter("filter")($scope.projectItems, {itemNumber: productList[itemCount]});
								project.entries.push({ 
	                            	productCode : newTemp[0].itemNumber, 
	                            	productQuantity: newTemp[0].quantity
	                            });
								itemCount++;
								if(itemCount == productList.length){
									$scope.changedProductList = [];
									projectService.renameProject(project, projectId).then(function(result){
										$scope.getProjectItems();
										$scope.toggleSaveChangesModal(false);
										$scope.saveProject = true;
									})									
								}		
							},
							function error(result){
								console.log(result);
							});
				});
			}	
		}
	}

	$scope.changedProductList = [];
	$scope.listChangedProduct = function(productCode){
		if($scope.changedProductList.length != undefined && $scope.changedProductList.length != 0){
			if($scope.changedProductList.indexOf(productCode) == -1) {
				$scope.changedProductList.push(productCode);
			}
		}else{
			$scope.changedProductList.push(productCode);
		}
	}
	
	$transitions.onStart({}, function($transition, event){
		var state = $transition.$from().self.url.search('project-detail')
		if (state != -1  && $scope.changedProductList.length != undefined && $scope.changedProductList.length!= 0){
			$scope.transitionPage = $transition.$to().name;
			$scope.toggleSaveChangesModal(true);
			$transition.abort();
		}
    });
	
	$scope.updateProjectQuantity();
	$scope.searchProjProduct = "";
	$scope.searchProjProductError = "";
	$scope.getProductQuickOrder = function(productCode){
		angular.element(document.querySelectorAll(".project-product-search .angular-popover")).removeClass("width-230");
		$scope.searchProjProduct = productCode;
		$scope.searchProjProductError = "";
		if(productCode != undefined && productCode.trim() != 0){
			$scope.searchProductItem = {
					item: ""
			};
			var url = config.baseUrl + config.productPath + productCode;
			productService.getProduct(url).then(
				function success(result){
					console.log(result);
					
					$scope.searchProductItem.item = result.data;
					$scope.searchProductItem.item.quantity = 0;
					
					//Get images
					var fields = ['images'];
					var url = config.baseUrl + config.productPath + productCode + "?fields=" + fields;
					productService.getProduct(url).then(
						function success(result){
							$scope.searchProductItem.item.image = config.baseUrl + result.data.images[0].url;
						},
						function error(result){
							console.log(result);
						});
				},
				function error(result){
					console.log(result);
					if(result.data.errors.length == 1){
						if(result.data.errors[0].type == "UnknownIdentifierError"){
							$scope.searchProjProductError = result.data.errors[0].message;
							angular.element(document.querySelectorAll(".project-product-search .angular-popover")).addClass("width-230");
						};
					}
			});
		};
		angular.element(document.querySelectorAll(".project-product-search .angular-popover")).removeClass("hide-popover-element");
		angular.element(document.querySelectorAll(".project-product-search .angular-popover-triangle")).removeClass("hide-popover-element");
	};
	
	$scope.closeAddProjectPopover = function(){
		var $win = $(window); // or $box parent container
		$win.on("click.Bst", function(event){
			 var $box = $(".project-product-search");
			if ( 
			    $box.has(event.target).length == 0 //checks if descendants of $box was clicked
			    &&
			    !$box.is(event.target) //checks if the $box itself was clicked
			  ){
					angular.element(document.querySelectorAll(".project-product-search .angular-popover")).addClass("hide-popover-element");
					angular.element(document.querySelectorAll(".project-product-search .angular-popover-triangle")).addClass("hide-popover-element");
				}else{
					if(($scope.searchProjProduct.length == 0 || $scope.searchProjProduct == undefined) && ($scope.searchProjProductError.length == 0 || $scope.searchProjProductError == undefined)){
						angular.element(document.querySelectorAll(".project-product-search .angular-popover")).addClass("hide-popover-element");
						angular.element(document.querySelectorAll(".project-product-search .angular-popover-triangle")).addClass("hide-popover-element");
					}
				}
		});
	}

	$scope.closeAddProjectPopover();
	$scope.clickUploadCsv = function(){
		setTimeout(function() {
	        document.getElementById('upload-csv').click()
	    }, 0);
	}
	$scope.isFromCsv = false;
	$scope.isLastFromCsv = false;
	$scope.addDataFromCsv = function(result){
		$scope.newData = [];
		$scope.itemIndex = [];
		$scope.deleteItemList = []; //items to be updated
		angular.forEach(result, function(value){
			//check duplicate productcode in csv
			  var dupCount = 0;
			  if(value["Customer Material # or Southwire Material #"] != undefined && value["Quantity"] != undefined){
				  var totalQuantity = 0;
				  angular.forEach(result, function(data){
					if(value["Customer Material # or Southwire Material #"] == data["Customer Material # or Southwire Material #"]){
						dupCount++;
						if(dupCount > 1){
							totalQuantity = parseInt(value["Quantity"]) + parseInt(data["Quantity"]);
							value["Quantity"] = totalQuantity;
						}
					}
				  })	
				//check duplicate item in project details
				var hasDuplicateItem = false;
				var duplicateItem = null;
				angular.forEach($scope.projectItems , function(item){
					if(item.itemNumber == value["Customer Material # or Southwire Material #"]){
						hasDuplicateItem = true;
						duplicateItem = item;
					}
				});	
				//adds product code to deleteItemList
				if(dupCount == 1){
					if(hasDuplicateItem){
						value["Quantity"] = parseInt(value["Quantity"]) + parseInt(duplicateItem.quantity);
						$scope.deleteItemList.push(value["Customer Material # or Southwire Material #"]);
					}
					$scope.newData.push(value);						
				}else{
					var hasDuplicate = false;
					for(i in $scope.newData){
						if($scope.newData[i]["Customer Material # or Southwire Material #"]  == value["Customer Material # or Southwire Material #"]){
							hasDuplicate = true;
							break;
						}
					};
					if(!hasDuplicate){
						if(hasDuplicateItem){
							value["Quantity"] = parseInt(value["Quantity"]) + parseInt(duplicateItem.quantity);
							$scope.deleteItemList.push(value["Customer Material # or Southwire Material #"]);
						}
						$scope.newData.push(value);						
					}
				}
			  };
		 });
		
		$scope.iterationCounter = 0;
		for(i in $scope.newData){
			$scope.isLastFromCsv = false;				
			$scope.isFromCsv = true;
			var hasDuplicate = false;
			var duplicateItem = {};
			var url = config.baseUrl + config.productPath + $scope.newData[i]["Customer Material # or Southwire Material #"];
			productService.getProduct(url).then(
				function success(result){			
					$scope.iterationCounter++;
					for(i in $scope.newData){
						if($scope.newData[i]["Customer Material # or Southwire Material #"] == result.data.code){
							$scope.itemIndex.push({
									productCode : $scope.newData[i]["Customer Material # or Southwire Material #"],
									productQuantity : $scope.newData[i]["Quantity"]
							});
						}						
					}							
					if($scope.iterationCounter == $scope.newData.length){
						$scope.addCsvToExistingProject();
					}
				},
				function error(result){
					$scope.iterationCounter++;
					console.log(result);
					if(($scope.iterationCounter) == $scope.newData.length){
						$scope.addCsvToExistingProject();
					} 
				});
		}
	};
	
	$scope.addCsvToExistingProject = function(){
		var projectId = $scope.projectId;
		$scope.deleteCounter = 0 ;
		if($scope.deleteItemList.length > 0){
			angular.forEach($scope.deleteItemList , function(item){
				projectService.deleteProjectItem($scope.projectId, item).then(function(result){
					$scope.deleteCounter++;				
					if($scope.deleteCounter == $scope.deleteItemList.length){
						var project = {
								name : $scope.projectName,
								entries : $scope.itemIndex
							}
						projectService.renameProject(project, projectId).then(function(result){
								$scope.getProjectItems();
						})
					}
				});
			});
		}else{
			var project = {
					name : $scope.projectName,
					entries : $scope.itemIndex
				}
			projectService.renameProject(project, projectId).then(function(result){
					$scope.getProjectItems();
			})
		}
		
	}	
	
	//MODALS
	$scope.modalTrigger = "";
	$scope.toggleOrderDetailsModal = function(condition, index){
		if(condition === true){
			$scope.modalTrigger = "is-active";	
			initProductDetails(index);
		} else {
			$scope.modalTrigger = "";
			$scope.initializeReccomendedProducts();
		}
	};	
	
	$scope.saveChangesModalTrigger = "";
	$scope.toggleSaveChangesModal = function(condition){
		if(condition === true){
			$scope.saveChangesModalTrigger = "is-active";
		} else {
			$scope.saveChangesModalTrigger = "";
		}
	};
	
	$scope.proceedWithoutSave = function(){
		$scope.changedProductList = [];
		$state.go($scope.transitionPage);
	}

	$scope.errorModalTrigger = "";
	$scope.errorRedirect = function(){
		$state.go('base.project');
		$scope.errorModalTrigger = "";
	}
	
	$scope.showOrderDetailsMobile = function(index){
		initProductDetails(index);
		document.getElementById("order-detail-mobile").style.height = "100%";
		document.getElementsByTagName("body")[0].style.overflowY = 'hidden';
		document.getElementById("overlay").style.display = "block";
	};
	
	$scope.hideOrderDetailsMobile = function(){
		document.getElementById("order-detail-mobile").style.height = "0px";
		document.getElementsByTagName("body")[0].style.overflowY = 'auto';
		document.getElementById("overlay").style.display = "none";
	};
	
	$scope.initializeReccomendedProducts = function(){
		//Recommended Products - Desktop
		$scope.recommendedProductsDesktop = [];
		$scope.recommendedProductsDesktop = JSON.parse($window.localStorage.getItem("recommendedProducts"));
		$scope.recommendedProductsDesktop = utilityFactory.splitArray($scope.recommendedProductsDesktop, 4);
		$scope.recommendedProductsPagesDesktop = [];
		$scope.recommendedProductsIndexDesktop = 0;
		$scope.generateRecommendedProductsPageDesktop = function(recommendedProductsIndex){
			$scope.recommendedProductsPagesDesktop = utilityFactory.generatePagination($scope.recommendedProductsPagesDesktop, $scope.recommendedProductsDesktop, recommendedProductsIndex);
		}
		$scope.generateRecommendedProductsPageDesktop($scope.recommendedProductsIndexDesktop);
		
		//Recommended Products - Tablet
		$scope.recommendedProductsTablet = [];
		$scope.recommendedProductsTablet = JSON.parse($window.localStorage.getItem("recommendedProducts"));
		$scope.recommendedProductsTablet = utilityFactory.splitArray($scope.recommendedProductsTablet, 2);
		$scope.recommendedProductsPagesTablet = [];
		$scope.recommendedProductsIndexTablet = 0;
		$scope.generateRecommendedProductsPageTablet = function(recommendedProductsIndex){
			$scope.recommendedProductsPagesTablet = utilityFactory.generatePagination($scope.recommendedProductsPagesTablet, $scope.recommendedProductsTablet, recommendedProductsIndex);
		}
		$scope.generateRecommendedProductsPageTablet($scope.recommendedProductsIndexTablet);
		
		//Recommended Products - Mobile
		$scope.recommendedProductsMobile = [];
		$scope.recommendedProductsMobile = JSON.parse($window.localStorage.getItem("recommendedProducts"));
		$scope.recommendedProductsPagesMobile = [];
		$scope.recommendedProductsIndexMobile = 0;
		$scope.generateRecommendedProductsPageMobile = function(recommendedProductsIndex){
			$scope.recommendedProductsPagesMobile = utilityFactory.generatePagination($scope.recommendedProductsPagesMobile, $scope.recommendedProductsMobile, recommendedProductsIndex);
		}
		$scope.generateRecommendedProductsPageMobile($scope.recommendedProductsIndexMobile);
		
		//Initialization
		$scope.visibleRecommendedProductsDesktop = $scope.recommendedProductsDesktop[$scope.recommendedProductsIndexDesktop];
		$scope.visibleRecommendedProductsTablet = $scope.recommendedProductsTablet[$scope.recommendedProductsIndexTablet];
		
		$scope.incrementPageIndex = function(index, product){
			if(product == "recommendedProductDesktop"){
				returnObject = utilityFactory.incrementPageIndex($scope[""+index], $scope.recommendedProductsPagesDesktop, $scope.recommendedProductsDesktop);
				$scope.recommendedProductsIndexDesktop = returnObject[0];
				$scope.recommendedProductsPagesDesktop = returnObject[1];
				$scope.visibleRecommendedProductsDesktop = $scope.recommendedProductsDesktop[$scope.recommendedProductsIndexDesktop];
			}else if(product == "recommendedProductTablet"){
				returnObject = utilityFactory.incrementPageIndex($scope[""+index], $scope.recommendedProductsPagesTablet, $scope.recommendedProductsTablet);
				$scope.recommendedProductsIndexTablet = returnObject[0];
				$scope.recommendedProductsPagesTablet = returnObject[1];
				$scope.visibleRecommendedProductsTablet = $scope.recommendedProductsTablet[$scope.recommendedProductsIndexTablet];
			}else if(product == "recommendedProductMobile"){
				returnObject = utilityFactory.incrementPageIndex($scope[""+index], $scope.recommendedProductsPagesMobile, $scope.recommendedProductsMobile);
				$scope.recommendedProductsIndexMobile = returnObject[0];
				$scope.recommendedProductsPagesMobile = returnObject[1];
			};
		};
		
		$scope.decrementPageIndex = function(index, product){
			if(product == "recommendedProductDesktop"){
				returnObject = utilityFactory.decrementPageIndex($scope[""+index], $scope.recommendedProductsPagesDesktop, $scope.recommendedProductsDesktop);
				$scope.recommendedProductsIndexDesktop = returnObject[0];
				$scope.recommendedProductsPagesDesktop = returnObject[1];
				$scope.visibleRecommendedProductsDesktop = $scope.recommendedProductsDesktop[$scope.recommendedProductsIndexDesktop];
			}else if(product == "recommendedProductTablet"){
				returnObject = utilityFactory.decrementPageIndex($scope[""+index], $scope.recommendedProductsPagesTablet, $scope.recommendedProductsTablet);
				$scope.recommendedProductsIndexTablet = returnObject[0];
				$scope.recommendedProductsPagesTablet = returnObject[1];
				$scope.visibleRecommendedProductsTablet = $scope.recommendedProductsTablet[$scope.recommendedProductsIndexTablet];
			}else if(product == "recommendedProductMobile"){
				returnObject = utilityFactory.decrementPageIndex($scope[""+index], $scope.recommendedProductsPagesMobile, $scope.recommendedProductsMobile);
				$scope.recommendedProductsIndexMobile = returnObject[0];
				$scope.recommendedProductsPagesMobile = returnObject[1];
			};
		};
		
		$scope.updatePageIndex = function(index, value, product){
			if(product == "recommendedProductDesktop"){
				returnObject = utilityFactory.updatePageIndex($scope[""+index], $scope.recommendedProductsPagesDesktop, $scope.recommendedProductsDesktop, value);
				$scope.recommendedProductsIndexDesktop = returnObject[0];
				$scope.recommendedProductsPagesDesktop = returnObject[1];
				$scope.visibleRecommendedProductsDesktop = $scope.recommendedProductsDesktop[$scope.recommendedProductsIndexDesktop];
			}else if(product == "recommendedProductTablet"){
				returnObject = utilityFactory.updatePageIndex($scope[""+index], $scope.recommendedProductsPagesTablet, $scope.recommendedProductsTablet, value);
				$scope.recommendedProductsIndexTablet = returnObject[0];
				$scope.recommendedProductsPagesTablet = returnObject[1];
				$scope.visibleRecommendedProductsTablet = $scope.recommendedProductsTablet[$scope.recommendedProductsIndexTablet];
			}else if(product == "recommendedProductMobile"){
				returnObject = utilityFactory.updatePageIndex($scope[""+index], $scope.recommendedProductsPagesMobile, $scope.recommendedProductsMobile, value);
				$scope.recommendedProductsIndexMobile = returnObject[0];
				$scope.recommendedProductsPagesMobile = returnObject[1];
			};
		};
		
		
		//Initialization
		$scope.visibleRecommendedProductsDesktop = $scope.recommendedProductsDesktop[$scope.recommendedProductsIndexDesktop];
		
		$scope.incrementPageIndex = function(index, product){
			if(product == "recommendedProductMobile"){
				returnObject = utilityFactory.incrementPageIndex($scope[""+index], $scope.recommendedProductsPagesMobile, $scope.recommendedProductsMobile);
				$scope.recommendedProductsIndexMobile = returnObject[0];
				$scope.recommendedProductsPagesMobile = returnObject[1];
			} else if(product == "savedProductMobile"){
				returnObject = utilityFactory.incrementPageIndex($scope[""+index], $scope.savedProductsPagesMobile, $scope.savedProductsMobile);
				$scope.savedProductsIndexMobile = returnObject[0];
				$scope.savedProductsPagesMobile = returnObject[1];
			} else if(product == "recommendedProductDesktop"){
				returnObject = utilityFactory.incrementPageIndex($scope[""+index], $scope.recommendedProductsPagesDesktop, $scope.recommendedProductsDesktop);
				$scope.recommendedProductsIndexDesktop = returnObject[0];
				$scope.recommendedProductsPagesDesktop = returnObject[1];
				$scope.visibleRecommendedProductsDesktop = $scope.recommendedProductsDesktop[$scope.recommendedProductsIndexDesktop];
			} else if(product == "savedProductDesktop"){
				returnObject = utilityFactory.incrementPageIndex($scope[""+index], $scope.savedProductsPagesDesktop, $scope.savedProductsDesktop);
				$scope.savedProductsIndexDesktop = returnObject[0];
				$scope.savedProductsPagesDesktop = returnObject[1];
				$scope.visibleSavedProductsDesktop = $scope.savedProductsDesktop[$scope.savedProductsIndexDesktop];
			} else if (product == "customerReviewsAndComments"){
				returnObject = utilityFactory.incrementPageIndex($scope[""+index], $scope.customerReviewsAndRatingsPage, $scope.customerReviewsAndRatingsList);
				$scope.customerReviewsAndRatingsIndex = returnObject[0];
				$scope.customerReviewsAndRatingsPage = returnObject[1];
				$scope.visibleCustomerReviewsAndRatingsList = $scope.customerReviewsAndRatingsList[$scope.customerReviewsAndRatingsIndex];
			}
		}
		
		$scope.decrementPageIndex = function(index, product){
			if(product == "recommendedProductMobile"){
				returnObject = utilityFactory.decrementPageIndex($scope[""+index], $scope.recommendedProductsPagesMobile, $scope.recommendedProductsMobile);
				$scope.recommendedProductsIndexMobile = returnObject[0];
				$scope.recommendedProductsPagesMobile = returnObject[1];
			} else if(product == "savedProductMobile"){
				returnObject = utilityFactory.decrementPageIndex($scope[""+index], $scope.savedProductsPagesMobile, $scope.savedProductsMobile);
				$scope.savedProductsIndexMobile = returnObject[0];
				$scope.savedProductsPagesMobile = returnObject[1];
			} else if(product == "recommendedProductDesktop"){
				returnObject = utilityFactory.decrementPageIndex($scope[""+index], $scope.recommendedProductsPagesDesktop, $scope.recommendedProductsDesktop);
				$scope.recommendedProductsIndexDesktop = returnObject[0];
				$scope.recommendedProductsPagesDesktop = returnObject[1];
				$scope.visibleRecommendedProductsDesktop = $scope.recommendedProductsDesktop[$scope.recommendedProductsIndexDesktop];
			} else if(product == "savedProductDesktop"){
				returnObject = utilityFactory.decrementPageIndex($scope[""+index], $scope.savedProductsPagesDesktop, $scope.savedProductsDesktop);
				$scope.savedProductsIndexDesktop = returnObject[0];
				$scope.savedProductsPagesDesktop = returnObject[1];
				$scope.visibleSavedProductsDesktop = $scope.savedProductsDesktop[$scope.savedProductsIndexDesktop];
			} else if (product == "customerReviewsAndComments"){
				returnObject = utilityFactory.decrementPageIndex($scope[""+index], $scope.customerReviewsAndRatingsPage, $scope.customerReviewsAndRatingsList);
				$scope.customerReviewsAndRatingsIndex = returnObject[0];
				$scope.customerReviewsAndRatingsPage = returnObject[1];
				$scope.visibleCustomerReviewsAndRatingsList = $scope.customerReviewsAndRatingsList[$scope.customerReviewsAndRatingsIndex];
			}
		}
		
		$scope.updatePageIndex = function(index, value, product){
			if(product == "recommendedProductMobile"){
				returnObject = utilityFactory.updatePageIndex($scope[""+index], $scope.recommendedProductsPagesMobile, $scope.recommendedProductsMobile, value);
				$scope.recommendedProductsIndexMobile = returnObject[0];
				$scope.recommendedProductsPagesMobile = returnObject[1];
			} else if(product == "savedProductMobile"){
				returnObject = utilityFactory.updatePageIndex($scope[""+index], $scope.savedProductsPagesMobile, $scope.savedProductsMobile, value);
				$scope.savedProductsIndexMobile = returnObject[0];
				$scope.savedProductsPagesMobile = returnObject[1];
			} else if(product == "recommendedProductDesktop"){
				returnObject = utilityFactory.updatePageIndex($scope[""+index], $scope.recommendedProductsPagesDesktop, $scope.recommendedProductsDesktop, value);
				$scope.recommendedProductsIndexDesktop = returnObject[0];
				$scope.recommendedProductsPagesDesktop = returnObject[1];
				$scope.visibleRecommendedProductsDesktop = $scope.recommendedProductsDesktop[$scope.recommendedProductsIndexDesktop];
			} else if(product == "savedProductDesktop"){
				returnObject = utilityFactory.updatePageIndex($scope[""+index], $scope.savedProductsPagesDesktop, $scope.savedProductsDesktop, value);
				$scope.savedProductsIndexDesktop = returnObject[0];
				$scope.savedProductsPagesDesktop = returnObject[1];
				$scope.visibleSavedProductsDesktop = $scope.savedProductsDesktop[$scope.savedProductsIndexDesktop];
			} else if (product == "customerReviewsAndComments"){
				returnObject = utilityFactory.updatePageIndex($scope[""+index], $scope.customerReviewsAndRatingsPage, $scope.customerReviewsAndRatingsList, value);
				$scope.customerReviewsAndRatingsIndex = returnObject[0];
				$scope.customerReviewsAndRatingsPage = returnObject[1];
				$scope.visibleCustomerReviewsAndRatingsList = $scope.customerReviewsAndRatingsList[$scope.customerReviewsAndRatingsIndex];
			}
		}
	}
	
	$scope.initializeReccomendedProducts();
	$scope.selectColor = function(product, color){
		product.color = color;
		for(i in $scope.imageList){
			if(color == $scope.imageList[i].code){
				product.image = $scope.imageList[i].url;
			}
		}
	};
	
	function initProductDetails(index){		
		$scope.product = index;
		$scope.hasColor = false;
		$scope.hasFit = false;
		$scope.hasSize = false;
		$scope.imageList = [];
		var productOptionList = []
		
		//Modify
		$scope.product.product = {}
		$scope.product.product.code = $scope.product.itemNumber;
		$scope.product.product.name = $scope.product.name;
		$scope.product.rating = 3;
		
		if($scope.product.variantOptions != undefined){
			$scope.hasColor = true;
			$scope.hasFit = true;
			$scope.hasSize = true;
		}					
		//Get other Fields
		var url = config.baseUrl + config.productPath + $scope.product.product.code + "?fields=images,categories";
		productService.getProduct(url).then(
			function success(result){
				if(result.data.categories != undefined){
					for(i in result.data.categories){
						if(result.data.categories[i].code.indexOf('B2B_') >= 0 && $scope.product.product.code.indexOf('_')){
							$scope.imageList.push(
									{
									"code" : $scope.product.product.code,
									"url" : config.baseUrl + result.data.images[0].url,
									"color" : result.data.categories[2].code.split("_").pop()
									});
							if(result.data.categories[0].code.indexOf('B2B_') >= 0){
								$scope.product.size = (result.data.categories[0].code.split("B2B_").pop()).replace(/_/g , ".");
							}
							break;
						}
					}
				}else{
					$scope.imageList.push(
							{
							"code" : value.code,
							"url" : config.baseUrl + result.data.images[0].url
							});
				}
			},
			function error(result){
				console.log(result);
		});	
		
		$scope.product.customerReviewAndRatings = [
			{
				'shortComment': 'Solid, Dependable, Comfortable 1',
				'customerName': 'John S.',
				'date': 'January 25, 2017',
				'rating': 5,
				'size': 12,
				'color': 'Brown',
				'showBtnShowAll': true,
				'showAllComment': false,
				'longComment': [
					'Lorem ipsum dolor sit amet, consectetur adipiscing elit. In pharetra dictum eros, non tempor sem ornare et. Fusce sed arcu non ligula accumsan eleifend. Phasellus vulputate massa et ligula bibendum, id posuere turpis feugiat.',
					'Nam nec posuere eros. Donec rhoncus lacinia pharetra. Morbi scelerisque, ante a porttitor tincidunt, metus lorem sodales mauris, non faucibus eros dolor et mauris. Ut vel placerat diam. Morbi at lorem hendrerit, luctus enim vitae, semper est.',
					'In tempus risus hendrerit purus rhoncus, sed scelerisque mauris luctus. Aliquam erat volutpat. Donec nibh massa, mollis vitae ante eu, tincidunt commodo nisi. Aenean imperdiet blandit est ac consequat. Duis imperdiet lectus sem, non pretium orci sollicitudin mollis. Sed condimentum sem eu sapien mattis sagittis. Mauris bibendum, mi in accumsan vulputate, ipsum eros elementum metus, eu fermentum enim lorem vel ante. Maecenas egestas pharetra ipsum, nec malesuada sapien sollicitudin sit amet. Nulla facilisi. Phasellus iaculis turpis dui, eget efficitur justo finibus et. Donec id leo felis.'
				]
			},
			{
				'shortComment': 'Solid, Dependable, Comfortable 2',
				'customerName': 'John S.',
				'date': 'January 25, 2017',
				'rating': 3,
				'size': 12,
				'color': 'Brown',
				'showBtnShowAll': true,
				'showAllComment': false,
				'longComment': [
					'Lorem ipsum dolor sit amet, consectetur adipiscing elit. In pharetra dictum eros, non tempor sem ornare et. Fusce sed arcu non ligula accumsan eleifend. Phasellus vulputate massa et ligula bibendum, id posuere turpis feugiat.',
					'Nam nec posuere eros. Donec rhoncus lacinia pharetra. Morbi scelerisque, ante a porttitor tincidunt, metus lorem sodales mauris, non faucibus eros dolor et mauris. Ut vel placerat diam. Morbi at lorem hendrerit, luctus enim vitae, semper est.',
					'In tempus risus hendrerit purus rhoncus, sed scelerisque mauris luctus. Aliquam erat volutpat. Donec nibh massa, mollis vitae ante eu, tincidunt commodo nisi. Aenean imperdiet blandit est ac consequat. Duis imperdiet lectus sem, non pretium orci sollicitudin mollis. Sed condimentum sem eu sapien mattis sagittis. Mauris bibendum, mi in accumsan vulputate, ipsum eros elementum metus, eu fermentum enim lorem vel ante. Maecenas egestas pharetra ipsum, nec malesuada sapien sollicitudin sit amet. Nulla facilisi. Phasellus iaculis turpis dui, eget efficitur justo finibus et. Donec id leo felis.'
				]
			},
			{
				'shortComment': 'Solid, Dependable, Comfortable 3',
				'customerName': 'John S.',
				'date': 'January 25, 2017',
				'rating': 0,
				'size': 12,
				'color': 'Brown',
				'showBtnShowAll': false,
				'showAllComment': true,
				'longComment': [
					'Lorem ipsum dolor sit amet, consectetur adipiscing elit. In pharetra dictum eros, non tempor sem ornare et. Fusce sed arcu non ligula accumsan eleifend. Phasellus vulputate massa et ligula bibendum, id posuere turpis feugiat.'
					]
			},
			{
				'shortComment': 'Solid, Dependable, Comfortable 4',
				'customerName': 'John S.',
				'date': 'January 25, 2017',
				'rating': 3,
				'size': 12,
				'color': 'Brown',
				'showBtnShowAll': true,
				'showAllComment': false,
				'longComment': [
					'Lorem ipsum dolor sit amet, consectetur adipiscing elit. In pharetra dictum eros, non tempor sem ornare et. Fusce sed arcu non ligula accumsan eleifend. Phasellus vulputate massa et ligula bibendum, id posuere turpis feugiat.',
					'Nam nec posuere eros. Donec rhoncus lacinia pharetra. Morbi scelerisque, ante a porttitor tincidunt, metus lorem sodales mauris, non faucibus eros dolor et mauris. Ut vel placerat diam. Morbi at lorem hendrerit, luctus enim vitae, semper est.',
					'In tempus risus hendrerit purus rhoncus, sed scelerisque mauris luctus. Aliquam erat volutpat. Donec nibh massa, mollis vitae ante eu, tincidunt commodo nisi. Aenean imperdiet blandit est ac consequat. Duis imperdiet lectus sem, non pretium orci sollicitudin mollis. Sed condimentum sem eu sapien mattis sagittis. Mauris bibendum, mi in accumsan vulputate, ipsum eros elementum metus, eu fermentum enim lorem vel ante. Maecenas egestas pharetra ipsum, nec malesuada sapien sollicitudin sit amet. Nulla facilisi. Phasellus iaculis turpis dui, eget efficitur justo finibus et. Donec id leo felis.'
				]
			},
			{
				'shortComment': 'Solid, Dependable, Comfortable 5',
				'customerName': 'John S.',
				'date': 'January 25, 2017',
				'rating': 3,
				'size': 12,
				'color': 'Brown',
				'showBtnShowAll': true,
				'showAllComment': false,
				'longComment': [
					'Lorem ipsum dolor sit amet, consectetur adipiscing elit. In pharetra dictum eros, non tempor sem ornare et. Fusce sed arcu non ligula accumsan eleifend. Phasellus vulputate massa et ligula bibendum, id posuere turpis feugiat.',
					'Nam nec posuere eros. Donec rhoncus lacinia pharetra. Morbi scelerisque, ante a porttitor tincidunt, metus lorem sodales mauris, non faucibus eros dolor et mauris. Ut vel placerat diam. Morbi at lorem hendrerit, luctus enim vitae, semper est.',
					'In tempus risus hendrerit purus rhoncus, sed scelerisque mauris luctus. Aliquam erat volutpat. Donec nibh massa, mollis vitae ante eu, tincidunt commodo nisi. Aenean imperdiet blandit est ac consequat. Duis imperdiet lectus sem, non pretium orci sollicitudin mollis. Sed condimentum sem eu sapien mattis sagittis. Mauris bibendum, mi in accumsan vulputate, ipsum eros elementum metus, eu fermentum enim lorem vel ante. Maecenas egestas pharetra ipsum, nec malesuada sapien sollicitudin sit amet. Nulla facilisi. Phasellus iaculis turpis dui, eget efficitur justo finibus et. Donec id leo felis.'
				]
			},
			{
				'shortComment': 'Solid, Dependable, Comfortable 6',
				'customerName': 'John S.',
				'date': 'January 25, 2017',
				'rating': 3,
				'size': 12,
				'color': 'Brown',
				'showBtnShowAll': true,
				'showAllComment': false,
				'longComment': [
					'Lorem ipsum dolor sit amet, consectetur adipiscing elit. In pharetra dictum eros, non tempor sem ornare et. Fusce sed arcu non ligula accumsan eleifend. Phasellus vulputate massa et ligula bibendum, id posuere turpis feugiat.',
					'Nam nec posuere eros. Donec rhoncus lacinia pharetra. Morbi scelerisque, ante a porttitor tincidunt, metus lorem sodales mauris, non faucibus eros dolor et mauris. Ut vel placerat diam. Morbi at lorem hendrerit, luctus enim vitae, semper est.',
					'In tempus risus hendrerit purus rhoncus, sed scelerisque mauris luctus. Aliquam erat volutpat. Donec nibh massa, mollis vitae ante eu, tincidunt commodo nisi. Aenean imperdiet blandit est ac consequat. Duis imperdiet lectus sem, non pretium orci sollicitudin mollis. Sed condimentum sem eu sapien mattis sagittis. Mauris bibendum, mi in accumsan vulputate, ipsum eros elementum metus, eu fermentum enim lorem vel ante. Maecenas egestas pharetra ipsum, nec malesuada sapien sollicitudin sit amet. Nulla facilisi. Phasellus iaculis turpis dui, eget efficitur justo finibus et. Donec id leo felis.'
				]
			},
			{
				'shortComment': 'Solid, Dependable, Comfortable 7',
				'customerName': 'John S.',
				'date': 'January 25, 2017',
				'rating': 3,
				'size': 12,
				'color': 'Brown',
				'showBtnShowAll': true,
				'showAllComment': false,
				'longComment': [
					'Lorem ipsum dolor sit amet, consectetur adipiscing elit. In pharetra dictum eros, non tempor sem ornare et. Fusce sed arcu non ligula accumsan eleifend. Phasellus vulputate massa et ligula bibendum, id posuere turpis feugiat.',
					'Nam nec posuere eros. Donec rhoncus lacinia pharetra. Morbi scelerisque, ante a porttitor tincidunt, metus lorem sodales mauris, non faucibus eros dolor et mauris. Ut vel placerat diam. Morbi at lorem hendrerit, luctus enim vitae, semper est.',
					'In tempus risus hendrerit purus rhoncus, sed scelerisque mauris luctus. Aliquam erat volutpat. Donec nibh massa, mollis vitae ante eu, tincidunt commodo nisi. Aenean imperdiet blandit est ac consequat. Duis imperdiet lectus sem, non pretium orci sollicitudin mollis. Sed condimentum sem eu sapien mattis sagittis. Mauris bibendum, mi in accumsan vulputate, ipsum eros elementum metus, eu fermentum enim lorem vel ante. Maecenas egestas pharetra ipsum, nec malesuada sapien sollicitudin sit amet. Nulla facilisi. Phasellus iaculis turpis dui, eget efficitur justo finibus et. Donec id leo felis.'
				]
			},
			{
				'shortComment': 'Solid, Dependable, Comfortable 8',
				'customerName': 'John S.',
				'date': 'January 25, 2017',
				'rating': 3,
				'size': 12,
				'color': 'Brown',
				'showBtnShowAll': true,
				'showAllComment': false,
				'longComment': [
					'Lorem ipsum dolor sit amet, consectetur adipiscing elit. In pharetra dictum eros, non tempor sem ornare et. Fusce sed arcu non ligula accumsan eleifend. Phasellus vulputate massa et ligula bibendum, id posuere turpis feugiat.',
					'Nam nec posuere eros. Donec rhoncus lacinia pharetra. Morbi scelerisque, ante a porttitor tincidunt, metus lorem sodales mauris, non faucibus eros dolor et mauris. Ut vel placerat diam. Morbi at lorem hendrerit, luctus enim vitae, semper est.',
					'In tempus risus hendrerit purus rhoncus, sed scelerisque mauris luctus. Aliquam erat volutpat. Donec nibh massa, mollis vitae ante eu, tincidunt commodo nisi. Aenean imperdiet blandit est ac consequat. Duis imperdiet lectus sem, non pretium orci sollicitudin mollis. Sed condimentum sem eu sapien mattis sagittis. Mauris bibendum, mi in accumsan vulputate, ipsum eros elementum metus, eu fermentum enim lorem vel ante. Maecenas egestas pharetra ipsum, nec malesuada sapien sollicitudin sit amet. Nulla facilisi. Phasellus iaculis turpis dui, eget efficitur justo finibus et. Donec id leo felis.'
				]
			},
			{
				'shortComment': 'Solid, Dependable, Comfortable 9',
				'customerName': 'John S.',
				'date': 'January 25, 2017',
				'rating': 3,
				'size': 12,
				'color': 'Brown',
				'showBtnShowAll': true,
				'showAllComment': false,
				'longComment': [
					'Lorem ipsum dolor sit amet, consectetur adipiscing elit. In pharetra dictum eros, non tempor sem ornare et. Fusce sed arcu non ligula accumsan eleifend. Phasellus vulputate massa et ligula bibendum, id posuere turpis feugiat.',
					'Nam nec posuere eros. Donec rhoncus lacinia pharetra. Morbi scelerisque, ante a porttitor tincidunt, metus lorem sodales mauris, non faucibus eros dolor et mauris. Ut vel placerat diam. Morbi at lorem hendrerit, luctus enim vitae, semper est.',
					'In tempus risus hendrerit purus rhoncus, sed scelerisque mauris luctus. Aliquam erat volutpat. Donec nibh massa, mollis vitae ante eu, tincidunt commodo nisi. Aenean imperdiet blandit est ac consequat. Duis imperdiet lectus sem, non pretium orci sollicitudin mollis. Sed condimentum sem eu sapien mattis sagittis. Mauris bibendum, mi in accumsan vulputate, ipsum eros elementum metus, eu fermentum enim lorem vel ante. Maecenas egestas pharetra ipsum, nec malesuada sapien sollicitudin sit amet. Nulla facilisi. Phasellus iaculis turpis dui, eget efficitur justo finibus et. Donec id leo felis.'
				]
			},
			{
				'shortComment': 'Solid, Dependable, Comfortable 10',
				'customerName': 'John S.',
				'date': 'January 25, 2017',
				'rating': 3,
				'size': 12,
				'color': 'Brown',
				'showBtnShowAll': true,
				'showAllComment': false,
				'longComment': [
					'Lorem ipsum dolor sit amet, consectetur adipiscing elit. In pharetra dictum eros, non tempor sem ornare et. Fusce sed arcu non ligula accumsan eleifend. Phasellus vulputate massa et ligula bibendum, id posuere turpis feugiat.',
					'Nam nec posuere eros. Donec rhoncus lacinia pharetra. Morbi scelerisque, ante a porttitor tincidunt, metus lorem sodales mauris, non faucibus eros dolor et mauris. Ut vel placerat diam. Morbi at lorem hendrerit, luctus enim vitae, semper est.',
					'In tempus risus hendrerit purus rhoncus, sed scelerisque mauris luctus. Aliquam erat volutpat. Donec nibh massa, mollis vitae ante eu, tincidunt commodo nisi. Aenean imperdiet blandit est ac consequat. Duis imperdiet lectus sem, non pretium orci sollicitudin mollis. Sed condimentum sem eu sapien mattis sagittis. Mauris bibendum, mi in accumsan vulputate, ipsum eros elementum metus, eu fermentum enim lorem vel ante. Maecenas egestas pharetra ipsum, nec malesuada sapien sollicitudin sit amet. Nulla facilisi. Phasellus iaculis turpis dui, eget efficitur justo finibus et. Donec id leo felis.'
				]
			}
		];
		
		//Customer Reviews & Comments
		$scope.customerReviewsAndRatingsList = [];
		angular.copy($scope.product.customerReviewAndRatings, $scope.customerReviewsAndRatingsList);
		$scope.customerReviewsAndRatingsList = utilityFactory.splitArray($scope.customerReviewsAndRatingsList, 3);
		$scope.customerReviewsAndRatingsPage = [];
		$scope.customerReviewsAndRatingsIndex = 0;
		
		$scope.generateReviewsAndRatingsIndexPage = function(customerReviewsAndRatingsIndex){
			$scope.customerReviewsAndRatingsPage = utilityFactory.generatePagination($scope.customerReviewsAndRatingsPage, $scope.customerReviewsAndRatingsList, customerReviewsAndRatingsIndex);
		};
		$scope.generateReviewsAndRatingsIndexPage($scope.customerReviewsAndRatingsIndex);
		
		$scope.visibleCustomerReviewsAndRatingsList = $scope.customerReviewsAndRatingsList[$scope.customerReviewsAndRatingsIndex];
		$scope.displayCurrentRating = function(rating){
			return utilityFactory.displayCurrentRating(rating);
		}
		
		$scope.displayUnratedRating = function(rating){
			return utilityFactory.displayUnratedRating(rating);
		}
		
		$scope.starRating = [
			{'id':'1', 'value': '4 Stars & Up'},
		    {'id':'2', 'value': '3 Stars & Up'},
		    {'id':'3', 'value': '2 Stars & Up'},
		    {'id':'4', 'value': '1 Stars & Up'}
		];
		$scope.selectedStarRating = $scope.starRating[0];
		
		//Customer Reviews & Comments
		$scope.showMoreComment = function(customerReviewAndRating){
			customerReviewAndRating.showAllComment = true;
			customerReviewAndRating.showBtnShowAll = !customerReviewAndRating.showBtnShowAll;
		}
		
		$scope.incrementPageIndex = function(index, product){
			returnObject = utilityFactory.incrementPageIndex($scope[""+index], $scope.customerReviewsAndRatingsPage, $scope.customerReviewsAndRatingsList);
			$scope.customerReviewsAndRatingsIndex = returnObject[0];
			$scope.customerReviewsAndRatingsPage = returnObject[1];
			$scope.visibleCustomerReviewsAndRatingsList = $scope.customerReviewsAndRatingsList[$scope.customerReviewsAndRatingsIndex];
		};
		
		$scope.decrementPageIndex = function(index, product){
			returnObject = utilityFactory.decrementPageIndex($scope[""+index], $scope.customerReviewsAndRatingsPage, $scope.customerReviewsAndRatingsList);
			$scope.customerReviewsAndRatingsIndex = returnObject[0];
			$scope.customerReviewsAndRatingsPage = returnObject[1];
			$scope.visibleCustomerReviewsAndRatingsList = $scope.customerReviewsAndRatingsList[$scope.customerReviewsAndRatingsIndex];
		};
		
		$scope.updatePageIndex = function(index, value, product){
			returnObject = utilityFactory.updatePageIndex($scope[""+index], $scope.customerReviewsAndRatingsPage, $scope.customerReviewsAndRatingsList, value);
			$scope.customerReviewsAndRatingsIndex = returnObject[0];
			$scope.customerReviewsAndRatingsPage = returnObject[1];
			$scope.visibleCustomerReviewsAndRatingsList = $scope.customerReviewsAndRatingsList[$scope.customerReviewsAndRatingsIndex];
		};
	};
	
	
}])