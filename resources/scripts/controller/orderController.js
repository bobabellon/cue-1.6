var app = angular.module('orderController', ['userService' , 'datatables', 'datatables.colreorder']);

app.controller("orderController", ['$scope','$cookieStore','$state','config','userService','$window', 'DTOptionsBuilder', 'DTColumnDefBuilder',
	function($scope, $cookieStore, $state, config, userService, $window, DTOptionsBuilder, DTColumnDefBuilder){
	var vm = this;
	vm.dtInstance = {};
    vm.dtOptions = DTOptionsBuilder.newOptions()
        // Activate col reorder plugin
        .withOption('responsive', true)
        .withOption('paging', false)
        .withColReorder()
        .withColReorderOption('iFixedColumnsLeft', 1)
    vm.dtColumnDefs = [
        DTColumnDefBuilder.newColumnDef(0).notSortable()
    ];
	//Initialization
	var perPage = 10;
    $scope.retrieveOrderList = function(){
		$scope.ordersList = [];
		userService.getOrders(perPage, 0).then(
				function success(result){
					var order = result.data;
					if(order.orders == undefined){
						order = {orders:[]}
					}else if(order.orders.length == 0){
						order.orders = [];
					};
					$scope.ordersList = order.orders;
		});
	}    
    
    $scope.resetSort = function(element){
		var counter = 0;		
		vm.dtInstance.DataTable.on('order.dt', function(){
			if(vm.dtInstance.DataTable.settings().order().length === 1 && counter == 0){
	            let order = vm.dtInstance.DataTable.settings().order()[0],
	            	th = $("order-table th:eq(" + order[0] + ")");
	            if(th.attr("data-sort-next") === "false"){
	            	vm.dtInstance.DataTable.order([]).draw();
	                th.removeAttr("data-sort-next");
	            }else{
	            	th.attr("data-sort-next", order[1] !== "desc");
	            }
	            $("order-table th").each(function(k, v){
	            	if(k !== order[0]){
	                	$("order-table th:eq(" + k + ")").removeAttr("data-sort-next")
	                }
	            });
	            counter++
	        }
	    });
	}
    
    $scope.retrieveOrderList();
    if(JSON.parse($window.localStorage.getItem("order")).pagination == undefined){
    	$scope.currentPage = 0
    }else{
    	$scope.currentPage = JSON.parse($window.localStorage.getItem("order")).pagination.currentPage;    	
    }
	
	$scope.perPageDropdownList = [5,10,15,20,25,30,35,40];
	$scope.ordersPerPage = $scope.perPageDropdownList[$scope.perPageDropdownList.indexOf(JSON.parse($window.localStorage.getItem("order")).pagination.pageSize)];
	
	$scope.totalPages = JSON.parse($window.localStorage.getItem("order")).pagination.totalPages;

	$scope.initPagination = function(){
		$scope.totalPagesList = [];
		var lowCounter = 2;
		var lowIndex = angular.copy($scope.currentPage);
		
		while (lowCounter >= 1) {
		    if((lowIndex - lowCounter) >= 0){
		    	$scope.totalPagesList.push(lowIndex - lowCounter);
		    };
		    lowCounter--;
		};
		$scope.totalPagesList.push($scope.currentPage);
		var highCounter = 1;
		var highIndex = angular.copy($scope.currentPage);
		
		while (highCounter < 3) {
		    if((highIndex + highCounter) > $scope.currentPage && (highIndex + highCounter) < $scope.totalPages){
		    	$scope.totalPagesList.push(highIndex + highCounter);
		    };
		    highCounter++;
		};
	};
	$scope.initPagination();
	
	$scope.changePage = function(pageNumber){
		$scope.currentPage = pageNumber;
		$scope.updateOrdersTable();
	}
	
	$scope.updatePerPage = function(){
		$scope.currentPage = 0;
		$scope.updateOrdersTable();
	};
	
	$scope.updateOrdersTable = function(){
		userService.getOrders($scope.ordersPerPage, $scope.currentPage).then(
			function success(result){
				console.log(result);
				
				$scope.ordersList = [];
				$scope.ordersList = result.data.orders;
				$scope.currentPage = result.data.pagination.currentPage;
				$scope.totalPages = result.data.pagination.totalPages;
				$scope.initPagination();
				$scope.initFilters();
			},
			function error(result){
				console.log(result);
		});
	};
	
	$scope.statusFilter = [{'name':'Shipped', 'selected': false, 'type': 'status', isEnabled: false},
						  {'name':'Pending', 'selected': false, 'type': 'status', isEnabled: false},
						  {'name':'On Hold', 'selected': false, 'type': 'status', isEnabled: false}];
	
	$scope.projectFilter = [{'name':'Townhouse Development', 'selected': false, 'type': 'project'},
					     {'name':'Offices', 'selected': false, 'type': 'project'},
					     {'name':'C&N Bank', 'selected': false, 'type': 'project'},
					     {'name':'Wawa', 'selected': false, 'type': 'project'}];
	
	$scope.initFilters = function(){
		var shipped = false;
		var pending = false;
		var onHold = false;
		
		angular.forEach($scope.ordersList , function(order){
			if(!shipped){
				if(order.status.toUpperCase() == "SHIPPED"){
					shipped = true;
				};
			}else if(!pending){
				if(order.status.toUpperCase() == "PENDING"){
					pending = true;
				};
			}else if(!onHold){
				if(order.status.toUpperCase() == "ON HOLD"){
					onHold = true;
				};
			};
		});
		
		angular.forEach($scope.statusFilter, function(filter){
			if(filter.name.toUpperCase() == "SHIPPED"){
				filter.isEnabled = shipped;
			}else if(filter.name.toUpperCase() == "PENDING"){
				filter.isEnabled = pending;
			}else if(filter.name.toUpperCase() == "ON HOLD"){
				filter.isEnabled = onHold;
			};
		});
	};
	$scope.initFilters();	
	$scope.isHover = false;
	$scope.selectedFilter = [];
	
	$scope.addFilter = function(filter, selected){
		var index = -1;
		angular.forEach($scope.selectedFilter, function(value) {
			if(value.name == filter.name){
				index = $scope.selectedFilter.indexOf(value)
			}
		});
		
		//Filter
		var statusList = [];
		
		//Add
	    if(index == -1 && selected){
	    	$scope.selectedFilter.push(filter);
	    	if(filter.type == "status"){
				angular.forEach($scope.selectedFilter, function(filter) {
					if(filter.type == "status"){
						statusList.push(filter);
					}
				});
				$scope.filterOrdersByStatus(statusList);
	    	};
	      
	    //Remove
	    } else if (!selected && index != -1){
	      $scope.selectedFilter.splice(index, 1);
	      filter.selected = false;
	      
	      if(filter.type == "status"){
	    	  angular.forEach($scope.statusFilter, function(value) {
	  			if(value.name.toLowerCase() == filter.name.toLowerCase()){
	  				value.selected = false;
	  				angular.forEach($scope.selectedFilter, function(filter) {
	  					if(filter.type == "status"){
	  						statusList.push(filter);
	  					};
	  				});
	  			};
	    	  });
	    	  if(statusList.length > 0){
	    		  $scope.filterOrdersByStatus(statusList);
	    	  } else {
	    		  $scope.ordersList = JSON.parse($window.localStorage.getItem("order")).orders;
	    	  };
	      } else if(filter.type == "project"){
	    	  angular.forEach($scope.projectFilter, function(value) {
	  			if(value.name.toLowerCase() == filter.name.toLowerCase()){
	  				value.selected = false;
	  			}
	  		});
	      };
	    };
	};
	
	$scope.filterOrdersByStatus = function(statusList){
		var filteredOrdersList = [];
		var copiedOrdersList = angular.copy($scope.ordersList);
		angular.forEach(copiedOrdersList, function(order) {
			angular.forEach(statusList, function(status) {
				if(order.status.toLowerCase() == status.name.toLowerCase()){
					filteredOrdersList.push(order);
				}
			});
		});
		$scope.ordersList = filteredOrdersList;
	};
	
	
	$scope.modalTrigger = "";
	$scope.toggleOrderFilterModal = function(condition){
		if(condition === true){
			$scope.modalTrigger = "is-active";
		} else {
			$scope.modalTrigger = "";
		}
	};
	
	//Navigation
	$scope.goToOrderDetails = function(orderCode){
		$window.localStorage.setItem('orderCode', orderCode);
		$state.go('base.orderDetails');
	};

	$scope.searchOrder = function(){
		vm.dtInstance.DataTable.search($scope.searchOrders).draw();
	}
}]);