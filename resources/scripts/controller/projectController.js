var app = angular.module('projectController', ['projectService', 'productService']);

app.controller("projectController", ['$scope','config', '$state','$compile', '$stateParams', '$cookieStore','$rootScope', 'projectService', 'ngPopoverFactory','productService','toaster', '$rootScope','$window',
	function($scope, config, $state, $compile,$stateParams, $cookieStore, $rootScope, projectService, ngPopoverFactory, productService, toaster,$rootScope,$window){
	
	//$window.location.href = "/oncue-web#/projects";//redirect to angular 6 on page load

	$rootScope.user = JSON.parse($window.localStorage.getItem('user'));
	$scope.oldProjectName = "";
	$scope.isDisplayOptionsOpen = false;
	$scope.selectedDisplayOption = 1;
	$scope.csvDataList = [];
		
	$scope.toggleDisplayOptions = function(){
		$scope.isDisplayOptionsOpen = !$scope.isDisplayOptionsOpen;
	};
	$scope.selectDisplay = function(num){
		$scope.selectedDisplayOption = num;
		$scope.isDisplayOptionsOpen = false;
	};
	$scope.sortBy = "A-Z"
	$scope.sortByList = [
	                 "A-Z",
	                 "Z-A"
	                 ]
	$scope.sortTo = null;
	$scope.soldToList = [
	                 "Alpha",
	                 "Delta"
	                 ]
	$scope.sortToggle = true;
	//MODALS
	$scope.modalTrigger = "";
	$scope.toggleProjectFilterModal = function(condition){
		if(condition === true){
			$scope.modalTrigger = "is-active";
		} else {
			$scope.modalTrigger = "";
		};
	};
	
	$scope.createProjmodalTrigger = "";
	$scope.toggleProjectCreateModal = function(condition){
		$scope.projectName = "";
		if(condition === true){
			$scope.createProjmodalTrigger = "is-active";
		} else {
			$scope.createProjmodalTrigger = "";
			$scope.isValidProjectName = true;	
		};
	};
	$scope.changeNameProjmodalTrigger = "";
	$scope.toggleProjectChangeNameModal = function(condition, project){
		if(condition === true){
			$scope.projectNameChange = project.name;
			$scope.projectNameId = project.customerProjectId;
			$scope.oldProjectName = project.name;
			$scope.changeNameProjmodalTrigger = "is-active";
		} else {
			$scope.changeNameProjmodalTrigger = "";
			$scope.isValidProjectRename = true;	
		};
	};
	$scope.deleteProjmodalTrigger = "";
	$scope.toggleDeleteProjectModal = function(condition, project){
		$scope.projectDelete = project;
		if(condition === true){
			$scope.deleteProjmodalTrigger = "is-active";
		} else {
			$scope.deleteProjmodalTrigger = "";
		};
	
	}
	$scope.errorModalTrigger = "";
	$scope.errorRedirect = function(){
		$state.go('base.project');
		$scope.errorModalTrigger = "";
	}
	//FUnctions
	$scope.retrieveProjectList = function(){
		projectService.retrieveProjectList().then(function(result){
			$scope.projectList = result;
			if($scope.changeName){
				$scope.changeName = false;
				toaster.pop('success', "", "Changes Saved")
			}
			if(result.result != undefined && result.result.validLicense == "FALSE"){
    			$scope.errorMsg = result.result.message;
    			$scope.errorModalTrigger = "is-active";							
			}
		},
		function error(result){
			console.log(result);
			$scope.errorMsg = "Oops! An error occured";
			$scope.errorModalTrigger = "is-active";
		});
	}
	
	$scope.retrieveProjectList();
	
	$scope.addProject = function(){
		$scope.validateProjectName();
		if($scope.isValidProjectName){
			if($scope.csvDataList.length > 0){
				var project = {
						name : $scope.projectName,
						entries : $scope.csvDataList
					}
			}else{
				var project = {
						name : $scope.projectName
					}
			}			
			projectService.createProject(project).then(function(result){
				$scope.retrieveProjectList();
				if(result.result != undefined && result.result.validLicense == "FALSE"){
	    			$scope.errorMsg = result.result.message;
	    			$scope.errorModalTrigger = "is-active";							
				}
			},
			function error(result){
				console.log(result);
    			$scope.errorMsg = "Oops! An error occured";
				$scope.errorModalTrigger = "is-active";
			});
			$scope.toggleProjectCreateModal(false);
		}
	}
	
	$scope.clickUploadCsv = function(){
		setTimeout(function() {
	        document.getElementById('upload-csv').click()
	    }, 0);
	}
	
	$scope.addDataFromCsv = function(result){
		$scope.filename = angular.element(document.querySelectorAll("#upload-csv"))[0].files[0].name;
		$scope.csvData = [];
		$scope.csvDataList = [];
		angular.forEach(result, function(value){
			//check duplicate productcode in csv
			  var dupCount = 0;
			  if(value["Customer Material # or Southwire Material #"] != undefined && value["Quantity"] != undefined){
				  var totalQuantity = 0;
				  angular.forEach(result, function(data){
					if(value["Customer Material # or Southwire Material #"] == data["Customer Material # or Southwire Material #"]){
						dupCount++;
						if(dupCount > 1){
							totalQuantity = parseInt(value["Quantity"]) + parseInt(data["Quantity"]);
							value["Quantity"] = totalQuantity;
						}
					}
				  })	
				//adds product code to deleteItemList
				if(dupCount == 1){
					$scope.csvData.push(value);						
				}else{
					var hasDuplicate = false;
					for(i in $scope.csvData){
						if($scope.csvData[i]["Customer Material # or Southwire Material #"]  == value["Customer Material # or Southwire Material #"]){
							hasDuplicate = true;
							break;
						}
					};
					if(!hasDuplicate){
						$scope.csvData.push(value);					
					}
				}
			  };
		 });
		
		for(i in $scope.csvData){
			var url = config.baseUrl + config.productPath + $scope.csvData[i]["Customer Material # or Southwire Material #"];
			productService.getProduct(url).then(
				function success(result){			
					for(i in $scope.csvData){
						if($scope.csvData[i]["Customer Material # or Southwire Material #"] == result.data.code){
							$scope.csvDataList.push({
									productCode : $scope.csvData[i]["Customer Material # or Southwire Material #"],
									productQuantity : $scope.csvData[i]["Quantity"]
							});
						}						
					}		
				},
				function error(result){
					console.log(result);
				});
		}
	}
	
	$scope.changeProjectName = function(){
		$scope.validateProjectRename();
		if($scope.isValidProjectRename){
			var oldProjectName = $scope.oldProjectName;
			var project = {
					name : $scope.projectNameChange,
					entries : []
				}
			projectService.renameProject(project, $scope.projectNameId).then(function(result){
				$scope.retrieveProjectList();
				$scope.changeName = true;
				if(result.result != undefined && result.result.validLicense == "FALSE"){
	    			$scope.errorMsg = result.result.message;
	    			$scope.errorModalTrigger = "is-active";							
				}
			},
			function error(result){
				console.log(result);
    			$scope.errorMsg = "Oops! An error occured";
				$scope.errorModalTrigger = "is-active";
			});
			$scope.toggleProjectChangeNameModal(false);
		}
	}
	
	$scope.duplicateProject = function(projectItem){	
		if(projectItem.entries != undefined){
			var project = {
					name : "Copy of " + projectItem.name,
					entries : projectItem.entries
				}
		}else{
			var project = {
					name : "Copy of " + projectItem.name
				}
		}
		
		projectService.createProject(project).then(function(result){
			$scope.retrieveProjectList();
			if(result.result != undefined && result.result.validLicense == "FALSE"){
    			$scope.errorMsg = result.result.message;
    			$scope.errorModalTrigger = "is-active";							
			}
		},
		function error(result){
			console.log(result);
			$scope.errorMsg = "Oops! An error occured";
			$scope.errorModalTrigger = "is-active";
		});
	}
	
	$scope.deleteProjectFolder = function(){
		var item = $scope.projectDelete;
		projectService.deleteProject(item.customerProjectId).then(function(result){
			$scope.retrieveProjectList();
			if(result.result != undefined && result.result.validLicense == "FALSE"){
    			$scope.errorMsg = result.result.message;
    			$scope.errorModalTrigger = "is-active";							
			}
		},
		function error(result){
			console.log(result);
			$scope.errorMsg = "Oops! An error occured";
			$scope.errorModalTrigger = "is-active";
		});	
		$scope.toggleDeleteProjectModal(false);
	}
	$scope.minDate;
	$scope.maxDate = new Date().toDateString();
	$scope.filterByDate = function(){
		$scope.minDate = new Date($scope.dateFromFilter).toDateString();
		projectService.retrieveProjectList().then(function(result){			
			
			if($scope.dateFromFilter != undefined && $scope.dateFromFilter.length > 0 && $scope.dateFromFilter != "Invalid Date" &&
					$scope.dateToFilter != undefined && $scope.dateToFilter.length > 0 && $scope.dateToFilter != "Invalid Date"){
				
				if(angular.isDate(new Date($scope.dateFromFilter)) && angular.isDate(new Date($scope.dateToFilter))){
					
					var dateFrom = new Date($scope.dateFromFilter);
					var dateTo = new Date($scope.dateToFilter);
					
					var filteredProjectList = [];
					
					var copiedProjectList = angular.copy(result);
					angular.forEach(copiedProjectList, function(project) {
						var createDate = new Date(project.createDate);
						createDate.setHours(0,0,0,0);
						if(angular.equals(createDate, dateFrom)){
							filteredProjectList.push(project);
							
						}else if(angular.equals(createDate, dateTo)){
							filteredProjectList.push(project);
							
						}else if(createDate > dateFrom && createDate < dateTo){
							filteredProjectList.push(project);
							
						};
					});
					$scope.projectList = filteredProjectList;
				};
				
			};
			if(result.result != undefined && result.result.validLicense == "FALSE"){
    			$scope.errorMsg = result.result.message;
    			$scope.errorModalTrigger = "is-active";							
			}
		},
		function error(result){
			console.log(result);
			$scope.errorMsg = "Oops! An error occured";
			$scope.errorModalTrigger = "is-active";
		});
	};
	
	$scope.sortName = function(){
		$scope.sortToggle
		if($scope.sortBy == "A-Z"){
			$scope.sortToggle = true;
		}else{
			$scope.sortToggle = false;
		}
	}
	
	$scope.isValidProjectName = true;	
	$scope.isValidProjectRename = true;	
	$scope.validateProjectName = function(){
		if($scope.projectName.length == 0){
			$scope.isValidProjectName = false;			
		}else{
			$scope.isValidProjectName = true;				
		}
	}
	
	$scope.validateProjectRename = function(){
		if($scope.projectNameChange.length == 0){
			$scope.isValidProjectRename = false;			
		}else{
			$scope.isValidProjectRename = true;				
		}
	}
	
	$scope.closeProjectPopover = function(){
		var $win = $(window); // or $box parent container
		$win.on("click.Bst", function(event){
			 var $box = $(".project-context");
			if ( 
			    $box.has(event.target).length == 0 //checks if descendants of $box was clicked
			    &&
			    !$box.is(event.target) //checks if the $box itself was clicked
			  ){
					angular.element(document.querySelectorAll("project-list .angular-popover")).addClass("hide-popover-element");
					angular.element(document.querySelectorAll("project-list .angular-popover-triangle")).addClass("hide-popover-element");
				}
		});
	}
	$scope.closeProjectPopover();
	
	$scope.goToProjectDetail = function(project){
		$state.go('base.projectDetail', {
	        projectId: project.customerProjectId
	      });
	}
}])