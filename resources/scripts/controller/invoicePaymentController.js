var app = angular.module('invoicePaymentController', [ 'datatables', 'datatables.colreorder', 'pdf']);

app.controller("invoicePaymentController", ['$scope','$cookieStore','config','$window', 'DTOptionsBuilder', 'DTColumnDefBuilder', 'utilityFactory', 'pdfDelegate',
	function($scope, $cookieStore, config, $window, DTOptionsBuilder, DTColumnDefBuilder, utilityFactory, pdfDelegate){
	
	var vm = this;
	$scope.dtInstance = {};
	vm.dtOptions = DTOptionsBuilder.newOptions()
        .withOption('paging', false)
        .withOption('info', false)
        .withColReorder()
        .withColReorderOption('iFixedColumnsLeft', 1)
    vm.dtColumnDefs = [
    ];
	
	$scope.selectedInvoices = utilityFactory.getSelectedInvoices();
	
	$scope.checkInvoice = function(){
		$scope.totalInvoice = 0;		
		angular.forEach($scope.selectedInvoices, function(invoice){
			if(invoice){
				$scope.totalInvoice += invoice.total;
			};
		});
	};
	
	$scope.checkInvoice();
	

	$scope.modalTrigger = "";
	$scope.toggleInvoiceFilterModal = function(condition){
		if(condition === true){
			$scope.modalTrigger = "is-active";
		} else {
			$scope.modalTrigger = "";
		}
	};
	
	$scope.pdfModalTrigger = "";
	$scope.toggleInvoicePdfModal = function(condition){
		if(condition === true){
			$scope.pdfModalTrigger = "is-active";
		} else {
			$scope.pdfModalTrigger = "";
		}
	};
	
	$scope.initializePdfModal = function (invoiceNumber, url,element){
		 $scope.docDefinition = utilityFactory.createDocDefinition(invoiceNumber);		 
		 var pdfDocGenerator = pdfMake.createPdf($scope.docDefinition);
		 pdfDocGenerator.getDataUrl(function(dataUrl){
			 $scope.pdfUrl = dataUrl;
				pdfDelegate
			    .$getByHandle('invoice-pdf-container')
			    .load($scope.pdfUrl);
				$scope.invoicePdfNumber = element.text 
		 });
	}
	
	$scope.downloadPdf = function(){
		pdfMake.createPdf($scope.docDefinition).download($scope.invoiceNumber + ".pdf");
	}
//	initialize mock data
	$scope.totalInvoice = $scope.totalInvoice.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",")
	
	$scope.paymentTypeList = [
	                      "EFT(Electronic Funds Transfer",
	                      "EFT(Electronic Funds Transfer",
	                      "EFT(Electronic Funds Transfer"
	                      ]
	$scope.paymentSource = "B&N BANK"
		
		
		
		
	
}]);