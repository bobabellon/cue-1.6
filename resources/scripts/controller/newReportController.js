var app = angular.module('newReportController', ['datatables', 'datatables.colreorder', 'pdf', 'reportService', 'semantic-ui-dropdown']);

app.controller("newReportController", ['$scope', '$rootScope', '$cookieStore', 'config', 'reportService', '$window',
    'DTOptionsBuilder', 'DTColumnDefBuilder', '$timeout', 'pdfDelegate',
    'utilityFactory', '$state', '$stateParams', '$q',
    function($scope, $rootScope, $cookieStore, config, reportService, $window,
        DTOptionsBuilder, DTColumnDefBuilder, $timeout, pdfDelegate,
        utilityFactory, $state, $stateParams, $q) {
        //datatables Init
        var vm = this;
        vm.dtInstance = {};
        $scope.category = [];
        $scope.subcategory = [];
        $scope.states = [];
        $scope.shipToAccount = [];
        $scope.soldToAccount = [];
        $scope.isEdit = $cookieStore.get("editReport");
        $scope.isDivSelected = true;
        $scope.isPayerSelected = false;
        $scope.isSoldToSelected = false;
        $scope.isShipToSelected = false;

        vm.dtOptions = DTOptionsBuilder.newOptions()
            // Activate col reorder plugin
            .withOption('responsive', true)
            .withOption('paging', false)
            .withOption('info', false)
            .withColReorder()
            .withColReorderOption('iFixedColumnsLeft', 1)
        vm.dtColumnDefs = [
            DTColumnDefBuilder.newColumnDef(0).notSortable()
        ];

        $scope.initCreateReport = function() {
            $q.all([
                /*reportService.getProductSummaryOption("customer"),
                reportService.getProductSummaryOption("timePeriod"),
                reportService.getProductSummaryOption("categories"),
                reportService.getProductSummaryOption("division"),
                reportService.getProductSummaryOption("subcategories"),
                reportService.getProductSummaryOption("states"),
                reportService.getProductSummaryOption("account"),
                reportService.getProductSummaryOption("reportFormat"),
                reportService.getProductSummaryOption("chart"),
                reportService.getProductSummaryOption("timePeriod"),
                reportService.getProductSummaryOption("year"),*/
                reportService.getDivisions()
            ]).then(function(data) {
                    $scope.reportOption = $cookieStore.get("reportOption");
                    //				$scope.reportOption.customer = data[0];
                    $scope.reportOption.timePeriod = data[1];
                    $scope.reportOption.categories = data[2];
                    //				$scope.reportOption.division = data[3];
                    //				$scope.reportOption.subcategories = data[4];
                    $scope.reportOption.states = data[5];
                    $scope.reportOption.soldTo = data[6];
                    $scope.reportOption.reportFormat = data[7];
                    //				$scope.reportOption.chart = data[8];
                    $scope.reportOption.chart = ["Bar Chart", "Line Chart", "Pie Chart"];
                    $scope.reportOption.timePeriod = data[9];
                    $scope.reportOption.year = data[10];
                    $scope.reportOption.unit = ["MSF", "Both", "Dollar"];
                    $scope.reportOption.division = data[11];
                    $scope.reportOption.shipTo = null;

                    if ($scope.isEdit) {
                        var editRepObj = $cookieStore.get("editReportObj");
                        $scope.reportName = editRepObj.reportName;
                        $scope.reportOption.reportType = editRepObj.type;
                        //					$scope.category = editRepObj.category;
                        $scope.chartType = editRepObj.chartType;
                        //					$scope.customer = editRepObj.customer;
                        $scope.division = editRepObj.division;
                        //					$scope.states = editRepObj.states;
                        //					$scope.shipToAccount = editRepObj.shipToAccount;
                        //					$scope.reportFormat = editRepObj.reportFormat;
                        //					$scope.timePeriod = editRepObj.timePeriod;
                        $scope.unit = editRepObj.unit;
                        $scope.reportIndex = editRepObj.index;
                    }
                },
                function error(result) {
                    console.log(result);
                    $scope.errorMsg = "Oops! An error occured";
                    $scope.errorModalTrigger = "is-active";
                });

        }
        $scope.initCreateReport();
        $scope.changedTimePeriod = function(data) {
            $scope.timePeriod = data;
        }
        $scope.changedStartDate = function(data) {
            $scope.startDate = data;
        }
        $scope.changedEndDate = function(data) {
            $scope.endDate = data;
        }
        $scope.changeUnit = function(data) {
            $scope.unit = data;
        }
        $scope.runReport = function() {
            if ($scope.reportName.length != undefined || $scope.reportName.length != 0) {
                $scope.newReport = {};
                $scope.newReport.reportName = $scope.reportName;
                $scope.newReport.type = $scope.reportOption.reportType;
                $scope.newReport.category = $scope.category;
                $scope.newReport.chartType = $scope.chartType;
                $scope.newReport.customer = $scope.customer;
                $scope.newReport.division = $scope.division;
                $scope.newReport.states = $scope.states;
                $scope.newReport.shipToAccount = $scope.shipToAccount;
                $scope.newReport.reportFormat = $scope.reportFormat;
                $scope.newReport.data = []
                $scope.newReport.timePeriod = $scope.timePeriod;
                $scope.newReport.unit = $scope.unit;
                if ($scope.timePeriod == 'Custom') {
                    $scope.newReport.dateRange = {
                        startDate: new Date($scope.startDate),
                        endDate: new Date($scope.endDate)
                    }
                }
                var reportId = 0;
                if ($scope.isEdit) {
                    var temp = $cookieStore.get("reports");
                    temp[$scope.reportIndex] = $scope.newReport;
                    $cookieStore.put("reports", temp);
                    reportId = $scope.reportIndex;
                } else {
                    $cookieStore.put("newReport", $scope.newReport);
                    reportId = 0;
                    if ($cookieStore.get("reports") != undefined) {
                        reportId = $cookieStore.get("reports").length;
                    }
                }
                $state.go('base.reportDetail', {
                    reportId: reportId
                });
                var userInfo = {
                    "username": $cookieStore.get('user').userName,
                    "timestamp": new Date(),
                    "reportType": $scope.reportOption.reportType,
                    "action": "RUN",
                    "company": config.company,
                    "__class__": "Logger"

                }

                reportService.logUserData(userInfo).then(function(result) {
                        console.log(result)
                    },
                    function error(result) {
                        console.log(result);
                        $scope.errorMsg = "Oops! An error occured";
                        $scope.errorModalTrigger = "is-active";
                    });
            }
        }

        $scope.selectAllCheckCategory = function() {
            var result = null;
            result = $scope.category.find($scope.findAll);
            if (result != undefined) {
                if ($scope.category.length > 1 && $scope.category[0] == "All") {
                    var index = $scope.category.indexOf("All");
                    if (index >= 0) {
                        $scope.category.splice(index, 1);
                    }
                } else {
                    $scope.category = ["All"];
                }
            } else if (($scope.reportOption.categories.length - 1) == $scope.category.length) {
                $scope.category = ["All"];
            }
        }

        $scope.selectAllCheckSubCategory = function() {
            var result = null;
            //		result = $scope.subcategory.find($scope.findAll);
            if (result != undefined) {
                if ($scope.subcategory.length > 1 && $scope.subcategory[0] == "All") {
                    var index = $scope.subcategory.indexOf("All");
                    if (index >= 0) {
                        $scope.subcategory.splice(index, 1);
                    }
                } else {
                    $scope.subcategory = ["All"];
                }
            } else if (($scope.reportOption.subcategories.length - 1) == $scope.subcategory.length) {
                $scope.subcategory = ["All"];
            }

        }

        $scope.selectAllCheckState = function() {
            var result = null;
            result = $scope.states.find($scope.findAll);
            if (result != undefined) {
                if ($scope.states.length > 1 && $scope.states[0] == "All") {
                    var index = $scope.states.indexOf("All");
                    if (index >= 0) {
                        $scope.states.splice(index, 1);
                    }
                } else {
                    $scope.states = ["All"];
                }
            } else if (($scope.reportOption.states.length - 1) == $scope.states.length) {
                $scope.states = ["All"];
            }
        }

        $scope.selectAllCheckAccount = function() {
            var result = null;
            //		result = $scope.shipToAccount.find($scope.findAll);
            if (result != undefined) {
                if ($scope.shipToAccount.length > 1 && $scope.shipToAccount[0] == "All") {
                    var index = $scope.shipToAccount.indexOf("All");
                    if (index >= 0) {
                        $scope.shipToAccount.splice(index, 1);
                    }
                } else {
                    $scope.shipToAccount = ["All"];
                }
            }
            /*else if(($scope.reportOption.account.length - 1) == $scope.shipToAccount.length){
            	$scope.shipToAccount = ["All"];				
            }*/

            if ($scope.shipToAccount.length != 0) {
                $scope.isShipToSelected = true;
                reportService.getSubCateg().then(function(result) {
                        $scope.reportOption.subcategories = result;
                    },
                    function error(result) {
                        console.log(result);
                        $scope.errorMsg = "Oops! An error occured";
                        $scope.errorModalTrigger = "is-active";
                    });
            } else {
                $scope.reportOption.subcategories = "";
                $scope.isShipToSelected = false;
            }
        }

        $scope.selectAllSoldCheckAccount = function() {
            var result = null;
            //		result = $scope.soldToAccount.find($scope.findAll);
            if (result != undefined) {
                if ($scope.soldToAccount.length > 1 && $scope.soldToAccount[0] == "All") {
                    var index = $scope.soldToAccount.indexOf("All");
                    if (index >= 0) {
                        $scope.soldToAccount.splice(index, 1);
                    }
                } else {
                    $scope.soldToAccount = ["All"];
                }
            }
            /*else if(($scope.reportOption.soldTo.length - 1) == $scope.soldToAccount.length){
            	$scope.soldToAccount = ["All"];				
            }*/

            if ($scope.soldToAccount.length != 0) {
                $scope.isSoldToSelected = true;
                reportService.getShipTo($scope.division.code, $scope.customer.code, $scope.soldToAccount.code).then(function(result) {
                        $scope.reportOption.shipTo = result;
                    },
                    function error(result) {
                        console.log(result);
                        $scope.errorMsg = "Oops! An error occured";
                        $scope.errorModalTrigger = "is-active";
                    });
            } else {
                $scope.reportOption.shipTo = "";
                $scope.reportOption.subcategories = "";
                $scope.isSoldToSelected = false;
                $scope.isShipToSelected = false;
            }
        }

        $scope.selectAllDivision = function() {
            var result = null;
            //		result = $scope.division.find($scope.findAll);
            if (result != undefined) {
                if ($scope.division.length > 1 && $scope.division[0] == "All") {
                    var index = $scope.division.indexOf("All");
                    if (index >= 0) {
                        $scope.division.splice(index, 1);
                    }
                } else {
                    $scope.division = ["All"];
                }
                $scope.isDivSelected = false;
            }
            /*else if(($scope.reportOption.division.length - 1) == $scope.division.length){
            	$scope.division = ["All"];	
            	$scope.isDivSelected = false;
            }	*/

            if ($scope.division != undefined && $scope.division.length != 0) {
                $scope.isDivSelected = false;
                reportService.getPayers($scope.division.code).then(function(result) {
                        $scope.reportOption.customer = result;
                    },
                    function error(result) {
                        console.log(result);
                        $scope.errorMsg = "Oops! An error occured";
                        $scope.errorModalTrigger = "is-active";
                    });
            } else {
                $scope.reportOption.customer = "";
                $scope.reportOption.soldTo = "";
                $scope.reportOption.shipTo = "";
                $scope.reportOption.subcategories = "";
                $scope.isDivSelected = true;
                $scope.isPayerSelected = false;
                $scope.isSoldToSelected = false;
                $scope.isShipToSelected = false;
            }
        }

        $scope.selectPayer = function() {
            if ($scope.customer != undefined) {
                $scope.isPayerSelected = true;
                reportService.getSoldTo($scope.division.code, $scope.customer.code).then(function(result) {
                        $scope.reportOption.soldTo = result;
                    },
                    function error(result) {
                        console.log(result);
                        $scope.errorMsg = "Oops! An error occured";
                        $scope.errorModalTrigger = "is-active";
                    });
            } else {
                $scope.isPayerSelected = false;
            }
        }

        $scope.findAll = function(value) {
            return value == 'All';
        }

        $scope.errorModalTrigger = "";
        $scope.errorRedirect = function() {
            $state.go('base.reports');
            $scope.errorModalTrigger = "";
        }

    }
]);