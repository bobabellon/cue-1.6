var app = angular.module('homeController', [ 'datatables', 'datatables.colreorder', 'userService']);

app.controller("homeController", ['$scope','utilityFactory','$cookieStore','$state','$rootScope','$location','$anchorScroll','$window', 'DTOptionsBuilder', 'DTColumnDefBuilder', '$compile', '$timeout', 'userService', '$location','config',
	function($scope, utilityFactory, $cookieStore, $state, $rootScope, $location, $anchorScroll, $window, DTOptionsBuilder, DTColumnDefBuilder, $compile, $timeout, userService,$location,config){
	$window.location.href = "/oncue-web#/";
	
	$scope.$timeout = $timeout;
	// $scope.invoiceList = ['a'];
	var vmOrder = this;
	vmOrder.dtInstanceOrder = {};
	vmOrder.dtInstanceOrderM = {};
	vmOrder.dtOptionsOrder = DTOptionsBuilder.newOptions()
        // Activate col reorder plugin
        .withOption('responsive', true)
        .withOption('paging', false)
        .withColReorder()
        .withColReorderOption("width" , '100px')        
        .withOption('order' , [])
    vmOrder.dtColumnDefsOrder = [];
	
	$scope.loadAngular = function(){
    	$compile($(".dtr-details")) ($scope);
    }
    
    var vmInvoice = this;
    vmInvoice.dtInstanceInvoice = {};
    vmInvoice.dtInstanceInvoiceM = {};
	vmInvoice.dtOptionsInvoice = DTOptionsBuilder.newOptions()
        // Activate col reorder plugin
        .withOption('responsive', true)
        .withOption('paging', false)
        .withColReorder()
    vmInvoice.dtColumnDefsInvoice = [
         DTColumnDefBuilder.newColumnDef(0).notSortable(),
         DTColumnDefBuilder.newColumnDef(5).notSortable()
    ];
    
	$scope.resetSortOrder = function(element){
		var counter = 0;		
		vmOrder.dtInstanceOrder.DataTable.on('order.dt', function(){
			if(vmOrder.dtInstanceOrder.DataTable.settings().order().length === 1 && counter == 0){
	            let order = vmOrder.dtInstanceOrder.DataTable.settings().order()[0],
	            	th = $("#homeOrder th:eq(" + order[0] + ")");
	            if(th.attr("data-sort-next") === "false"){
	            	vmOrder.dtInstanceOrder.DataTable.order([]).draw();
	                th.removeAttr("data-sort-next");
	            }else{
	            	th.attr("data-sort-next", order[1] !== "desc");
	            }
	            $("#homeOrder th").each(function(k, v){
	            	if(k !== order[0]){
	                	$("#homeOrder th:eq(" + k + ")").removeAttr("data-sort-next")
	                }
	            });
	            counter++
	        }
	    });
	}
	
	$scope.resetSortOrderMobile = function(element){
		var counter = 0;		
		vmOrder.dtInstanceOrderM.DataTable.on('order.dt', function(){
			if(vmOrder.dtInstanceOrderM.DataTable.settings().order().length === 1 && counter == 0){
	            let order = vmOrder.dtInstanceOrderM.DataTable.settings().order()[0],
	            	th = $("#home-order-mobile th:eq(" + order[0] + ")");
	            if(th.attr("data-sort-next") === "false"){
	            	vmOrder.dtInstanceOrderM.DataTable.order([]).draw();
	                th.removeAttr("data-sort-next");
	            }else{
	            	th.attr("data-sort-next", order[1] !== "desc");
	            }
	            $("#home-order-mobile th").each(function(k, v){
	            	if(k !== order[0]){
	                	$("#home-order-mobile th:eq(" + k + ")").removeAttr("data-sort-next")
	                }
	            });
	            counter++
	        }
	    });
	}
	
	$scope.resetSortInvoice = function(element){
		var counter = 0;		
		vmInvoice.dtInstanceInvoice.DataTable.on('order.dt', function(){
			if(vmInvoice.dtInstanceInvoice.DataTable.settings().order().length === 1 && counter == 0){
	            let order = vmInvoice.dtInstanceInvoice.DataTable.settings().order()[0],
	            	th = $("#home-invoice-table th:eq(" + order[0] + ")");
	            if(th.attr("data-sort-next") === "false"){
	            	vmInvoice.dtInstanceInvoice.DataTable.order([]).draw();
	                th.removeAttr("data-sort-next");
	            }else{
	            	th.attr("data-sort-next", order[1] !== "desc");
	            }
	            $("#home-invoice-table th").each(function(k, v){
	            	if(k !== order[0]){
	                	$("#home-invoice-table th:eq(" + k + ")").removeAttr("data-sort-next")
	                }
	            });
	            counter++
	        }
	    });
	}
	
	$scope.resetSortInvoiceMobile = function(element){
		var counter = 0;		
		vmInvoice.dtInstanceInvoiceM.DataTable.on('order.dt', function(){
			if(vmInvoice.dtInstanceInvoiceM.DataTable.settings().order().length === 1 && counter == 0){
	            let order = vmInvoice.dtInstanceInvoiceM.DataTable.settings().order()[0],
	            	th = $("#home-invoice-table-mobile th:eq(" + order[0] + ")");
	            if(th.attr("data-sort-next") === "false"){
	            	vmInvoice.dtInstanceInvoiceM.DataTable.order([]).draw();
	                th.removeAttr("data-sort-next");
	            }else{
	            	th.attr("data-sort-next", order[1] !== "desc");
	            }
	            $("#home-invoice-table-mobile th").each(function(k, v){
	            	if(k !== order[0]){
	                	$("#home-invoice-table-mobile th:eq(" + k + ")").removeAttr("data-sort-next")
	                }
	            });
	            counter++
	        }
	    });
	}
	
	$scope.filterOrderTable = function() {
//        $('.footable').trigger('footable_filter', {
//            filter: $scope.orderFilter
//        });
		vmOrder.dtInstanceOrder.DataTable.search($scope.orderFilter).draw();
		vmOrder.dtInstanceOrderM.DataTable.search($scope.orderFilter).draw();
    };
    
    $scope.filterInvoiceTable = function() {
		vmOrder.dtInstanceInvoice.DataTable.search($scope.invoiceFilter).draw();
		vmOrder.dtInstanceInvoiceM.DataTable.search($scope.invoiceFilter).draw();
    };
    
    $scope.invoiceView = 1;
    $scope.changeInvoiceView = function(view){
    	$scope.invoiceView = view;
    };
    
    $scope.invoiceIndex = 0;
    $scope.invoiceStatusOpen = 0;
    $scope.invoiceStatusPastDue = 0;
    $scope.invoiceStatusPaid = 0;
    
    $scope.invoiceList = JSON.parse($window.localStorage.getItem("invoiceList"));
    $scope.getInvoiceStatus = function(){
    	angular.forEach($scope.invoiceList, function(invoice) {
    		if(invoice.status.toUpperCase() == "OPEN"){
    			$scope.invoiceStatusOpen++;
    		} else if (invoice.status.toUpperCase() == "PAST DUE"){
    			$scope.invoiceStatusPastDue++;
    		} else if (invoice.status.toUpperCase() == "PAID"){
    			$scope.invoiceStatusPaid++;
    		};
    	});
    };
    $scope.getInvoiceStatus();
    
    $scope.orderStatusShipped = 0;
    $scope.orderStatusPending = 0;
    $scope.orderStatusOnHold= 0;
    
    $scope.getOrderStatus = function(){
    	angular.forEach($scope.ordersList, function(order) {
    		if(order.status.toUpperCase() == "SHIPPED"){
    			$scope.orderStatusShipped++;
    		} else if (order.status.toUpperCase() == "PENDING"){
    			$scope.orderStatusPending++;
    		} else if (order.status.toUpperCase() == "ON HOLD"){
    			$scope.orderStatusOnHold++;
    		};
    	});
    };
    //Get Orders
	//Modify
	var perPage = 10;
	$scope.retrieveOrderList = function(){
		$scope.ordersList = [];
		userService.getOrders(perPage, 0).then(
				function success(result){
					var order = result.data;
					if(order.orders == undefined){
						order = {orders:[]}
					}else if(order.orders.length == 0){
						order.orders = [];
					};
					$scope.ordersList = order.orders;
		});
	}    
    $scope.getOrderStatus();
    $scope.retrieveOrderList();
    //Initialize Pagination
    $scope.initPagination = function(){
		$scope.totalPagesList = [];
		
		var lowCounter = 2;
		var lowIndex = angular.copy($scope.invoiceIndex);
		
		while (lowCounter >= 1) {
		    if((lowIndex - lowCounter) >= 0){
		    	$scope.totalPagesList.push(lowIndex - lowCounter);
		    }
		    lowCounter--;
		}
		$scope.totalPagesList.push($scope.invoiceIndex);
		var highCounter = 1;
		var highIndex = angular.copy($scope.invoiceIndex);
		
		while (highCounter < 3) {
		    if((highIndex + highCounter) > $scope.invoiceIndex && (highIndex + highCounter) < $scope.invoiceList.length){
		    	$scope.totalPagesList.push(highIndex + highCounter);
		    }
		    highCounter++;
		}
    }
    $scope.initPagination();
	
    $scope.changePage = function(index){
    	$scope.invoiceIndex = index;
    	$scope.initPagination();
	};
    
	
	//Recommended Products - Desktop
	$scope.recommendedProductsDesktop = [];
	$scope.recommendedProductsDesktop = JSON.parse($window.localStorage.getItem("recommendedProducts"));
	$scope.recommendedProductsDesktop = utilityFactory.splitArray($scope.recommendedProductsDesktop, 4);
	$scope.recommendedProductsPagesDesktop = [];
	$scope.recommendedProductsIndexDesktop = 0;
	$scope.generateRecommendedProductsPageDesktop = function(recommendedProductsIndex){
		$scope.recommendedProductsPagesDesktop = utilityFactory.generatePagination($scope.recommendedProductsPagesDesktop, $scope.recommendedProductsDesktop, recommendedProductsIndex);
	}
	$scope.generateRecommendedProductsPageDesktop($scope.recommendedProductsIndexDesktop);
	
	//Recommended Products - Tablet
	$scope.recommendedProductsTablet = [];
	$scope.recommendedProductsTablet = JSON.parse($window.localStorage.getItem("recommendedProducts"));
	$scope.recommendedProductsTablet = utilityFactory.splitArray($scope.recommendedProductsTablet, 2);
	$scope.recommendedProductsPagesTablet = [];
	$scope.recommendedProductsIndexTablet = 0;
	$scope.generateRecommendedProductsPageTablet = function(recommendedProductsIndex){
		$scope.recommendedProductsPagesTablet = utilityFactory.generatePagination($scope.recommendedProductsPagesTablet, $scope.recommendedProductsTablet, recommendedProductsIndex);
	}
	$scope.generateRecommendedProductsPageTablet($scope.recommendedProductsIndexTablet);
	
	//Recommended Products - Mobile
	$scope.recommendedProductsMobile = [];
	$scope.recommendedProductsMobile = JSON.parse($window.localStorage.getItem("recommendedProducts"));
	$scope.recommendedProductsPagesMobile = [];
	$scope.recommendedProductsIndexMobile = 0;
	$scope.generateRecommendedProductsPageMobile = function(recommendedProductsIndex){
		$scope.recommendedProductsPagesMobile = utilityFactory.generatePagination($scope.recommendedProductsPagesMobile, $scope.recommendedProductsMobile, recommendedProductsIndex);
	}
	$scope.generateRecommendedProductsPageMobile($scope.recommendedProductsIndexMobile);
	
	//Initialization
	$scope.visibleRecommendedProductsDesktop = $scope.recommendedProductsDesktop[$scope.recommendedProductsIndexDesktop];
	$scope.visibleRecommendedProductsTablet = $scope.recommendedProductsTablet[$scope.recommendedProductsIndexTablet];
	
	$scope.incrementPageIndex = function(index, product){
		if(product == "recommendedProductDesktop"){
			returnObject = utilityFactory.incrementPageIndex($scope[""+index], $scope.recommendedProductsPagesDesktop, $scope.recommendedProductsDesktop);
			$scope.recommendedProductsIndexDesktop = returnObject[0];
			$scope.recommendedProductsPagesDesktop = returnObject[1];
			$scope.visibleRecommendedProductsDesktop = $scope.recommendedProductsDesktop[$scope.recommendedProductsIndexDesktop];
		}else if(product == "recommendedProductTablet"){
			returnObject = utilityFactory.incrementPageIndex($scope[""+index], $scope.recommendedProductsPagesTablet, $scope.recommendedProductsTablet);
			$scope.recommendedProductsIndexTablet = returnObject[0];
			$scope.recommendedProductsPagesTablet = returnObject[1];
			$scope.visibleRecommendedProductsTablet = $scope.recommendedProductsTablet[$scope.recommendedProductsIndexTablet];
		}else if(product == "recommendedProductMobile"){
			returnObject = utilityFactory.incrementPageIndex($scope[""+index], $scope.recommendedProductsPagesMobile, $scope.recommendedProductsMobile);
			$scope.recommendedProductsIndexMobile = returnObject[0];
			$scope.recommendedProductsPagesMobile = returnObject[1];
		};
	};
	
	$scope.decrementPageIndex = function(index, product){
		if(product == "recommendedProductDesktop"){
			returnObject = utilityFactory.decrementPageIndex($scope[""+index], $scope.recommendedProductsPagesDesktop, $scope.recommendedProductsDesktop);
			$scope.recommendedProductsIndexDesktop = returnObject[0];
			$scope.recommendedProductsPagesDesktop = returnObject[1];
			$scope.visibleRecommendedProductsDesktop = $scope.recommendedProductsDesktop[$scope.recommendedProductsIndexDesktop];
		}else if(product == "recommendedProductTablet"){
			returnObject = utilityFactory.decrementPageIndex($scope[""+index], $scope.recommendedProductsPagesTablet, $scope.recommendedProductsTablet);
			$scope.recommendedProductsIndexTablet = returnObject[0];
			$scope.recommendedProductsPagesTablet = returnObject[1];
			$scope.visibleRecommendedProductsTablet = $scope.recommendedProductsTablet[$scope.recommendedProductsIndexTablet];
		}else if(product == "recommendedProductMobile"){
			returnObject = utilityFactory.decrementPageIndex($scope[""+index], $scope.recommendedProductsPagesMobile, $scope.recommendedProductsMobile);
			$scope.recommendedProductsIndexMobile = returnObject[0];
			$scope.recommendedProductsPagesMobile = returnObject[1];
		};
	};
	
	$scope.updatePageIndex = function(index, value, product){
		if(product == "recommendedProductDesktop"){
			returnObject = utilityFactory.updatePageIndex($scope[""+index], $scope.recommendedProductsPagesDesktop, $scope.recommendedProductsDesktop, value);
			$scope.recommendedProductsIndexDesktop = returnObject[0];
			$scope.recommendedProductsPagesDesktop = returnObject[1];
			$scope.visibleRecommendedProductsDesktop = $scope.recommendedProductsDesktop[$scope.recommendedProductsIndexDesktop];
		}else if(product == "recommendedProductTablet"){
			returnObject = utilityFactory.updatePageIndex($scope[""+index], $scope.recommendedProductsPagesTablet, $scope.recommendedProductsTablet, value);
			$scope.recommendedProductsIndexTablet = returnObject[0];
			$scope.recommendedProductsPagesTablet = returnObject[1];
			$scope.visibleRecommendedProductsTablet = $scope.recommendedProductsTablet[$scope.recommendedProductsIndexTablet];
		}else if(product == "recommendedProductMobile"){
			returnObject = utilityFactory.updatePageIndex($scope[""+index], $scope.recommendedProductsPagesMobile, $scope.recommendedProductsMobile, value);
			$scope.recommendedProductsIndexMobile = returnObject[0];
			$scope.recommendedProductsPagesMobile = returnObject[1];
		};
	};
	
//Navigation
    $scope.goToInvoice = function(){
    	$state.go('base.invoice')
    };
    
    $scope.goToOrders = function(){
		$state.go('base.orders');
	};
    
    $scope.goToProductDetails = function(productCode){
		$window.localStorage.setItem('productCode',productCode);
		$state.go('base.productDetail');
//		$location.hash('header');
//		$anchorScroll();
	};
	
	$scope.payNow = function(element){
		var invoiceNo = element.getElementsByClassName("invoiceNo")[0].innerText;
		var invoices = [];
		angular.forEach($scope.invoiceList, function(item){
			 if(invoiceNo == item.invoiceNumber){
				invoices.push(item);
				utilityFactory.setSelectedInvoices(invoices);
			 }
	     });
		$state.go('base.invoicePayment');
	}
	
	$scope.goToOrderDetails = function(orderCode){
		$window.localStorage.setItem('orderCode',orderCode);
		$state.go('base.orderDetails');
	};
	
	$scope.goToInvoiceDetail = function(invoice){
		var invoices = [];
		angular.forEach($scope.invoiceList, function(item){
			 if(invoice.invoiceNumber == item.invoiceNumber){
				invoices.push(item);
				utilityFactory.setSelectedInvoices(invoices);
			 }
	     });
		$state.go('base.invoiceDetail', {
	        invoiceId: invoice.invoiceNumber
	      });
	}
}])