var app = angular.module('invoiceController', [ 'datatables', 'datatables.colreorder', 'pdf', 'datatables.fixedheader']);

app.controller("invoiceController", ['$scope', '$rootScope', '$cookieStore','config','$window', 'DTOptionsBuilder', 'DTColumnDefBuilder', '$compile', '$timeout', 'pdfDelegate', 'utilityFactory', '$state', 
	function($scope, $rootScope, $cookieStore, config, $window, DTOptionsBuilder, DTColumnDefBuilder, $compile, $timeout, pdfDelegate, utilityFactory, $state){
	$scope.$timeout = $timeout;

	$scope.ordersList = JSON.parse($window.localStorage.getItem("order")).orders;
	$scope.invoiceList = JSON.parse($window.localStorage.getItem("invoiceList"));
	var vm = this;
	vm.dtInstance = {};
    vm.dtOptions = DTOptionsBuilder.newOptions()
        // Activate col reorder plugin
        .withOption('paging', false)
        .withOption('responsive', true)        
        .withColReorder()
        .withColReorderOption('iFixedColumnsLeft', 1)
        .withButtons([
			{
				 extend: 'print',
				 customize: function ( win ) {
					 $(win.document.body).find( 'td:first-child')
			         .css( 'display', 'none' );
					 $(win.document.body).find( 'th:first-child')
			         .css( 'display', 'none' );
					 $(win.document.body).find( 'td:nth-child(3)')
				        .css( 'display', 'none' );
					 $(win.document.body).find( 'th:nth-child(3)')
			         .css( 'display', 'none' );
					 $(win.document.body).find( 'td')
			         .css( 'word-break', 'break-word' );
					 $(win.document.body).find( 'td:nth-child(8)')
				        .css( 'display', 'none' );
					 $(win.document.body).find( 'th:nth-child(8)')
			         .css( 'display', 'none' );
					 $(win.document.body).find( 'td:nth-child(9)')
				     .css( 'display', 'none' );
					 $(win.document.body).find( 'th:nth-child(9)')
			         .css( 'display', 'none' );
				 }
			},
            {
            	 extend: 'csvHtml5',
                 title:  "Invoice - ",
                 exportOptions: {
                     columns: 'th:not(:first-child):not(:nth-child(3)):not(:nth-child(7)):not(:nth-child(8))'
                 }
            }
        ]);
    vm.dtColumnDefs = [
        DTColumnDefBuilder.newColumnDef(6).notSortable(),
        DTColumnDefBuilder.newColumnDef(0).notSortable(),
        DTColumnDefBuilder.newColumnDef(8).withClass('none'),
        DTColumnDefBuilder.newColumnDef(3).notVisible(),
        DTColumnDefBuilder.newColumnDef(9).notVisible(),
        DTColumnDefBuilder.newColumnDef(10).notVisible(),
        DTColumnDefBuilder.newColumnDef(11).notVisible()
    ];
	
    $scope.searchInvoice = function(){
		vm.dtInstance.DataTable.search($scope.searchInvoices).draw();
	}
    
    $scope.loadAngular = function(){
    	$compile($(".dtr-details")) ($scope);
    }
	
    $scope.exportReportExcel = function(){
		setTimeout(function(){
	        document.getElementsByClassName('buttons-csv')[0].click()
	    }, 0);
	}
    
    $scope.printInvoiceList = function(){
		setTimeout(function(){
	        document.getElementsByClassName('buttons-print')[0].click()
	    }, 0);
	}

    $scope.resetSort = function(element){
		var counter = 0;		
		vm.dtInstance.DataTable.on('order.dt', function(){
			if(vm.dtInstance.DataTable.settings().order().length === 1 && counter == 0){
	            let order = vm.dtInstance.DataTable.settings().order()[0],
	            	th = $("#invoice-table th:eq(" + order[0] + ")");
	            if(th.attr("data-sort-next") === "false"){
	            	vm.dtInstance.DataTable.order([]).draw();
	                th.removeAttr("data-sort-next");
	            }else{
	            	th.attr("data-sort-next", order[1] !== "desc");
	            }
	            $("#invoice-table th").each(function(k, v){
	            	if(k !== order[0]){
	                	$("#invoice-table th:eq(" + k + ")").removeAttr("data-sort-next")
	                }
	            });
	            counter++
	        }
	    });
	}
    
    
    
	$scope.countInvoiceStatus = function(){
		$scope.totalOpenCount = 0;
		$scope.openCount = 0;
		$scope.paidCount = 0;
		$scope.scheduledCount = 0;
		$scope.watchlistCount = 0;
		$scope.pastDueCount = 0;
		for(i in $scope.invoiceList){
			if($scope.invoiceList[i].status == 'OPEN' || $scope.invoiceList[i].status == 'PAST DUE'){
				$scope.totalOpenCount++;
			}else if($scope.invoiceList[i].status == 'SCHEDULED'){
				$scope.scheduledCount++;
			}else if($scope.invoiceList[i].status == 'PAID'){
				$scope.paidCount++;
			}
			
			if($scope.invoiceList[i].status == 'OPEN'){
				$scope.openCount++;
			}
			
			if($scope.invoiceList[i].status == 'PAST DUE'){
				$scope.pastDueCount++;
			}
		}
	}
	$scope.countInvoiceStatus();
	$scope.statusFilter = [
		{'name':'Paid', 'selected': false, 'type': 'status'},
		{'name':'Open', 'selected': false, 'type': 'status'},
		{'name':'Past Due', 'selected': false, 'type': 'status'},
		{'name':'Scheduled', 'selected': false, 'type': 'status'}
	];
	
	$scope.perPageList = [5,10,15,20,25,30,35,40];
	$scope.selectedPerPage = $scope.perPageList[0];
	
	$scope.allInvoiceSelected = false;
	
	$scope.toggleAll = function(){
//		$scope.$apply();
	     angular.forEach($scope.invoiceList, function(item){
	    	 item.selected = $scope.allInvoiceSelected; 
	     });
	};
	$scope.invoiceTab = "Open";	
	$scope.toggleInvoiceTables = function(status){		
		$scope.invoiceTab = status;
		$scope.selectedFilter = [];
		if(status == "Open"){
			$scope.watchList = false;	
			$scope.statusFilter[0].selected = false;
			$scope.statusFilter[1].selected = true;
			$scope.statusFilter[2].selected = true;
			$scope.statusFilter[3].selected = false;
			$scope.toggleFilter($scope.statusFilter[1], true);
			$scope.toggleFilter($scope.statusFilter[2], true);
		}else if(status == "Paid"){
			$scope.watchList = false;		
			$scope.statusFilter[0].selected = true;
			$scope.statusFilter[1].selected = false;
			$scope.statusFilter[2].selected = false;
			$scope.statusFilter[3].selected = false;
			$scope.toggleFilter($scope.statusFilter[0], true);	
			$scope.toggleFilter(filter, selected);			
		}else if(status == "Watchlist"){
			$scope.watchList = true;			
		}
	}
	$scope.selectedFilter = [];
	
	$scope.toggleFilter = function(filter, selected){
		var index = -1;
		angular.forEach($scope.selectedFilter, function(value) {
			if(value.name == filter.name){
				index = $scope.selectedFilter.indexOf(value)
			}
		});		
		//Filter
		var statusList = [];		
		//Add
	    if(index == -1 && selected){
	    	$scope.selectedFilter.push(filter);
	    	
	    	if(filter.type == "status"){
				angular.forEach($scope.selectedFilter, function(filter) {
					if(filter.type == "status"){
						statusList.push(filter);
					}
				});
				$scope.filterInvoiceByStatus(statusList);
	    	};
	      
	    //Remove
	    } else if (!selected && index != -1){
	      $scope.selectedFilter.splice(index, 1);
	      filter.selected = false;
	      
	      if(filter.type == "status"){
				angular.forEach($scope.statusFilter, function(value) {
					if(value.name.toLowerCase() == filter.name.toLowerCase()){
						value.selected = false;
						angular.forEach($scope.selectedFilter, function(selectedFilter) {
							if(selectedFilter.type == "status" && selectedFilter.selected){
								statusList.push(selectedFilter);
							};
						});
					};
				});
				
				if(statusList.length > 0){
					$scope.filterInvoiceByStatus(statusList);
				} else {
					$scope.invoiceList = JSON.parse($window.localStorage.getItem("invoiceList"));
				};
	      };
	    };
	};
	
	$scope.filterInvoiceByStatus = function(statusList){
		var filteredInvoiceList = [];
		var copiedInvoiceList = angular.copy(JSON.parse($window.localStorage.getItem("invoiceList")));
		angular.forEach(copiedInvoiceList, function(order) {
			angular.forEach(statusList, function(status) {
				if(order.status.toLowerCase() == status.name.toLowerCase()){
					filteredInvoiceList.push(order);
				}
			});
		});
		$scope.invoiceList = filteredInvoiceList;
	};
	
	$scope.maxDate = new Date().toDateString();
	$scope.filterByDate = function(){
		$scope.minDate = new Date($scope.dateFromFilter).toDateString();
		
		if($scope.dateFromFilter != undefined && $scope.dateFromFilter.length > 0 && $scope.dateFromFilter != "Invalid Date" &&
				$scope.dateToFilter != undefined && $scope.dateToFilter.length > 0 &&  $scope.dateToFilter != "Invalid Date"){
			
			if(angular.isDate(new Date($scope.dateFromFilter)) && angular.isDate(new Date($scope.dateToFilter))){
				
				var dateFrom = new Date($scope.dateFromFilter);
				var dateTo = new Date($scope.dateToFilter);
				
				var filteredInvoiceList = [];
				var copiedInvoiceList = angular.copy(JSON.parse($window.localStorage.getItem("invoiceList")));
				angular.forEach(copiedInvoiceList, function(invoice) {
					var invoiceDate = new Date(invoice.dateIssued);
					
					if(angular.equals(invoiceDate, dateFrom)){
						filteredInvoiceList.push(invoice);
						
					}else if(angular.equals(invoiceDate, dateTo)){
						filteredInvoiceList.push(invoice);
						
					}else if(invoiceDate > dateFrom && invoiceDate < dateTo){
						filteredInvoiceList.push(invoice);
						
					};
				});
				$scope.invoiceList = filteredInvoiceList;
			};
			
		};
	};
	
	$scope.paySelectedInvoiceButtonEnabled = false;
	$scope.totalSelectedInvoices = 0;
	$scope.checkInvoice = function(){
		$scope.totalSelectedInvoices = 0;
		$scope.selectedInvoiceList = [];
		
		angular.forEach($scope.invoiceList, function(invoice){
			if(invoice.selected){
				$scope.selectedInvoiceList.push(invoice);
				$scope.totalSelectedInvoices += invoice.total;
			};
		});
		
		if($scope.totalSelectedInvoices > 0){
			$scope.paySelectedInvoiceButtonEnabled = true;
		}else{
			$scope.paySelectedInvoiceButtonEnabled = false;
		};
	};
	$scope.checkInvoice();
	
	$scope.modalTrigger = "";
	$scope.toggleInvoiceFilterModal = function(condition){
		if(condition === true){
			$scope.modalTrigger = "is-active";
		} else {
			$scope.modalTrigger = "";
		}
	};
	
	$scope.pdfModalTrigger = "";
	$scope.toggleInvoicePdfModal = function(condition){
		if(condition === true){
			$scope.pdfModalTrigger = "is-active";
		} else {
			$scope.pdfModalTrigger = "";
		}
	};
	
	$scope.initializePdfModal = function (invoiceNumber, url,element){
		$scope.invoiceNumber = invoiceNumber;
		$scope.docDefinition = utilityFactory.createDocDefinition(invoiceNumber);		 
		var pdfDocGenerator = pdfMake.createPdf($scope.docDefinition);
		pdfDocGenerator.getDataUrl(function(dataUrl){
			 $scope.pdfUrl = dataUrl;
				pdfDelegate
			    .$getByHandle('invoice-pdf-container')
			    .load($scope.pdfUrl);
				$scope.invoicePdfNumber = element.text 
		});
	}
	
	$scope.downloadPdf = function(){
		pdfMake.createPdf($scope.docDefinition).download($scope.invoiceNumber + ".pdf");
	}
	
	$scope.payInvoices = function(){
		utilityFactory.setSelectedInvoices($scope.selectedInvoiceList);
		$state.go('base.invoicePayment');
	}
	$scope.toggleInvoiceTables($scope.invoiceTab);
	
	$scope.payNow = function(element){
		var invoiceNo = element.getElementsByClassName("invoiceNo")[0].innerText;
		var invoices = [];
		angular.forEach($scope.invoiceList, function(item){
			 if(invoiceNo == item.invoiceNumber){
				invoices.push(item);
				utilityFactory.setSelectedInvoices(invoices);
			 }
	     });
		$state.go('base.invoicePayment');
	}
	
	$scope.goToInvoiceDetail = function(invoice){
		var invoices = [];
		angular.forEach($scope.invoiceList, function(item){
			 if(invoice.invoiceNumber == item.invoiceNumber){
				invoices.push(item);
				utilityFactory.setSelectedInvoices(invoices);
			 }
	     });
		$state.go('base.invoiceDetail', {
	        invoiceId: invoice.invoiceNumber
	      });
	}
}]);