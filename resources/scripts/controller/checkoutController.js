var app = angular.module('checkoutController', ['userService', 'datatables']);

app.controller("checkoutController", ['$scope','utilityFactory','$cookieStore','$state','config','$rootScope','userService','$window', 'DTOptionsBuilder', 'DTColumnDefBuilder',
	function($scope, utilityFactory, $cookieStore, $state, config, $rootScope, userService, $window, DTOptionsBuilder, DTColumnDefBuilder){
	//datatables Init
	var vmModalCheckout = this;
	$scope.dtInstance = {};
	vmModalCheckout.dtOptionsModal = DTOptionsBuilder.newOptions()
        // Activate col reorder plugin
        .withOption('responsive',  {
	            details : false
	        })
        .withOption('paging', false)
    vmModalCheckout.dtColumnDefsModal = [
    ];

	var vmCheckout = this;
	$scope.dtInstance = {};
	vmCheckout.dtOptionsCheckout = DTOptionsBuilder.newOptions()
        // Activate col reorder plugin
        .withOption('responsive', false)
        .withOption('paging', false)
        .withOption('info', false)
        .withColReorder()
        .withColReorderOption('iFixedColumnsLeft', 1)
    vmCheckout.dtColumnDefsCheckout = [
    ];
	
	
    
	$rootScope.shippingFee = 75;
	
	$scope.costCenterList = [
		'Retail',
		'Retail',
		'Retail',
		'Retail'
	];
	$scope.selectedCostCenter = $scope.costCenterList[0];

	$scope.billAccount = {'name': 'Maria Smith', 'address1': '5241 East Anderson Ave.', 'address2': 'Wayne PA 19080'};
	
	
	$scope.shippingMethodList = [
		'Standard Delivery: 3-5 Business Days',
		'Express Delivery: 1-2 Business Days'
	];
	
	$scope.selectedShippingMethod = $scope.shippingMethodList[0];
	
	$scope.userAgreement = false;
	
	$scope.getSelectedPayment = function(){
		console.log($scope.selectedPaymentType);
	}
	
	$scope.getSelectedAddress = function(){
		console.log($scope.selectedShippingAddress);
	}
	
	$scope.summaryModalTrigger = "";
	$scope.toggleOrderSummaryModal = function(condition){
		if(condition === true){
			$scope.summaryModalTrigger = "is-active";
		} else {
			$scope.summaryModalTrigger = "";
		}
	};
	
	$scope.orderConfirmationModalTrigger = "";
	$scope.toggleOrderConfirmationModal = function(condition){
		if(condition === true){
			$scope.orderConfirmationModalTrigger = "is-active";
		} else {
			$scope.orderConfirmationModalTrigger = "";
		}
	};
	
	$scope.placeOrder = function(){
		//Modify
		var securityCode = "123";
		
		//Create Address
		var regionISOCode = "US-NY";
		var firstName = "Power";
		var lastName = "Tools";
		var town = "Town";
		var postalCode = "1234";
		var line1 = "sample address";
		var titleCode = "mr";
		var countryISOCode = "US";
		userService.addAddress(regionISOCode, firstName, lastName, town, postalCode, line1, titleCode, countryISOCode).then(
			function success(result){
				console.log(result);
				
				//Set Delivery
				var addressId = result.data.id;
				userService.setDeliveryAddress(addressId).then(
					function success(result){
						console.log(result);
						
						//Get All Delivery Modes
						userService.getAllDeliveryModes().then(
							function success(result){
								console.log(result);
								
								if(result.data.deliveryModes.length > 0){
									//Set Cart Delivery Mode
									var deliveryModeId = result.data.deliveryModes[0].code;
									userService.setCartDeliveryMode(deliveryModeId).then(
										function success(result){
											console.log(result);
											
											//Create Payment
											var paymentInformation = {
												billingAddressCountryCode : "US",
												billingAddressLine1 : "Address line 1",
//												startMonth : "",
												saved : "true",
												billingAddressLine2 : "Address line 2",
												billingAddressRegionISOCode : "US-NY",
//												issueNumber : "",
												cardType : "visa",
												expiryMonth : "12",
//												startYear : "",
												billingAddressTitleCode : "mr",
												defaultPaymentInfo : "true",
												expiryYear : "2020",
												accountHolderName : "Power Tools",
												billingAddressPostalCode : "1234",
												billingAddressLastName : "Tools",
												billingAddressTown : "Queensbury",
												subscriptionId : "",
												cardNumber : "0000000000000000",
												billingAddressFirstName : "Power"
											};
											userService.addPaymentDetails(paymentInformation).then(
												function success(result){
													console.log(result);
													
													//Add Order
													userService.addOrders(JSON.parse($window.localStorage.getItem("cart")).code, securityCode).then(
														function success(result){
															console.log(result);
															
															$cookieStore.put('orderCode', result.data.code);
															$window.localStorage.setItem('freeTextSearch', "");
															
															//Get Orders
															//Modify
															var perPage = 10;
															userService.getOrders(perPage, 0).then(
																function success(result){
																	console.log(result);
																	
																	$rootScope.order = result.data;
																	if($rootScope.order.orders.length == 0){
																		$rootScope.order.orders = [];
																	};
																	$window.localStorage.setItem("order", JSON.stringify($rootScope.order));
																	
																	//Get Carts
																	userService.getCarts().then(
																		function success(cartResult){
																			if(cartResult.data.hasOwnProperty("carts")){
																				console.log("User has a cart");
																				console.log(cartResult.data.carts[0]);	//remove
																				utilityFactory.getAllShoppingCartItem().then(function(result){
																					$rootScope.shoppingCart = result;
																				});
																				$window.localStorage.setItem("cart", JSON.stringify({ code: cartResult.data.carts[0].code }));
//																				$state.go('base.placeOrderSuccessful');
																			} else {
																				userService.createCart().then(function(cartResult) {
																					console.log("creating cart");
																					utilityFactory.getAllShoppingCartItem().then(function(result){
																						$rootScope.shoppingCart = result;
																					});
																					$window.localStorage.setItem("cart", JSON.stringify({ code: cartResult.data.code }));
																					$state.go('base.placeOrderSuccessful');
																				});
																			}
																		},
																		function error(result){
																			console.log(result);
																	});
																},
																function error(result){
																	console.log(result);
															});
														},
														function error(result){
															console.log(result);
													});
												},
												function error(result){
													console.log(result);
											});
										},
										function error(result){
											console.log(result);
									});
								};
							},
							function error(result){
								console.log(result);
						});
					},
					function error(result){
						console.log(result);
				});
			},
			function error(result){
				console.log(result);
		});
	}
}]);