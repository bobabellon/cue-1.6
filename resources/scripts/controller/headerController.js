var app = angular.module('headerController', ['userService',
											  'productService' , 'datatables']);

app.controller("headerController", ['$scope','$state','$cookieStore','utilityFactory', 'ngPopoverFactory','config','userService','productService','$rootScope','$window','categoryService', 'DTOptionsBuilder', 'DTColumnDefBuilder','userService',
	function($scope, $state, $cookieStore, utilityFactory, ngPopoverFactory, config, userService, productService, $rootScope, $window, categoryService, DTOptionsBuilder, DTColumnDefBuilder,userService){
	$rootScope.user = JSON.parse($window.localStorage.getItem('user'));	
	//datatables Init
	var vm = this;
	$scope.dtInstance = {};
    vm.dtOptions = DTOptionsBuilder.newOptions()
        .withOption('paging', false)
        .withOption('ordering', false)
    vm.dtColumnDefs = [
    ];
    
    var vmMobile = this;
	$scope.dtInstance = {};
	vmMobile.dtOptionsMobile = DTOptionsBuilder.newOptions()
    	.withOption('responsive', true)
        .withOption('paging', false)
        .withOption('ordering', false)
    vmMobile.dtColumnDefsMobile = [
    ];
    $rootScope.currency = config.currency
	
	//Init Constants
//	$scope.initConstants = function(){
//		$cookieStore.put('token', '52fbea19-1732-4930-8eb5-3f534ea7bf31')
//		var user = {
//			userName: 'userlogin1@certainteed.com'
//		};
//		
//		if($cookieStore.get('freeTextSearch') != null && $cookieStore.get('freeTextSearch') != undefined){
//			$scope.freeTextSearch = $cookieStore.get('freeTextSearch');
//		}
//		
//		userService.getUserDetails(user.userName).then(
//			function success(result){
//				user.firstName = result.data.firstName;
//				user.lastName = result.data.lastName;
//				user.name = result.data.name;
//				$rootScope.user = user;
//				$cookieStore.put('user', user);
//
//				//Get Invoices
//				
//				//Get Orders
//				//Modify
//				var perPage = 10;
//				userService.getOrders(perPage, 0).then(
//					function success(result){
//						console.log(result);
//						
//						$rootScope.order = result.data;
//						if($rootScope.order.orders == undefined){
////							$rootScope.order = {orders:[]}
//						}else if($rootScope.order.orders.length == 0){
//							$rootScope.order.orders = [];
//						};
//						$window.localStorage.setItem("order", JSON.stringify($rootScope.order));
//						
//						//Get Hand Tools Category
//						categoryService.getAllHandToolsCategory().then(
//							function success(result){
//								console.log(result);
//								
//								$rootScope.handToolsCategory = result.data;
//								if($rootScope.handToolsCategory.length == 0){
//									$rootScope.handToolsCategory = [];
//								};
//								$window.localStorage.setItem("handToolsCategory", JSON.stringify($rootScope.handToolsCategory));
//								
//								//Get Footwear Category
//								categoryService.getAllFootWearCategory().then(
//									function success(result){
//										console.log(result);
//										
//										$rootScope.footWearCategory = result.data;
//										if($rootScope.footWearCategory.length == 0){
//											$rootScope.footWearCategory = [];
//										};
//										$window.localStorage.setItem("footWearCategory", JSON.stringify($rootScope.footWearCategory));
//										
//										//Get Measuring and Layout Tools
//										//Modify
//										
//										//Get Carts
//										userService.getCarts().then(
//											function success(cartResult){
//												if(cartResult.data.hasOwnProperty("carts")){
//													console.log("User has a cart");
//													console.log(cartResult.data.carts[0]);	//remove
//													utilityFactory.getAllShoppingCartItem().then(function(result){
//														$rootScope.shoppingCart = result;
//													});
//													$cookieStore.put("cart", { code:cartResult.data.carts[0].code });
////														$state.go('base.home');
//												} else {
//													userService.createCart().then(function(cartResult) {
//														console.log("creating cart");
//														utilityFactory.getAllShoppingCartItem().then(function(result){
//															$rootScope.shoppingCart = result;
//														});
//														$cookieStore.put("cart", { code: cartResult.data.code });
//													});
//												}
//											},
//											function error(result){
//												console.log(result);
//										});
//									},
//									function error(result){
//										console.log(result);
//								});
//							},
//							function error(result){
//								console.log(result);
//						});
//					},
//					function error(result){
//						console.log(result);
//				});
//			},
//			function error(result){
//				console.log(result);
//		});
//	}
//	//current
//	$rootScope.shoppingCart = {}
//	$rootScope.shoppingCart.items = [];
//	$scope.GetAllShoppingCartItem = function(){
//		utilityFactory.getAllShoppingCartItem().then(function(result){
//			$rootScope.shoppingCart = result;
//		});
//	};
//	$scope.initConstants();
//	$scope.GetAllShoppingCartItem();
//	
//	$rootScope.invoices = [];
//	$scope.getInvoiceList = function(){
//		$rootScope.invoices = utilityFactory.getInvoiceList();
//	}
//	$scope.getInvoiceList();
//	$window.localStorage.setItem("invoiceList", JSON.stringify($rootScope.invoices));
//	
//	//Recommended Products
//	$rootScope.recommendedProducts = [
//		{
//			'productCode': 3793687,
//			'name':'Pit Boss 6" Steel Toe 1',
//			'image': config.imagePath + "/shoes/" +  'timberland_direct_attach_brown_boot_angle.png',
//			'price': 75
//		},
//		{
//			'productCode': 3793687,
//			'name':'PowerWelt Waterproof 6" Steel Tow 2',
//			'image': config.imagePath + "/shoes/" +  'timberland_direct_attach_brown_boot_back.png',
//			'price': 75
//		},
//		{
//			'name':'Mudsill Low Steel Toe 3',
//			'image': config.imagePath + "/shoes/" +  'timberland_direct_attach_brown_boot_side.png',
//			'price': 75
//		},
//		{
//			'productCode': 3793687,
//			'name':'Pit Boss 6" Steel Toe 4',
//			'image': config.imagePath + "/shoes/" +  'timberland_direct_attach_brown_boot_angle.png',
//			'price': 75
//		},
//		{
//			'name':'PowerWelt Waterproof 6" Steel Tow 5',
//			'image': config.imagePath + "/shoes/" +  'timberland_direct_attach_brown_boot_back.png',
//			'price': 75
//		},
//		{
//			'productCode': 3793687,
//			'name':'Mudsill Low Steel Toe 6',
//			'image': config.imagePath + "/shoes/" +  'timberland_direct_attach_brown_boot_side.png',
//			'price': 75
//		},
//		{
//			'productCode': 3793687,
//			'name':'Pit Boss 6" Steel Toe 7',
//			'image': config.imagePath + "/shoes/" +  'timberland_direct_attach_brown_boot_angle.png',
//			'price': 75
//		},
//		{
//			'productCode': 3793687,
//			'name':'PowerWelt Waterproof 6" Steel Tow 8',
//			'image': config.imagePath + "/shoes/" +  'timberland_direct_attach_brown_boot_back.png',
//			'price': 75
//		},
//		{
//			'productCode': 3793687,
//			'name':'PowerWelt Waterproof 6" Steel Tow 9',
//			'image': config.imagePath + "/shoes/" +  'timberland_direct_attach_brown_boot_back.png',
//			'price': 75
//		},
//		{
//			'productCode': 3793687,
//			'name':'PowerWelt Waterproof 6" Steel Tow 10',
//			'image': config.imagePath + "/shoes/" +  'timberland_direct_attach_brown_boot_back.png',
//			'price': 75
//		},
//		{
//			'productCode': 3793687,
//			'name':'PowerWelt Waterproof 6" Steel Tow 11',
//			'image': config.imagePath + "/shoes/" +  'timberland_direct_attach_brown_boot_back.png',
//			'price': 75
//		}
//	];
//	$window.localStorage.setItem("recommendedProducts", JSON.stringify($rootScope.recommendedProducts));
	
	//////////////////////////////////////////////////////////////////////////////////////////////////

	$scope.GetAllShoppingCartItem = function(){
		utilityFactory.getAllShoppingCartItem().then(function(result){
			$rootScope.shoppingCart = result;
		});
	};
	$scope.GetAllShoppingCartItem();
	$rootScope.invoices = [];
	$scope.getInvoiceList = function(){
		$rootScope.invoices = utilityFactory.getInvoiceList();
	}
	$scope.getInvoiceList();
	$window.localStorage.setItem("invoiceList", JSON.stringify($rootScope.invoices));
	
//	Saved Products
	$rootScope.savedProducts = [
		{
			'name':'Pit Boss 6" Steel Toe',
			'image': config.imagePath + "/shoes/" +  'timberland_direct_attach_brown_boot_angle.png'
		},
		{
			'name':'PowerWelt Waterproof 6" Steel Tow',
			'image': config.imagePath + "/shoes/" +  'timberland_direct_attach_brown_boot_back.png'
		},
		{
			'name':'Mudsill Low Steel Toe',
			'image': config.imagePath + "/shoes/" +  'timberland_direct_attach_brown_boot_side.png'
		},
		{
			'name':'PowerWelt Waterproof 6" Steel Tow',
			'image': config.imagePath + "/shoes/" +  'timberland_direct_attach_brown_boot_back.png'
		},
		{
			'name':'Mudsill Low Steel Toe',
			'image': config.imagePath + "/shoes/" +  'timberland_direct_attach_brown_boot_side.png'
		},
		{
			'name':'PowerWelt Waterproof 6" Steel Tow',
			'image': config.imagePath + "/shoes/" +  'timberland_direct_attach_brown_boot_back.png'
		},
		{
			'name':'Mudsill Low Steel Toe',
			'image': config.imagePath + "/shoes/" +  'timberland_direct_attach_brown_boot_side.png'
		}
	];
	
	$scope.initSmartFixedNavigation = function(){
		utilityFactory.smartFixedNavigation();
	};
	$scope.initSmartFixedNavigation();
	
	$scope.initMegaSiteNavigation = function(){
		utilityFactory.megaSiteNavigation();
	};
	$scope.initMegaSiteNavigation();
	
//	$scope.goToHomePage = function(){
//		$scope.isSuggestedProductsVisible = false;
//		$state.go('base.home');
//	}
	
	$rootScope.suggestedProducts = [];
	$scope.isSuggestedProductsVisible = false
	
	$scope.hideSuggestedProducts = function(){
		$scope.isSuggestedProductsVisible = false;
	}
	
	$scope.goToProductDetails = function(productCode, freeTextSearch){
		$scope.isSuggestedProductsVisible = false;
		$cookieStore.put('productCode', productCode);
		$window.localStorage.setItem('freeTextSearch', freeTextSearch);
		if ($state.current.url == "/product-detail"){
			$state.reload();
//			$state.go('base.productDetail');
		} else {
			$state.go('base.productDetail');
		}
	};
	
	$rootScope.searchSuggestion = false;
	$scope.searchSuggestion = function(freeTextSearch){
		if(freeTextSearch != undefined && freeTextSearch.length > 0){
			$rootScope.searchSuggestion = true;
			url = config.baseUrl + config.productPath + "search?query=" + freeTextSearch;
			productService.searchProduct(url).then(
				function success(result){
					console.log(result);
					$scope.isSuggestedProductsVisible = true;
					$rootScope.suggestedProducts = result.data.products;
					$rootScope.searchSuggestion = false;
				},
				function error(result){
					console.log(result);
					console.log("Error!");
			});
		}
	};
	
	$scope.goToSearchResults = function(freeTextSearch){
		$rootScope.currentPage = 0;
		if(freeTextSearch == undefined){
			freeTextSearch = "";
		}
		$cookieStore.put('freeTextSearch', freeTextSearch);
		$scope.isSuggestedProductsVisible = false;
		
		if ($state.current.url == "/search-results"){
			$state.reload();
//			$state.go('base.search');
		} else {
			$state.go('base.search');
		}
	};
	
	$scope.goToShoppingCart = function(){
		$scope.hideCart();
		$scope.hideCartMobile();
		$state.go('base.shoppingCart');
	};
	
	$scope.goToOrders = function(){
		$state.go('base.orders');
	};
	
	$scope.goToInvoice = function(){
		
    	$state.go('base.invoice')
    };
	
	$scope.goToOrdersMobile = function(){
		toggleSmartFixedNav();
		$state.go('base.orders');
	};
	
	$scope.goToInvoiceMobile = function(){
		toggleSmartFixedNav();
    	$state.go('base.invoice')
    };
    
    function toggleSmartFixedNav(){
    	var navigationContainer = $('#cd-nav-mobile'),
		mainNavigation = navigationContainer.find('#cd-main-nav-mobile ul');
		$('.cd-nav-trigger').toggleClass('menu-is-open');
		//we need to remove the transitionEnd event handler (we add it when scolling up with the menu open)
		mainNavigation.off('webkitTransitionEnd otransitionend oTransitionEnd msTransitionEnd transitionend').toggleClass('is-visible');
    };
	
	$scope.goToProductCategory = function(){
		toggleSmartFixedNav();
		ngPopoverFactory.closePopover('product-catalog-trigger-desktop');
		ngPopoverFactory.closePopover('product-catalog-trigger-tablet');
		document.getElementsByTagName("body")[0].style.overflowY = 'auto';
		$state.go('base.category');
	};
	
	$scope.overflow = ""
	$scope.popOverCall = function(bool){
		if(bool){
			document.getElementsByTagName("body")[0].style.overflowY = 'hidden';
		} else {
			document.getElementsByTagName("body")[0].style.overflowY = 'auto';
		};
	};
//	Uncomment the code below when the base url was changed back to powertools base url.
//	$scope.handToolsCategoryList = JSON.parse($window.localStorage.getItem("handToolsCategory")).subcategories;
//	$scope.footWearCategoryList = JSON.parse($window.localStorage.getItem("footWearCategory")).subcategories;
	
	$scope.orderItem = [];
	$scope.initOrderItems = function(){
		$scope.orderItem = [
			{
				'item': {}
			},
			{
				'item': {}
			},
			{
				'item': {}
			},
			{
				'item': {}
			},
			{
				'item': {}
			}
		];
	};
	$scope.initOrderItems();
	
	
	$scope.getProductQuickOrder = function(productCode, index){
		if(productCode != undefined && productCode.trim() != 0){
			var url = config.baseUrl + config.productPath + productCode;
			productService.getProduct(url).then(
				function success(result){
					console.log(result);
					
					$scope.orderItem[index].item = result.data;
					$scope.orderItem[index].item.quantity = 0;
					
					//Get images
					var fields = ['images'];
					var url = config.baseUrl + config.productPath + productCode + "?fields=" + fields;
					productService.getProduct(url).then(
						function success(result){
							$scope.orderItem[index].item.image = config.baseUrl + result.data.images[0].url;
							$scope.checkQuickOrdersList();
						},
						function error(result){
							console.log(result);
						});
				},
				function error(result){
					console.log(result);
					if(result.data.errors.length == 1){
						if(result.data.errors[0].type == "UnknownIdentifierError"){
							$scope.orderItem[index].item.error = result.data.errors[0].message;
						};
						$scope.checkQuickOrdersList();
					}
			});
		};
	};
	
	$scope.addToCartButtonDisabled = false;
	$scope.checkQuickOrdersList = function(){
		var errorCount = 0;
		if($scope.orderItem.length > 0){
			angular.forEach($scope.orderItem, function(order){
				if(order.item.error != undefined && order.item.error != ""){
					errorCount++;
				};
				if(order.item.stock != undefined){
					if(order.item.quantity > order.item.stock.stockLevel){
						errorCount++;
					};
				};
				if((order.item.name != undefined && order.item.quantity == undefined) || (order.item.name != undefined && order.item.quantity == 0)){
					errorCount++;
				};
			});
		} else {
			errorCount++;
		};
		
		if(errorCount > 0){
			$scope.addToCartButtonDisabled = true;
		} else {
			$scope.addToCartButtonDisabled = false;
		};
	};
	
	$scope.quickAddProductToCart = function(){
		angular.forEach($scope.orderItem, function(order){
			if(order.item.code != undefined && order.item.quantity != undefined){
				var url = config.baseUrl + config.userPath + JSON.parse($window.localStorage.getItem('user')).userName + "/carts/" + JSON.parse($window.localStorage.getItem('cart')).code + "/entries?code=" + 
				order.item.code + "&qty=" + order.item.quantity;
				userService.addToCart(url).then(
					function success(result){
						$scope.product = result.data;
						console.log($scope.product);
						
						$scope.initOrderItems();
						utilityFactory.getAllShoppingCartItem().then(function(result){
							$rootScope.shoppingCart = result;
							/*angular.forEach($rootScope.shoppingCart.items, function(value){
								value.options = [];
								value.itemNumber.indexOf('_') >= 0 ? value.hasOptions = true : value.hasOptions = false;
								if(value.categories.length != 0 && value.categories.length != undefined){
									angular.forEach(value.categories, function(option) {
										if(option.code.indexOf('B2B_') >= 0){
											value.options.push((option.code.split("B2B_").pop()).replace(/_/g , "."));
										}
									})
								}
							});*/
						});
					},
					function error(result){
						console.log(result);
				});
			};
		});
	};
	
	$scope.clearQuickOrderError = function(index){
		$scope.orderItem[index].item.error = "";
		$scope.checkQuickOrdersList();
	};
	
	$scope.removeOrder = function(index){
		$scope.orderItem.splice(index, 1);
		$scope.checkQuickOrdersList();
	};
	
	$scope.addOrder = function(){
		var item = {
			item: {}
		};
		$scope.orderItem.push(item);
	};
	
	$scope.quickOrderModalTrigger = "";
	$scope.toggleQuickOrderModal = function(condition){
		if(condition === true){
			$scope.quickOrderModalTrigger = "is-active";
		} else {
			$scope.quickOrderModalTrigger = "";
		}
	}
	
	$scope.showCart = function(){
		document.getElementById("cart").style.width = "550px";
		document.getElementsByTagName("body")[0].style.overflowY = 'hidden';
	};
	
	$scope.hideCart = function(){
		document.getElementById("cart").style.width = "0px";
		document.getElementsByTagName("body")[0].style.overflowY = 'auto';
	};
	
	$scope.showCartMobile = function(){
		document.getElementById("cart-mobile").style.height = "100%";
		document.getElementsByTagName("body")[0].style.overflowY = 'hidden';
	};
	
	$scope.hideCartMobile = function(){
		document.getElementById("cart-mobile").style.height = "0px";
		document.getElementsByTagName("body")[0].style.overflowY = 'auto';
	};
	
	$scope.updateItemQuantityToCart = function(index, quantity, product, isValid){
		if(isValid && product.quantity != quantity){
			var fields = ['statusMessage'];
			var url = config.baseUrl + config.userPath + JSON.parse($window.localStorage.getItem('user')).userName + "/carts/" + JSON.parse($window.localStorage.getItem('cart')).code + "/entries/" + index 
			+ "?qty=" + quantity;
			if(quantity != undefined){
				userService.updateItemQuantityToCart(url).then(
					function success(result){
						console.log(result);
						utilityFactory.getAllShoppingCartItem().then(function(result){
							$rootScope.shoppingCart = result;
							/*angular.forEach($rootScope.shoppingCart.items, function(value){
								value.options = [];
								value.itemNumber.indexOf('_') >= 0 ? value.hasOptions = true : value.hasOptions = false;
								if(value.categories.length != 0 && value.categories.length != undefined){
									angular.forEach(value.categories, function(option) {
										if(option.code.indexOf('B2B_') >= 0){
											value.options.push((option.code.split("B2B_").pop()).replace(/_/g , "."));
										}
									})
								}
							});*/
						});
					},
					function error(result){
						console.log(result);
				});
			}
		}
	}
	
	$scope.removeToCart = function(index){
		var fields = ['statusMessage'];
		var url = config.baseUrl + config.userPath + JSON.parse($window.localStorage.getItem('user')).userName + "/carts/" + JSON.parse($window.localStorage.getItem('cart')).code + "/entries/" + index ;
		userService.removeToCart(url).then(
			function success(result){
				console.log(result);
				if(result.status == 200){
					utilityFactory.getAllShoppingCartItem().then(function(result){
						$rootScope.shoppingCart = result;
						/*angular.forEach($rootScope.shoppingCart.items, function(value){
							value.options = [];
							value.itemNumber.indexOf('_') >= 0 ? value.hasOptions = true : value.hasOptions = false;
							if(value.categories.length != 0 && value.categories.length != undefined){
								angular.forEach(value.categories, function(option) {
									if(option.code.indexOf('B2B_') >= 0){
										value.options.push((option.code.split("B2B_").pop()).replace(/_/g , "."));
									}
								})
							}
						});*/
					});
				}
			},
			function error(result){
				console.log(result);
		});
	}
	
	$scope.showNotification = function(){
		document.getElementById("notification").style.width = "450px";
		document.getElementsByTagName("body")[0].style.overflowY = 'hidden';
	};
	
	$scope.hideNotification = function(){
		document.getElementById("notification").style.width = "0px";
		document.getElementsByTagName("body")[0].style.overflowY = 'auto';
	};
	
	$scope.showNotificationMobile = function(){
		document.getElementById("notification-mobile").style.width = "100%";
		document.getElementsByTagName("body")[0].style.overflowY = 'hidden';
	};
	
	$scope.hideNotificationMobile = function(){
		document.getElementById("notification-mobile").style.width = "0px";
		document.getElementsByTagName("body")[0].style.overflowY = 'auto';
	};
	userService.getNotifications().then(response=>{
		console.log(response.data.notificationList);
		let notificationList = [];
		
		notificationList = response.data.notificationList.map(elem =>{
			let current_datetime = new Date(elem.timestamp);
			
			return {
				notificationType:elem.type,
				date:(current_datetime.getMonth() + 1) + "/" + current_datetime.getDate() + "/" + current_datetime.getFullYear(),
				type:elem.tag,
				notificationHead:elem.notification.title,
				notificationMessage:elem.notification.body
			}
		});
		console.log("notification", notificationList);
		$scope.notifications = notificationList;
	});
	// $scope.notifications = [
	// 	{
	// 		'notificationType': 'Orders',
	// 		'date': '8/27/2017',
	// 		'type': 'new',
	// 		'notificationHead': 'Order #123467',
	// 		'notificationMessage': 'has shipped.'
	// 	},
	// 	{
	// 		'notificationType': 'Invoices',
	// 		'date': '8/27/2017',
	// 		'type': 'new',
	// 		'notificationHead': 'Invoices #456789',
	// 		'notificationMessage': 'is available.'
	// 	},
	// 	{
	// 		'notificationType': 'Invoices',
	// 		'date': '8/27/2017',
	// 		'type': 'pastDue',
	// 		'notificationHead': 'Invoices #456789',
	// 		'notificationMessage': 'is past due.'
	// 	},
	// 	{
	// 		'notificationType': 'Projects',
	// 		'date': '8/27/2017',
	// 		'type': '',
	// 		'notificationHead': 'Projects #678912',
	// 		'notificationMessage': 'was created.'
	// 	},
	// 	{
	// 		'notificationType': 'Orders',
	// 		'date': '8/27/2017',
	// 		'type': '',
	// 		'notificationHead': 'Order #123467',
	// 		'notificationMessage': 'has shipped.'
	// 	},
	// 	{
	// 		'notificationType': 'Invoices',
	// 		'date': '8/27/2017',
	// 		'type': '',
	// 		'notificationHead': 'Invoices #456789',
	// 		'notificationMessage': 'is available.'
	// 	},
	// 	{
	// 		'notificationType': 'Invoices',
	// 		'date': '8/27/2017',
	// 		'type': 'pastDue',
	// 		'notificationHead': 'Invoices #456789',
	// 		'notificationMessage': 'is past due.'
	// 	},
	// 	{
	// 		'notificationType': 'Projects',
	// 		'date': '8/27/2017',
	// 		'type': '',
	// 		'notificationHead': 'Projects #678912',
	// 		'notificationMessage': 'was created.'
	// 	},
	// 	{
	// 		'notificationType': 'Orders',
	// 		'date': '8/27/2017',
	// 		'type': '',
	// 		'notificationHead': 'Order #123467',
	// 		'notificationMessage': 'has shipped.'
	// 	},
	// 	{
	// 		'notificationType': 'Invoices',
	// 		'date': '8/27/2017',
	// 		'type': '',
	// 		'notificationHead': 'Invoices #456789',
	// 		'notificationMessage': 'is available.'
	// 	},
	// 	{
	// 		'notificationType': 'Invoices',
	// 		'date': '8/27/2017',
	// 		'type': 'pastDue',
	// 		'notificationHead': 'Invoices #456789',
	// 		'notificationMessage': 'is past due.'
	// 	},
	// 	{
	// 		'notificationType': 'Projects',
	// 		'date': '8/27/2017',
	// 		'type': '',
	// 		'notificationHead': 'Projects #678912',
	// 		'notificationMessage': 'was created.'
	// 	}
	// ];
	
	/* Smart Navigation - Mobile */
	$scope.smartMainNav = true;
	$scope.smartProductCatalogNav = false;
		$scope.smartHandTools = false;
		$scope.smartMeasuringAndLayoutTools = false;
		$scope.smartFootwear = false;
	$scope.smartQuickOrder = false;
	
	$scope.backsmartMainNav = function(){
		$scope.smartMainNav = true;
		$scope.smartProductCatalogNav = false;
		$scope.smartHandTools = false;
		$scope.smartMeasuringAndLayoutTools = false;
		$scope.smartFootwear = false;
		$scope.smartQuickOrder = false;
	};
	
	$scope.goToSmartProductCatalogNav = function(){
		$scope.smartMainNav = false;
		$scope.smartProductCatalogNav = true;
		$scope.smartHandTools = false;
		$scope.smartMeasuringAndLayoutTools = false;
		$scope.smartFootwear = false;
		$scope.smartQuickOrder = false;
	};
	
	$scope.goToSmartHandTools = function(){
		$scope.smartMainNav = false;
		$scope.smartProductCatalogNav = false;
		$scope.smartHandTools = true;
		$scope.smartMeasuringAndLayoutTools = false;
		$scope.smartFootwear = false;
		$scope.smartQuickOrder = false;
	};
	
	$scope.goToSmartMeasuringAndLayoutTools = function(){
		$scope.smartMainNav = false;
		$scope.smartProductCatalogNav = false;
		$scope.smartHandTools = false;
		$scope.smartMeasuringAndLayoutTools = true;
		$scope.smartFootwear = false;
		$scope.smartQuickOrder = false;
	}
	
	$scope.goToSmartFootwear = function(){
		$scope.smartMainNav = false;
		$scope.smartProductCatalogNav = false;
		$scope.smartHandTools = false;
		$scope.smartMeasuringAndLayoutTools = false;
		$scope.smartFootwear = true;
		$scope.smartQuickOrder = false;
	}
	
	$scope.goToSmartQuickOrder = function(){
		$scope.smartMainNav = false;
		$scope.smartProductCatalogNav = false;
		$scope.smartHandTools = false;
		$scope.smartMeasuringAndLayoutTools = false;
		$scope.smartFootwear = false;
		$scope.smartQuickOrder = true;
	}
	
	$scope.goToMyProjects = function(){
		$state.go('base.project');
	}
	$scope.goToMyReports = function(){
		$state.go('base.reports');
	}
	$scope.logoutUser = function(){
		$state.go('login');
	}
	
	$scope.goToMyAccount = function(){
		$state.go('base.account');
		angular.element(document.querySelectorAll(".cd-nav-gallery")).addClass("is-hidden");
		angular.element(document.querySelectorAll(".cd-overlay")).removeClass("is-visible");
		angular.element(document.querySelectorAll(".header-profile-btn ")).removeClass("selected");
	}
}])